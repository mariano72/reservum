<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>


<script>

    $(document).ready(function() {

        ocultar_mostrar_combos();

    <?php echo $gestion_seleccion_idioma ?>



        $("#provincias").change(function() {

            $.post("<?php echo base_url() . "index.php/controller_comun_no_seg/carga_municipios_json"?>", { provincias:$(this).val() },
                    function(data) {

                        //               $("#municipios").html(data);
                        // Fill sub category select
                        // Clear all options from sub category select
                        $("select#municipios option").remove();
                        $.each(data, function(i, j) {
                            var row = "<option value=\"" + j.value + "\">" + j.text + "</option>";
                            $(row).appendTo("select#municipios");
                        });
                    }, 'json'
            )
        });

        $("#pais").change(function() {
            ocultar_mostrar_combos();
        });

        //Como definir una función que puede ser utilizada en varios puntos por jquery
        function ocultar_mostrar_combos() {
            if ($("#pais").val()!='ESP'){
            
                $("#provincias").prop('disabled', true);
                $("#municipios").prop('disabled', true);
            } else {
                $("#provincias").prop('disabled', false);
                $("#municipios").prop('disabled', false);
            }
        }


    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


    });

</script>

<div id="container">


<?php echo $header ?>
<!-- //#sub-header -->
<?php echo $capa_mensaje ?>

    <?php
            $nombre = null;
    $cif = null;
    $email = null;
    $email2 = null;
    $password = null;
    $password2 = null;
    $descripcion_corta = null;
    $descripcion = null;
    $pais = null;
    $idioma = null;
    $provincias = null;
    $municipios = null;
    $tipovia = null;
    $calle = null;
    $numero = null;
    $codigopostal = null;
    $telefono = null;
    $telefono2 = null;
    $fax = null;
    $tarifa = null;


    if (isset($datos_refresco_form)) {
        $nombre = $datos_refresco_form['nombre'];
        $cif = $datos_refresco_form['cif'];
        $email = $datos_refresco_form['email'];
        $email2 = $datos_refresco_form['email2'];
        $password = $datos_refresco_form['password'];
        $password2 = $datos_refresco_form['password2'];
        $descripcion_corta = $datos_refresco_form['descripcion_corta'];
        $descripcion = $datos_refresco_form['descripcion'];
        $pais = $datos_refresco_form['pais'];
        $idioma = $datos_refresco_form['idioma'];
        if (!isset($datos_refresco_form['provincias'])) {
            $provincias = null;
        } else {
            $provincias = $datos_refresco_form['provincias'];

        }
        if (!isset($datos_refresco_form['municipios'])) {
            $municipios = null;
        } else {
            $municipios = $datos_refresco_form['municipios'];

        }

        $tipovia = $datos_refresco_form['tipovia'];
        $calle = $datos_refresco_form['calle'];
        $numero = $datos_refresco_form['numero'];
        $codigopostal = $datos_refresco_form['codigopostal'];
        $telefono = $datos_refresco_form['telefono'];
        $telefono2 = $datos_refresco_form['telefono2'];
        $fax = $datos_refresco_form['fax'];
        $tarifa = $datos_refresco_form['tarifa'];
    }



?>
<div id="main-content">
    <h2>Login</h2>
    <BR>
    <!-- //.article -->
    <div class="article-wrapper">
        <form name="formulario" class="formredondo" id="formulario" action="alta_comercio" method="POST">
            <div class="article">
                <h3></h3><br>

                <p>
                <table class="tabladatosoculta">
                    <tr>
                        <td> <?php echo lang('alta_comercio_nombre') ?></td>
                        <td><input type="text" name="nombre" id="nombre" class="formulario1"
                                   value="<?php echo $nombre ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_nifcif') ?></td>
                        <td><input type="text" name="cif" id="cif" class="formulario1" value="<?php echo $cif ?>">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_email') ?></td>
                        <td><input type="text" name="email" id="email" class="formulario1"
                                   value="<?php echo $email ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_email2') ?></td>
                        <td><input type="text" name="email2" id="email2" class="formulario1"
                                   value="<?php echo $email2 ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_password') ?></td>
                        <td><input type="password" name="password" id="password" class="formulario1" value=""></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_password2') ?></td>
                        <td><input type="password" name="password2" id="password2" class="formulario1" value="">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_desc_corta') ?></td>
                        <td><textarea cols="50" rows="5" name="descripcion_corta" id="descripcion_corta"
                                      class="formulario1"><?php echo $descripcion_corta ?></textarea></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_descripcion') ?></td>
                        <td><textarea cols="50" rows="5" name="descripcion" id="descripcion"
                                      class="formulario1"><?php echo $descripcion ?></textarea></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_tarifa') ?></td>
                        <td><SELECT NAME="tarifa" id="tarifa">
                            <?php foreach ($tarifas as $valor) {
                            if ($valor->id_tarifa == $tarifa) {
                                echo "<OPTION VALUE=" . $valor->id_tarifa . " selected>" . $valor->descripcion . "</OPTION>";
                            } else {
                                echo "<OPTION VALUE=" . $valor->id_tarifa . ">" . $valor->descripcion . "</OPTION>";
                            }

                        }
                            ?>
                        </SELECT></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_pais') ?></td>
                        <td><SELECT NAME="pais" id="pais">
                            <?php foreach ($paises as $valor) {
                            if ($valor->id_pais == $pais) {
                                echo "<OPTION VALUE=" . $valor->id_pais . " selected>" . $valor->pais . "</OPTION>";
                            } else {
                                echo "<OPTION VALUE=" . $valor->id_pais . ">" . $valor->pais . "</OPTION>";
                            }

                        }
                            ?>
                        </SELECT></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_idioma') ?></td>
                        <td><SELECT NAME="idioma" id="idioma">
                            <?php foreach ($idiomas as $valor) {
                            if ($valor->id == $idioma) {
                                echo "<OPTION VALUE=" . $valor->id . " selected>" . $valor->nombre . "</OPTION>";
                            } else {
                                echo "<OPTION VALUE=" . $valor->id . ">" . $valor->nombre . "</OPTION>";
                            }

                        }
                            ?>
                        </SELECT></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_telefono') ?></td>
                        <td><input type="text" name="telefono" id="telefono" class="formulario1"
                                   value="<?php echo $telefono ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_telefono2') ?></td>
                        <td><input type="text" name="telefono2" id="telefono2" class="formulario1"
                                   value="<?php echo $telefono2 ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_fax') ?></td>
                        <td><input type="text" name="fax" id="fax" class="formulario1" value="<?php echo $fax ?>">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_provincia') ?></td>
                        <td><SELECT NAME="provincias" id="provincias" class="formulario1">
                            <OPTION VALUE=""><?php echo lang('alta_comercio_busca_municipio') ?></OPTION>
                            <?php foreach ($records2 as $row) : ?>
                            <?php if ($provincias != $row->codprov) { ?>
                                <OPTION VALUE="<?php echo $row->codprov; ?>"><?php echo $row->descprov; ?></OPTION>
                                <?php } else { ?>
                                <OPTION VALUE="<?php echo $row->codprov; ?>"
                                        selected><?php echo $row->descprov; ?></OPTION>
                                <?php } ?>
                            <?php endforeach; ?>
                        </SELECT></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_municipio') ?></td>
                        <td><SELECT NAME="municipios" id="municipios" class="formulario1">
                            <OPTION VALUE=""><?php echo lang('alta_comercio_busca_municipio') ?></OPTION>
                            <?php foreach ($records as $row) : ?>
                            <?php if ($municipios != $row->codmuni) { ?>
                                <OPTION VALUE="<?php echo $row->codmuni; ?>"><?php echo $row->nommuni; ?></OPTION>
                                <?php } else { ?>
                                <OPTION VALUE="<?php echo $row->codmuni; ?>"
                                        selected><?php echo $row->nommuni; ?></OPTION>
                                <?php } ?>
                            <?php endforeach; ?>

                        </SELECT></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_tipovia') ?></td>
                        <td><input type="text" name="tipovia" id="tipovia" class="formulario1"
                                   value="<?php echo $tipovia ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_calle') ?></td>
                        <td><input type="text" name="calle" id="calle" class="formulario1"
                                   value="<?php echo $calle ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_numero') ?></td>
                        <td><input type="text" name="numero" id="numero" class="formulario1"
                                   value="<?php echo $numero ?>"></td>
                    </tr>
                    <tr>
                        <td><?php echo lang('alta_comercio_codigopostal') ?></td>
                        <td><input type="text" name="codigopostal" id="codigopostal" class="formulario1"
                                   value="<?php echo $codigopostal ?>"></td>
                    </tr>

                </table>


        </form>


       

    </div>

</div>
<!-- //.article -->


</div>
<!-- //#main-content -->

<div class="btnWrap" align="center">
    <a class="btnStyle" id="accionboton" href="#"><?php echo lang('alta_comercio_boton_alta') ?></a>
</div>

<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
