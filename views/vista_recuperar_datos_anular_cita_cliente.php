<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>
<?php echo $scripts_definition ?>

<script>
    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>



    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>




        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });


    })
</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>

    <div id="main-content">
        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('anular_cita_cabecera') ?></li>
        </div>
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">
            <div class="article">
                <h3></h3>

                <p>

                <form name="formulario" id="formulario"
                      action="<? echo site_url() . "/controller_comercio/anular_cita_cliente/"  ?>" method="POST">
                    <input type="hidden" name="fechareserva" value="<?php echo $fechareserva  ?>">
                    <input type="hidden" name="email" value="<?php echo $email  ?>">
                    <input type="hidden" name="telefono" value="<?php echo $telefono ?>">
                    <input type="hidden" name="id_usuario" value="<?php echo $id_usuario ?>">


                    <TABLE class="tabladatos">
                        <tr>
                            <th> <?php echo lang('anular_cita_fecha') ?></th>
                            <th><?php echo lang('anular_cita_hora_inicio') ?></th>
                            <th> <?php echo lang('anular_cita_hora_fin') ?></th>
                            <th> <?php echo lang('anular_cita_duracion') ?> </th>
                            <th> <?php echo lang('anular_cita_recurso') ?> </th>
                            <th> <?php echo lang('anular_cita_servicio') ?></th>
                            <th> <?php echo lang('anular_cita_nombre') ?> </th>
                            <th> <?php echo lang('anular_cita_email') ?> </th>
                            <th> <?php echo lang('anular_cita_telefono') ?> </th>
                            <th><?php echo lang('anular_cita_anular') ?></th>
                        </tr>
                        <?

                        foreach ($misreservas as $row) {

                            echo "<TR>";
                            echo "<TD>" . $row->fecha_reserva . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>";
                            echo "<TD>" . $row->nombre_recurso . "</TD><TD>" . $row->nombre_servicio . "</TD>";

                            if ($row->flag_usuario_reserva == 'COMERCIO') {
                                echo "<TD>" . $row->nombre_usuario . "</TD><TD>" . $row->email . "</TD><TD>" . $row->telefono . "</TD>";
                            } elseif ($row->flag_usuario_reserva == 'USUARIO') {
                                echo "<TD>" . $row->nombre_reserva . "</TD><TD>" . $row->email_reserva . "</TD><TD>" . $row->telefono_reserva . "</TD>";

                            }
                            echo "<TD><input type=radio name=id_reserva value=" . $row->id_reserva . "></TD>";
                            echo "</TR>";
                        }

                        ?>
                    </TABLE>
                    <br>

                    <div class="btnWrap" align="center">
                        <a class="btnStyle" id="accionboton" href="#"><?php echo lang('anular_cita_anular') ?></a>
                    </div>


                </form>
            </div>
            <div class="article">


            </div>
        </div>
       
    </div>
    <!-- //.article -->
</div>
<!-- //#main-content -->


<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
