<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>


<body>
<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });

</script>
<div id="container">


    <?php echo $header ?>

    <!-- //#sub-header -->

    <?php echo $capa_mensaje ?>

    <?php

    $numero_reservas = null;
    $numero_reservas_mes = null;
    $numero_reservas_activas = null;
    $nombre = null;
    $email = null;
    $fecha_reserva_log = null;

    foreach ($numeroreservas as $value) {
        $numero_reservas = $value->numero;
    }

    foreach ($numeroreservasmes as $value) {
        $numero_reservas_mes = $value->numero;
    }
    foreach ($numeroreservasactivas as $value) {
        $numero_reservas_activas = $value->numero;
    }
    foreach ($ultimareserva as $value) {
        $fecha_reserva_log = $value->fecha_reserva_log;
        $flag_usuario_reserva = $value->flag_usuario_reserva;
        if ($flag_usuario_reserva == 'COMERCIO') {
            $nombre = $value->nombre1;
            $email = $value->email1;
        } else {
            $nombre = $value->nombre2;
            $email = $value->email2;
        }
    }

    ?>

    <div id="main-content">
        <h2></h2>


        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('inicio_com_cabecera') ?></li>
        </div>
        <div class="article-wrapper">
            </ul>

            <br>

            <div class="article">
                <h3></h3>

                <p>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('inicio_com_ult_res_cab') ?></th>
                    </tr>
                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('inicio_com_ult_fecha') ?></th>
                        <th><?php echo lang('inicio_com_ult_horaini') ?></th>
                        <th><?php echo lang('inicio_com_ult_horafin') ?></th>
                        <th> <?php echo lang('inicio_com_ult_duracion') ?> </th>
                        <th><?php echo lang('inicio_com_ult_usuario') ?> </th>
                        <th><?php echo lang('inicio_com_ult_servicio') ?></th>
                        <th><?php echo lang('inicio_com_ult_mail') ?></th>
                        <th> <?php echo lang('inicio_com_ult_telef') ?> </th>
                    </tr>

    <?php
               foreach ($misreservas as $row) {
                    if ($row->flag_usuario_reserva == 'USUARIO') {
                        echo "<TR>";
                        echo "<TD>" . date("d-m-Y", strtotime($row->fecha_reserva)) . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>"
                             . "<TD>" . $row->nombre_usuario . "</Td><TD>" . $row->nombre_servicio . "</Td><TD>" . $row->email . "</Td><TD>" . $row->telefono . "</Td>";
                        echo "</TR>";
                    } else {
                        echo "<TR>";
                        echo "<TD>" . date("d-m-Y", strtotime($row->fecha_reserva)) . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>"
                             . "<TD>" . $row->nombre_reserva . "</Td><TD>" . $row->nombre_servicio . "</Td><TD>" . $row->email_reserva . "</Td><TD>" . $row->telefono_reserva . "</Td>";
                        echo "</TR>";
                    }
                }

                    ?>
                </TABLE>
                </p>
            </div>

            <!-- //.article -->
            <BR>
            <ul class="article-actions">
                <li><img src="<?= base_url() . APPPATH ?>estaticos/images/icons/date.gif"
                         alt=""/><?php echo lang('inicio_com_ult_res_fec') ?> <?php echo $fecha_reserva_log ?> </li>
                <li><img src="<?= base_url() . APPPATH ?>estaticos/images/icons/author.gif" alt=""/>
                    <?php echo lang('inicio_com_ult_res_rea') ?> <?php echo $nombre ?> -  <?php echo $email ?>
                </li>

            </ul>

            <ul class="article-actions">
                <li><img src="<?= base_url() . APPPATH ?>estaticos/images/icons/comments.gif"
                         alt=""/> <?php echo $numero_reservas_activas ?> <?php echo lang('inicio_com_ult_res_curso') ?>
                </li>
                <li><img src="<?= base_url() . APPPATH ?>estaticos/images/icons/comments.gif"
                         alt=""/> <?php echo $numero_reservas_mes ?> <?php echo lang('inicio_com_ult_res_mes') ?></li>
                <li><img src="<?= base_url() . APPPATH ?>estaticos/images/icons/comments.gif"
                         alt=""/> <?php echo $numero_reservas ?> <?php echo lang('inicio_com_ult_res_total') ?></li>
            </ul>
        </div>
        <BR>

        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <p>

                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('inicio_com_prox') ?></th>
                    </tr>
                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('inicio_com_ult_fecha') ?></th>
                        <th><?php echo lang('inicio_com_ult_horaini') ?></th>
                        <th><?php echo lang('inicio_com_ult_horafin') ?></th>
                        <th> <?php echo lang('inicio_com_ult_duracion') ?> </th>
                        <th><?php echo lang('inicio_com_ult_usuario') ?> </th>
                        <th><?php echo lang('inicio_com_ult_servicio') ?></th>
                        <th><?php echo lang('inicio_com_ult_mail') ?></th>
                        <th> <?php echo lang('inicio_com_ult_telef') ?> </th>
                    </tr>
    <?php
                  foreach ($misproximasreservas as $row) {
                    if ($row->flag_usuario_reserva == 'USUARIO') {
                        echo "<TR>";
                        echo "<TD>" . date("d-m-Y", strtotime($row->fecha_reserva)) . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>"
                             . "<TD>" . $row->nombre_usuario . "</Td><TD>" . $row->nombre_servicio . "</Td><TD>" . $row->email . "</Td><TD>" . $row->telefono . "</Td>";
                        echo "</TR>";
                    } else {
                        echo "<TR>";
                        echo "<TD>" . date("d-m-Y", strtotime($row->fecha_reserva)) . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>"
                             . "<TD>" . $row->nombre_reserva . "</Td><TD>" . $row->nombre_servicio . "</Td><TD>" . $row->email_reserva . "</Td><TD>" . $row->telefono_reserva . "</Td>";
                        echo "</TR>";
                    }
                }

                    ?>
                </TABLE>
                </p>
            </div>
        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->

    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
