<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <?php echo $head ?>
<body>

 <?php echo $scripts_definition ?>


<script>

$(document).ready(function(){

<?php echo $gestion_seleccion_idioma ?>


<?php
  if (isset($validation_errors) || (isset($mensaje_ok))) {
    echo "$('#capamensaje').attr('style', 'visibility: visible');";
    echo "$('#capamensaje').hide();";
    echo "$('#capamensaje').slideDown('slow');";
    if (isset($validation_errors)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
    }
    if (isset($mensaje_ok)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
    }
  } else {
    $capa_mensaje="";
  }
?>


});

</script>

<div id="container">


  <?php echo $header ?>
  <!-- //#sub-header -->
 
   <?php echo $capa_mensaje ?>


    <?php
        $email=null;

        if (isset($datos_refresco_form)) {
          $email=$datos_refresco_form['email'];
       }



    ?>
  <div id="main-content">
    <h2>Login</h2>
    <BR>
    <!-- //.article -->
    <div class="article-wrapper">
      
      <div class="article">
        <h3>Introducir los siguientes datos</h3><br>
        <p>
        <form name="formulario" class="formredondo" action="<?php echo base_url() . "index.php/controller_alta/recuperar_password"   ?>" method="POST">
		<table class="tabladatosoculta">
	     <tr>
	        <td><?php echo lang('recuperar_pass_email') ?></td>
			<td><input type="text" name="email" id="email" class="formulario1" value=""></td>
		 </tr>
        
		 <tr>
		    <td><input type="submit" text="Enviar"></td>
		 </tr>	
	   </table>
      </form>
		
       </p>
   
    </div>

    <!-- //.article -->
  </div>
  <!-- //#main-content -->
<br>



  <div id="footer">
     <?php echo $footer ?>
  </div>
  <!-- //#footer -->
  
</div>
<!-- //#container -->
</body>
</html>
