<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>


    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });

</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('lista_tarifas_cabecera') ?></li>
      </div>
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <TABLE class="tabladatos">
                    <tr>
                        <th> <?php echo lang('lista_tarifas_lista') ?>  </th>
                    </tr>


                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('lista_tarifas_nombre') ?></th>
                        <th><?php echo lang('lista_tarifas_fecha_ini') ?></th>
                        <th><?php echo lang('lista_tarifas_fecha_fin') ?></th>
                        <th><?php echo lang('lista_tarifas_hora_ini') ?></th>
                        <th><?php echo lang('lista_tarifas_hora_fin') ?></th>
                        <th><?php echo lang('lista_tarifas_nombre_ser') ?></th>
                        <th><?php echo lang('lista_tarifas_importe') ?></th>
                        <th><?php echo lang('lista_tarifas_iva') ?></th>
                        <th><?php echo lang('lista_tarifas_importetotal') ?></th>
                        <th><?php echo lang('lista_tarifas_estado') ?></th>
                        <!-- <th><?php echo lang('lista_tarifas_dias') ?></th> -->
                    </tr>
    <?php

                    foreach ($datos_tarifas as $row) {

                        echo "<tr>";

                        echo    "<td><a href=\"" . site_url() . "/controller_comercio_adm_tarifas/previa_alta_tarifa/" . $row->id_sertar  . "\">" . $row->nombre_tarifa . "</a></td>";
                        echo    "<td>" . $row->fecha_inicio . "</td>";
                        echo    "<td>" . $row->fecha_fin . "</td>";
                        echo    "<td>" . $row->hora_inicio . "</td>";
                        echo    "<td>" . $row->hora_fin . "</td>";
                        echo    "<td>" . $row->nombre_servicio . "</td>";
                        echo    "<td>" . $row->importe . "</td>";
                        echo    "<td>" . $row->iva . "</td>";
                        echo    "<td>" . $row->importe_total . "</td>";
                        echo    "<td>" . $row->estado . "</td>";
                       // echo    "<td>" . $row->dias_afectados . "</td>";
                        echo "</tr>";
                    }
                    ?>
                </TABLE>


                <br>
            </div>
            <!-- //.article -->
        </div>
        <!-- //#main-content -->


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
