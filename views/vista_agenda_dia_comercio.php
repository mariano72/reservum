<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>


<?php echo $scripts_definition ?>


<script>
    $(document).ready(function() {

        $('#fechareserva').datepicker({ minDate: '-0d' });
        $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);


        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });

    <?php echo $gestion_seleccion_idioma ?>




    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

        $(".tr_destacado").click(function() {
            /* personally I would throw a url attribute (<tr url="http://www.hunterconcepts.com">) on the tr and pull it off on click */
            window.location = $(this).attr("url");

        });

    });

</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>

    <div id="main-content">
        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('agenda_dia_cabecera') ?></li>
        </div>
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">


            <div class="article">
                <h3></h3>

                <p>

                <form id="formulario2" action="<?php echo site_url() ?>/controller_comercio/agenda_dia_comercio"
                      method="POST">
                    <table class="tabladatos">
                        <th><?php echo lang('agenda_dia_seleccionar') ?></th>
                        <th></th>
                        <tr>
                            <td><input name="fechareserva" id="fechareserva" type="text"
                                       value="<?php echo $fechareserva ?>"/></td>
                            <td><input type="submit" value="<?php echo lang('agenda_dia_ver_agenda') ?>"></td>
                        </tr>
                    </table>
                    <input name="comercio" id="comercio" type="hidden" value="<?php echo $id_comercio ?>"/>


                </form>
                <BR>

                <?
                $recurso_anterior = null;
                $indice = 0;
                foreach ($reservas as $row) {
                    if ($recurso_anterior != $row->nombre_recurso) {
                        if ($indice == 0) {
                            $tabla = "<TABLE class=\"tabladatos\">";
                        } else {
                            $tabla = "</TABLE><BR><TABLE class=\"tabladatos\">";
                        }
                        echo $tabla . "<tr><th> " . $row->nombre_recurso . "</th></tr></TABLE>";
                        echo "<TABLE class=\"tabladatos\"><tr><th>" . lang('agenda_dia_hora_inicio') . "</th><th>" . lang('agenda_dia_hora_fin') . "</th><th>" . lang('agenda_dia_servicio') . "</th>
              <th>" . lang('agenda_dia_usuario') . "</th><th>" . lang('agenda_dia_email') . "</th><th>" . lang('agenda_dia_telefono') . "</th></tr>";
                    }
                    echo "<TR class =\"tr_destacado\" url=\"" . site_url() . "/controller_comercio/consultar_detalle_reserva/" . $row->id_reserva . "\">";
                    $linea = "<TD width=\"15%\">" . $row->hora_inicio . "</TD><TD width=\"15%\">" . $row->hora_fin . "</TD><TD width=\"15%\">" . $row->nombre_servicio . "</TD>";
                    if ($row->flag_usuario_reserva == "USUARIO") {
                        $linea2 = "<TD width=\"15%\">" . $row->nombre_reserva . "</TD><TD width=\"15%\">" . $row->email_reserva . "</TD><TD width=\"15%\">" . $row->telefono_reserva . "</TD>";
                    } else {
                        $linea2 = "<TD width=\"15%\">" . $row->nombre_usuario . "</TD><TD width=\"15%\">" . $row->email . "</TD><TD width=\"15%\">" . $row->telefono . "</TD>";

                    }
                    echo $linea . $linea2;
                    $recurso_anterior = $row->nombre_recurso;
                    $indice = $indice + 1;

                    echo "</TR>";
                }
                echo "</TABLE><BR>";
                ?>

                </p>
            </div>
        </div>

        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
