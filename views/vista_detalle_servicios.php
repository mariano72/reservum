<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });
        $('#importe_total').attr("readonly", "readonly");

        $("#iva").change(function () {
            calcular_importe_total();
        });
        $("#importe").change(function() {
                    //  $("#importe").number( "#importe", 1, ',', '' );
                    calcular_importe_total();
                }
        );

        //Como definir una función que puede ser utilizada en varios puntos por jquery
        var calcular_importe_total = function() {
            var importe = $("#importe").val();
            var iva = $("#iva").val();
            var importe_total = parseFloat(importe) + (parseFloat(importe) * parseFloat(iva));
            importe_total = importe_total.toFixed(2);
            $("#importe_total").val(importe_total);
        };
    })

</script>


<div id="container">

    <?php echo $header ?>

    <!-- //#sub-header -->

    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('alta_servicios_cabecera_alta') ?></li>
      </div>
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <p>
    <?php
             $id_servicio = null;
        $nombre = null;
        $descripcion = null;
        $importe = null;
        $iva = null;
        $importe_total = null;
        $minutos_elegidos = null;


        if (isset($datos_refresco_form)) {
            //	print_r($datos_refresco_form);
            $id_servicio = $datos_refresco_form['id_servicio'];
            $nombre = $datos_refresco_form['nombre'];
            $descripcion = $datos_refresco_form['descripcion'];
            $minutos_elegidos = $datos_refresco_form['duracion_minutos'];
            $importe = $datos_refresco_form['importe'];
            $iva_seleccionado = $datos_refresco_form['iva'];
            $importe_total = $datos_refresco_form['importe_total'];
            $url_destino = $datos_refresco_form['url_destino'];

        } else {

            foreach ($datos_servicios as $row) {
                $id_servicio = $row->id_servicio;
                $nombre = $row->nombre;
                $descripcion = $row->descripcion;
                $minutos_elegidos = $row->duracion_minutos;
                $importe = $row->importe;
                $iva_seleccionado = $row->iva;
                $importe_total = $row->importe_total;
            }
        }


        ?>

                <form name="formulario" id="formulario" action="<?php echo site_url() . $url_destino; ?>" method="POST">
                    
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('alta_servicios_nombre') ?></th>
                            <th><?php echo lang('alta_servicios_descripcion') ?></th>
                            <th><?php echo lang('alta_servicios_Duracion') ?></th>
                            <th><?php echo lang('alta_servicios_Importe') ?></th>
                            <th>
                                <?php echo lang('alta_servicios_IVA') ?></th>
                            <th><?php echo lang('alta_servicios_Importe_total') ?></th>
                        </tr>
                        <TR>
                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio  ?>">
                            <input type="hidden" name="url_destino" value="<?php echo $url_destino  ?>">
                            <TD><input type="text" name="nombre" value="<?php echo $nombre  ?>"></TD>
                            <TD><input type="text" name="descripcion" value="<?php echo $descripcion ?>"></TD>
                            <TD><SELECT NAME="duracion_minutos">
                                <?php foreach ($duracion_minutos as $indice => $valor) {
                                if ($indice == $minutos_elegidos) {
                                    echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                                } else {
                                    echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                                }

                            }
                                ?>
                            </SELECT></TD>
                            <TD><input type="text" name="importe" id="importe" value="<?php echo $importe ?>"></TD>
                            <td><SELECT NAME="iva" id="iva">
                                <?php foreach ($tipos_iva as $indice => $valor) {
                                if ($indice == $iva_seleccionado) {
                                    echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                                } else {
                                    echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                                }

                            }
                                ?>
                            </SELECT></TD>
                            <TD><input type="text" name="importe_total" id="importe_total"
                                       value="<?php echo $importe_total ?>"></TD>


                        </TR>
                    </TABLE>

                    <br>
                    <div class="btnWrap" align="center">
                        <a class="btnStyle" id="accionboton" href="#">Alta/modificar servicio</a>
                    </div>

                </form>


                <br>

                <p>


                </p>

            </div>
            <!-- //.article -->
        </div>
        <!-- //#main-content -->


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
