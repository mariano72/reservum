<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>
    $(document).ready(function() {




    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });
</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>

    <div id="main-content">
        <h2></h2>

        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('anular_cabecera') ?></li>
        </div>
        <!-- //.article -->
        <div class="article-wrapper">
            <div class="article">
                <h3></h3>

                <p>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('anular_cabecera_tabla') ?></th>
                    </tr>
                </table>
                <form name="formulario" id="formulario"
                      action="<? echo site_url() . "/controller_ini/anular_reserva/"  ?>" method="POST">

                    <TABLE class="tablahoras">
                        <tr>
                            <th> <?php echo lang('anular_fecha') ?></th>
                            <th><?php echo lang('anular_horaini') ?> </th>
                            <th><?php echo lang('anular_horafi') ?> </th>
                            <th> <?php echo lang('anular_duracion') ?>  </th>
                            <th><?php echo lang('anular_nombre') ?>  </th>
                            <th> <?php echo lang('anular_recurso') ?> </th>
                            <th> <?php echo lang('anular_servicio') ?> </th>
                            <th> <?php echo lang('anular_direccion') ?> </th>
                            <th> <?php echo lang('anular_numero') ?> </th>
                            <th><?php echo lang('anular_codpostal') ?></th>
                            <th> <?php echo lang('anular_telefono') ?></th>
                            <th><?php echo lang('anular_anular') ?></th>
                        </tr>
                        <?

                        foreach ($misreservas as $row) {
                            echo "<TR>";
                            echo "<TD>" . $row->fecha_reserva . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>";
                            echo "<TD>" . $row->nombre_comercio . "</TD><TD>" . $row->nombre_recurso . "</TD><TD>" . $row->nombre_servicio . "</TD>";
                            echo "<TD>" . $row->nombre_calle . "</TD><TD>" . $row->numero . "</TD><TD>" . $row->codigo_postal . "</TD><TD>" . $row->telefono . "</TD>";
                            echo "<TD><input type=radio name=\"id_reserva\" value=" . $row->id_reserva . "></TD>";
                            echo "</TR>";
                        }
                        echo "</TABLE>";
                        ?>

                </form>

                </p>
            </div>


        </div>
        <div class="btnWrap" align="center">
            <a class="btnStyle" id="accionboton" href="#"><?php echo lang('anular_boton') ?></a>
        </div>
    </div>
    <!-- //.article -->
</div>
<!-- //#main-content -->


<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
