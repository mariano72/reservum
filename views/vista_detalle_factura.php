<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

       


    });


</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3><?php echo lang('detalle_factura_cabecera') ?></h3>

                <p>
    <?php

        foreach ($datos_factura as $row) {
            $id_factura = $row->id_factura;
            $numero_factura = $row->numero_factura;
            $fecha_factura = $row->fecha_factura;
            $estado_factura = $row->estado_factura;
            $estado = $row->estado;
            $fecha_desde = $row->fecha_desde;
            $fecha_hasta = $row->fecha_hasta;
            $importe = $row->importe;
            $iva = $row->iva;
            $importe_total = $row->importe_total;
            $concepto = $row->concepto;
            $desc_tarifa = $row->desc_tarifa;
            $fecha_pago=$row->fecha_pago;
            $paypal_gateway=$row->paypal_gateway;
        }

        if (isset($datos_refresco_form)) {
            //print_r($datos_refresco_form);
            //$id_recurso=$datos_refresco_form['id_recurso'];


        }
        $boton_paypal = null;

        if ($estado != 'PAGADA') {
            $boton_paypal = "<input type='image' name='submit' src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif'
                           border='0' align='top' alt='Check out with PayPal'/>";
        }
        ?>

                <form name="formulario"
                      action="<?php echo site_url()  ?>/controller_comercio_adm_facturas/pagar_factura" method="POST">
                    <input type="hidden" name="id_factura" value="<?php echo $id_factura ?>">
                    <input type="hidden" name="numero_factura" value="<?php echo $numero_factura ?>">
                    <input type="hidden" name="importe_total" value="<?php echo $importe_total ?>">
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('detalle_factura_numero') ?></th>
                            <th><?php echo lang('detalle_factura_fecha') ?> </th>
                            <th> <?php echo lang('detalle_factura_estado') ?> </th>
                            <th><?php echo lang('detalle_factura_periodo') ?></th>
                        </tr>
                        <TR>

                            <TD><?php echo $numero_factura ?></TD>
                            <TD><?php echo $fecha_factura ?></TD>
                            <TD><?php echo $estado_factura ?></TD>
                            <TD><?php echo $fecha_desde . " - " . $fecha_hasta ?></TD>

                        </TR>
                        <tr>
                            <th><?php echo lang('detalle_factura_importe') ?></th>
                            <th><?php echo lang('detalle_factura_iva') ?> </th>
                            <th> <?php echo lang('detalle_factura_importetotal') ?> </th>
                            <th><?php echo lang('detalle_factura_tarifa') ?> </th>

                        </tr>
                        <TR>

                            <TD><?php echo $importe ?></TD>
                            <TD><?php echo $iva ?></TD>
                            <TD><?php echo $importe_total ?></TD>
                            <TD><?php echo $desc_tarifa ?>
                            </TD>

                        </TR>
                        <tr>
                            <th><?php echo lang('detalle_factura_fecha_pago') ?></th>
                            <th><?php echo lang('detalle_factura_paypal_gateway') ?> </th>


                        </tr>
                        <TR>

                            <TD><?php echo $fecha_pago ?></TD>
                            <TD><?php echo $paypal_gateway ?></TD>

                            </TD>

                        </TR>
                        <tr>
                            <th> <?php echo lang('detalle_factura_pagar') ?></th>
                            <TD>
                                <?php echo $boton_paypal ?>
                            </TD>
                            <td></td>
                            <td></td>
                        </tr>

                    </TABLE>
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('detalle_factura_concepto') ?></th>


                        </tr>
                        <tr>

                            <TD><?php echo $concepto ?></TD>


                        </tr>

                    </TABLE>


                </form>

                <br>

                <p>


                </p>

            </div>
            <!-- //.article -->
        </div>
        <!-- //#main-content -->


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
