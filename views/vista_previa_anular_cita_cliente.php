<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>


    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

        $('#fechareserva').datepicker({ minDate: '-0d' });
        $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);

        $("#email ,#telefono").change(function() {
          $("#fechareserva").val("");
        });
        $("#fechareserva").change(function() {
          $("#email ,#telefono").val("");
        });

        $("#accionboton").click(function() {
            if ($("#fechareserva").val()==''){
              $("#formularioespecial").attr('action', '<? echo site_url() . "/controller_comercio/recuperar_datos_anular_cita_cliente/"  ?>');
            } else {
              $("#formularioespecial").attr('action', '<? echo site_url() . "/controller_comercio/recuperar_datos_anular_cita_comercio_por_fecha/"  ?>');
            }
            
            $('#formularioespecial').submit();
            return false;
        });


    });
</script>


<div id="container">


    <?php echo $header ?>

    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>




    <div id="main-content">
       <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('previa_anular_cita') ?></li>
        </div>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">
            <div class="article">
                <h3></h3>


                <form name="formularioespecial" id="formularioespecial" method="POST">
                    <TABLE class="tabladatos">
                        
                        <th><?php echo lang('previa_anular_cabecera1') ?></th>
                    </table>
                    <TABLE class="tabladatos">

                        <tr>
                            <td><?php echo lang('previa_anular_mail') ?></td>
                            <td><input type="text" name="email" id="email" value=""><br></td>

                        </tr>
                        <tr>
                            <td><?php echo lang('previa_anular_telefono') ?></td>
                            <td><input type="text" name="telefono" id="telefono" value=""></td>
                        </tr>
                    </TABLE>
                    <br>
                    <TABLE class="tabladatos">
                       
                        <th><?php echo lang('previa_anular_cabecera2') ?></th>
                    </table>
                    <TABLE class="tabladatos">

                        <tr>
                            <td><?php echo lang('previa_anular_fecha') ?></td>
                            <td><input name="fechareserva" id="fechareserva" type="text"/></td>
                        </tr>

                    </TABLE>

                </form>
                 <br>
                <div class="btnWrap" align="center">
                    <a class="btnStyle" id="accionboton" href="#"><?php echo lang('previa_anular_buscar') ?></a>
                </div>

            </div>

        </div>

    </div>
    <!-- //.article -->
</div>
<!-- //#main-content -->


<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
