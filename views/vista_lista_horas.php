<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>


<?php echo $scripts_definition ?>

<script>
    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


        $('#fechareserva').datepicker({ minDate: '-0d' });
        $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);


        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });

    <? if (strlen($mensajeerror) != 0) {
            echo "$(function() {
		$( \"#dialog\" ).dialog(
		   {
			modal: true,
			resizable: false,
			buttons: {
				      Ok: function() {
					  $( this ).dialog( \"close\" );
				     }
		   }});
	});";

        }
    ?>

        $(".td_destacado").click(function() {
            /* personally I would throw a url attribute (<tr url="http://www.hunterconcepts.com">) on the tr and pull it off on click */
            window.location = $(this).attr("url");

        });


    });
</script>

<div id="dialog" title="Alerta" style="font-size:10px">
    <? echo $mensajeerror; ?>
</div>

<div id="container">


    <?php echo $header ?>

    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>

    <?
    $id_servicio = "";
    $nombre = "";
    $duracion = 0;

    foreach ($datos_servicio as $row)
    {
        $id_servicio = $row->id_servicio;
        $nombre = $row->nombre;
        $duracion = $row->duracion_minutos;
    }
    ?>


    <div id="main-content">
        <h2>RESERVAS</h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">


            <div class="article">
                <h3></h3>

                <p>

                <form id="formulario2" action="<? echo site_url() . "/controller_comun/dame_agenda_dia/"  ?>"
                      method="POST">
                    <div>
                        <input name="comercio" id="comercio" type="hidden" value="<?php echo $id_comercio ?>"/>
                        <input name="id_servicio" id="id_servicio" type="hidden" value="<?php echo $id_servicio ?>"/>
                        <table class="tabladatos">
                            <th><?php echo lang('reservar_com_fecha_reserva') ?></th>
                            <th></th>
                            <tr>
                                <td><input name="fechareserva" id="fechareserva" type="text"
                                           value="<?php echo $fechareserva ?>"/></td>
                                <td><input type="submit" value="<?php echo lang('reservar_com_ver_agenda') ?>"></td>
                            </tr>
                        </table>

                        <br>
                        <TABLE class="tablahoras">
                            <th> <?php echo lang('lista_horas_servicio') ?></th>
                            <TR>
                                <TD><?php echo $nombre . " --- " . $duracion ?> <?php echo lang('lista_horas_minutos') ?></TD>
                            </TR>
                        </TABLE>
                        <BR>
                        <TABLE class="tablahoras">

                            <tr>
                                <td class="celda2"><?php echo lang('lista_horas_reservada') ?></td>
                                <td class="celda3"><?php echo lang('lista_horas_nd') ?></td>
                                <td class="celda4"><?php echo lang('lista_horas_en_proceso') ?></td>
                            </tr>

                        </table>

                    </div>
                </form>




    <?php


                $indice = 0;
                $numero_franjas = 0;
                $recurso_ant = null;
                $transform_fecha = explode("/", $fechareserva);
                $fechareserva = $transform_fecha[2] . "-" . $transform_fecha[1] . "-" . $transform_fecha[0];
                $total_tramos = count($horas);

                foreach ($horas as $valor) {

                    $recurso = $valor[0];


                    if ($recurso != $recurso_ant) { // Se crea una tabla nueva para cada nuevo recurso
                        echo "</TABLE>";
                        echo "<BR><BR><TABLE class=\"tablahoras\"><tr><th> " . $valor[1] . "</th></tr></TABLE><TABLE class=\"tablahoras\"><tbody>";
                        $numero_franjas = 0;
                    }

                    $valor_desglosado = explode(":", $valor[2]);
                    $franja_hora_mostrar = $valor_desglosado[0] . ":" . $valor_desglosado[1];
                    $franja_hora_no_seleccionable = $valor_desglosado[0] . ":" . $valor_desglosado[1];
              

                    $franja_hora = site_url("/controller_comun/previa_reserva/" . $id_comercio . "/" . $recurso . "/" . $id_servicio . "/" . urlencode($franja_hora_mostrar) . "/" . $fechareserva);
                    $hash=generar_hash($franja_hora,20);
                    $franja_hora=$franja_hora . "/" . $hash;
                    //Si tenemos más de 16 franjas saltamos de línea para que no descuadre la pantalla
                    if ($numero_franjas % 15 == 0) {
                        $linea = "<TR></TR>";
                        echo $linea;
                    }
                    $linea = "<TD url=\"" . $franja_hora . "\"  class=\"td_destacado\">" . $franja_hora_mostrar . "</TD>";

                    //Se marcan reservas ya hechas
                    if (in_array($indice, $reservas_marcadas)) {

                        $linea = "<TD class=\"celda2\">" . $franja_hora_no_seleccionable . "</TD>";

                    } elseif (in_array($indice, $reservas_marcadas_pendientes)) {

                        $linea = "<TD class=\"celda4\">" . $franja_hora_no_seleccionable . "</TD>";


                    } elseif (in_array($indice, $restricciones_marcadas)) {

                        $linea = "<TD class=\"celda3\">" . $franja_hora_no_seleccionable . "</TD>";

                    }
                    echo $linea;


                    $indice++;
                    $numero_franjas++;
                    $recurso_ant = $valor[0];

                }


                echo "</tbody></TABLE>";

                ?>

                </p>
            </div>
        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
