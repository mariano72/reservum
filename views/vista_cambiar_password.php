<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <?php echo $head ?>
<body>

 <?php echo $scripts_definition ?>


<script>

$(document).ready(function(){

<?php echo $gestion_seleccion_idioma ?>


<?php
  if (isset($validation_errors) || (isset($mensaje_ok))) {
    echo "$('#capamensaje').attr('style', 'visibility: visible');";
    echo "$('#capamensaje').hide();";
    echo "$('#capamensaje').slideDown('slow');";
    if (isset($validation_errors)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
    }
    if (isset($mensaje_ok)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
    }
  } else {
    $capa_mensaje="";
  }
?>


});

</script>

<div id="container">


  <?php echo $header ?>
  <!-- //#sub-header -->
 
   <?php echo $capa_mensaje ?>


    <?php
        $password=null;
        $newpassword=null;
        $newpassword2=null;

        if (isset($datos_refresco_form)) {
          $password=$datos_refresco_form['password'];
          $newpassword=$datos_refresco_form['newpassword'];
          $newpassword2=$datos_refresco_form['newpassword2'];
        }



    ?>
  <div id="main-content">
       <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('cambio_pass_cabecera') ?></li>
        </div>
    <h2></h2>
  
    <!-- //.article -->
    <div class="article-wrapper">
      
      <div class="article">
        <h3></h3>
        <p>
        <form name="formulario" class="formredondo" action="<?php echo base_url() . "index.php/controller_comun/cambiar_password"   ?>" method="POST">
		<table class="tabladatosoculta">
	     <tr>
	        <td><?php echo lang('cambio_pass_password') ?></td>
			<td><input type="password" name="password" id="password" class="formulario1" value=""></td>
		 </tr>
         <tr>
           <td><?php echo lang('cambio_pass_newpassword') ?></td>
           <td><input type="password" name="newpassword" id="newpassword" class="formulario1" value=""></td>
         </tr>

         <tr>
          	<td><?php echo lang('cambio_pass_newpassword2') ?></td>
			<td><input type="password" name="newpassword2" id="newpassword2" class="formulario1" value=""></td>
		 </tr>

		 <tr>
		    <td><input type="submit" text="Enviar"></td>
		 </tr>	
	   </table>
      </form>
		
       </p>
   
    </div>

    <!-- //.article -->
  </div>
  <!-- //#main-content -->
<br>



  <div id="footer">
     <?php echo $footer ?>
  </div>
  <!-- //#footer -->
  
</div>
<!-- //#container -->
</body>
</html>
