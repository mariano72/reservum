<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>



        $("fechainicio").attr('disabled', 'disabled');
        $("fechafin").attr('disabled', 'disabled');



    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


		      


        $(function() {
            $("#fechainicio").datepicker({ minDate: '-0d' });
            $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);
        });

        $(function() {
            $("#fechafin").datepicker({ minDate: '-0d' });
            $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);
        });


        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });





    });

</script>


<div id="container">


<?php echo $header ?>
<!-- //#sub-header -->
<?php echo $capa_mensaje ?>


<div id="main-content">
<div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('alta_restricciones_cabecera') ?></li>
      </div>

    <h2></h2>
    <BR>
    <!-- //.article -->
    <div class="article-wrapper">

        <div class="article">
            <h3></h3>

            <p>
    <?php

        $id_restriccion = null;
        $id_recurso = null;
        $fecha_inicio = null;
        $fecha_fin = null;
        $hora_inicio = null;
        $hora_fin = null;
        $lunes = null;
        $martes = null;
        $miercoles = null;
        $jueves = null;
        $viernes = null;
        $sabado = null;
        $domingo = null;
        $dias_afectados = null;
        $restricciones = null;


        if (isset($datos_refresco_form) && strlen($validation_errors) > 0) {
            $id_recurso = $datos_refresco_form['id_recurso'];
            $id_restriccion = $datos_refresco_form['id_restriccion'];
            $fecha_inicio = $datos_refresco_form['fecha_inicio'];
            $fecha_fin = $datos_refresco_form['fecha_fin'];
            $hora_inicio = $datos_refresco_form['hora_inicio'];
            $hora_fin = $datos_refresco_form['hora_fin'];
            if (!isset($datos_refresco_form['restricciones'])) {
                $restricciones = null;
            }
            else {
                $restricciones = $datos_refresco_form['restricciones'];
            }
            if (!isset($datos_refresco_form['lunes'])) {
                $lunes = "";
            }
            else {
                $lunes = "S";
            }
            if (!isset($datos_refresco_form['martes'])) {
                $martes = "";
            }
            else {
                $martes = "S";
            }
            if (!isset($datos_refresco_form['miercoles'])) {
                $miercoless = "";
            }
            else {
                $miercoles = "S";
            }
            if (!isset($datos_refresco_form['jueves'])) {
                $jueves = "";
            }
            else {
                $jueves = "S";
            }
            if (!isset($datos_refresco_form['viernes'])) {
                $viernes = "";
            }
            else {
                $viernes = "S";
            }
            if (!isset($datos_refresco_form['sabado'])) {
                $sabado = "";
            }
            else {
                $sabado = "S";
            }
            if (!isset($datos_refresco_form['domingo'])) {
                $domingo = "";
            }
            else {
                $domingo = "S";
            }
            if (!isset($datos_refresco_form['dias_afectados'])) {
                $dias_afectados = "";
            }
            else {
                $dias_afectados = "S";
            }

        }


        ?>

            <form name="formulario" id="formulario"
                  action="<?php echo site_url() . "/controller_comercio_adm_restricciones/alta_restriccion"  ?>"
                  method="POST">
                <input type="hidden" name="id_restriccion" value="<?php echo $id_restriccion  ?>">
                <input type="hidden" name="id_recurso" value="<?php echo $id_recurso  ?>">
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('lista_recurso_rec_dias_fecini') ?></th>
                        <th><?php echo lang('lista_recurso_rec_dias_fecfin') ?>  </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_horini') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_horfin') ?> </th>
                    </tr>
                    <TR>

                        <TD>
                            <div><p><input name="fecha_inicio" id="fechainicio" type="text"
                                           value="<?php echo $fecha_inicio ?>"/></p></div>
                        </TD>
                        <TD>
                            <div><p><input name="fecha_fin" id="fechafin" type="text" value="<?php echo $fecha_fin ?>"/>
                            </p></div>
                        </TD>
                        <TD><SELECT NAME="hora_inicio">
                            <?php foreach ($horas_seleccionadas as $indice => $valor) {
                            if ($indice == $hora_inicio) {
                                echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                            } else {
                                echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                            }

                        }
                            ?>
                        </SELECT></TD>
                        <TD><SELECT NAME="hora_fin">
                            <?php foreach ($horas_seleccionadas as $indice => $valor) {
                            if ($indice == $hora_fin) {
                                echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                            } else {
                                echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                            }

                        }

                            if ($lunes == 'S') {
                                $check_lunes = "<input type=\"checkbox\" name=\"lunes\" value=\"S\" checked>";
                            } else {
                                $check_lunes = "<input type=\"checkbox\" name=\"lunes\" value=\"S\">";
                            }

                            if ($martes == 'S') {
                                $check_martes = "<input type=\"checkbox\" name=\"martes\"  value=\"S\" checked>";
                            } else {
                                $check_martes = "<input type=\"checkbox\" name=\"martes\"  value=\"S\" >";
                            }
                            if ($miercoles == 'S') {
                                $check_miercoles = "<input type=\"checkbox\" name=\"miercoles\"  value=\"S\" checked>";
                            } else {
                                $check_miercoles = "<input type=\"checkbox\" name=\"miercoles\"  value=\"S\" >";
                            }
                            if ($jueves == 'S') {
                                $check_jueves = "<input type=\"checkbox\" name=\"jueves\"  value=\"S\" checked>";
                            } else {
                                $check_jueves = "<input type=\"checkbox\" name=\"jueves\"  value=\"S\" >";
                            }
                            if ($viernes == 'S') {
                                $check_viernes = "<input type=\"checkbox\" name=\"viernes\"  value=\"S\" checked>";
                            } else {
                                $check_viernes = "<input type=\"checkbox\" name=\"viernes\"  value=\"S\" >";
                            }
                            if ($sabado == 'S') {
                                $check_sabado = "<input type=\"checkbox\" name=\"sabado\"  value=\"S\" checked>";
                            } else {
                                $check_sabado = "<input type=\"checkbox\" name=\"sabado\" value=\"S\" >";
                            }
                            if ($domingo == 'S') {
                                $check_domingo = "<input type=\"checkbox\" name=\"domingo\"  value=\"S\" checked>";
                            } else {
                                $check_domingo = "<input type=\"checkbox\" name=\"domingo\"  value=\"S\" >";
                            }

                            ?>

                        </SELECT></TD>
                    </TR>
                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th> <?php echo lang('alta_restricciones_dias_apl') ?></tr>
                    </th>

                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th> <?php echo lang('lista_recurso_rec_lista_lun') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_mar') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_mie') ?> </th>
                        <th><?php echo lang('lista_recurso_rec_lista_jue') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_vie') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_sab') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_dom') ?> </th>
                    </tr>
                    <tr>
                        <td><?php echo $check_lunes ?></td>
                        <td><?php echo $check_martes ?></td>
                        <td><?php echo $check_miercoles ?></td>
                        <td><?php echo $check_jueves ?></td>
                        <td><?php echo $check_viernes ?></td>
                        <td><?php echo $check_sabado ?></td>
                        <td><?php echo $check_domingo ?></td>
                    </tr>
                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th> <?php echo lang('alta_restricciones_dias_lista_rec') ?> </th>
                    </tr>
                </table>
                <TABLE class="tabladatos">
                    <tr>
                        <th> <?php echo lang('alta_restricciones_dias_nombre') ?> </th>
                        <th> <?php echo lang('alta_restricciones_dias_estado') ?> </th>
                        <th><?php echo lang('alta_restricciones_dias_restriccion') ?></th>
                    </tr>
    <?php

                    foreach ($datos_recursos as $row) {
                        $chequeado = false;
                        echo "<TR>";
                        echo "<TD>" . $row->nombre . "</TD><TD>" . $row->estado . "</TD>";
                        if (count($restricciones) > 0) {

                            if (in_array($row->id_recurso, $restricciones)) {
                                echo "<td><input type=\"checkbox\" name=\"restricciones[]\" value=\"" . $row->id_recurso . "\" checked></td>";
                            } else {
                                echo "<td><input type=\"checkbox\" name=\"restricciones[]\" value=\"" . $row->id_recurso . "\"></td>";
                            }

                        } else {
                            echo "<td><input type=\"checkbox\" name=\"restricciones[]\" value=\"" . $row->id_recurso . "\"></td>";

                        }

                        echo "</TR>";


                    }
                    echo "</TABLE>";
                    ?>
                    <br>
                    <div class="btnWrap" align="center">
                        <a class="btnStyle" id="accionboton" href="#">Nueva restriccion</a>
                    </div>

            </form>


            <br>

            <p>


            </p>

        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
