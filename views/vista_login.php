<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>


    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });

</script>


<div id="container">

    <?php echo $header ?>

    <?php echo $capa_mensaje ?>

    <!-- //#sub-header -->

    <?php
            $username = null;
    $username_comercio = null;
    if (!isset($id_comercio)) {
        $id_comercio = null;
    }

    if (isset($datos_refresco_form)) {
        $username = $datos_refresco_form['username'];
        $username_comercio = $datos_refresco_form['username_comercio'];
        $id_comercio = $datos_refresco_form['id_comercio'];
    }



    ?>


    <div id="main-content">
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">

                <p>

                <form name="formulario" class="formredondo"
                      action="<?php echo site_url() . "/login/procesa_login_usuario" ?>" method="POST">

                    <table class="tabladatosoculta">
                        <thead><?php echo lang('login_alta_usuarios') ?></thead>
                        <tr>
                            <td><?php echo lang('login_alta_mail') ?></td>
                            <td><input type="text" name="username" id="username" class="formulario1"
                                       value="<?php echo $username  ?>"></td>
                        </tr>
                        <tr>
                            <td><?php echo lang('login_alta_password') ?></td>
                            <td><input type="password" name="password" id="password" class="formulario1" value=""></td>
                        </tr>

                        <tr>
                            <td><input type="submit" text="Enviar"></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="<? echo site_url() . "/controller_alta/ir_alta_usuario_reservas"  ?>"><?php echo lang('login_alta_nuevo_user') ?></a></td>
                        </tr>
                        <tr>
                            <td><a href="<? echo site_url() . "/controller_alta/previa_recuperar_password"  ?>"><?php echo lang('login_alta_pass_olvidado') ?></a></td>
                        </tr>
                    </table>
                    <input type="hidden" name="id_comercio" id="id_comercio" class="formulario1"
                           value="<?php echo $id_comercio; ?>">
                    <input type="hidden" name="username_comercio" value="">

                </form>

                </p>
            </div>
            <div class="article">
                <br><br><br><br>

                <h3></h3><br>

                <p>

                <form name="formulario2" class="formredondo"
                      action="<?php echo site_url() . "/login/procesa_login_comercio" ?>" method="POST">
                    <table class="tabladatosoculta">
                        <thead><?php echo lang('login_alta_comercios') ?></thead>
                        <tr>
                            <td><?php echo lang('login_alta_mail') ?></td>
                            <input type="hidden" name="username" value="">
                            <td><input type="text" name="username_comercio" id="username_comercio" class="formulario1"
                                       value="<?php echo $username_comercio  ?>"></td>
                        </tr>
                        <tr>
                            <td><?php echo lang('login_alta_password') ?></td>
                            <td><input type="password" name="password" id="passwordcomercio" class="formulario1"
                                       value=""></td>
                        </tr>
                        <tr>
                            <td><input type="submit" text="Enviar"></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="<? echo site_url() . "/controller_alta_comercio/ir_alta_comercio"  ?>"><?php echo lang('login_alta_nuevo_user') ?></a></td>
                        </tr>
                        <tr>
                            <td><a href="<? echo site_url() . "/controller_alta/previa_recuperar_password"  ?>"><?php echo lang('login_alta_pass_olvidado') ?></a></td>
                        </tr>
                    </table>
                    <input type="hidden" name="id_comercio" id="id_comercio2" class="formulario1"
                           value="<?php echo $id_comercio; ?>">
                </form>

                </p>


            </div>
        </div>

        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
