<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>



        $("fechainicio").attr('disabled', 'disabled');
        $("fechafin").attr('disabled', 'disabled');



    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>





        $(function() {
            $("#fechainicio").datepicker({ minDate: '-0d' });
            $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);
        });

        $(function() {
            $("#fechafin").datepicker({ minDate: '-0d' });
            $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);
        });


        // $('#importe_total').attr("readonly", "readonly");

        $("#iva").change(function () {
            calcular_importe_total();
        });
        $("#importe").change(function() {
                    //  $("#importe").number( "#importe", 1, ',', '' );
                    calcular_importe_total();
                }
        );
        $("#importe_total").change(function() {
                    //  $("#importe").number( "#importe", 1, ',', '' );
                    calcular_importe();
                }
        );

        //Como definir una función que puede ser utilizada en varios puntos por jquery
        var calcular_importe_total = function() {
            var importe = $("#importe").val();
            var iva = $("#iva").val();
            var importe_total = parseFloat(importe) + (parseFloat(importe) * parseFloat(iva));
            importe_total = importe_total.toFixed(2);
            $("#importe_total").val(importe_total);
        };
        var calcular_importe = function() {
            var importe_total = $("#importe_total").val();
            var iva = $("#iva").val();
            var importe = parseFloat(importe_total) - (parseFloat(importe_total) * parseFloat(iva));
            importe = importe.toFixed(2);
            $("#importe").val(importe);
        };

    });

</script>


<div id="container">


<?php echo $header ?>
<!-- //#sub-header -->
<?php echo $capa_mensaje ?>


<div id="main-content">
<div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('alta_tarifas_cabecera') ?></li>
      </div>

<h2></h2>

<!-- //.article -->
<div class="article-wrapper">

<div class="article">
<h3></h3>

<p>
    <?php

        $id_sertar = null;
        $nombre=null;
        $fecha_inicio = null;
        $fecha_fin = null;
        $hora_inicio = null;
        $hora_fin = null;
        $lunes = null;
        $martes = null;
        $miercoles = null;
        $jueves = null;
        $viernes = null;
        $sabado = null;
        $domingo = null;
        $dias_afectados =null;
        $importe=null;
        $importe_total=null;

        if (isset($datos_refresco_form)) {
            $id_sertar  = $datos_refresco_form['id_sertar'];
            $estado = $datos_refresco_form['estado'];
            $nombre = $datos_refresco_form['nombre'];
            $id_servicio = $datos_refresco_form['servicios'];
            $fecha_inicio = $datos_refresco_form['fecha_inicio'];
            $fecha_fin = $datos_refresco_form['fecha_fin'];
            $hora_inicio = $datos_refresco_form['hora_inicio'];
            $hora_fin = $datos_refresco_form['hora_fin'];
            $importe = $datos_refresco_form['importe'];
            $importe_total = $datos_refresco_form['importe_total'];
            $iva_seleccionado = $datos_refresco_form['iva'];
            $url_destino = $datos_refresco_form['url_destino'];


            if (!isset($datos_refresco_form['lunes'])) {
                $lunes = "";
            }
            else {
                $lunes = "S";
            }
            if (!isset($datos_refresco_form['martes'])) {
                $martes = "";
            }
            else {
                $martes = "S";
            }
            if (!isset($datos_refresco_form['miercoles'])) {
                $miercoless = "";
            }
            else {
                $miercoles = "S";
            }
            if (!isset($datos_refresco_form['jueves'])) {
                $jueves = "";
            }
            else {
                $jueves = "S";
            }
            if (!isset($datos_refresco_form['viernes'])) {
                $viernes = "";
            }
            else {
                $viernes = "S";
            }
            if (!isset($datos_refresco_form['sabado'])) {
                $sabado = "";
            }
            else {
                $sabado = "S";
            }
            if (!isset($datos_refresco_form['domingo'])) {
                $domingo = "";
            }
            else {
                $domingo = "S";
            }
            if (!isset($datos_refresco_form['dias_afectados'])) {
                $dias_afectados = "";
            }
            else {
                $dias_afectados = "S";
            }

        } else {


            foreach ($datos_tarifa as $row) {
                $id_sertar = $row->id_sertar;
                $estado = $row->estado;
                $nombre = $row->nombre_tarifa;
                $id_servicio = $row->id_servicio;
                $fecha_inicio = $row->fecha_inicio;
                $fecha_fin = $row->fecha_fin;
                $hora_inicio = $row->hora_inicio;
                $hora_fin = $row->hora_fin;
                $importe = $row->importe;
                $importe_total = $row->importe_total;
                $iva_seleccionado = $row->iva;
                $dias_afectados = $row->dias_afectados;
            }
            $dias = explode(',', $dias_afectados);

            foreach ($dias as $valor) {

                if ($valor == '0' || $lunes=='S') {$lunes = 'S';} else {
                    $lunes = '';
                }
                if ($valor == '1' || $martes=='S') {$martes = 'S';} else {
                    $martes = '';
                }
                if ($valor == '2' || $miercoles=='S') {$miercoles = 'S';} else {
                    $miercoles = '';
                }

                if ($valor == '3' || $jueves=='S') {$jueves = 'S';} else {
                    $jueves = '';
                }
                if ($valor == '4' || $viernes=='S') {$viernes = 'S';} else {
                    $viernes = '';
                }
                if ($valor == '5' || $sabado=='S') {$sabado = 'S';} else {
                    $sabado = '';
                }
                if ($valor == '6' || $domingo=='S') {$domingo = 'S';} else {
                    $domingo = '';
                }
            }
            $param3 = explode('-', $fecha_inicio);
            if (count($param3) <> 1) {
                $fecha_inicio = $param3[2] . "/" . $param3[1] . "/" . $param3[0];
            }

            $param3 = explode('-', $fecha_fin);
            if (count($param3) <> 1) {
                $fecha_fin = $param3[2] . "/" . $param3[1] . "/" . $param3[0];
            }

        }


        ?>

<form name="formulario" id="formulario"
      action="<?php echo site_url() . $url_destino  ?>"
      method="POST">
    <input type="hidden" name="id_sertar" value="<?php echo $id_sertar  ?>">
    <input type="hidden" name="url_destino" value="<?php echo $url_destino  ?>">

    <TABLE class="tabladatos">
        <tr>
            <th><?php echo lang('alta_tarifa_fecini') ?></th>
            <th><?php echo lang('alta_tarifa_fecfin') ?>  </th>
            <th> <?php echo lang('alta_tarifa_horaini') ?> </th>
            <th> <?php echo lang('alta_tarifa_horafin') ?> </th>
            <th> <?php echo lang('alta_tarifa_nombre') ?> </th>
            <th> <?php echo lang('alta_tarifa_estado') ?> </th>

        </tr>
        <TR>

            <TD>
                <div><p><input name="fecha_inicio" id="fechainicio" type="text"
                               value="<?php echo $fecha_inicio ?>"/></p></div>
            </TD>
            <TD>
                <div><p><input name="fecha_fin" id="fechafin" type="text" value="<?php echo $fecha_fin ?>"/>
                </p></div>
            </TD>
            <TD><SELECT NAME="hora_inicio">
                <?php foreach ($horas_seleccionadas as $indice => $valor) {
                if ($indice == $hora_inicio) {
                    echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                } else {
                    echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                }

            }
                ?>
            </SELECT></TD>
            <TD><SELECT NAME="hora_fin">
    <?php
                    foreach ($horas_seleccionadas as $indice => $valor) {
        if ($indice == $hora_fin) {
            echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
        } else {
            echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
        }

    }
        ?>
            </SELECT></TD>
            <td><input name="nombre" type="text" value="<?php echo $nombre ?>"/></td>
            <TD>
                <SELECT NAME="estado">
                    <?php foreach ($estados_tarifas as $indice => $valor) {
                    if ($indice == $estado) {
                        echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                    } else {
                        echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                    }

                }
                    ?>
                </SELECT></TD>
        </TR>
        <tr>
            <th><?php echo lang('alta_tarifa_servicio') ?></th>
            <th><?php echo lang('alta_tarifa_importe') ?>  </th>
            <th> <?php echo lang('alta_tarifa_iva') ?> </th>
            <th> <?php echo lang('alta_tarifa_importetotal') ?> </th>

        </tr>
        <TR>
            <TD>
                <SELECT NAME="servicios" id="servicios">
                    <?php foreach ($datos_servicios as $row) {
                    if ($row->id_servicio == $id_servicio) {
                        echo "<OPTION VALUE=" . $row->id_servicio . " selected>" . $row->nombre . "</OPTION>";
                    } else {
                        echo "<OPTION VALUE=" . $row->id_servicio . ">" . $row->nombre . "</OPTION>";
                    }


                }




                    ?>
                </SELECT></TD>
            <td><input name="importe" id="importe" type="text"
                       value="<?php echo $importe ?>"/></td>
            <td><SELECT NAME="iva" id="iva">
                <?php foreach ($tipos_iva as $indice => $valor) {
                if ($indice == $iva_seleccionado) {
                    echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                } else {
                    echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                }

            }
                ?>
            </SELECT></TD>
            <td><input name="importe_total" id="importe_total" type="text"
                       value="<?php echo $importe_total ?>"/></td>
        </TR>
    </TABLE>
    <?php
 if ($lunes == 'S') {
    $check_lunes = "<input type=\"checkbox\" name=\"lunes\" value=\"S\" checked>";
} else {
    $check_lunes = "<input type=\"checkbox\" name=\"lunes\" value=\"S\">";
}

    if ($martes == 'S') {
        $check_martes = "<input type=\"checkbox\" name=\"martes\"  value=\"S\" checked>";
    } else {
        $check_martes = "<input type=\"checkbox\" name=\"martes\"  value=\"S\" >";
    }
    if ($miercoles == 'S') {
        $check_miercoles = "<input type=\"checkbox\" name=\"miercoles\"  value=\"S\" checked>";
    } else {
        $check_miercoles = "<input type=\"checkbox\" name=\"miercoles\"  value=\"S\" >";
    }
    if ($jueves == 'S') {
        $check_jueves = "<input type=\"checkbox\" name=\"jueves\"  value=\"S\" checked>";
    } else {
        $check_jueves = "<input type=\"checkbox\" name=\"jueves\"  value=\"S\" >";
    }
    if ($viernes == 'S') {
        $check_viernes = "<input type=\"checkbox\" name=\"viernes\"  value=\"S\" checked>";
    } else {
        $check_viernes = "<input type=\"checkbox\" name=\"viernes\"  value=\"S\" >";
    }
    if ($sabado == 'S') {
        $check_sabado = "<input type=\"checkbox\" name=\"sabado\"  value=\"S\" checked>";
    } else {
        $check_sabado = "<input type=\"checkbox\" name=\"sabado\" value=\"S\" >";
    }
    if ($domingo == 'S') {
        $check_domingo = "<input type=\"checkbox\" name=\"domingo\"  value=\"S\" checked>";
    } else {
        $check_domingo = "<input type=\"checkbox\" name=\"domingo\"  value=\"S\" >";
    }

    ?>
    <TABLE class="tabladatos">
        <tr>
            <th> <?php echo lang('alta_tarifa_seleccion_dias') ?></tr>
        </th>

    </TABLE>
    <TABLE class="tabladatos">
        <tr>
            <th> <?php echo lang('alta_tarifa_lunes') ?> </th>
            <th> <?php echo lang('alta_tarifa_martes') ?> </th>
            <th> <?php echo lang('alta_tarifa_miercoles') ?> </th>
            <th><?php echo lang('alta_tarifa_jueves') ?> </th>
            <th> <?php echo lang('alta_tarifa_viernes') ?> </th>
            <th> <?php echo lang('alta_tarifa_sabado') ?> </th>
            <th> <?php echo lang('alta_tarifa_domingo') ?> </th>
        </tr>
        <tr>
            <td><?php echo $check_lunes ?></td>
            <td><?php echo $check_martes ?></td>
            <td><?php echo $check_miercoles ?></td>
            <td><?php echo $check_jueves ?></td>
            <td><?php echo $check_viernes ?></td>
            <td><?php echo $check_sabado ?></td>
            <td><?php echo $check_domingo ?></td>
        </tr>
    </TABLE>


    <br>

    <div class="btnWrap" align="center">
        <a class="btnStyle" id="accionboton" href="#"><?php echo lang('alta_tarifa_boton') ?></a>
    </div>

</form>


<br>

<p>


</p>

</div>
<!-- //.article -->
</div>
<!-- //#main-content -->


<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
