<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <?php echo $head ?>
<body>

 <?php echo $scripts_definition ?>

<script>

$(document).ready(function(){


<?php echo $gestion_seleccion_idioma ?>




<?php
  if (isset($validation_errors) || (isset($mensaje_ok))) {
    echo "$('#capamensaje').attr('style', 'visibility: visible');";
    echo "$('#capamensaje').hide();";
    echo "$('#capamensaje').slideDown('slow');";
    if (isset($validation_errors)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
    }
    if (isset($mensaje_ok)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
    }
  } else {
    $capa_mensaje="";
  }
?>

		
	
});
</script>



<div id="container">

  <?php echo $header ?>

  <!-- //#sub-header -->
   <?php echo $capa_mensaje ?>
  
  
  <div id="main-content">
      <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('admin_recursos_cabecera') ?></li>
      </div>
    <h2></h2>
    <BR>
    <!-- //.article -->
    <div class="article-wrapper">
      
    
    </div>
 <div class="article-wrapper">

      <div class="article">
       <BR>
          <TABLE class="tabladatos"><tr><th><?php echo lang('admin_recursos_nombre') ?></th><th> <?php echo lang('admin_recursos_descripcion') ?> </th>
              <th> <?php echo lang('admin_recursos_estado') ?> </th><th> <?php echo lang('admin_recursos_imagen') ?> </th></tr>

	 <? 	
	  
	  
	  foreach($datos_recursos as $row) {
	   if ($row->foto_recurso<>null){
	   $cadena_foto_recurso="<a target=\"_blank\" href=" . base_url() . APPPATH ."uploads/" .  $row->foto_recurso . " >Ver imagen</a>";
	   } else  {
	   $cadena_foto_recurso="Foto no disponible";
	   }
	   echo "<TR>";
	   echo "<TD><a href=\"" . site_url() ."/controller_comercio_adm_recursos/detalle_recursos/" . $row->id_recurso . "\">" . $row->nombre .  "</a></TD><TD>" . $row->descripcion . "</TD><TD>" . $row->estado . "</TD><TD>" . $cadena_foto_recurso . "</TD>";
	
	   echo "</TR>";
	  }
	  echo "</TABLE>";

	?><br><br> 


		<p><br />Page rendered in {elapsed_time} seconds</p>
   </div>
</div>		
    <!-- //.article -->
  </div>
  <!-- //#main-content -->


  <div id="footer">
     <?php echo $footer ?>
  </div>
  <!-- //#footer -->
  
</div>
<!-- //#container -->
</body>
</html>
