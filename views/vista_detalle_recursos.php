<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <?php echo $head ?>
<body>

 <?php echo $scripts_definition ?>

<script>

$(document).ready(function(){

<?php echo $gestion_seleccion_idioma ?>

  <?php
    if (isset($validation_errors) || (isset($mensaje_ok))) {
      echo "$('#capamensaje').attr('style', 'visibility: visible');";
      echo "$('#capamensaje').hide();";
      echo "$('#capamensaje').slideDown('slow');";
      if (isset($validation_errors)){
       $capa_mensaje="<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
      }
      if (isset($mensaje_ok)){
       $capa_mensaje="<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
      }
    } else {
      $capa_mensaje="";
    }
  ?>
/*
		$("#tipos_fragmentacion").change(function(){

        	$.post("<?php echo base_url() . "index.php/controller_comercio_adm_recursos/retorna_jquery_combo" ?>",{ tipos_fragmentacion:$(this).val() },
				function(data){

    
				// Fill sub category select 
                // Clear all options from sub category select 
                  $("select#hora_inicio option").remove(); 
                  $("select#hora_fin option").remove();
                   $.each(data, function(i,j){
                    var row = "<option value=\"" + i + "\">" + j + "</option>";
                    $(row).appendTo("select#hora_inicio");
                    $(row).appendTo("select#hora_fin");
                   }); 
                 },'json'
                )
		});

*/
		
});
		
		
</script>



<div id="container">


      <?php echo $header ?>
  <!-- //#sub-header -->
   <?php echo $capa_mensaje ?>
  
  
  <div id="main-content">
    <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('alta_recursos_cabecera') ?></li>
      </div>
    <h2></h2>
  
    <!-- //.article -->
    <div class="article-wrapper">
      
      <div class="article">
        <h3></h3>
        <p>
     <?php
	     $id_recurso=null;
         $nombre=null;
         $descripcion=null;
		 $estado=null;
         $hora_inicio=null;
         $hora_fin=null;
		 $fragmentacion_horas=null;
		 $mostrar_dias_avance=null;
		 $min_dias_anulacion=null;
		 $horas_seleccionadas=null;
		
		 
		 if (isset($datos_refresco_form)) {
		  //print_r($datos_refresco_form);
		  $id_recurso=$datos_refresco_form['id_recurso'];
		  $url_destino=$datos_refresco_form['url_destino'];
		  $nombre=$datos_refresco_form['nombre'];
		  $descripcion=$datos_refresco_form['descripcion'];
		  $estado=$datos_refresco_form['estado'];
		  $hora_inicio=$datos_refresco_form['hora_inicio'];
		  $hora_fin=$datos_refresco_form['hora_fin'];
		  $fragmentacion_horas=$datos_refresco_form['tipos_fragmentacion'];
	      $mostrar_dias_avance=$datos_refresco_form['mostrar_dias_avance'];
	 	  $min_dias_anulacion=$datos_refresco_form['min_dias_anulacion'];

		 } else {

			foreach($datos_recursos as $row){
				$id_recurso=$row->id_recurso;
				$nombre=$row->nombre;
				$descripcion=$row->descripcion;
				$estado=$row->estado;
			}
			foreach($datos_calendario as $row){  
				$hora_inicio=$row->hora_inicio;
				$hora_fin=$row->hora_fin;
				$fragmentacion_horas=$row->fragmentacion_horas;
				$mostrar_dias_avance=$row->mostrar_dias_avance;
				$min_dias_anulacion=$row->min_dias_anulacion;
			}
 
		 
		 }

        
         
       /*
        if ($fragmentacion_horas=='EN_PUNTO') {$horas_seleccionadas=$horas_en_punto;}
        if ($fragmentacion_horas=='MEDIAS')   {$horas_seleccionadas=$horas_medias;}
		if ($fragmentacion_horas=='CUARTOS')  {$horas_seleccionadas=$horas_cuartos;}
        if ($fragmentacion_horas==null)       {$horas_seleccionadas=$horas_en_punto;}
       */
      $horas_seleccionadas=$horas_cuartos;
	  ?>	
	  
	<form name="formulario" id="formulario" action="<?php echo site_url() . $url_destino; ?>"
	 enctype="multipart/form-data" method="POST">
      <BR>
	 <TABLE class="tabladatos"><tr><th><?php echo lang('alta_recursos_nombre') ?></th><th><?php echo lang('alta_recursos_descripcion') ?> </th><th> <?php echo lang('alta_recursos_estado') ?> </th></tr>
      <TR>
	    <input type="hidden" name="id_recurso" value="<?php echo $id_recurso  ?>">
		<input type="hidden" name="url_destino" value="<?php echo $url_destino  ?>">
		
	   <TD><input type="text" name="nombre"  value="<?php echo $nombre ?>"></TD>
	   <TD><input type="text" name="descripcion" value="<?php echo $descripcion ?>"></TD>
	   <TD>
	   <SELECT NAME="estado"> 
	   <?php foreach ($estados_recursos as $indice=>$valor){
	     if ($indice==$estado){
		  echo "<OPTION VALUE=" . $indice . " selected>" . $valor .  "</OPTION>";
		 } else {
		  echo "<OPTION VALUE=" . $indice . ">" . $valor .  "</OPTION>";
		 }
	   
	   }
	   ?>
	   </SELECT></TD>
	   </TR>
     </TABLE>
     
	  
	  		
	  <TABLE class="tabladatos"><tr><th><?php echo lang('alta_recursos_bloques_horarios') ?></th><th> <?php echo lang('alta_recursos_hora_inicio') ?> </th>
          <th> <?php echo lang('alta_recursos_hora_fin') ?>   </th>
          <th><?php echo lang('alta_recursos_dias_avance') ?> </th><th> <?php echo lang('alta_recursos_minimo_dias') ?> </th></tr>
      <TR>
       <TD><SELECT NAME="tipos_fragmentacion" id="tipos_fragmentacion">
	   <?php foreach ($tipos_fragmentacion as $indice=>$valor){
	     if ($indice==$fragmentacion_horas){
		  echo "<OPTION VALUE=" . $indice . " selected>" . $valor .  "</OPTION>";
		 } else {
		  echo "<OPTION VALUE=" . $indice . ">" . $valor .  "</OPTION>";
		 }

	   }
	   ?>
	   </SELECT></TD>
	   <TD><SELECT NAME="hora_inicio" id="hora_inicio"> 
	   <?php foreach ($horas_seleccionadas as $indice=>$valor){
	     if ($indice==$hora_inicio){
		  echo "<OPTION VALUE=" . $indice . " selected>" . $valor .  "</OPTION>";
		 } else {
		  echo "<OPTION VALUE=" . $indice . ">" . $valor .  "</OPTION>";
		 }
	   
	   }
	   ?>
	   </SELECT></TD>
	   <TD><SELECT NAME="hora_fin" id="hora_fin"> 
	   <?php foreach ($horas_seleccionadas as $indice=>$valor){
	     if ($indice==$hora_fin){
		  echo "<OPTION VALUE=" . $indice . " selected>" . $valor .  "</OPTION>";
		 } else {
		  echo "<OPTION VALUE=" . $indice . ">" . $valor .  "</OPTION>";
		 }
	   
	   }
	   ?></TD>

	   <TD><input type="text" name="mostrar_dias_avance" value="<?php echo $mostrar_dias_avance ?>"></TD>
	   <TD><input type="text" name="min_dias_anulacion" value="<?php echo $min_dias_anulacion ?>"></TD>
	   
	   </TR>
     </TABLE>
	 <table class="tabladatos">
       <th><?php echo lang('alta_recursos_foto') ?></th>
       <tr>
        <td><input type="file" name="userfile"></td>
       </tr>
	 </table>
     <br>
     <div class="btnWrap" align="center">
                        <a class="btnStyle" id="accionboton" href="#">Alta/Modificar recurso</a>
                    </div>
    
	</form>   
	
	
	
 <br>
 <p>
   

	
	
</p>

	</div>	
    <!-- //.article -->
  </div>
  <!-- //#main-content -->


  <div id="footer">
     <?php echo $footer ?>
  </div>
  <!-- //#footer -->
  
</div>
<!-- //#container -->
</body>
</html>
