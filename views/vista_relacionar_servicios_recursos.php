<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });
    })

</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>

    <?php

    foreach ($datos_servicio as $row) {
        $nombre_servicio = $row->nombre;
    }

    ?>
    <div id="main-content">
        <h2>Relacionar servicios</h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">


        </div>
        <div class="article-wrapper">

            <div class="article">
                <form name="formulario" id="formulario"
                      action="<?php echo site_url();?>/controller_comercio_adm_servicios/actualizar_relacion_servicios"
                      method="POST">
                    <input type="hidden" name="servicios" value="<?php echo $id_servicio; ?>"/>
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('relacionar_servicios_rec_escoge') ?></th>
                            <th><?php echo $nombre_servicio ?></th>
                        </tr>
                    </TABLE>
                    <BR>
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('relacionar_servicios_rec_nombre') ?></th>
                            <th><?php echo lang('relacionar_servicios_rec_estado') ?></th>
                            <th><?php echo lang('relacionar_servicios_rec_relacion') ?></th>
                        </tr>

    <?php
             $i = 1;

                        foreach ($datos_recursos as $row) {

                            if (count($servicios_relacionados[$i]) == 1) {
                                if ($servicios_relacionados[$i][0]->estado == 'A') {
                                    $checkbox = "<input type=\"checkbox\" name=\"relacion[]\" value=\"" . $row->id_recurso . "-" . $servicios_relacionados[$i][0]->id_relacion . "\" checked>";
                                } elseif ($servicios_relacionados[$i][0]->estado == 'B') {
                                    $checkbox = "<input type=\"checkbox\" name=\"relacion[]\" value=\"" . $row->id_recurso . "-" . $servicios_relacionados[$i][0]->id_relacion . "\">";
                                }

                            } else {
                                $checkbox = "<input type=\"checkbox\" name=\"relacion[]\" value=\"" . $row->id_recurso . "-\">";
                            }


                            echo "<TR>";
                            echo "<TD>" . $row->nombre . "</TD><TD>" . $row->estado . "</TD><TD>" . $checkbox . "</TD>";
                            echo "</TR>";
                            $i++;
                        }
                        echo "</TABLE>";

                        ?>

                        <br>

                        <div class="btnWrap" align="center">
                            <a class="btnStyle" id="accionboton" href="#">Relacionar a los recursos escogidos</a>
                        </div>
                </form>


                </table>
                <p><br/>Page rendered in {elapsed_time} seconds</p>
            </div>
        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
