<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

        $("#formulariocontacto").submit(function(event) {

            /* stop form from submitting normally */
            event.preventDefault();

            /* get some values from elements on the page: */
            var $form = $(this),term = $form.find('input[name="s"]').val(),url = $form.attr('action');

            /* Send the data using post */
            var posting = $.post(url, { name: $("#name").val() ,email: $("#email").val(),phone: $("#phone").val(),message: $("#message").val()});

            /* Put the results in a div */
            posting.done(function(data) {
                alert('<?php echo lang('home_contacto_enviado') ?>');
            });
        });


    });

</script>


<div id="container">


    <?php echo $header ?>




    <div id="main-content">


        <div class="res_banner">
            <div class="res_copy">
                <h1><?php echo lang('home_masaje_cabecera') ?></h1>

                <div class="keypoints">
                    <div class="follwsteps">
                        <img src="<?php echo base_url() . APPPATH ?>estaticos/stylesheets/images/Banner-Image11masaje.png"/>

                        <p><?php echo lang('home_masaje_paso1') ?></p>
                    </div>
                    <div class="arrow">
                        <img src="<?php echo base_url() . APPPATH ?>estaticos/stylesheets/images/Arrow.png"/></div>
                    <div class="follwsteps">
                        <img src="<?php echo base_url() . APPPATH ?>estaticos/stylesheets/images/Banner-Image22masaje.png"/>

                        <p><?php echo lang('home_masaje_paso2') ?></p>
                    </div>
                    <div class="arrow">
                        <img src="<?php echo base_url() . APPPATH ?>estaticos/stylesheets/images/Arrow.png"/></div>
                    <div class="follwsteps">
                        <img src="<?php echo base_url() . APPPATH ?>estaticos/stylesheets/images/Banner-Image33.png"/>

                        <p><?php echo lang('home_masaje_paso3') ?></p>
                    </div>
                    <div class="arrow">
                        <img src="<?php echo base_url() . APPPATH ?>estaticos/stylesheets/images/Arrow.png"/></div>
                    <div class="follwsteps">
                        <img src="<?php echo base_url() . APPPATH ?>estaticos/stylesheets/images/Banner-Image44.png"/>

                        <p><?php echo lang('home_masaje_paso4') ?></p>
                    </div>

                </div>
                <p><?php echo lang('home_masaje_mensaje') ?></p>
                <ul>
                    <li><?php echo lang('home_masaje_caracteristica2') ?></li>
                    <li><?php echo lang('home_masaje_caracteristica3') ?></li>
                    <li><?php echo lang('home_masaje_caracteristica4') ?></li>
                    <li><?php echo lang('home_masaje_caracteristica5') ?></li>
                </ul>

            </div>

            <br><br>

        </div>
        <!-- //#main-content -->
        <div class="btnWrap" align="center">
            <a href="<?php echo site_url() . "/controller_alta_comercio/ir_alta_comercio" ?>"
               class="btnStyle"><?php echo lang('home_masaje_boton1') ?></a>
            <a href="<?php echo site_url() . "/controller_home/masajes#ver_video" ?>"
               class="btnStyle"><?php echo lang('home_masaje_boton2') ?></a>
            <a href="<?php echo site_url() . "/controller_ini/reservar" ?>"
               class="btnStyle"><?php echo lang('home_masaje_boton3') ?></a>
        </div>


        <div id="mensaje" class="res-banner2">
            <h1><?php echo lang('home_masaje_banner2_h1') ?></h1>

            <h2><?php echo lang('home_masaje_banner2_h2') ?></h2>
        </div>

        <div class="middleMainwrap">
            <div class="restats">
                <h3><?php echo lang('home_estadisticas_cabecera') ?></h3>

                <div class="resDetails">
                    <ul class="resDetails">
                        <li><span class="firstXt"><?php echo lang('home_estadisticas_miles') ?></span><span
                                class="lastXt"><?php echo lang('home_estadisticas_est1') ?></span></li>
                        <li><span class="firstXt"><?php echo lang('home_estadisticas_miles') ?></span><span
                                class="lastXt"> <?php echo lang('home_estadisticas_est2') ?></span></li>
                        <li><span class="firstXt"><?php echo lang('home_estadisticas_miles') ?></span><span
                                class="lastXt"> <?php echo lang('home_estadisticas_est3') ?></span></li>
                    </ul>


                    <h3><a name="ver_video"></a> <?php echo lang('home_video_h3') ?></h3>
                    <ul class="resDetails">
                        <li><?php echo lang('home_video_desc') ?></li>
                    </ul>
                    <iframe width="400" height="315" src="<?php echo lang('home_videos_presentacion_enlace') ?>"
                            frameborder="0" allowfullscreen></iframe>
                </div>


            </div>

            <div class="resForm" id="formulariocontacto"
                 action="<?php echo site_url() ?>/controller_home/alta_contacto">
                <h2><a name="contactar"></a><?php echo lang('home_formulario_cabecera') ?></h2><br>

                <form action="<?php echo site_url() ?>/controller_home/alta_contacto" method="POST">
                    <label class="name">
                        <?php echo lang('home_estadisticas_nombre') ?>
                        <input type="text" name="name" value="" id="name">
                    </label>
                    <label class="email">
                        <?php echo lang('home_estadisticas_email') ?>
                        <input type="text" name="email" value="" id="email">
                    </label>
                    <label class="phone">
                        <?php echo lang('home_estadisticas_telf') ?>
                        <input type="text" name="phone" value="" id="phone">
                    </label>
                    <label class="message">
                        <?php echo lang('home_estadisticas_mensaje') ?>
                        <textarea name="message" id="message"></textarea>
                    </label>

                    <input type="submit" name="Buscar">
                </form>
            </div>

        </div>

        <div class="socialbox">
            <ul class="social-icon">
                <li class="facebook"><a href="#" target="_blank">facebook</a></li>
                <li class="twt"><a href="https://twitter.com/Reservum" target="_blank">Twitter</a></li>
                <li class="gplus"><a href="#" target="_blank">Google +</a></li>
                <li class="utube"><a href="http://www.youtube.com/user/Reservum/videos" target="_blank">YouTube</a></li>
            </ul>
        </div>
        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</div>
</body>
</html>
