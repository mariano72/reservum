<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {



    <?php echo $gestion_seleccion_idioma ?>

    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>




    });

</script>


<div id="container">


    <?php echo $header ?>

    <!-- //#sub-header -->

    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('consultar_reserva_cabecera') ?></li>
        </div>

        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

      
                    <?
                    foreach ($datos_reserva as $row) {
                        $fecha_reserva = $row->fecha_reserva;
                        $nombre_recurso = $row->nombre_recurso;
                        $nombre_servicio = $row->nombre_servicio;
                        $flag_usuario_reserva = $row->flag_usuario_reserva;
                        $nombre_reserva = $row->nombre_reserva;
                        $email_reserva = $row->email_reserva;
                        $telefono_reserva = $row->telefono_reserva;
                        $nombre = $row->nombre;
                        $email = $row->email;
                        $telefono = $row->telefono;

                        $hora_inicio = $row->hora_inicio;
                        $hora_fin = $row->hora_fin;
                        $duracion_minutos = $row->duracion;
                        $nombre_tarifa = $row->nombre_tarifa;
                        $importe_total = $row->importe_total;

                    }


                    ?>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('consultar_reserva_datos_generales') ?></th>
                    </tr>
                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('consultar_reserva_recurso_nombre') ?></th>
                        <th><?php echo lang('consultar_reserva_servicio_nombre') ?></th>
                        <th><?php echo lang('consultar_reserva_recurso_fecha_reserva') ?></th>
                        <th><?php echo lang('consultar_reserva_datos_hora_inicio') ?></th>
                        <th><?php echo lang('consultar_reserva_datos_hora_fin') ?></th>
                        <th><?php echo lang('consultar_reserva_duracion') ?></th>
                        <th><?php echo lang('consultar_reserva_materiales_tarifa') ?></th>
                        <th><?php echo lang('consultar_reserva_importe') ?></th>
                    </tr>

                    <TR>
                        <TD><?php echo $nombre_recurso ?></a></TD>
                        <TD><?php echo $nombre_servicio ?></TD>
                        <TD><?php echo $fecha_reserva ?></td>
                        <TD><?php echo $hora_inicio ?></a></TD>
                        <TD><?php echo $hora_fin ?></TD>
                        <TD><?php echo $duracion_minutos ?></TD>
                        <TD><?php echo $nombre_tarifa ?></TD>
                        <TD><?php  echo $importe_total ?></TD>

                    </TR>
                </TABLE>



                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('consultar_reserva_datos_personales') ?></th>
                    </tr>
                </TABLE>
                <table class="tabladatos">
                    <tr>
                        <th><?php echo lang('consultar_reserva_nombre_usuario') ?></th>
                        <th><?php echo lang('consultar_reserva_email_usuario') ?></th>
                        <th><?php echo lang('consultar_reserva_servicio_telefono_usuario') ?></th>
                    </tr>
                    <tr>

                        <td>
                            <? echo $nombre_reserva ?>
                        </td>
                        <td><? echo $email_reserva ?></td>
                        <td> <? echo $telefono_reserva ?></td>
                    </tr>

                </table>
                 <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('consultar_reserva_materiales_descripcion') ?></th>
                    </tr>
                </TABLE>
                <table class="tabladatos">
                    <tr>
                        <th><?php echo lang('consultar_reserva_materiales_importe') ?></th>
                        <th><?php echo lang('consultar_reserva_materiales_tarifa') ?></th>
                    </tr>

    <?php

                    foreach ($datos_materiales as $value) {
                        echo "<TR><TD>" . $value->descripcion . "</TD><TD>" . $value->importe . "</TD></TR>";
                    }

                    ?>
                   
                </table>

                </form>
                <br>

          
            </div>
            <!-- //.article -->
        </div>
        <!-- //#main-content -->


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
