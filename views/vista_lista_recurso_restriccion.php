<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {


    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


    });
</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('lista_recurso_rec_cabecera') ?></li>
        </div>
        <h2></h2>
       
        <!-- //.article -->
        <div class="article-wrapper">


        </div>
        <div class="article-wrapper">

            <div class="article">
                <BR>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('lista_recurso_rec_lista') ?></th>
                    </tr>
                </table>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('lista_recurso_rec_nombre') ?></th>
                        <th><?php echo lang('lista_recurso_rec_estado') ?></th>
                    </tr>
                    <?php

                    foreach ($datos_recursos as $row) {
                        echo "<TR>";
                        echo "<TD><a href=\"" . site_url() . "/controller_comercio_adm_restricciones/administrar_restricciones/" . $row->id_recurso . "\">" . $row->nombre . "</a></TD><TD>" . $row->estado . "</TD>";

                        echo "</TR>";
                    }
                    ?>
                </TABLE>
                <BR><BR>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('lista_recurso_rec_lista_restr') ?></th>
                    </tr>
                </table>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('lista_recurso_rec_lista_numero') ?> </th>
                        <th><?php echo lang('lista_recurso_rec_lista_fecini') ?></th>
                        <th><?php echo lang('lista_recurso_rec_lista_fecfin') ?></th>
                        <th><?php echo lang('lista_recurso_rec_lista_horini') ?></th>
                        <th> <?php echo lang('lista_recurso_rec_lista_horfin') ?></th>
                        <th> <?php echo lang('lista_recurso_rec_lista_lun') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_mar') ?></th>
                        <th><?php echo lang('lista_recurso_rec_lista_mie') ?></th>
                        <th> <?php echo lang('lista_recurso_rec_lista_jue') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_vie') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_sab') ?> </th>
                        <th> <?php echo lang('lista_recurso_rec_lista_dom') ?> </th>
                    </tr>

                    <?php
                    $contador = 1;
                    foreach ($datos_restricciones as $row) {

                        echo "<TR>";
                        echo "<TD><a href=\"" . site_url() . "/controller_comercio_adm_restricciones/previa_modificar_restriccion/" . $row->id_restriccion . "\">" . $contador . "</a></TD><TD>" . $row->fecha_inicio . "</TD><TD>" . $row->fecha_fin . "</TD><TD>" . $row->hora_inicio . "</TD>" .
                             "<TD>" . $row->hora_fin . "</TD><TD>" . $row->lunes . "</TD><TD>" . $row->martes . "</TD><TD>" . $row->miercoles . "</TD>" .
                             "<TD>" . $row->jueves . "</TD><TD>" . $row->viernes . "</TD><TD>" . $row->sabado . "</TD><TD>" . $row->domingo . "</TD>";

                        echo "</TR>";
                        $contador++;
                    }
                    echo "</TABLE>";


                    ?><br><br>


                    <p><br/>Page rendered in {elapsed_time} seconds</p>
            </div>
        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
