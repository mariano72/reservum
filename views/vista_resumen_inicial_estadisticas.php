<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>


<body>
<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });

</script>
<div id="container">


    <?php echo $header ?>

    <!-- //#sub-header -->

    <?php echo $capa_mensaje ?>


    <div id="main-content">

          <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('resumen_inicial_estadisticas') ?></li>
      </div>
        <h2></h2>
        <br>

        <div class="article-wrapper">

            <div class="article">
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('resumen_inicial_reservas') ?></th>
                    </tr>
                </table>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('resumen_inicial_total_reservas') ?></th>
                        <th><?php echo lang('resumen_inicial_total_reservas_anyo') ?></th>
                        <th><?php echo lang('resumen_inicial_total_reservas_mes') ?></th>
                        <th><?php echo lang('resumen_inicial_total_reservas_semana') ?></th>


                    </tr>
                    <tr>
                        <td><?php echo $total_reservas ?></td>
                        <td><?php echo $total_reservas_anyo ?></td>
                        <td><?php echo $total_reservas_mes ?></td>
                        <td><?php echo $total_reservas_semana ?></td>
                    </tr>
                </table>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('resumen_inicial_servicios') ?></th>
                    </tr>
                </table>
            </div>
        </div>
        <!-- //.article -->


        <!-- //#main-content -->

    </div>

    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->

</body>
</html>
