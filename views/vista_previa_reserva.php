<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    calcular_importe_total();

    <?php echo $gestion_seleccion_idioma ?>

    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

        $(".materiales").change(function () {
            calcular_importe_total();
        });
        
        $("#accionboton2").click(function() {
         $("#importe_total_materiales").val($("#importetotal").text());
         //alert($("#importe_total_materiales").val());
         //alert($("#importe_total").val());
         $('#formulario').submit();
          return false;
        });


          //Como definir una función que puede ser utilizada en varios puntos por jquery
          function calcular_importe_total() {
           var importe_total_materiales=parseFloat($("#importe_total").val());
           $('.materiales').each(function() {
            if (this.checked) {
              var check=$(this).val();
              //check= check.indexOf("-");
              check=check.substr(check.indexOf("-")+1,check.length);
             importe_total_materiales=importe_total_materiales + parseFloat(check);

            }

           });
            
           $("#importetotal").text(importe_total_materiales);

        };


        

    });

</script>


<div id="container">


<?php echo $header ?>

<!-- //#sub-header -->

<?php echo $capa_mensaje ?>


<div id="main-content">
<div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('previa_reserva_cabecera') ?></li>
        </div>
<h2></h2>

<!-- //.article -->
<div class="article-wrapper">

<div class="article">
<h3></h3>

<p>
    <?
    $nombre_cliente = null;
    $email = null;
    $telefono_cliente = null;
    $id_sertar = null;
    $nombre_tarifa = null;
    $check_materiales = null;
    $importe_total_materiales =null;

    if (isset($datos_refresco_form)) {
        $id_comercio = $datos_refresco_form['id_comercio'];
        $nombre_comercio = $datos_refresco_form['nombre_comercio'];
        $descripcion_corta = $datos_refresco_form['descripcion_corta'];

        $id_direccion = $datos_refresco_form['id_direccion'];
        $provincia = $datos_refresco_form['provincia'];
        $municipio = $datos_refresco_form['municipio'];
        $nombre_calle = $datos_refresco_form['nombre_calle'];
        $numero = $datos_refresco_form['numero'];
        $codigo_postal = $datos_refresco_form['codigo_postal'];
        $telefono_comercio = $datos_refresco_form['telefono_comercio'];

        $id_recurso = $datos_refresco_form['id_recurso'];
        $nombre_recurso = $datos_refresco_form['nombre_recurso'];
        $descripcion_recurso = $datos_refresco_form['descripcion_recurso'];

        $id_servicio = $datos_refresco_form['id_servicio'];
        $nombre_servicio = $datos_refresco_form['nombre_servicio'];
        $descripcion_servicio = $datos_refresco_form['descripcion_servicio'];
        $importe_total = $datos_refresco_form['importe_total'];

        $fechareserva = $datos_refresco_form['fechareserva'];
        $hora_inicio = $datos_refresco_form['hora_inicio'];
        $hora_fin = $datos_refresco_form['hora_fin'];
        $duracion_minutos = $datos_refresco_form['duracion_minutos'];
        $id_reserva = $datos_refresco_form['id_reserva'];

        $nombre_cliente = $datos_refresco_form['nombre_cliente'];
        $email = $datos_refresco_form['email'];
        $telefono_cliente = $datos_refresco_form['telefono_cliente'];

        $id_sertar = $datos_refresco_form['id_sertar'];
        $nombre_tarifa = $datos_refresco_form['nombre_tarifa'];

        //MATERIALES
        if (isset($datos_refresco_form['materiales'])) {
            $check_materiales = $datos_refresco_form['materiales'];
        }
        $importe_total_materiales = $datos_refresco_form['importe_total_materiales'];

    } else {
        $id_recurso = $this->uri->segment(4);
        $id_servicio = $this->uri->segment(5);

        foreach ($datos_comercio as $row) {
            $id_comercio = $row->id_comercio;
            $nombre_comercio = $row->nombre;
            $descripcion_corta = $row->descripcion_corta;

        }

        foreach ($datos_direccion as $row) {
            $id_direccion = $row->id_direccion;
            $provincia = $row->provincia;
            $municipio = $row->municipio;
            $nombre_calle = $row->nombre_calle;
            $numero = $row->numero;
            $codigo_postal = $row->codigo_postal;
            $telefono_comercio = $row->telefono;

        }

        foreach ($datos_recurso as $row) {
            $nombre_recurso = $row->nombre;
            $descripcion_recurso = $row->descripcion;
        }

        foreach ($datos_servicio as $row) {
            $id_servicio = $row->id_servicio;
            $nombre_servicio = $row->nombre;
            $descripcion_servicio = $row->descripcion;
            $importe_total = $row->importe_total;
        }

        foreach ($datos_tarifa as $row) {
            $id_sertar = $row->id_sertar;
            $nombre_tarifa = $row->nombre_tarifa;
            $importe_total = $row->importe_total;

        }

        //Recuperamos los datos del objeto de la sesión de la reserva
        $fechareserva = $datos_reserva['fechareserva'];
        $hora_inicio = $datos_reserva['hora_inicio'];
        $hora_fin = $datos_reserva['hora_fin'];
        $duracion_minutos = $datos_reserva['duracion_minutos'];
    }


    echo "<TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_resumen') . "</th></tr></TABLE>";
    echo "<TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_nombre') . "</th><th>" . lang('previa_reserva_descripcion') . "</th>
	    <th>" . lang('previa_reserva_direccion') . "</th><th>" . lang('previa_reserva_telefono') . "</th><th>" . lang('previa_reserva_mapa') . "</th></tr>";

    echo "<TR>";
    echo "<TD>" . $nombre_comercio . "</a></TD><TD>" . $descripcion_corta . "</TD><TD>" . $nombre_calle . " " . $numero . " " . $codigo_postal .
         "</TD><TD>" . $telefono_comercio . "</TD><TD>Ver Mapa</TD>";

    echo "</TR>";

    echo "</TABLE>";

    echo "<BR><TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_datos_recurso') . "</th></tr></TABLE>";
    echo "<TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_recurso_nombre') . "</th><th>" . lang('previa_reserva_recurso_descripcion') . "</th></tr>";

    echo "<TR>";
    echo "<TD>" . $nombre_recurso . "</a></TD><TD>" . $descripcion_recurso . "</TD>";

    echo "</TR>";

    echo "</TABLE>";

    echo "<BR><TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_datos_servicio') . "</th></tr></TABLE>";
    echo "<TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_servicio_nombre') . "</th><th>" . lang('previa_reserva_servicio_descripcion') . "</th></tr>";
    echo "<TR>";
    echo "<TD>" . $nombre_servicio . "</a></TD><TD>" . $descripcion_servicio . "</TD>";

    echo "</TR>";

    echo "</TABLE>";
    echo "<BR><TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_recurso_reserva') . "</th></tr></TABLE>";
    echo "<TABLE class=\"tabladatos\"><tr><th>" . lang('previa_reserva_recurso_fecha_reserva') . "</th><th>" . lang('previa_reserva_datos_hora_inicio') . "</th>
	         <th>" . lang('previa_reserva_servicio_hora_fin') . "</th><th>" . lang('previa_reserva_servicio_duracion') . "</th>
	         <th>TARIFA</th><th>IMPORTE TOTAL</th></tr>";


    echo "<TR>";
    echo "<TD>" . $fechareserva . "</td><TD>" . $hora_inicio . "</a></TD><TD>" . $hora_fin . "</TD><TD>" . $duracion_minutos . "</TD><TD>" .
         $nombre_tarifa . "</TD><TD id=\"importetotal\">" . $importe_total . "</TD>";

    echo "</TR>";

    echo "</TABLE>";

    ?>
</p>


<br>

<p>
    <?php echo lang('previa_reserva_servicio_confirmar') ?>

<form name="formulario" id="formulario" action="<? echo site_url();?>/controller_comun/realiza_reserva"
      method="POST">

    <input type="hidden" name="id_comercio" value="<? echo $id_comercio ?>">
    <input type="hidden" name="nombre_comercio" value="<? echo $nombre_comercio ?>">
    <input type="hidden" name="descripcion_corta" value="<? echo $descripcion_corta ?>">
    <input type="hidden" name="id_direccion" value="<? echo $id_direccion  ?>">
    <input type="hidden" name="provincia" value="<? echo $provincia  ?>">
    <input type="hidden" name="municipio" value="<? echo $municipio  ?>">
    <input type="hidden" name="nombre_calle" value="<? echo $nombre_calle  ?>">
    <input type="hidden" name="numero" value="<? echo $numero ?>">
    <input type="hidden" name="codigo_postal" value="<? echo $codigo_postal ?>">
    <input type="hidden" name="telefono_comercio" value="<? echo $telefono_comercio ?>">
    <input type="hidden" name="id_recurso" value="<? echo $id_recurso ?>">
    <input type="hidden" name="nombre_recurso" value="<? echo $nombre_recurso ?>">
    <input type="hidden" name="descripcion_recurso" value="<? echo $descripcion_recurso ?>">
    <input type="hidden" name="id_servicio" value="<? echo  $id_servicio ?>">
    <input type="hidden" name="nombre_servicio" value="<? echo $nombre_servicio ?>">
    <input type="hidden" name="descripcion_servicio" value="<? echo $descripcion_servicio ?>">
    <input type="hidden" name="hora_inicio" value="<? echo  $hora_inicio  ?>">
    <input type="hidden" name="fechareserva" value="<? echo $fechareserva ?>">
    <input type="hidden" name="duracion_minutos" value="<? echo $duracion_minutos ?>">
    <input type="hidden" name="hora_fin" value="<? echo $hora_fin ?>">
    <input type="hidden" name="id_reserva" value="<? echo $id_reserva ?>">
    <input type="hidden" name="id_sertar" value="<? echo $id_sertar ?>">
    <input type="hidden" name="nombre_tarifa" value="<? echo $nombre_tarifa ?>">
    <input type="hidden" id="importe_total" name="importe_total" value="<? echo $importe_total ?>">
    <input type="hidden" id="importe_total_materiales" name="importe_total_materiales" value="<? echo $importe_total_materiales ?>">

    <? if ($this->session->userdata('soycomercio')) { ?>
    <table class="tabladatos">
        <tr>
            <th><?php echo lang('previa_reserva_servicio_comercio_nombre') ?></th>
            <th><?php echo lang('previa_reserva_servicio_comercio_email') ?></th>
            <th><?php echo lang('previa_reserva_servicio_comercio_telefono') ?></th>
        </tr>
        <tr>

            <td><input type="text" name="nombre_cliente" class="formulario1" value="<? echo $nombre_cliente ?>">
            <td><input type="text" name="email" class="formulario1" value="<? echo $email ?>"></td>
            <td><input type="text" name="telefono_cliente" class="formulario1"
                       value="<? echo $telefono_cliente ?>"></td>
        </tr>

    </table>
    <? } ?>
    <table class="tabladatos">
        <tr>
            <th><?php echo lang('previa_reserva_materiales_nombre') ?></th>
            <th><?php echo lang('previa_reserva_materiales_importe') ?></th>
            <th><?php echo lang('previa_reserva_materiales_seleccion') ?></th>
        </tr>

    <?php

        foreach ($datos_materiales as $value) {
            $valor_check=$value->id_material . "-" . $value->importe;
            if (count($check_materiales)>0 && (in_array($valor_check, $check_materiales))){

                $checkbox = "<input type=\"checkbox\" class=\"materiales\" name=\"materiales[]\" value=\"" . $value->id_material . "-" .  $value->importe  . "\" checked>";
            } else {
                $checkbox = "<input type=\"checkbox\" class=\"materiales\" name=\"materiales[]\" value=\"" . $value->id_material . "-" . $value->importe  . "\">";
            }
            echo "<TR><TD>" . $value->descripcion . "</TD><TD>" . $value->importe . "</TD><TD>" . $checkbox . "</TD></TR>";
        }


        ?>
        <tr>
            <td><?php echo lang('previa_reserva_servicio_confirmar_3min') ?></td>
            <TD></TD>
            <TD></TD>
        </tr>
    </table>

</form>
<br>

<div class="btnWrap" align="center">
    <a class="btnStyle" id="accionboton2" href="#"><?php echo lang('previa_reserva_boton') ?></a>
</div>


</div>
<!-- //.article -->
</div>
<!-- //#main-content -->


<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
