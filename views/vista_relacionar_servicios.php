<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });
    })

</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->

    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('relacionar_servicios_cabecera') ?></li>
      </div>
        <h2></h2>

        <!-- //.article -->
        <div class="article-wrapper">


        </div>
        <div class="article-wrapper">

            <div class="article">
                <form name="formulario" id="formulario"
                      action="<?php echo site_url();?>/controller_comercio_adm_servicios/mostrar_relacion_servicios_recursos"
                      method="POST">
                    <TABLE class="tablahoras">
                        <tr>
                            <th><?php echo lang('relacionar_servicios_escoge') ?></th>
                        </tr>
                        <tr>
                            <td>
                                <SELECT NAME="servicios" id="servicios">
                                    <?php foreach ($datos_servicios as $row) {
                                    if ($row->id_servicio == $id_servicio) {
                                        echo "<OPTION VALUE=" . $row->id_servicio . " selected>" . $row->nombre . "</OPTION>";
                                    } else {
                                        echo "<OPTION VALUE=" . $row->id_servicio . ">" . $row->nombre . "</OPTION>";
                                    }


                                }

                                    echo "</select></td> </tr></TABLE>";


                                    ?>
                                    <br>
                                    <div class="btnWrap" align="center">
                                        <a class="btnStyle" id="accionboton" href="#">Escoger servicio</a>
                                    </div>
                </form>


                </table>
                <p><br/>Page rendered in {elapsed_time} seconds</p>
            </div>
        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
