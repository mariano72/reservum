<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>


    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });

</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('alta_materiales_cabecera_admin') ?></li>
      </div>
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <TABLE class="tabladatos">
                    <tr>
                        <th> <?php echo lang('lista_materiales_lista') ?>  </th>
                    </tr>

                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('lista_materiales_nombre') ?></th>
                        <th><?php echo lang('lista_materiales_importe') ?></th>
                        <th><?php echo lang('lista_materiales_estado') ?></th>
                        <th><?php echo lang('lista_materiales_nombre_ser') ?></th>
                        
                    </tr>
    <?php
                    foreach ($datos_materiales as $row) {

                        echo "<tr>";

                        echo    "<td><a href=\"" . site_url() . "/controller_comercio_adm_materiales/previa_alta_materiales/" . $row->id_material  . "\">" . $row->descripcion . "</a></td>";
                        echo    "<td>" . $row->importe . "</td>";
                        echo    "<td>" . $row->estado . "</td>";
                        echo    "<td>" . $row->nombre . "</td>";
                        echo "</tr>";
                    }
                    ?>
                </TABLE>


                <br>
            </div>
            <!-- //.article -->
        </div>
        <!-- //#main-content -->


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
