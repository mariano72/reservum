<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>


<body>
<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });

</script>
<div id="container">


    <?php echo $header ?>

    <!-- //#sub-header -->

    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <h2></h2>


        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('mis_clientes_cabecera') ?></li>
        </div>
        <div class="article-wrapper">
            </ul>

            <br>

            <div class="article">
                <h3></h3>

                <p>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('mis_clientes_listado') ?></th>
                    </tr>
                </TABLE>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('mis_clientes_usuario') ?> </th>
                        <th><?php echo lang('mis_clientes_mail') ?></th>
                        <th> <?php echo lang('mis_clientes_telefono') ?> </th>
                    </tr>

    <?php
               foreach ($mis_clientes as $row) {
                    if ($row->flag_usuario_reserva == 'USUARIO') {
                        echo  "<TR><TD>" . $row->nombre_usuario . "</Td><TD>" . $row->email . "</Td><TD>" . $row->telefono . "</Td><TR>";
                    } else {
                        echo "<TR><TD>" . $row->nombre_reserva . "</Td><TD>" . $row->email_reserva . "</Td><TD>" . $row->telefono_reserva . "</Td>";

                    }
                }

                    ?>
                </TABLE>
                </p>
            </div>


            <!-- //.article -->
        </div>
        <!-- //#main-content -->

        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
