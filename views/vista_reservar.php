<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

        $("#provincias").change(function() {

            $.post("<?php echo base_url() . "index.php/controller_comun_no_seg/carga_municipios_json"?>", { provincias:$(this).val() },
                    function(data) {

                        //               $("#municipios").html(data);
                        // Fill sub category select
                        // Clear all options from sub category select
                        $("select#municipios option").remove();
                        $.each(data, function(i, j) {
                            var row = "<option value=\"" + j.value + "\">" + j.text + "</option>";
                            $(row).appendTo("select#municipios");
                        });
                    }, 'json'
            )
        });


    })
    $(function() {
        $("input:submit, a, button", ".demo").button();
        $("a", ".demo").click(function() {
            return false;
        });
    });
</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('reservar_cabecera') ?></li>
        </div>
        <h2></h2>

        <!-- //.article -->
        <div class="article-wrapper">
            <div class="article">
                <h3></h3>

                <p>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('reservar_cabecera_tabla') ?></th>
                    </tr>
                </table>
                <TABLE class="tabladatos">
                    <tr>
                        <th><?php echo lang('reservar_fecha') ?></th>
                        <th> <?php echo lang('reservar_horaini') ?> </th>
                        <th> <?php echo lang('reservar_horafi') ?> </th>
                        <th> <?php echo lang('reservar_duracion') ?> </th>
                        <th> <?php echo lang('reservar_nombre') ?>  </th>
                        <th> <?php echo lang('reservar_recurso') ?> </th>
                        <th> <?php echo lang('reservar_servicio') ?> </th>
                        <th> <?php echo lang('reservar_direccion') ?> </th>
                        <th> <?php echo lang('reservar_numero') ?></th>
                        <th> <?php echo lang('reservar_codpostal') ?></th>
                        <th> <?php echo lang('reservar_telefono') ?></th>
                    </tr>

                    <?
                    foreach ($misreservas as $row) {
                        echo "<TR>";
                        echo "<TD>" . $row->fecha_reserva . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>";
                        echo "<TD><a  href=\"" . site_url() . "/controller_home/consultar_ficha_comercio/" . $row->codigo_comercio . "\">" . lang('reservar_link_reservar') . " " .
                             $row->nombre_comercio . "</a></TD><TD>" .
                             $row->nombre_recurso . "</TD><TD>" . $row->nombre_servicio . "</TD>";
                        echo "<TD>" . $row->nombre_calle . "</TD><TD>" . $row->numero . "</TD><TD>" . $row->codigo_postal . "</TD><TD>" . $row->telefono . "</TD>";
                        echo "</TR>";
                    }

                    ?>
                    </TABLE>
                    </p>
            </div>
            <br>
            <div class="article">
                <div class="article-wrapper-head">
                    <ul class="article-actions">
                        <li><?php echo lang('reservar_busca_comercio') ?></li>
                </div>
                <h3></h3>

                <p>

                <form name="formulario" action="lista_comercios_municipios" method="POST">

                    <SELECT NAME="provincias" id="provincias" class="formulario1">
                        <OPTION VALUE="SinSeleccionar"><?php echo lang('reservar_busca_provincia') ?></OPTION>
                        <?php foreach ($records2 as $row) : ?>
                        <?php if ($provincias != $row->codprov) { ?>
                            <OPTION VALUE="<?php echo $row->codprov; ?>"><?php echo $row->descprov; ?></OPTION>
                            <?php } else { ?>
                            <OPTION VALUE="<?php echo $row->codprov; ?>" selected><?php echo $row->descprov; ?></OPTION>
                            <?php } ?>
                        <?php endforeach; ?>
                    </SELECT>

                    <SELECT NAME="municipios" id="municipios" class="formulario1">
                        <OPTION VALUE="SinSeleccionar"><?php echo lang('reservar_busca_municipio') ?></OPTION>
                        <?php foreach ($records as $row) : ?>
                        <?php if ($municipios != $row->codmuni) { ?>
                            <OPTION VALUE="<?php echo $row->codmuni; ?>"><?php echo $row->nommuni; ?></OPTION>
                            <?php } else { ?>
                            <OPTION VALUE="<?php echo $row->codmuni; ?>" selected><?php echo $row->nommuni; ?></OPTION>
                            <?php } ?>
                        <?php endforeach; ?>

                    </SELECT>
                    <input type="submit" name="enviar">
                </form>


                </p>
            </div>
        </div>
        <div class="article-wrapper">

            <div class="article">
              <TABLE class="tablahoras"><tr><th><?php echo lang('reservar_nombre_tabla') ?></th><th><?php echo lang('reservar_descripcion') ?> </th></tr>

                <?
                foreach ($comercios as $row) {
                    echo "<TR>";
                    echo "<TD><a href=\"../controller_comun/reserva_cita_comercio/" . $row->id_comercio . "\">" . $row->nombre . "</a></TD><TD>" . $row->descripcion_corta . "</TD>";
                    echo "</TR>";
                }


                ?>
                </TABLE>

            </div>
        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
