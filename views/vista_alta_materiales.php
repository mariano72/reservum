<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>



        $("fechainicio").attr('disabled', 'disabled');
        $("fechafin").attr('disabled', 'disabled');



    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>





     

    });

</script>


<div id="container">


<?php echo $header ?>
<!-- //#sub-header -->
<?php echo $capa_mensaje ?>


<div id="main-content">
    <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('alta_materiales_cabecera') ?></li>
      </div>
<h2></h2>
<BR>
<!-- //.article -->
<div class="article-wrapper">

<div class="article">
<h3></h3>

<p>
    <?php
        $descripcion=null;
        $importe=null;
        $id_material=null;
            
        if (isset($datos_refresco_form)) {
            $id_material= $datos_refresco_form['id_material'];
            $estado = $datos_refresco_form['estado'];
            $descripcion = $datos_refresco_form['descripcion'];
            $id_servicio = $datos_refresco_form['servicios'];
            $importe = $datos_refresco_form['importe'];
            $url_destino = $datos_refresco_form['url_destino'];

        } else {


            foreach ($datos_material as $row) {
                $id_material= $row->id_material;
                $estado = $row->estado;
                $descripcion= $row->descripcion;
                $id_servicio = $row->id_servicio;
                $importe = $row->importe;
            }

           

        }


        ?>

<form name="formulario" id="formulario"
      action="<?php echo site_url() . $url_destino  ?>"
      method="POST">
    <input type="hidden" name="id_material" value="<?php echo $id_material  ?>">
    <input type="hidden" name="url_destino" value="<?php echo $url_destino  ?>">

    <TABLE class="tabladatos">
        <tr>

            <th> <?php echo lang('alta_materiales_descripcion') ?> </th>
            <th> <?php echo lang('alta_materiales_estado') ?> </th>
            <th><?php echo lang('alta_materiales_servicio') ?></th>
            <th><?php echo lang('alta_materiales_importe') ?>  </th>
        </tr>
        <TR>

            

            <td><input name="descripcion" type="text" value="<?php echo $descripcion ?>"/></td>
            <TD>
                <SELECT NAME="estado">
                    <?php foreach ($estados_materiales as $indice => $valor) {
                    if ($indice == $estado) {
                        echo "<OPTION VALUE=" . $indice . " selected>" . $valor . "</OPTION>";
                    } else {
                        echo "<OPTION VALUE=" . $indice . ">" . $valor . "</OPTION>";
                    }

                }
                    ?>
                </SELECT></TD>
       
            <TD>
                <SELECT NAME="servicios" id="servicios">
                    <?php foreach ($datos_servicios as $row) {
                    if ($row->id_servicio == $id_servicio) {
                        echo "<OPTION VALUE=" . $row->id_servicio . " selected>" . $row->nombre . "</OPTION>";
                    } else {
                        echo "<OPTION VALUE=" . $row->id_servicio . ">" . $row->nombre . "</OPTION>";
                    }


                }




                    ?>
                </SELECT></TD>
            <td><input name="importe" id="importe" type="text"
                       value="<?php echo $importe ?>"/></td>
            

        </TR>
    </TABLE>


    <br>

    <div class="btnWrap" align="center">
        <a class="btnStyle" id="accionboton" href="#"><?php echo lang('alta_materiales_boton') ?></a>
    </div>

</form>


<br>

<p>


</p>

</div>
<!-- //.article -->
</div>
<!-- //#main-content -->


<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
