<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <?php echo $head ?>
<body>

 <?php echo $scripts_definition ?>

<script>

$(document).ready(function(){


<?php echo $gestion_seleccion_idioma ?>




<?php
  if (isset($validation_errors) || (isset($mensaje_ok))) {
    echo "$('#capamensaje').attr('style', 'visibility: visible');";
    echo "$('#capamensaje').hide();";
    echo "$('#capamensaje').slideDown('slow');";
    if (isset($validation_errors)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
    }
    if (isset($mensaje_ok)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
    }
  } else {
    $capa_mensaje="";
  }
?>

		
	
});
</script>



<div id="container">

  <?php echo $header ?>

  <!-- //#sub-header -->
   <?php echo $capa_mensaje ?>
  
  
  <div id="main-content">
        <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('lista_facturas_cabecera') ?></li>
      </div>
    <h2></h2>
    <BR>
    <!-- //.article -->
    <div class="article-wrapper">
      
    
    </div>
 <div class="article-wrapper">

      <div class="article">
   
          <TABLE class="tabladatos"><tr><th><?php echo lang('lista_facturas_numero') ?></th><th> <?php echo lang('lista_facturas_tarifa') ?> </th>
              <th> <?php echo lang('lista_facturas_estado') ?> </th><th> <?php echo lang('lista_facturas_fecha') ?> </th><th> <?php echo lang('lista_facturas_periodo') ?> </th>
           <th> <?php echo lang('lista_facturas_importetotal') ?></th></tr>

	 <? 	
	  
	  
	  foreach($datos_facturas as $row) {
	   echo "<TR>";
	   echo "<TD><a href=\"" . site_url() ."/controller_comercio_adm_facturas/detalle_factura/" . $row->id_factura . "\">" . $row->numero_factura .  "</a></TD><TD>" . $row->desc_tarifa .
            "</TD><TD>" . $row->estado_factura . "</TD><TD>" . $row->fecha_factura . "</TD><TD>" . $row->fecha_desde . " - " . $row->fecha_hasta . "</TD><TD>" . $row->importe_total . "</TD>";
	
	   echo "</TR>";
	  }
	  echo "</TABLE>";

	?><br><br> 


		<p><br />Page rendered in {elapsed_time} seconds</p>
   </div>
</div>		
    <!-- //.article -->
  </div>
  <!-- //#main-content -->


  <div id="footer">
     <?php echo $footer ?>
  </div>
  <!-- //#footer -->
  
</div>
<!-- //#container -->
</body>
</html>
