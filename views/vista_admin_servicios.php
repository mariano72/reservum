<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <?php echo $head ?>
<body>

 <?php echo $scripts_definition ?>

<script>

$(document).ready(function(){

<?php echo $gestion_seleccion_idioma ?>




<?php
  if (isset($validation_errors) || (isset($mensaje_ok))) {
    echo "$('#capamensaje').attr('style', 'visibility: visible');";
    echo "$('#capamensaje').hide();";
    echo "$('#capamensaje').slideDown('slow');";
    if (isset($validation_errors)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
    }
    if (isset($mensaje_ok)){
     $capa_mensaje="<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
    }
  } else {
    $capa_mensaje="";
  }
?>


});
	
</script>



<div id="container">


      <?php echo $header ?>
  <!-- //#sub-header -->
   <?php echo $capa_mensaje ?>
  
  
  <div id="main-content">
      <div class="article-wrapper-head">
          <ul class="article-actions">
              <li><?php echo lang('alta_servicios_cabecera') ?></li>
      </div>
    <h2></h2>
    <BR>
    <!-- //.article -->
    <div class="article-wrapper">
      
    
    </div>
 <div class="article-wrapper">

      <div class="article">
      <TABLE class="tabladatos"><tr><th><?php echo lang('alta_servicios_nombre') ?></th><th><?php echo lang('alta_servicios_descripcion') ?></th><th><?php echo lang('alta_servicios_Duracion') ?></th><th><?php echo lang('alta_servicios_Importe') ?></th><th>
	   <?php echo lang('alta_servicios_IVA') ?></th><th><?php echo lang('alta_servicios_Importe_total') ?></th></tr>
	 <? 	

	 
	  foreach($datos_servicios as $row) {

	   echo "<TR>";
	   echo "<TD><a href=" . site_url() . "/controller_comercio_adm_servicios/detalle_servicios/" . $row->id_servicio . ">" . $row->nombre .  "</a></TD><TD>" . $row->descripcion . "</TD><TD>" .
             $row->duracion_minutos . "</TD><TD>" . $row->importe . "</TD><TD>" . $row->iva . "</TD><TD>" . $row->importe_total . "</TD>";
	
	   echo "</TR>";
	  }
	  echo "</TABLE>";

	?><br><br> 


		<p><br />Page rendered in {elapsed_time} seconds</p>
   </div>
</div>		
    <!-- //.article -->
  </div>
  <!-- //#main-content -->


  <div id="footer">
     <?php echo $footer ?>
  </div>
  <!-- //#footer -->
  
</div>
<!-- //#container -->
</body>
</html>
