<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

        $(function() {
            $("input:submit, a, button", ".demo").button();
            $("a", ".demo").click(function() {
                return false;
            });
        });

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>
    });
</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>


    <div id="main-content">
        <h2></h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <p>
    <?php
       foreach ($datos_direccion as $row) {
        $id_direccion = $row->id_direccion;
        $provincia = $row->provincia;
        $municipio = $row->municipio;
        $nombre_calle = $row->nombre_calle;
        $numero = $row->numero;
        $codigo_postal = $row->codigo_postal;
        $telefono = $row->telefono;
        $telefono2 = $row->telefono2;
        $fax = $row->fax;

    }
        foreach ($datos_comercio as $row) {
            $id_comercio = $row->id_comercio;
            $nombre = $row->nombre;
            $cif = $row->cif;
            $descripcion_corta = $row->descripcion_corta;
            $descripcion = $row->descripcion;
            $foto_comercio = $row->foto_comercio;
            $estado = $row->estado;
            $email = $row->mail;
        }

        foreach ($datos_servicios as $row) {
            $nombre_servicio = $row->nombre;
            $descripcion_servicio = $row->descripcion;
            $duracion_minutos = $row->duracion_minutos;
            $importe_total = $row->importe_total;
            $id_servicio = $row->id_servicio;
        }

        if ($estado == 'A') {
            $url = "<a class=\"btnStyle\" href=\"" . site_url() . "/controller_comun/reserva_cita_comercio/" . $id_comercio . "\">" . lang('ficha_comercio_quiero_reservar') . "</a>";
        } else {
            $url = "";
        }

        $lang['ficha_comercio_imagen'] = "Imagen";
        $lang['ficha_comercio_descripcion_corta'] = "Descripción corta";
        $lang['ficha_comercio_descripcion'] = "Descripción";

        ?>

                <form name="formulario"
                      action="<?php echo site_url() . "/controller_comercio/actualizar_datos_comercio"?>"
                      enctype="multipart/form-data" method="POST">
                    <input type="hidden" name="id_comercio" value="<? echo $id_comercio ?>">
                    <TABLE class="tabladatos">
                        <tr>
                            <th> <?php echo lang('ficha_comercio_datos_contacto') ?></th>
                        </tr>
                    </TABLE>
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('ficha_comercio_nombre') ?></th>
                            <th> <?php echo lang('ficha_comercio_cif') ?></th>
                            <th> <?php echo lang('ficha_comercio_provincia') ?></th>
                            <th> <?php echo lang('ficha_comercio_poblacion') ?></th>
                            <th> <?php echo lang('ficha_comercio_direccion') ?></th>
                            <th> <?php echo lang('ficha_comercio_numero') ?></th>
                            <th> <?php echo lang('ficha_comercio_codigopostal') ?></th>

                        </tr>
                        <TR>
                            <TD><?php echo $nombre  ?></TD>
                            <TD><?php echo $cif ?></TD>
                            <TD><?php echo $provincia ?></TD>
                            <TD><?php echo $municipio ?></TD>
                            <TD><?php echo $nombre_calle ?></TD>
                            <TD><?php echo $numero  ?></TD>
                            <TD><?php echo $codigo_postal  ?></TD>

                        </TR>
                        <tr>
                            <th><?php echo lang('ficha_comercio_email') ?></th>
                            <th> <?php echo lang('ficha_comercio_telefono') ?></th>
                            <th> <?php echo lang('ficha_comercio_telefono2') ?></th>
                            <th> <?php echo lang('ficha_comercio_fax') ?></th>
                            <th><?php echo lang('ficha_comercio_comollegar') ?></th>
                            <th></th>

                        </tr>
                        <TD><?php echo $email ?></TD>
                        <TD><?php echo $telefono  ?></TD>
                        <TD><?php echo $telefono2  ?></TD>
                        <TD><?php echo $fax  ?></TD>
                        <TD><?php echo lang('ficha_comercio_vermapa') ?></TD>
                        <TD></TD>
                        <TD>

                        </TD>


                        <TR>

                        </TR>
                    </TABLE>
                    <BR>

                    <div class="btnWrap" align="center">

                        <?php echo $url ?>
                    </div>
                    <br>
                    <TABLE class="tabladatos">
                        <tr>
                            <th> <?php echo lang('ficha_comercio_ser_catalogo') ?></th>
                        </tr>
                    </TABLE>
                    <TABLE class="tabladatos">
                        <tr>
                            <th> <?php echo lang('ficha_comercio_ser_nombre') ?></th>
                            <th> <?php echo lang('ficha_comercio_ser_descripcion') ?></th>
                            <th> <?php echo lang('ficha_comercio_ser_duracion') ?></th>
                            <th> <?php echo lang('ficha_comercio_ser_precio') ?></th>
                        </tr>
                        <?php


                        foreach ($datos_servicios as $row) {
                            echo "<TR>";
                            echo "<TD>" . $row->nombre . "</a></TD><TD>" . $row->descripcion . "</TD><TD>" . $row->duracion_minutos . "</TD><TD>" . $row->importe_total . "</TD>";
                            echo "</TR>";
                        }
                        echo "</TABLE>";

                        ?>
                        <BR>
                        <TABLE class="tabladatos">
                            <tr>
                                <th> <?php echo lang('ficha_comercio_descripcion_corta') ?></th>
                            </tr>
                            <TR>
                                <TD><?php echo $descripcion_corta  ?></TD>
                            </TR>
                        </TABLE>
                        <BR>
                        <TABLE class="tabladatos">
                            <tr>
                                <th> <?php echo lang('ficha_comercio_descripcion') ?></th>
                            </tr>
                            <TR>
                                <TD><?php echo $descripcion  ?></TD>
                            </TR>
                        </TABLE>


                </form>


                <br>

                <p>


                </p>

            </div>
            <!-- //.article -->
        </div>
        <!-- //#main-content -->


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
