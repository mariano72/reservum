<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {



    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>

    });


</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>



    <div id="main-content">
        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('completar_ficha_comercio_cabecera') ?></li>
        </div>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <p>
    <?php
            foreach ($datos_direccion as $row) {
        $id_direccion = $row->id_direccion;
        $provincia = $row->provincia;
        $municipio = $row->municipio;
        $nombre_calle = $row->nombre_calle;
        $numero = $row->numero;
        $codigo_postal = $row->codigo_postal;
        $telefono = $row->telefono;
        $telefono2 = $row->telefono2;
        $fax = $row->fax;

    }
        foreach ($datos_comercio as $row) {
            $id_comercio = $row->id_comercio;
            $nombre = $row->nombre;
            $cif = $row->cif;
            $descripcion_corta = $row->descripcion_corta;
            $descripcion = $row->descripcion;
            $foto_comercio = $row->foto_comercio;
            $codigo_comercio = $row->codigo_comercio;

        }


        ?>

                <form name="formulario" id="formulario"
                      action="<?php echo site_url() . "/controller_ficha/actualizar_datos_comercio"?>"
                      enctype="multipart/form-data" method="POST">
                    <input type="hidden" name="id_comercio" value="<? echo $id_comercio ?>">
                 
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('completar_ficha_comercio_nombre') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_cif') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_provincia') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_poblacion') ?></th>
                        </tr>
                        <TR>
                            <TD><input type="text" name="nombre" size="20" value="<?php echo $nombre  ?>"></TD>
                            <TD><?php echo $cif ?></TD>
                            <TD><?php echo $provincia ?></TD>
                            <TD><?php echo $municipio ?></TD>
                        </TR>
                        <TR>
                        <TR>
                            <th><?php echo lang('completar_ficha_comercio_direccion') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_numero') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_codigopostal') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_telefono') ?></th>
                        </TR>
                        <TR>
                            <TD><input type="text" name="nombre_calle" value="<?php echo $nombre_calle       ?>"></TD>
                            <TD><input type="text" name="numero" maxlength="4" size="3" value="<?php echo $numero  ?>">
                            </TD>
                            <TD><input type="text" name="codigo_postal" maxlength="5" size="6"
                                       value="<?php echo $codigo_postal  ?>"></TD>
                            <TD><input type="text" name="telefono" maxlength="9" size="9"
                                       value="<?php echo $telefono  ?>"></TD>
                        </TR>
                        <tr>
                            <th><?php echo lang('completar_ficha_comercio_telefono2') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_fax') ?></th>
                            <th><?php echo lang('completar_ficha_comercio_imagen') ?></th>
                            <th></th>
                        </tr>
                        <TD><input type="text" name="telefono2" maxlength="9" size="9"
                                   value="<?php echo $telefono2  ?>"></TD>
                        <TD><input type="text" name="fax" maxlength="5" size="9" value="<?php echo $fax  ?>"></TD>
                        <TD><a target="_blank"
                               href="<?php echo base_url() . APPPATH ?>uploads/<?php echo $foto_comercio  ?>">Ver
                            imagen</a></TD>

                        <TR>

                        </TR>
                    </TABLE>

                    <BR>
                    <TABLE class="tabladatos">
                        <th><?php echo lang('completar_ficha_comercio_url_reserva') ?></th>
                        <TR class="redrow">
                            <TD class="celdadestacada"><?php echo site_url() . "/controller_home/consultar_ficha_comercio/" . $codigo_comercio ?></TD>
                        </TR>
                        <th><?php echo lang('completar_ficha_comercio_descripcion_corta') ?></th>
                        <TR>
                            <TD><textarea rows="10" cols="80"
                                          name="descripcion_corta"><?php echo $descripcion_corta  ?></textarea></TD>
                        </TR>
                    </TABLE>
                    <BR>
                    <TABLE class="tabladatos">
                        <tr>
                            <th><?php echo lang('completar_ficha_comercio_descripcion') ?></th>
                        </tr>
                        <TR>
                            <TD><textarea cols="80" rows="20" name="descripcion"><?php echo $descripcion  ?></textarea>
                            </TD>
                        </TR>
                    </TABLE>

                    <input type="file" name="userfile">
                    <br>

                    <div class="btnWrap" align="center">
                        <a class="btnStyle" id="accionboton" href="#">Completar ficha</a>
                    </div>
                </form>


                <br>

                <p>


                </p>

            </div>
            <!-- //.article -->
        </div>
        <!-- //#main-content -->


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
