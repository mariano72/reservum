<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>


    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>



    });

</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>

    <div id="main-content">
     <table class="tabladatos">
         <th>¿Que tipos de tarifa hay?</th>

            <tr><td><?php echo $faq1 ?></td></tr>
         <th>¿Para que necesito RESERVum?</th>

            <tr><td><?php echo $faq2 ?></td></tr>
         <th>¿Qué comercios pueden beneficiarse de reservum?</th>

            <tr><td><?php echo $faq3 ?></td></tr>         
     </table>



    </div>
    <!-- //.article -->
</div>
<!-- //#main-content -->


<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
