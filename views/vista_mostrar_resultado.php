<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>

<script>

    $(document).ready(function () {

        <?php echo $gestion_seleccion_idioma ?>


        <?php
            if (isset($validation_errors) || (isset($mensaje_ok))) {
                echo "$('#capamensaje').attr('style', 'visibility: visible');";
                echo "$('#capamensaje').hide();";
                echo "$('#capamensaje').slideDown('slow');";
                if (isset($validation_errors)) {
                    $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
                }
                if (isset($mensaje_ok)) {
                    $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
                }
            } else {
                $capa_mensaje = "";
            }
        ?>


    });

</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>


    <div id="main-content">


    </div>
    <!-- //.article -->
</div>
<!-- //#main-content -->

<?
$url_google_qr="https://chart.googleapis.com/chart?cht=qr&chs=200x200&choe=UTF-8&chl=";
$url_reservum=base_url() . "index.php/controller_comun_no_seg/localiza_reserva_qr/" . $localizador;
$url_qr=$url_google_qr . $url_reservum;

?>
<!-- //.article -->
<div class="article-wrapper">
    <div class="article">
        <h3><?php echo lang('qr_titulo') ?></h3>
        <img src="<?php  if (isset($mensaje_ok)) {echo $url_qr;} ?>">
        <p>
            <?php if ($this->session->userdata('soyusuario')) : ?>
        <TABLE class="tabladatos">
            <tr>
                <th><?php echo lang('anular_cabecera_tabla') ?></th>
            </tr>
        </table>

        <TABLE class="tablahoras">
            <tr>
                <th> <?php echo lang('anular_fecha') ?></th>
                <th><?php echo lang('anular_horaini') ?> </th>
                <th><?php echo lang('anular_horafi') ?> </th>
                <th> <?php echo lang('anular_duracion') ?>  </th>
                <th><?php echo lang('anular_nombre') ?>  </th>
                <th> <?php echo lang('anular_recurso') ?> </th>
                <th> <?php echo lang('anular_servicio') ?> </th>
                <th> <?php echo lang('anular_direccion') ?> </th>
                <th> <?php echo lang('anular_numero') ?> </th>
                <th><?php echo lang('anular_codpostal') ?></th>
                <th> <?php echo lang('anular_telefono') ?></th>

            </tr>
            <?

                foreach ($misreservas as $row) {
                    echo "<TR>";
                    echo "<TD>" . $row->fecha_reserva . "</TD><TD>" . $row->hora_inicio . "</TD><TD>" . $row->hora_fin . "</Td><TD>" . $row->duracion . "</Td>";
                    echo "<TD>" . $row->nombre_comercio . "</TD><TD>" . $row->nombre_recurso . "</TD><TD>" . $row->nombre_servicio . "</TD>";
                    echo "<TD>" . $row->nombre_calle . "</TD><TD>" . $row->numero . "</TD><TD>" . $row->codigo_postal . "</TD><TD>" . $row->telefono . "</TD>";
                    echo "</TR>";
                }

            ?>
        </TABLE>
        <?php endif; ?>

        </p>
    </div>
</div>

<div id="footer">
    <?php echo $footer ?>
</div>
<!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
