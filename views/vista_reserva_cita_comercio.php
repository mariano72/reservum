<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>
<?php echo $scripts_definition ?>

<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>

    <?php
        if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


        $("#provincias").change(function() {

            $.post("carga_municipios_json", { provincias:$(this).val() },
                    function(data) {

                        //               $("#municipios").html(data);
                        // Fill sub category select
                        // Clear all options from sub category select
                        $("select#municipios option").remove();
                        $.each(data, function(i, j) {
                            var row = "<option value=\"" + j.value + "\">" + j.text + "</option>";
                            $(row).appendTo("select#municipios");
                        });
                    }, 'json'
            )
        });


        $(document).ready(function() {
            $('#fechareserva').datepicker({ minDate: '-0d' });
            $.datepicker.setDefaults($.datepicker.regional['<?php echo $jquery_idioma ?>']);

        });
    })
    $(function() {
        $("input:submit, a, button", ".demo").button();
        $("a", ".demo").click(function() {
            return false;
        });
    });
</script>


<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->
    <?php echo $capa_mensaje ?>

    <div id="main-content">
        <h2></h2>
        <BR>

        <div class="article-wrapper-head">
            <ul class="article-actions">
                <li><?php echo lang('reservar_com_cabecera') ?></li>
        </div>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <p>
                <TABLE class="tabladatos">
                    <tr>
                        <th> <?php echo lang('reservar_com_nombre') ?> </th>
                        <th> <?php echo lang('reservar_com_descripcion') ?> </th>
                    </tr>
                    <?
                    $id_servicio = null;
                    $fechareserva = null;
                    if (isset($datos_refresco_form)) {
                        //print_r($datos_refresco_form);
                        if (isset($datos_refresco_form['id_servicio'])) {
                            $id_servicio = $datos_refresco_form['id_servicio'];
                        }
                        $fechareserva = $datos_refresco_form['fechareserva'];
                    }

                    foreach ($datos_comercio as $row) {
                        echo "<TR>";
                        echo "<TD>" . $row->nombre . "</a></TD><TD>" . $row->descripcion_corta . "</TD>";

                        echo "</TR>";
                    }
                    echo "</TABLE>";

                    ?><br>

                    <?php echo lang('reservar_com_servicios') ?>

                </p>
            </div>
        </div>
        <div class="article-wrapper">

            <div class="article">
                <form id="formulario2" action="<? echo site_url();?>/controller_comun/validar_reserva_cita_comercio"
                      method="POST">
                    <BR>
                    <TABLE class="tabladatos">
                        <tr>
                            <th> <?php echo lang('reservar_com_nombre_ser') ?> </th>
                            <th> <?php echo lang('reservar_com_descripcion_ser') ?> </th>
                            <th> <?php echo lang('reservar_com_duracion') ?> </th>
                            <th> <?php echo lang('reservar_com_importe') ?></th>
                            <th> <?php echo lang('reservar_com_reservar') ?> </th>
                        </tr>
                        <?

                        foreach ($datos_servicios as $row) {
                            echo "<TR>";
                            echo "<TD>" . $row->nombre . "</a></TD><TD>" . $row->descripcion . "</TD><TD>" . $row->duracion_minutos . "</TD><TD>" . $row->importe_total . "</TD>";

                            if ($row->id_servicio == $id_servicio) {
                                echo  "<TD><input type=radio name=id_servicio value=" . $row->id_servicio . " checked>";
                            } else {
                                echo  "<TD><input type=radio name=id_servicio value=" . $row->id_servicio . ">";
                            }
                            echo "</TR>";
                        }
                        echo "</TABLE>";

                        ?><br><br>


                        <input name="id_comercio" id="id_comercio" type="hidden" value="<?php echo $id_comercio ?>"/>
                        <table class="tabladatos">
                            <th><?php echo lang('reservar_com_fecha_reserva') ?></th>
                            <th></th>
                            <tr>
                                <td><input name="fechareserva" id="fechareserva" type="text"
                                           value="<?php echo $fechareserva ?>"/></td>
                                <td><input type="submit" value="<?php echo lang('reservar_com_ver_agenda') ?>"></td>
                            </tr>
                        </table>


                </form>


                <p><br/>Page rendered in {elapsed_time} seconds</p>
            </div>
        </div>
        <!-- //.article -->
    </div>
    <!-- //#main-content -->


    <div id="footer">
        <?php echo $footer ?>
    </div>
    <!-- //#footer -->

</div>
<!-- //#container -->
</body>
</html>
