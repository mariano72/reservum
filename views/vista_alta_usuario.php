<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $head ?>
<body>

<?php echo $scripts_definition ?>


<script>

    $(document).ready(function() {

    <?php echo $gestion_seleccion_idioma ?>


    <?php
      if (isset($validation_errors) || (isset($mensaje_ok))) {
            echo "$('#capamensaje').attr('style', 'visibility: visible');";
            echo "$('#capamensaje').hide();";
            echo "$('#capamensaje').slideDown('slow');";
            if (isset($validation_errors)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"validation\">" . $validation_errors . "</div>";
            }
            if (isset($mensaje_ok)) {
                $capa_mensaje = "<div id=\"capamensaje\" class=\"success\">" . $mensaje_ok . "</div>";
            }
        } else {
            $capa_mensaje = "";
        }
    ?>


    });

</script>

<div id="container">


    <?php echo $header ?>
    <!-- //#sub-header -->

    <?php echo $capa_mensaje ?>


    <?php
        $nombre = null;
    $mail = null;
    $mail2 = null;
    $password = null;
    $password2 = null;
    $telefono = null;
    $idioma = null;


    if (isset($datos_refresco_form)) {
        $nombre = $datos_refresco_form['nombre'];
        $mail = $datos_refresco_form['mail'];
        $mail2 = $datos_refresco_form['mail2'];
        $password = $datos_refresco_form['password'];
        $password2 = $datos_refresco_form['password2'];
        $telefono = $datos_refresco_form['telefono'];
        $idioma = $datos_refresco_form['idioma'];
    }



    ?>
    <div id="main-content">
        <h2>Login</h2>
        <BR>
        <!-- //.article -->
        <div class="article-wrapper">

            <div class="article">
                <h3></h3>

                <p>

                <form name="formulario" class="formredondo" id="formulario" action="alta_usuario_reservas"
                      method="POST">
                    <table class="tabladatosoculta">
                        <tr>
                            <td><?php echo lang('alta_usuario_nombre') ?></td>
                            <td><input type="text" name="nombre" id="nombre" class="formulario1"
                                       value="<?php echo $nombre ?>"></td>
                        </tr>
                        <tr>
                            <td><?php echo lang('alta_usuario_email') ?></td>
                            <td><input type="text" name="mail" id="mail" class="formulario1"
                                       value="<?php echo $mail ?>"></td>
                        </tr>
                        <tr>
                            <td><?php echo lang('alta_usuario_email2') ?></td>
                            <td><input type="text" name="mail2" id="mail2" class="formulario1"
                                       value="<?php echo $mail2 ?>"></td>
                        </tr>
                        <tr>
                            <td><?php echo lang('alta_usuario_password') ?></td>
                            <td><input type="password" name="password" id="password" class="formulario1" value=""></td>
                        </tr>
                        <tr>
                            <td><?php echo lang('alta_usuario_password2') ?></td>
                            <td><input type="password" name="password2" id="password2" class="formulario1" value="">
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo lang('alta_usuario_telefono') ?></td>
                            <td><input type="text" name="telefono" id="telefono" class="formulario1"
                                       value="<?php echo $telefono ?>"></td>
                        </tr>
                        <tr>
                            <td><?php echo lang('alta_usuario_idioma') ?></td>
                            <td><SELECT NAME="idioma" id="idioma">
                                <?php foreach ($idiomas as $valor) {
                                if ($valor->id == $idioma) {
                                    echo "<OPTION VALUE=" . $valor->id . " selected>" . $valor->nombre . "</OPTION>";
                                } else {
                                    echo "<OPTION VALUE=" . $valor->id . ">" . $valor->nombre . "</OPTION>";
                                }

                            }
                                ?>
                            </SELECT></td>
                        </tr>

                    </table>
                </form>


            </div>

            <!-- //.article -->
        </div>
        <!-- //#main-content -->
        <div class="btnWrap" align="center">
            <a class="btnStyle" id="accionboton" href="#"><?php echo lang('alta_usuario_boton_alta') ?></a>
        </div>


        <div id="footer">
            <?php echo $footer ?>
        </div>
        <!-- //#footer -->

    </div>
    <!-- //#container -->
</body>
</html>
