<?php
//FORMULARIO DE DATOS VISTA_DETALLE_RECURSOS.PHP

$lang['alta_recursos_nombre'] = "Name";
$lang['alta_recursos_descripcion'] = "Description";
$lang['alta_recursos_estado'] = "Status";
$lang['alta_recursos_hora_inicio'] = "Firt hour";
$lang['alta_recursos_hora_fin'] = "Last hour";
$lang['alta_recursos_bloques_horarios'] = "Time blocks";
$lang['alta_recursos_dias_avance'] = "Advanced days calendar";
$lang['alta_recursos_minimo_dias'] = "Minimum days cancelation";
$lang['alta_recursos_operacion_ok'] = "Resource added/modified correctly";
$lang['alta_recursos_foto'] = "Add photo";
$lang['alta_recursos_cabecera'] = "Add / modify resources";

//FORMULARIO VISTA_ADMIN_RECURSOS.PHP
$lang['admin_recursos_nombre'] = "Name";
$lang['admin_recursos_descripcion'] = "Description";
$lang['admin_recursos_estado'] = "Status";
$lang['admin_recursos_imagen'] = "Image";
$lang['admin_recursos_cabecera'] = "Resources list";

//FORMULARIO VISTA_ALTA_RECURSOS.PHP, VISTA_ACTUALIZAR_RECURSOS.PHP

$lang['admin_recursos_descripcion'] = "Descripció";
$lang['admin_recursos_estado'] = "Estat";
$lang['admin_recursos_imagen'] = "Imatge";




//VALIDACIONES CALLBACK
$lang['alta_recursos_compara_fechas'] = "Initial date can't be greater or equal than end date";
$lang['alta_recursos_compara_horas'] = "Initial hour can't be greater or equal than end hour";
$lang['alta_recursos_check_tarifa'] = "You try to add more resources.Please check your rate";