<?php
//FORMULARIO DE DATOS VISTA_ANULAR_CITA_CLIENTE.PHP

$lang['anular_cita_cabecera'] = "Cancel reservation";
$lang['anular_cita_fecha'] = "Date";
$lang['anular_cita_hora_inicio'] = "Init. hour";
$lang['anular_cita_hora_fin'] = "End hour";
$lang['anular_cita_duracion'] = "Duration";
$lang['anular_cita_recurso'] = "Resource";
$lang['anular_cita_servicio'] = "Service";
$lang['anular_cita_nombre'] = "Users name";
$lang['anular_cita_email'] = "E-mail";
$lang['anular_cita_telefono'] = "Phone";
$lang['anular_cita_anular'] = "Cancel";



//FORMULARIO DE DATOS VISTA_PREVIA_ANULAR_CITA_CLIENTE.PHP

$lang['previa_anular_cita'] = "Cancel customer reservation";
$lang['previa_anular_mail'] = "E-mail";
$lang['previa_anular_telefono'] = "Phone number";
$lang['previa_anular_fecha'] = "Reservation date";
$lang['previa_anular_cabecera1'] = "Search by user";
$lang['previa_anular_cabecera2'] = "Search by reservation date";
$lang['previa_anular_buscar'] = "Search";
$lang['previa_anular_mensaje_error'] = "No data retrieved, try with new search";


//FORMULARIO DE VISTA_INICIO_COMERCIO.PHP
$lang['inicio_com_cabecera'] = "Resumes scoreboard";
$lang['inicio_com_ult_res_fec'] = "Last reservation date ";
$lang['inicio_com_ult_res_rea'] = "Was realized for ";
$lang['inicio_com_ult_res_curso'] = " Reservations in progress";
$lang['inicio_com_ult_res_mes'] = " monthly reservations pending";
$lang['inicio_com_ult_res_total'] = " total reservations pending";
$lang['inicio_com_ult_res_cab'] = "Last reservations made";
$lang['inicio_com_ult_fecha'] = "Service Date";
$lang['inicio_com_ult_horaini'] = "Initial date ";
$lang['inicio_com_ult_horafin'] = "End date";
$lang['inicio_com_ult_duracion'] = "Duration";
$lang['inicio_com_ult_usuario'] = "Customer";
$lang['inicio_com_ult_servicio'] = "Service";
$lang['inicio_com_ult_mail'] = "E-Mail";
$lang['inicio_com_ult_telef'] = "Phone number";
$lang['inicio_com_prox'] = "Next reservations";

//FORMULARIO DE VISTA_AGENDA_DIA.PHP
$lang['agenda_dia_cabecera'] = "Day agenda";
$lang['agenda_dia_dia'] = "Day";
$lang['agenda_dia_hora_inicio'] = "Initial hour";
$lang['agenda_dia_hora_fin'] = "End hour";
$lang['agenda_dia_servicio'] = "Service";
$lang['agenda_dia_usuario'] = "Customer";
$lang['agenda_dia_email'] = "E-mail";
$lang['agenda_dia_telefono'] = "Phone number";
$lang['agenda_dia_seleccionar'] = "Select day";
$lang['agenda_dia_ver_agenda'] = "Go to calendar";

//FORMULARIO DE VISTA_MIS_CLIENTES.PHP
$lang['mis_clientes_cabecera'] = "My customers";
$lang['mis_clientes_listado'] = "Customer's list";
$lang['mis_clientes_usuario'] = "User name";
$lang['mis_clientes_mail'] = "E-mail";
$lang['mis_clientes_telefono'] = "Phone number";


//FORMULARIO CONSULTAR_RESERVA

$lang['consultar_reserva_cabecera'] = "Reservation information";
$lang['consultar_reserva_datos_generales'] = "Reservation data";
$lang['consultar_reserva_recurso_nombre'] = "Resource";
$lang['consultar_reserva_servicio_nombre'] = "Service";
$lang['consultar_reserva_recurso_fecha_reserva'] = "Reservation date";
$lang['consultar_reserva_datos_hora_inicio'] = "Initial hour";
$lang['consultar_reserva_datos_hora_fin'] = "Final hour";
$lang['consultar_reserva_duracion'] = "Duration";
$lang['consultar_reserva_datos_personales'] = "User's data";
$lang['consultar_reserva_nombre_usuario']="Name";
$lang['consultar_reserva_email_usuario']="E-Mail";
$lang['consultar_reserva_servicio_telefono_usuario']="Phone number";
$lang['consultar_reserva_importe']="Amount";
$lang['consultar_reserva_materiales_descripcion']="Extras and materials";
$lang['consultar_reserva_materiales_importe']="Materials amount";
$lang['consultar_reserva_materiales_tarifa']="Rate";

