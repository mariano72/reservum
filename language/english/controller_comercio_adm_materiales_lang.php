<?php

//FORMULARIO LISTA_DE_MATERIALES
$lang['lista_materiales_lista'] = "Equipment list";
$lang['lista_materiales_nombre'] = "Name";
$lang['lista_materiales_nombre_ser'] = "Service name";
$lang['lista_materiales_importe'] = "Amount";
$lang['lista_materiales_estado'] = "State";


//FORMULARIO ALTA_DE_materiales

$lang['alta_materiales_descripcion'] = "Description";
$lang['alta_materiales_estado'] = "Status";
$lang['alta_materiales_servicio'] = "Service";
$lang['alta_materiales_importe'] = "Amount";
$lang['alta_materiales_boton'] = "Add / Modify material";
$lang['alta_materiales_cabecera_admin'] = "Admin materials";
$lang['alta_materiales_cabecera'] = "Add / Modify materials";

$lang['alta_tarifa_ok'] = "New equipment added OK";
$lang['actualizar_tarifa_ok'] = "Equipment updated correctly";
    
