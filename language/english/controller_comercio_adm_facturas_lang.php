<?php
//FORMULARIO DE DATOS VISTA_LISTA_FACTURAS.PHP

$lang['lista_facturas_cabecera'] = "Invoice list";  
$lang['lista_facturas_numero'] = "Invoice number";
$lang['lista_facturas_descripcion'] = "Description";
$lang['lista_facturas_tarifa'] = "Rate";
$lang['lista_facturas_estado'] = "Invoice Status";
$lang['lista_facturas_periodo'] = "Period";
$lang['lista_facturas_fecha'] = "Date";
$lang['lista_facturas_importe'] = "Amount";
$lang['lista_facturas_iva'] = "Taxes";
$lang['lista_facturas_importetotal'] = "Total amount";


//FORMULARIO DE DATOS VISTA_DETALLE_FACTURA.PHP

$lang['detalle_factura_cabecera'] = "Invoice list";
$lang['detalle_factura_numero'] = "Invoice number";
$lang['detalle_factura_descripcion'] = "Description";
$lang['detalle_factura_tarifa'] = "Rate";
$lang['detalle_factura_estado'] = "Invoice Status";
$lang['detalle_factura_periodo'] = "Period";
$lang['detalle_factura_fecha'] = "Date";
$lang['detalle_factura_importe'] = "Amount";
$lang['detalle_factura_iva'] = "Taxes";
$lang['detalle_factura_importetotal'] = "Total amount";
$lang['detalle_factura_pagar'] = "Pay invoice";
$lang['detalle_factura_concepto'] = "Service description";
$lang['detalle_factura_fecha_pago'] = "Invoice pay date";
$lang['detalle_factura_paypal_gateway'] = "Paypal's reference number";