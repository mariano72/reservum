<?php


//FORMULARIO DE DATOS VISTA_RESERVAR

    $lang['reservar_com_cabecera'] = "Customer reservation";
    $lang['reservar_com_cabecera2'] = "Reservation";
    $lang['reservar_com_nombre'] = "Businnes name";
    $lang['reservar_com_descripcion'] = "Business description";
    $lang['reservar_com_nombre_ser'] = "Service name";
    $lang['reservar_com_descripcion_ser'] = "Service description";
    $lang['reservar_com_duracion'] = "Duration";
    $lang['reservar_com_importe'] = "Amount";
    $lang['reservar_com_reservar'] = "Reserve";
    $lang['reservar_com_servicios'] = "We offer the next services.Try one:";
    $lang['validar_reserva_comercio_alta'] = "Business services are OFFLINE";
    $lang['reservar_com_fecha_reserva'] = "Select reservation date";
    $lang['reservar_com_ver_agenda'] = "Go to calendar";


//FORMULARIO CAMBIAR PASSWORD.PHP

    $lang['cambio_pass_password'] = "Password";
    $lang['cambio_pass_newpassword'] = "New password";
    $lang['cambio_pass_newpassword2'] = "Repeat new password";
    $lang['cambio_pass_OK'] = "The password has been changed";
    $lang['cambio_pass_check_pass'] = "Your actual password is incorrect";
    $lang['cambio_pass_cabecera'] = "Change password";

//FORMULARIO VISTA_PREVIA_RESERVA.PHP
    $lang['previa_reserva_cabecera'] = "Check out the reservation";
    $lang['previa_reserva_resumen'] = "Reservation brief";
    $lang['previa_reserva_nombre'] = "Name";
    $lang['previa_reserva_descripcion'] = "Description";
    $lang['previa_reserva_direccion'] = "Address";
    $lang['previa_reserva_telefono'] = "Phone";
    $lang['previa_reserva_mapa'] = "Map";
    $lang['previa_reserva_datos_recurso'] = "Resource's data";
    $lang['previa_reserva_recurso_nombre'] = "Name";
    $lang['previa_reserva_recurso_descripcion'] = "Description";
    $lang['previa_reserva_datos_servicio'] = "Service's data";
    $lang['previa_reserva_servicio_nombre'] = "Name";
    $lang['previa_reserva_servicio_descripcion'] = "Description";
    $lang['previa_reserva_recurso_reserva'] = "Reservation data";
    $lang['previa_reserva_recurso_fecha_reserva'] = "Appointment data";
    $lang['previa_reserva_datos_hora_inicio'] = "Initial hour";
    $lang['previa_reserva_servicio_hora_fin'] = "End hour";
    $lang['previa_reserva_servicio_duracion'] = "Duration";
    $lang['previa_reserva_servicio_confirmar'] = "Fill the next 3 data field    ";
    $lang['previa_reserva_servicio_confirmar_3min'] = "Confirm the reservation(You have 3 minutes):";
    $lang['previa_reserva_servicio_confirmar_si'] = "YES";
    $lang['previa_reserva_servicio_confirmar_no'] = "NO";
    $lang['previa_reserva_servicio_comercio_nombre'] = "Name";
    $lang['previa_reserva_servicio_comercio_email'] = "E-Mail";
    $lang['previa_reserva_servicio_comercio_telefono'] = "Phone";
    $lang['previa_reserva_boton'] = "Confirm";
    $lang['previa_reserva_OK'] = "Reservation confirmed";
    $lang['previa_reserva_KO'] = "Reserva not confirmed.Maximun time to do the reservation has been exceeded";
    $lang['previa_reserva_materiales_nombre'] = "You can add those features to the reservation";
    $lang['previa_reserva_materiales_importe'] = "Amount";
    $lang['previa_reserva_materiales_seleccion'] = "Select";

//FORMULARIO VISTA_LISTA_HORAS.PHP
    $lang['lista_horas_servicio'] = "Selected service";
    $lang['lista_horas_minutos'] = " minutes";
    $lang['lista_horas_reservada'] = "Reserved";
    $lang['lista_horas_nd'] = "Not available";
    $lang['lista_horas_en_proceso'] = "Reservation in progress";


//FUNCION DE VALIDACION DE LA RESERVA
    $lang['validar_reserva_calendario_base'] = "Reservation is greater than calendar parameters";
    $lang['validar_reserva_calendario_festivos'] = "You have chosen a holiday";
    $lang['validar_reserva_hora'] = "The time of the booking is less than the current time";
    $lang['validar_reserva_fecha'] = "The booking date is less than the current time";
    $lang['validar_reserva_dia_habil'] = "There's a hourly/date constraint.Try another";
    $lang['validar_reserva_tiempo_disponible'] = "There is not enough time available for the selected service";
    $lang['validar_reserva_url'] = "Changed the URL. Try a new reservation";

    //FORMULARIO VISTA_ANULAR_RESERVA.PHP
    $lang['anular_fecha'] = "Date";
    $lang['anular_horaini'] = "Initial hour";
    $lang['anular_horafi'] = "End hour";
    $lang['anular_duracion'] = "Duration";
    $lang['anular_nombre'] = "Name";
    $lang['anular_recurso'] = "Resource";
    $lang['anular_servicio'] = "Service";
    $lang['anular_direccion'] = "Address";
    $lang['anular_numero'] = "Number";
    $lang['anular_telefono'] = "Phone";
    $lang['anular_codpostal'] = "Zip";
    $lang['anular_cabecera_tabla'] = "My active Reservations";
    $lang['qr_titulo']="QR RESERVATION ID";