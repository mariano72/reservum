<?php


// VISTA COMPLETAR_FICHA_COMERCIO.PHP

$lang['completar_ficha_comercio_nombre']="Commerce name";
$lang['completar_ficha_comercio_cif']="Fiscal code";
$lang['completar_ficha_comercio_provincia']="State";
$lang['completar_ficha_comercio_poblacion']="City";
$lang['completar_ficha_comercio_direccion']="Address";
$lang['completar_ficha_comercio_numero']="Number";
$lang['completar_ficha_comercio_codigopostal']="Postal code";
$lang['completar_ficha_comercio_telefono']="Phone";
$lang['completar_ficha_comercio_telefono2']="2º phone";
$lang['completar_ficha_comercio_fax']="Fax";
$lang['completar_ficha_comercio_imagen']="Image";
$lang['completar_ficha_comercio_descripcion_corta']="Short description";
$lang['completar_ficha_comercio_descripcion']="Description";
$lang['completar_ficha_comercio_url_reserva']="URL that we send to our customers or we must add to our web";


$lang['completar_ficha_comercio_val_nombre']="Name";
$lang['completar_ficha_comercio_val_direcc']="Address";
$lang['completar_ficha_comercio_val_telf']="Phone number";
$lang['completar_ficha_comercio_val_cp']="Zip code";
$lang['completar_ficha_comercio_desc1']="Short description";
$lang['completar_ficha_comercio_desc2']="Description";

$lang['completar_ficha_comercio_cabecera']="Complete card";


