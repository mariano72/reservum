<?php

//FORMULARIO DE DATOS VISTA_ALTA_USUARIO.PHP
$lang['alta_usuario_nombre'] = "Name";
$lang['alta_usuario_email'] = "E-Mail";
$lang['alta_usuario_email2'] = "Repeat E-Mail";
$lang['alta_usuario_password'] = "Password";
$lang['alta_usuario_password2'] = "Repeat password";
$lang['alta_usuario_telefono'] = "Phone number";
$lang['alta_usuario_mail_duplicado'] = "User's E-mail duplicate.Try another one";
$lang['alta_usuario_conf_ok'] = "New user added correctly";
$lang['alta_usuario_conf_ko'] = "New user not added correctly.Please try again";
$lang['recuperar_email_duplicado'] = "The E-mail exists,try another one.";
$lang['alta_usuario_idioma'] = "Language";
$lang['alta_usuario_boton_alta'] = "Nuevo usuario";

//FORMULARIO_RECUPERAR_PASSWORD

$lang['recuperar_pass_email'] = "E-mail";
$lang['recuperar_pass_OK'] = "New password sended";
$lang['recuperar_password_KO'] = "E-mail not exists";
$lang['recuperar_password_texto_mail'] = "Your new password is ";
