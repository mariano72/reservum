<?php


    //FORMULARIO DE DATOS VISTA_RESERVAR

    $lang['reservar_cabecera'] = "New reservation";
    $lang['reservar_cabecera_tabla'] = "Last reservations";
    $lang['reservar_fecha'] = "Date";
    $lang['reservar_horaini'] = "Init. hour";
    $lang['reservar_horafi'] = "End hour";
    $lang['reservar_duracion'] = "Duration";
    $lang['reservar_nombre'] = "Name";
    $lang['reservar_recurso'] = "Resource";
    $lang['reservar_servicio'] = "Service";
    $lang['reservar_direccion'] = "Address";
    $lang['reservar_numero'] = "Number";
    $lang['reservar_telefono'] = "Phone number";
    $lang['reservar_codpostal'] = "Zip code";

    $lang['reservar_busca_comercio'] = "Commerce & club search";
    $lang['reservar_busca_provincia'] = "State";
    $lang['reservar_busca_municipio'] = "City";
    $lang['reservar_link_reservar'] = "New reservation in";

    $lang['reservar_nombre_tabla'] = "Club name";
    $lang['reservar_descripcion'] = "Description";


    //FORMULARIO VISTA_ANULAR_RESERVA.PHP
    $lang['anular_fecha'] = "Date";
    $lang['anular_horaini'] = "Initial hour";
    $lang['anular_horafi'] = "End hour";
    $lang['anular_duracion'] = "Duration";
    $lang['anular_nombre'] = "Name";
    $lang['anular_recurso'] = "Resource";
    $lang['anular_servicio'] = "Service";
    $lang['anular_direccion'] = "Address";
    $lang['anular_numero'] = "Number";
    $lang['anular_telefono'] = "Phone";
    $lang['anular_codpostal'] = "Zip";
    $lang['anular_anular'] = "Cancel";
    $lang['anular_mensaje_validacion'] = "Cancel";
    $lang['anular_mensaje_ok'] = "Reserve canceled.";
    $lang['anular_boton'] = "Cancel";
    $lang['anular_mensaje_error'] = "You must select a reservation";
    $lang['anular_cabecera'] = "Cancel reservation";
    $lang['anular_cabecera_tabla'] = "Reservations that can be cancelled";