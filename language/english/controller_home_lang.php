<?php

//FORMULARIO DE DATOS VISTA_HOME.PHP
$lang['home_cabecera']="PUT ON-LINE YOUR RESOURCES AND YOUR SERVICES AND LET RESERVUM DO THE REST";
$lang['home_paso1']="Enter your resources e.g. a tennis court";
$lang['home_paso2']="Enter your services e.g. 1 hour of tennis game";
$lang['home_paso3']="Manage calendar and schedule of services to be offered";
$lang['home_paso4']="Excellent. Now you can offer your online reservations";
$lang['home_mensaje']="Reservum, the reservations manager that saves labor, occupation and allows optimizing its reserves are open 24 hours a día.Contract from 70 euros per year.";
$lang['home_caracteristica1']="Manage reservations of tennis ,padel, squash,soccer,basketball courts.";
$lang['home_caracteristica2']="Manage reservations of massage rooms ,chiropractor, physiotherapist.";
$lang['home_caracteristica3']="Give your customers the opportunity to book on-line.For FREE!!";
$lang['home_caracteristica4']="Fully compatible with tablets and smartphones.";
$lang['home_caracteristica5']="Open 24 * 7 * 365 for reservations.";
$lang['home_boton1']="I'm a commerce,sign up now for free";
$lang['home_boton2']="How it works?";
$lang['home_boton3']="I'm a user,I want to RESERVE";
$lang['home_banner2_h1']="CONNECT TO THE WORLD.I WANT TO BE YOUR PARTNER.¡¡60 FREE DAYS!!.REGISTER NOW.";
$lang['home_banner2_h2']="Pay with Paypal,credit card or bank transfer";

$lang['home_estadisticas_cabecera']="Join us";
$lang['home_estadisticas_miles']="Thousands";
$lang['home_estadisticas_est1']=" of users want to reserve";
$lang['home_estadisticas_est2']=" of commerces waiting reservations";
$lang['home_estadisticas_est3']=" of reservations in progress";

$lang['home_formulario_cabecera']="Do you have any questions? Contact us,in 24 hours your doubts will be resolved";
$lang['home_estadisticas_nombre']="Name";
$lang['home_estadisticas_email']="E-Mail";
$lang['home_estadisticas_telf']="Phone number";
$lang['home_estadisticas_mensaje']="Message";

$lang['home_video_h3']="How it works";
$lang['home_video_desc']="Take a look of this video and you know";
$lang['home_videos_presentacion_enlace']="http://www.youtube.com/embed/5LNy6_CfemE";

$lang['home_contacto_enviado']="E-mail sent OK";

//PANTALLA VIDEOS DE LA HOME
$lang['home_videos_1_video']="How to add a massage room and the services that we offer";
$lang['home_videos_1_video_enlace']="http://www.youtube.com/embed/Ie_zm6LPahA";
$lang['home_videos_2_video']="How to add constraints to the calendar";
$lang['home_videos_2_video_enlace']="http://www.youtube.com/embed/USvKj2dyDyo";

//PANTALLA HOME MASAJES
$lang['home_masaje_cabecera']="PUT ON-LINE YOUR RESOURCES AND YOUR SERVICES AND LET RESERVUM DO THE REST";
$lang['home_masaje_paso1']="Enter your resources e.g. a massage room";
$lang['home_masaje_paso2']="Enter your services e.g. 1 hour of back massage";
$lang['home_masaje_paso3']="Manage calendar and schedule of services to be offered";
$lang['home_masaje_paso4']="Excellent. Now you can offer your online reservations";
$lang['home_masaje_mensaje']="Reservum, the reservations manager that saves labor, occupation and allows optimizing your reserves are open 24 hours a día.Contract from 70 euros per year.";

$lang['home_masaje_caracteristica2']="Manage reservations of massage rooms ,chiropractor, physiotherapist.";
$lang['home_masaje_caracteristica3']="From 70 euros a year to forget your phone and give your customers the opportunity to book on-line.";
$lang['home_masaje_caracteristica4']="Fully compatible with tablets and smartphones.";
$lang['home_masaje_caracteristica5']="Open 24 * 7 * 365 for reservations.";
$lang['home_masaje_boton1']="I'm a commerce,sign up now for free";
$lang['home_masaje_boton2']="How it works?";
$lang['home_masaje_boton3']="I'm a user,I want to RESERVE";
$lang['home_masaje_banner2_h1']="CONNECT TO THE WORLD.I WANT TO BE YOUR PARTNER.¡¡60 FREE DAYS!!.REGISTER NOW.";
$lang['home_masaje_banner2_h2']="Pay with Paypal,credit card or bank transfer";

//PANTALLA  DE LA HOME
$lang['ficha_comercio_quiero_reservar']="I want to reserve a service";
$lang['ficha_comercio_datos_contacto']="Contact data";
$lang['ficha_comercio_nombre']="Commerce name";
$lang['ficha_comercio_cif']="Fiscal code";
$lang['ficha_comercio_provincia']="State";
$lang['ficha_comercio_poblacion']="City";
$lang['ficha_comercio_direccion']="Address";
$lang['ficha_comercio_numero']="Number";
$lang['ficha_comercio_codigopostal']="Postal code";
$lang['ficha_comercio_email']="E-mail";
$lang['ficha_comercio_telefono']="Phone";
$lang['ficha_comercio_telefono2']="2º phone";
$lang['ficha_comercio_fax']="Fax";
$lang['ficha_comercio_imagen']="Image";
$lang['ficha_comercio_descripcion_corta']="Short description";
$lang['ficha_comercio_descripcion']="Description";
$lang['ficha_comercio_url_reserva']="URL that we send to our customers or we must add to our web";

$lang['ficha_comercio_ser_catalogo']="Sevice list";
$lang['ficha_comercio_ser_nombre']="Name";
$lang['ficha_comercio_ser_descripcion']="Description";
$lang['ficha_comercio_ser_duracion']="Duration";
$lang['ficha_comercio_ser_precio']="Rate";


//PUNTOS DE MENU COMUNES A TODAS LAS PANTALLAS
$lang['menu_comun_pantallas_home']="Home";
$lang['menu_comun_pantallas_iniciar']="Login";
$lang['menu_comun_pantallas_videos']="Videos";
$lang['menu_comun_pantallas_masajes']="Massages";
$lang['menu_comun_pantallas_faq']="FAQ";
$lang['menu_comun_pantallas_contactar']="Contact us";
$lang['menu_comun_pantallas_salir']="Exit";

//MENUS DE USUARIO QUE PUEDEN APLICAR A TODAS LAS PANTALLAS
//MENU GENERAL COMERCIO
$lang['menu_comercio_comun_reservar']="Customer's reservation";
$lang['menu_comercio_comun_anular']="Cancel customer reservation";
$lang['menu_comercio_comun_agenda']="Day diary";
$lang['menu_comercio_comun_adm_recursos']="Admin Resources";
$lang['menu_comercio_comun_adm_servicios']="Admin Services";
$lang['menu_comercio_comun_adm_restricciones']="Admin Constraints";
$lang['menu_comercio_comun_estadisticas']="Statistics";
$lang['menu_comercio_comun_ficha']="Commerce card";
$lang['menu_comercio_comun_inicio']="Home";
$lang['menu_comercio_comun_camb_pass']="Change password";
$lang['menu_comercio_comun_adm_tarifas']="Admin rates";
$lang['menu_comercio_comun_adm_materiales']="Admin equipments";
$lang['menu_comercio_comun_mis_clientes']="My customers";

//MENU RECURSOS
$lang['menu_comercio_recursos_alta']="New resource";
$lang['menu_comercio_recursos_modificar']="Modify resource";
$lang['menu_comercio_recursos_menu']="Go to home";

//MENU SERVICIOS
$lang['menu_comercio_servicios_alta']="New service";
$lang['menu_comercio_servicios_modificar']="Modify service";
$lang['menu_comercio_servicios_relacionar']="Show services to link";
$lang['menu_comercio_servicios_menu']="Go to home";


//MENU RESTRICCIONES
$lang['menu_comercio_restricciones_alta']="New constraint";
$lang['menu_comercio_restricciones_modificar']="Modify constraint";
$lang['menu_comercio_restricciones_menu']="Go to home";


//MENU USUARIOS
$lang['menu_usuario_comun_reservar']="Reservation";
$lang['menu_usuario_comun_anular']="Cancel reservation";
$lang['menu_usuario_comun_inicio']="Home";
$lang['menu_usuario_comun_camb_pass']="Change password";

//MENU FACTURAS
$lang['menu_comercio_comun_facturas']="Budget list";


//MENU TARIFAS
$lang['menu_comercio_tarifas_alta']="New rate";
$lang['menu_comercio_tarifas_lista']="Show rates";
$lang['menu_comercio_tarifas_menu']="Go to home";

//MENU MATERIALES
$lang['menu_comercio_materiales_alta']="New equipment - extras";
$lang['menu_comercio_materiales_lista']="Show equipments - extras";
$lang['menu_comercio_materiales_menu']="Go to home";