<<?php

//FORMULARIO DE DATOS VISTA_ALTA_COMERCIO

$lang['alta_comercio_nombre'] = "Commerce name";
$lang['alta_comercio_nifcif'] = "Fiscal identification number";
$lang['alta_comercio_email'] = "E-Mail";
$lang['alta_comercio_email2'] = "Repeat E-Mail";
$lang['alta_comercio_password'] = "Password";
$lang['alta_comercio_password2'] = "Repeat password";
$lang['alta_comercio_desc_corta'] = "Short Description";
$lang['alta_comercio_descripcion'] = "Description";
$lang['alta_comercio_pais'] = "Country";
$lang['alta_comercio_idioma'] = "Language";
$lang['alta_comercio_tarifa'] = "Plan";
$lang['alta_comercio_telefono'] = "Phone number";
$lang['alta_comercio_telefono2'] = "2º phone number";
$lang['alta_comercio_fax'] = "Fax number";

$lang['alta_comercio_provincia'] = "State";
$lang['alta_comercio_municipio'] = "City";
$lang['alta_comercio_tipovia'] = "Type";
$lang['alta_comercio_calle'] = "Street name";
$lang['alta_comercio_numero'] = "Number";
$lang['alta_comercio_codigopostal'] = "Zip code";

$lang['alta_comercio_mail_duplicado'] = "User E-Mail exists, try another";
$lang['alta_usuario_conf_ok'] = "New user added correctly";
$lang['alta_usuario_conf_ko'] = "New user not added correctly , try to add again";
$lang['alta_usuario_mail_duplicado'] = "User's E-mail duplicate.Try another one";
$lang['alta_comercio_busca_municipio'] = "Select one";
$lang['alta_comercio_boton_alta'] = "New user";
