<?php
//FORMULARIO LISTA_DE_TARIFAS
$lang['lista_tarifas_lista'] = "Rate list";
$lang['lista_tarifas_nombre'] = "Name";
$lang['lista_tarifas_fecha_ini'] = "Initial date";
$lang['lista_tarifas_fecha_fin'] = "End date";
$lang['lista_tarifas_hora_ini'] = "Initial hour";
$lang['lista_tarifas_hora_fin'] = "End hour";
$lang['lista_tarifas_nombre_ser'] = "Service name";
$lang['lista_tarifas_importe'] = "Amount";
$lang['lista_tarifas_iva'] = "TAX";
$lang['lista_tarifas_importetotal'] = "Total amount";
$lang['lista_tarifas_estado'] = "State";
$lang['lista_tarifas_cabecera'] = "Admin rates";

    //FORMULARIO ALTA_DE_TARIFAS
$lang['alta_tarifa_fecini'] = "Initial date";
$lang['alta_tarifa_fecfin'] = "End date";
$lang['alta_tarifa_horaini'] = "Initial hour";
$lang['alta_tarifa_horafin'] = "End hour";
$lang['alta_tarifa_nombre'] = "Rate";
$lang['alta_tarifa_estado'] = "Status";
$lang['alta_tarifa_servicio'] = "Service";
$lang['alta_tarifa_importe'] = "Amount";
$lang['alta_tarifa_iva'] = "Taxes %";
$lang['alta_tarifa_importetotal'] = "Total amount";
$lang['alta_tarifa_seleccion_dias'] = "Select the days for application rate";
$lang['alta_tarifa_lunes'] = "Monday";
$lang['alta_tarifa_martes'] = "Tuesday";
$lang['alta_tarifa_miercoles'] = "Wednesday";
$lang['alta_tarifa_jueves'] = "Thursday";
$lang['alta_tarifa_viernes'] = "Friday";
$lang['alta_tarifa_sabado'] = "Saturday";
$lang['alta_tarifa_domingo'] = "Sunday";
$lang['alta_tarifa_boton'] = "Add / Modify rate";
$lang['alta_tarifas_cabecera'] = "Add / Modify rates";

$lang['alta_tarifa_ok'] = "New rate added OK";
$lang['actualizar_tarifa_ok'] = "New rate updated correctly";
    

//VALIDACIONES CALLBACK
$lang['alta_tarifa_compara_fechas'] = "Initial date must be greater than end date";
$lang['alta_tarifa_compara_horas'] = "Initial hour can't be greater or equal than end hour";
$lang['alta_tarifa_check_dias'] = "You must select almost 1 day of the week";
$lang['alta_tarifa_validar_insercion'] = "Incompatible rate in the system.<BR>Check the dates,hours and days of the new rate.<br>
                                          Can't be 2 rates sharing the same dates,hours or days";