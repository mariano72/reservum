<?php


//FORMULARIO DE DATOS  VISTA_LISTA_RECURSO_RESTRICCION.PHP
$lang['lista_recurso_rec_lista'] = "Resource list";
$lang['lista_recurso_rec_nombre'] = "Name";
$lang['lista_recurso_rec_estado'] = "Status";
$lang['lista_recurso_rec_lista_restr'] = "Constraint list";
$lang['lista_recurso_rec_lista_numero'] = "Nº";
$lang['lista_recurso_rec_lista_fecini'] = "Init. date";
$lang['lista_recurso_rec_lista_fecfin'] = "End date";
$lang['lista_recurso_rec_lista_horini'] = "Init hour";
$lang['lista_recurso_rec_lista_horfin'] = "End hour";
$lang['lista_recurso_rec_lista_lun'] = "Monday";
$lang['lista_recurso_rec_lista_mar'] = "Tuesday";
$lang['lista_recurso_rec_lista_mie'] = "Wednesday";
$lang['lista_recurso_rec_lista_jue'] = "Thursday";
$lang['lista_recurso_rec_lista_vie'] = "Friday";
$lang['lista_recurso_rec_lista_sab'] = "Saturday";
$lang['lista_recurso_rec_lista_dom'] = "Sunday";
$lang['lista_recurso_rec_cabecera'] = "Admin constraints";

//FORMULARIO DE DATOS  VISTA_ALTA_RESTRICCIONES.PHP
//COMPARTIMOS VARIOS CAMPOS CONEL FORMULARIO VISTA_LISTA_RECURSO_RESTRICCION.PHP ASÍ
// QUE SÓLO PONDREMOS LOS DIFERENTES

$lang['alta_restricciones_dias_apl'] = "Constraint application days";
$lang['alta_restricciones_dias_lista_rec'] = "Resource constraint application for";
$lang['alta_restricciones_dias_nombre'] = "Name";
$lang['alta_restricciones_dias_estado'] = "Status";
$lang['lista_recurso_rec_dias_fecini'] = "Initial date";
$lang['lista_recurso_rec_dias_fecfin'] = "End date";
$lang['alta_restricciones_dias_restriccion'] = "Apply constraint";
    
$lang['alta_restricciones_compara_horas'] = "Initial hour must be greater or equal to end hour";
$lang['alta_restricciones_compara_fechas'] = "Initial date must be greater or equal to end hour";
$lang['alta_restricciones_check_dias']="You must select one day to apply the constraint";

$lang['modifica_restricciones_ok']="Constraint modified OK";
$lang['alta_restricciones_ok']="Constraint added OK";

$lang['alta_restricciones_cabecera'] = "Add new constraints";
$lang['alta_restricciones_modif_cabecera'] = "Modify constraints";

//VALIDACIONES CALLBACK
$lang['alta_restricciones_compara_fechas'] = "Initial date must be greater than end date";
$lang['alta_restricciones_compara_horas'] = "Initial hour can't be greater or equal than end hour";