<?php


//FORMULARIO DE DATOS  VISTA_ADMIN_SERVICIOS.PHP
    	 	 	 
$lang['alta_servicios_nombre'] = "Name";
$lang['alta_servicios_descripcion'] = "Description";
$lang['alta_servicios_Duracion'] = "Duration";
$lang['alta_servicios_Importe'] = "Amount";
$lang['alta_servicios_IVA'] = "TAX";
$lang['alta_servicios_Importe_total'] = "Total amount";
$lang['alta_servicios_cabecera'] = "Service list";
$lang['alta_servicios_cabecera_alta'] = "Add / modify services";

//FORMULARIO DE DATOS  VISTA_RELACIONAR_SERVICIOS.PHP
$lang['relacionar_servicios_escoge'] = "Service to link";
$lang['relacionar_servicios_cabecera'] = "Link services";

//FORMULARIO DE DATOS  VISTA_RELACIONAR_SERVICIOS_RECURSOS.PHP
$lang['relacionar_servicios_rec_escoge'] = "Service to link";
$lang['relacionar_servicios_rec_nombre'] = "Name";
$lang['relacionar_servicios_rec_estado'] = "Status";
$lang['relacionar_servicios_rec_relacion'] = "Link resource and service";

