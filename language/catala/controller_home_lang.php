<?php

//FORMULARIO DE DATOS VISTA_HOME.PHP
$lang['home_cabecera']="RENDIBILITZA LES INSTAL.LACIONS ON-LINE<BR>I DEIXA QUE RESERVUM FACI LA RESTA";
$lang['home_paso1']="Introdueix les instal.lacions que vols mostrar on-line";
$lang['home_paso2']="Introdueix els serveis que vols mostrar on-line";
$lang['home_paso3']="Gestiona la agenda i els horaris dels serveis que oferiràs";
$lang['home_paso4']="Perfecte.Ja pots oferir les teves reserves <br>on-line";
$lang['home_mensaje']="Reservum, el gestor de reserves que l'estalvia feina, optimitza ocupació i permet que les seves reserves estiguin obertes les 24 hores del dia. Contracti des de 70 euros al any.";
$lang['home_caracteristica1']="Gestionar reserves de pistes de tennis, pàdel, squash, futbol, basket";
$lang['home_caracteristica2']="Gestionar reserves de sales de massatge, quiropràctics, fisioterapeutes, centres de estética.";
$lang['home_caracteristica3']="Ofereix GRATIS als teus clients la possibilitat de reservar on-line.";
$lang['home_caracteristica4']="Totalment compatible amb tablets i smartphones.";
$lang['home_caracteristica5']="Obert 24*7*365 per a rebre les seves reserves.";
$lang['home_boton1']="Soc una empresa,vull donar-me d'alta JA";
$lang['home_boton2']="¿Com funciona?";
$lang['home_boton3']="Soc un usuari,vull RESERVAR";
$lang['home_banner2_h1']="¡CONNECTAT AL MON!.DEIXAM SER EL TEU PARTNER I A MÉS A MÉS TINDRÀS 1 ANY GRATUIT";
$lang['home_banner2_h2']="Pagui amb Paypal, targeta de crèdit o transfèrencia bancària";

$lang['home_estadisticas_cabecera']="Uneix-te a nosaltres";
$lang['home_estadisticas_miles']="Milers";
$lang['home_estadisticas_est1']=" d'usuaris volen reservar";
$lang['home_estadisticas_est2']=" d'empreses esperant reserves";
$lang['home_estadisticas_est3']=" de reserves en curs";

$lang['home_formulario_cabecera']="¿Tens dubtes? Omple el següent formulari i en 24 horas et contestem";
$lang['home_estadisticas_nombre']="Nom";
$lang['home_estadisticas_email']="E-Mail";
$lang['home_estadisticas_telf']="telèfon";
$lang['home_estadisticas_mensaje']="Missatge";

$lang['home_video_h3']="Com funciona";
$lang['home_video_desc']="Mira aquest petit vídeo i ho sabràs al instant";
$lang['home_videos_presentacion_enlace']="http://www.youtube.com/embed/41SSVyoMdeQ";

$lang['home_contacto_enviado']="Missatge enviat correctament";

//PANTALLA VIDEOS DE LA HOME
$lang['home_videos_1_video']="Como donar d'alta una sala de massatge i els serveis que ofereix";
$lang['home_videos_1_video_enlace']="http://www.youtube.com/embed/Ie_zm6LPahA";
$lang['home_videos_2_video']="Como afegir restriccions horàries a les reserves";
$lang['home_videos_2_video_enlace']="http://www.youtube.com/embed/USvKj2dyDyo";

//PANTALLA HOME MASAJES
$lang['home_masaje_cabecera']="RENDIBILITZA LES TEVES INSTAL.LACIONS ON-LINE I DEIXA A RESERVUM LA RESTA";
$lang['home_masaje_paso1']="Introdueix les instal.lacions que vols mostrar on-line";
$lang['home_masaje_paso2']="Introdueix els serveis que vols mostrar on-line";
$lang['home_masaje_paso3']="Gestiona la agenda i els horaris dels serveis que oferiràs";
$lang['home_masaje_paso4']="Perfecte.Ja pots oferir els teus massatges <br>on-line";
$lang['home_masaje_mensaje']="Reservum, el gestor de reserves que t'estalvia feina, optimitza ocupació i permet que les teves reserves estiguin obertes les 24 hores del dia. Contracta des de 70 euros al any.";

$lang['home_masaje_caracteristica2']="Gestionar reserves de sales de massatge, quiropràctics, fisioterapeutes, centres de estética.";
$lang['home_masaje_caracteristica3']="Des de 70 euros al any oblidat del telèfon i ofereix als teus clients la possibilitat de reservar on-line.";
$lang['home_masaje_caracteristica4']="Totalment compatible amb tablets i smartphones.";
$lang['home_masaje_caracteristica5']="Obert 24*7*365 per a rebre les seves reserves.";
$lang['home_masaje_boton1']="Soc una empresa,vull donar-me d'alta JA";
$lang['home_masaje_boton2']="¿Com funciona?";
$lang['home_masaje_boton3']="Soc un usuari,vull RESERVAR";
$lang['home_masaje_banner2_h1']="¡CONNECTAT AL MON!.DEIXAM SER EL TEU PARTNER I A MÉS A MÉS TINDRÀS 60 DIES GRATUÏTS";
$lang['home_masaje_banner2_h2']="Pagui amb Paypal, targeta de crèdit o transfèrencia bancària";

//PANTALLA FICHA DEL COMERCIO
$lang['ficha_comercio_quiero_reservar']="Vull reservar un servei";
$lang['ficha_comercio_datos_contacto']="Dades de contacte";
$lang['ficha_comercio_nombre']="Nom comerç";
$lang['ficha_comercio_cif']="CIF";
$lang['ficha_comercio_provincia']="Província";
$lang['ficha_comercio_poblacion']="Població";
$lang['ficha_comercio_direccion']="Adreça";
$lang['ficha_comercio_numero']="Número";
$lang['ficha_comercio_codigopostal']="Codi postal";
$lang['ficha_comercio_email']="E-mail";
$lang['ficha_comercio_telefono']="Telèfon";
$lang['ficha_comercio_telefono2']="Telèfon 2";
$lang['ficha_comercio_fax']="Fax";
$lang['ficha_comercio_imagen']="Imatge";
$lang['ficha_comercio_descripcion_corta']="Descripció curta";
$lang['ficha_comercio_descripcion']="Descripció";
$lang['ficha_comercio_url_reserva']="URL que hem de facilitar als nostres clients o afagir a la nostra web";

$lang['ficha_comercio_ser_catalogo']="Catàleg de serveis";
$lang['ficha_comercio_ser_nombre']="Nom";
$lang['ficha_comercio_ser_descripcion']="Descripció";
$lang['ficha_comercio_ser_duracion']="Duració";
$lang['ficha_comercio_ser_precio']="Preu";


//PUNTOS DE MENU COMUNES A TODAS LAS PANTALLAS
$lang['menu_comun_pantallas_home']="Inici";
$lang['menu_comun_pantallas_iniciar']="Inici sessió";
$lang['menu_comun_pantallas_videos']="Videos";
$lang['menu_comun_pantallas_masajes']="Massatges";
$lang['menu_comun_pantallas_faq']="FAQ";
$lang['menu_comun_pantallas_contactar']="Contactar";
$lang['menu_comun_pantallas_salir']="Sortir";

//MENUS DE USUARIO QUE PUEDEN APLICAR A TODAS LAS PANTALLAS
//MENU GENERAL COMERCIO
$lang['menu_comercio_comun_reservar']="Reservar Cita Client";
$lang['menu_comercio_comun_anular']="Anul.lar Cita Client";
$lang['menu_comercio_comun_agenda']="Agenda del Dia";
$lang['menu_comercio_comun_adm_recursos']="Administrar instal.lacions";
$lang['menu_comercio_comun_adm_servicios']="Administrar Serveis";
$lang['menu_comercio_comun_adm_restricciones']="Administrar Restriccions";
$lang['menu_comercio_comun_estadisticas']="Estadístiques";
$lang['menu_comercio_comun_ficha']="Fitxa del comerç";
$lang['menu_comercio_comun_inicio']="Anar a inici";
$lang['menu_comercio_comun_camb_pass']="Canviar paraula pas";
$lang['menu_comercio_comun_adm_tarifas']="Administrar tarifes";
$lang['menu_comercio_comun_adm_materiales']="Administrar materials";
$lang['menu_comercio_comun_mis_clientes']="Els meus clients";
//MENU RECURSOS
$lang['menu_comercio_recursos_alta']="Alta instal.lació";
$lang['menu_comercio_recursos_modificar']="Modificar instal.lació";
$lang['menu_comercio_recursos_menu']="Torna a menú principal";

//MENU SERVICIOS
$lang['menu_comercio_servicios_alta']="Alta serveis";
$lang['menu_comercio_servicios_modificar']="Modificar serveis";
$lang['menu_comercio_servicios_relacionar']="Relacionar serveis";
$lang['menu_comercio_servicios_menu']="Torna a menú principal";

//MENU RESTRICCIONES
$lang['menu_comercio_restricciones_alta']="Alta restriccions";
$lang['menu_comercio_restricciones_modificar']="Modificar restriccions";
$lang['menu_comercio_restricciones_menu']="Torna a menú principal";


//MENU USUARIOS
$lang['menu_usuario_comun_reservar']="Reservar Cita";
$lang['menu_usuario_comun_anular']="Anul.lar Cita";
$lang['menu_usuario_comun_inicio']="Anar a inici";
$lang['menu_usuario_comun_camb_pass']="Canviar paraula pas";

//MENU FACTURAS
$lang['menu_comercio_comun_facturas']="Llista de factures";


//MENU TARIFAS
$lang['menu_comercio_tarifas_alta']="Alta tarifes";
$lang['menu_comercio_tarifas_lista']="Llista de tarifes";
$lang['menu_comercio_tarifas_menu']="Torna a menú principal";

//MENU MATERIALES
$lang['menu_comercio_materiales_alta']="Alta materials - extras";
$lang['menu_comercio_materiales_lista']="Llista de materials - extras";
$lang['menu_comercio_materiales_menu']="Torna a menú principal";