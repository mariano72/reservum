<?php
//FORMULARIO DE DATOS VISTA_ANULAR_CITA_CLIENTE.PHP

$lang['anular_cita_cabecera'] = "Anul.lar reserva";
$lang['anular_cita_fecha'] = "Data reserva";
$lang['anular_cita_hora_inicio'] = "Hora inici";
$lang['anular_cita_hora_fin'] = "Hora fi";
$lang['anular_cita_duracion'] = "Duració";
$lang['anular_cita_recurso'] = "Recurs";
$lang['anular_cita_servicio'] = "Servei";
$lang['anular_cita_nombre'] = "Nom usuari";
$lang['anular_cita_email'] = "E-mail";
$lang['anular_cita_telefono'] = "telèfon";
$lang['anular_cita_anular'] = "Anul.lar reserva";


//FORMULARIO DE DATOS VISTA_PREVIA_ANULAR_CITA_CLIENTE.PHP

$lang['previa_anular_cita'] = "Anul.lar cita del client";
$lang['previa_anular_mail'] = "E-mail";
$lang['previa_anular_telefono'] = "Telèfon";
$lang['previa_anular_fecha'] = "Data de reserva";
$lang['previa_anular_cabecera1'] = "Buscar per usuari";
$lang['previa_anular_cabecera2'] = "Buscar per data de reserva";
$lang['previa_anular_buscar'] = "Buscar reserves";
$lang['previa_anular_mensaje_error'] = "No s'han recuperat dades de la consulta";

//FORMULARIO DE VISTA_INICIO_COMERCIO.PHP
$lang['inicio_com_cabecera'] = "Cuadre de comandament resumit";
$lang['inicio_com_ult_res_fec'] = "La Última reserva ha estat el ";
$lang['inicio_com_ult_res_rea'] = "Va ser realitzada per ";
$lang['inicio_com_ult_res_curso'] = " reserves actualment en curs";
$lang['inicio_com_ult_res_mes'] = " reserves mensuals pendents d'atendre";
$lang['inicio_com_ult_res_total'] = " reserves totals pendents d'atendre";
$lang['inicio_com_ult_res_cab'] = "Últimes reserves realitzades";
$lang['inicio_com_ult_fecha'] = "Data servei";
$lang['inicio_com_ult_horaini'] = "Hora inici";
$lang['inicio_com_ult_horafin'] = "Hora fi";
$lang['inicio_com_ult_duracion'] = "Duració";
$lang['inicio_com_ult_usuario'] = "Usuari";
$lang['inicio_com_ult_servicio'] = "Servei";
$lang['inicio_com_ult_mail'] = "E-Mail";
$lang['inicio_com_ult_telef'] = "Telf";
$lang['inicio_com_prox'] = "Próximes reserves a atendre";

//FORMULARIO DE VISTA_AGENDA_DIA.PHP
$lang['agenda_dia_cabecera'] = "Agenda del dia";
$lang['agenda_dia_dia'] = "Dia a consultar";
$lang['agenda_dia_hora_inicio'] = "Hora inici";
$lang['agenda_dia_hora_fin'] = "Hora fi";
$lang['agenda_dia_servicio'] = "Servei";
$lang['agenda_dia_usuario'] = "Usuari";
$lang['agenda_dia_email'] = "E-mail";
$lang['agenda_dia_telefono'] = "telèfon";
$lang['agenda_dia_seleccionar'] = "Seleccionar dia";
$lang['agenda_dia_ver_agenda'] = "Veure agenda";

//FORMULARIO DE VISTA_MIS_CLIENTES.PHP
$lang['mis_clientes_cabecera'] = "Els meus clients";
$lang['mis_clientes_listado'] = "Llista de clients";
$lang['mis_clientes_usuario'] = "Nom usuari";
$lang['mis_clientes_mail'] = "E-mail";
$lang['mis_clientes_telefono'] = "Telèfon";

//FORMULARIO CONSULTAR_RESERVA

$lang['consultar_reserva_cabecera'] = "Consultar reserva";
$lang['consultar_reserva_datos_generales'] = "Dades de la reserva";
$lang['consultar_reserva_recurso_nombre'] = "Nom instal.lació";
$lang['consultar_reserva_servicio_nombre'] = "Servei";
$lang['consultar_reserva_recurso_fecha_reserva'] = "Data reserva";
$lang['consultar_reserva_datos_hora_inicio'] = "Hora inici";
$lang['consultar_reserva_datos_hora_fin'] = "Hora fi";
$lang['consultar_reserva_duracion'] = "Duració";
$lang['consultar_reserva_datos_personales'] = "Dades del usuari";
$lang['consultar_reserva_nombre_usuario']="Nom";
$lang['consultar_reserva_email_usuario']="E-Mail";
$lang['consultar_reserva_servicio_telefono_usuario']="Telèfon";
$lang['consultar_reserva_importe']="Import";
$lang['consultar_reserva_materiales_descripcion']="Materials i extres";
$lang['consultar_reserva_materiales_importe']="Import Materials";
$lang['consultar_reserva_materiales_tarifa']="Tarifa";

