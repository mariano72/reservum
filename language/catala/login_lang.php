<?php

//FORMULARIO DE DATOS VISTA_LOGIN.PHP
$lang['login_alta_nombre_pantalla'] = "Accés al sistema";
$lang['login_alta_usuarios'] = "Soc un usuari i vull reservar un servei";

$lang['login_alta_mail'] = "Introdueixi el seu E-mail";
$lang['login_alta_password'] = "Inrodueixi la paraula de pas";
$lang['login_alta_comercios'] = "Soc una empresa i vull administrar els meus serveis";

$lang['login_alta_log_incorrecto'] = "El login o el password son incorrectes";
$lang['login_alta_nuevo_user'] = "Vull donar d'alta un nou usuari";
$lang['login_alta_pass_olvidado'] = "He oblidat la meva paraula de pas";
