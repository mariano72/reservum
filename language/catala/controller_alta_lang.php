<?php

//FORMULARIO DE DATOS VISTA_ALTA_USUARIO.PHP
$lang['alta_usuario_nombre'] = "Nom";
$lang['alta_usuario_email'] = "E-Mail";
$lang['alta_usuario_email2'] = "Repetir E-Mail";
$lang['alta_usuario_password'] = "Paraula de pas";
$lang['alta_usuario_password2'] = "Repetir Paraula de pas";
$lang['alta_usuario_telefono'] = "telèfon";
$lang['alta_usuario_mail_duplicado'] = "Ja existeix un usuari amb aquest E-mail";
$lang['alta_usuario_conf_ok'] = "Procés d'alta finalitzat correctament";
$lang['alta_usuario_conf_ko'] = "Procés d'alta no completat correctament.Torni a intentar registrar-se";
$lang['recuperar_email_duplicado'] = "El E-mail ja existeix";
$lang['alta_usuario_idioma'] = "Idioma";
$lang['alta_usuario_boton_alta'] = "Nou usuari";

//FORMULARIO_RECUPERAR_PASSWORD


$lang['recuperar_pass_email'] = "Introdueixi el seu E-mail";
$lang['recuperar_pass_OK'] = "S'ha enviat al seu correu el nou password";
$lang['recuperar_password_KO'] = "Paraula de pas incorrecte";
$lang['recuperar_password_texto_mail'] = "La nova paraula de pas es ";