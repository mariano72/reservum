<?php


//FORMULARIO DE DATOS  VISTA_LISTA_RECURSO_RESTRICCION.PHP
$lang['lista_recurso_rec_lista'] = "LLista de recursos";
$lang['lista_recurso_rec_nombre'] = "Nom";
$lang['lista_recurso_rec_estado'] = "Estat";
$lang['lista_recurso_rec_lista_restr'] = "LLista de restriccions";
$lang['lista_recurso_rec_lista_numero'] = "Nº";
$lang['lista_recurso_rec_lista_fecini'] = "Dat.Inici";
$lang['lista_recurso_rec_lista_fecfin'] = "Dat.Fi";
$lang['lista_recurso_rec_lista_horini'] = "Hora inici";
$lang['lista_recurso_rec_lista_horfin'] = "Hora fi";
$lang['lista_recurso_rec_lista_lun'] = "Dilluns";
$lang['lista_recurso_rec_lista_mar'] = "Dimarts";
$lang['lista_recurso_rec_lista_mie'] = "Dimecres";
$lang['lista_recurso_rec_lista_jue'] = "Dijous";
$lang['lista_recurso_rec_lista_vie'] = "Divendres";
$lang['lista_recurso_rec_lista_sab'] = "Dissabte";
$lang['lista_recurso_rec_lista_dom'] = "Diumenge";
$lang['lista_recurso_rec_cabecera'] = "Administrar restriccions";

//FORMULARIO DE DATOS  VISTA_ALTA_RESTRICCIONES.PHP
//COMPARTIMOS VARIOS CAMPOS CONEL FORMULARIO VISTA_LISTA_RECURSO_RESTRICCION.PHP ASÍ
// QUE SÓLO PONDREMOS LOS DIFERENTES

$lang['alta_restricciones_dias_apl'] = "Dies d'aplicació de la restricció";
$lang['alta_restricciones_dias_lista_rec'] = "Llista de recursos a aplicar la restricció";
$lang['alta_restricciones_dias_nombre'] = "Nom";
$lang['alta_restricciones_dias_estado'] = "Estat";
$lang['lista_recurso_rec_dias_fecini'] = "Data Inici";
$lang['lista_recurso_rec_dias_fecfin'] = "Data Fi";
$lang['alta_restricciones_dias_restriccion'] = "Aplicar restricció";

$lang['alta_restricciones_compara_horas'] = "Hora inici no pot ser superior o igual a hora fi";
$lang['alta_restricciones_compara_fechas'] = "Data inici no pot ser superior o igual a data fi";
$lang['alta_restricciones_check_dias']="Ha de seleccionar al menys 1 dia de la setmana per aplicar la restricció";

$lang['modifica_restricciones_ok']="Restricció modificada correctament";
$lang['alta_restricciones_ok']="Restricció afegida correctament";

$lang['alta_restricciones_cabecera'] = "Alta de restriccions";
$lang['alta_restricciones_modif_cabecera'] = "Modificar restriccions";

//VALIDACIONES CALLBACK
$lang['alta_restricciones_compara_fechas'] = "Data inici no pot ser major  a data fi";
$lang['alta_restricciones_compara_horas'] = "Hora inici no pot ser major o igual a hora fi";
