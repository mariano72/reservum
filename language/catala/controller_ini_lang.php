<?php

              
//FORMULARIO DE DATOS VISTA_RESERVAR

$lang['reservar_cabecera'] = "Reservar cita";
$lang['reservar_cabecera_tabla'] = "Les meves darreres reserves";
$lang['reservar_fecha'] = "Data";
$lang['reservar_horaini'] = "Hora inici";
$lang['reservar_horafi'] = "Hora fi";
$lang['reservar_duracion'] = "Duració";
$lang['reservar_nombre'] = "Nombre";
$lang['reservar_recurso'] = "Recurs";
$lang['reservar_servicio'] = "Servei";
$lang['reservar_direccion'] = "Direcció";
$lang['reservar_numero'] = "Numero";
$lang['reservar_telefono'] = "Telefon";
$lang['reservar_codpostal'] = "Codi postal";

$lang['reservar_busca_comercio'] = "Recerca de clubs i serveis";
$lang['reservar_busca_provincia'] = "Provincia";
$lang['reservar_busca_municipio'] = "Municipi";
$lang['reservar_link_reservar'] = "Torna a reservar a";

$lang['reservar_nombre_tabla'] = "Nom del club o comerç";
$lang['reservar_descripcion'] = "Descripció";

//FORMULARIO VISTA_ANULAR_RESERVA.PHP
$lang['anular_fecha'] = "Data";
$lang['anular_horaini'] = "Hora inici";
$lang['anular_horafi'] = "Hora fi";
$lang['anular_duracion'] = "Duració";
$lang['anular_nombre'] = "Nom";
$lang['anular_recurso'] = "Recurs";
$lang['anular_servicio'] = "Servei";
$lang['anular_direccion'] = "Direcció";
$lang['anular_numero'] = "Número";
$lang['anular_telefono'] = "Telefon";
$lang['anular_codpostal'] = "CP";
$lang['anular_anular'] = "Anul.lar";
$lang['anular_mensaje_validacion'] = "Anul.lar";
$lang['anular_mensaje_ok']="La reserva s'ha anul.lat correctamen.";
$lang['anular_boton']="Anul.lar reserva";
$lang['anular_mensaje_error']="Has de seleccionar una reserva";
$lang['anular_cabecera'] = "Anul.lar cita";
$lang['anular_cabecera_tabla'] = "Llista de reserves que podem anul.lar";

