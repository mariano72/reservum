<?php
//FORMULARIO DE DATOS VISTA_LISTA_FACTURAS.PHP

$lang['lista_facturas_cabecera'] = "Llista de factures";
$lang['lista_facturas_numero'] = "Nº factura";
$lang['lista_facturas_descripcion'] = "Descripció";
$lang['lista_facturas_tarifa'] = "Tarifa";
$lang['lista_facturas_estado'] = "Estat";
$lang['lista_facturas_periodo'] = "Periode factura";
$lang['lista_facturas_fecha'] = "Data factura";
$lang['lista_facturas_importe'] = "Import";
$lang['lista_facturas_iva'] = "Iva";
$lang['lista_facturas_importetotal'] = "Import total";


//FORMULARIO DE DATOS VISTA_DETALLE_FACTURA.PHP

$lang['detalle_factura_cabecera'] = "Llista de factures";
$lang['detalle_factura_numero'] = "Nº factura";
$lang['detalle_factura_descripcion'] = "Descripció";
$lang['detalle_factura_tarifa'] = "Tarifa";
$lang['detalle_factura_estado'] = "Estat";
$lang['detalle_factura_periodo'] = "Periode factura";
$lang['detalle_factura_fecha'] = "Data factura";
$lang['detalle_factura_importe'] = "Import";
$lang['detalle_factura_iva'] = "Iva";
$lang['detalle_factura_importetotal'] = "Import total";
$lang['detalle_factura_pagar'] = "Pagar factura";
$lang['detalle_factura_concepto'] = "Concepte";
$lang['detalle_factura_fecha_pago'] = "Data pagament";
$lang['detalle_factura_paypal_gateway'] = "Nº de referéncia Paypal";