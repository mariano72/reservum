<?php

//FORMULARIO LISTA_DE_MATERIALES
$lang['lista_materiales_lista'] = "Llista de materials";
$lang['lista_materiales_nombre'] = "Nom";
$lang['lista_materiales_nombre_ser'] = "Nom del servei";
$lang['lista_materiales_importe'] = "Import";
$lang['lista_materiales_estado'] = "Estat";

//FORMULARIO ALTA_DE_materiales

$lang['alta_materiales_descripcion'] = "Descripció";
$lang['alta_materiales_estado'] = "Estat";
$lang['alta_materiales_servicio'] = "Servei";
$lang['alta_materiales_importe'] = "Import";
$lang['alta_materiales_boton'] = "Alta / Modificar material";
$lang['alta_materiales_cabecera_admin'] = "Administrar materials";
$lang['alta_materiales_cabecera'] = "Alta / Modificar materials";

$lang['alta_tarifa_ok'] = "El material s'ha donat d'alta correctament";
$lang['actualizar_tarifa_ok'] = "El material s'ha actualizat correctament";




