<?php

//FORMULARIO DE DATOS VISTA_ALTA_COMERCIOS

$lang['alta_comercio_nombre'] = "Nom del comerç";
$lang['alta_comercio_nifcif'] = "NIF /CIF";
$lang['alta_comercio_email'] = "E-Mail";
$lang['alta_comercio_email2'] = "Repetir E-Mail";
$lang['alta_comercio_password'] = "Paraula de pas";
$lang['alta_comercio_password2'] = "Repetir paraula de pas";
$lang['alta_comercio_desc_corta'] = "Descripció curta";
$lang['alta_comercio_descripcion'] = "Descripció";
$lang['alta_comercio_pais'] = "Pais";
$lang['alta_comercio_idioma'] = "Idioma";
$lang['alta_comercio_tarifa'] = "Tarifa";
$lang['alta_comercio_telefono'] = "telèfon";
$lang['alta_comercio_telefono2'] = "2 ºtelèfon";
$lang['alta_comercio_fax'] = "Fax";

$lang['alta_comercio_provincia'] = "Província";
$lang['alta_comercio_municipio'] = "Municipi";
$lang['alta_comercio_tipovia'] = "Tipus de vía";
$lang['alta_comercio_calle'] = "Nom del Carrer";
$lang['alta_comercio_numero'] = "Número";
$lang['alta_comercio_codigopostal'] = "Codi postal";

$lang['alta_comercio_mail_duplicado'] = "Ja existeix un usuari amb aquest E-mail";
$lang['alta_usuario_conf_ok'] = "Procés d'alta finalitzat correctament";
$lang['alta_usuario_conf_ko'] = "Procés d'alta no completat correctament.Torni a intentar registrar-se";
$lang['alta_usuario_mail_duplicado'] = "Ja existeix un usuari amb aquest E-mail";
$lang['alta_comercio_busca_municipio'] = "Sense seleccionar";
$lang['alta_comercio_boton_alta'] = "Nou usuari";



