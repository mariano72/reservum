<?php
//FORMULARIO DE DATOS VISTA_DETALLE_RECURSOS.PHP

$lang['alta_recursos_nombre'] = "Nom";
$lang['alta_recursos_descripcion'] = "Descripció";
$lang['alta_recursos_estado'] = "Estat";
$lang['alta_recursos_hora_inicio'] = "Hora inici";
$lang['alta_recursos_hora_fin'] = "Hora fi";
$lang['alta_recursos_bloques_horarios'] = "Blocs horaris";
$lang['alta_recursos_dias_avance'] = "Dies avançament calendari";
$lang['alta_recursos_minimo_dias'] = "Mínim díes anul.lació";
$lang['alta_recursos_operacion_ok'] = "L'alta/modificació  s'ha realitzat correctament";
$lang['alta_recursos_foto'] = "Afegir foto";
$lang['alta_recursos_cabecera'] = "Alta / modificació d'instal.lacions";


//FORMULARIO VISTA_ADMIN_RECURSOS.PHP
$lang['admin_recursos_nombre'] = "Nom";
$lang['admin_recursos_descripcion'] = "Descripció";
$lang['admin_recursos_estado'] = "Estat";
$lang['admin_recursos_imagen'] = "Imatge";
$lang['admin_recursos_cabecera'] = "Llista d'instal.lacions";


//FORMULARIO VISTA_ALTA_RECURSOS.PHP, VISTA_ACTUALIZAR_RECURSOS.PHP

$lang['admin_recursos_descripcion'] = "Descripció";
$lang['admin_recursos_estado'] = "Estat";
$lang['admin_recursos_imagen'] = "Imatge";



//VALIDACIONES CALLBACK
$lang['alta_recursos_compara_fechas'] = "Data inici no pot ser superior o igual a data fi";
$lang['alta_recursos_compara_horas'] = "Hora inici no pot ser superior o igual a hora fi";
$lang['alta_recursos_check_tarifa'] = "Ha superat el número de recursos per la tarifa contractada";