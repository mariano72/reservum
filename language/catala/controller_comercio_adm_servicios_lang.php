<?php

//FORMULARIO DE DATOS  VISTA_ADMIN_SERVICIOS.PHP
    	 	 	 
$lang['alta_servicios_nombre'] = "Nom";
$lang['alta_servicios_descripcion'] = "Descripció";
$lang['alta_servicios_Duracion'] = "Duració";
$lang['alta_servicios_Importe'] = "Import";
$lang['alta_servicios_IVA'] = "IVA";
$lang['alta_servicios_Importe_total'] = "Import total";
$lang['alta_servicios_cabecera'] = "Llista de serveis";
$lang['alta_servicios_cabecera_alta'] = "Alta / modificar serveis";


//FORMULARIO DE DATOS  VISTA_RELACIONAR_SERVICIOS.PHP
$lang['relacionar_servicios_escoge'] = "Servei a relacionar";
$lang['relacionar_servicios_cabecera'] = "Relacionar serveis";

//FORMULARIO DE DATOS  VISTA_RELACIONAR_SERVICIOS_RECURSOS.PHP
$lang['relacionar_servicios_rec_escoge'] = "Servei a relacionar";
$lang['relacionar_servicios_rec_nombre'] = "Nom";
$lang['relacionar_servicios_rec_estado'] = "Estat del recurs";
$lang['relacionar_servicios_rec_relacion'] = "Relacionar recurs i servei";

