<?php



// VISTA COMPLETAR_FICHA_COMERCIO.PHP

$lang['completar_ficha_comercio_nombre']="Nom comerç";
$lang['completar_ficha_comercio_cif']="CIF";
$lang['completar_ficha_comercio_provincia']="Província";
$lang['completar_ficha_comercio_poblacion']="Població";
$lang['completar_ficha_comercio_direccion']="Adreça";
$lang['completar_ficha_comercio_numero']="Número";
$lang['completar_ficha_comercio_codigopostal']="Codi postal";
$lang['completar_ficha_comercio_telefono']="Telèfon";
$lang['completar_ficha_comercio_telefono2']="Telèfon 2";
$lang['completar_ficha_comercio_fax']="Fax";
$lang['completar_ficha_comercio_imagen']="Imatge";
$lang['completar_ficha_comercio_descripcion_corta']="Descripció curta";
$lang['completar_ficha_comercio_descripcion']="Descripció";
$lang['completar_ficha_comercio_url_reserva']="URL que hem de facilitar als nostres clients o afegir a la nostra web";


$lang['completar_ficha_comercio_val_nombre']="Nom del comerç";
$lang['completar_ficha_comercio_val_direcc']="Adreça";
$lang['completar_ficha_comercio_val_telf']="Telèfon";
$lang['completar_ficha_comercio_val_cp']="Codi postal";
$lang['completar_ficha_comercio_desc1']="Descripció curta";
$lang['completar_ficha_comercio_desc2']="Descripció";

$lang['completar_ficha_comercio_cabecera']="Completar fitxa";


