<?php


//FORMULARIO DE DATOS VISTA_RESERVAR_CITA_COMERCIO

    $lang['reservar_com_cabecera'] = "Reservar cita del client";
    $lang['reservar_com_cabecera2'] = "Reservar cita";
    $lang['reservar_com_nombre'] = "Nom comerç";
    $lang['reservar_com_descripcion'] = "Descripció comerç";
    $lang['reservar_com_nombre_ser'] = "Nom servei";
    $lang['reservar_com_descripcion_ser'] = "Descripció servei";
    $lang['reservar_com_duracion'] = "Duració";
    $lang['reservar_com_importe'] = "Import";
    $lang['reservar_com_reservar'] = "Reservar";
    $lang['reservar_com_servicios'] = "L'hi oferim els següents serveis.Esculli un:";
    $lang['validar_reserva_comercio_alta'] = "El comerç està donat de baixa";
    $lang['reservar_com_fecha_reserva'] = "Selecciona data de la reserva";
    $lang['reservar_com_ver_agenda'] = "Veure agenda";


//FORMULARIO CAMBIAR PASSWORD.PHP

    $lang['cambio_pass_password'] = "Paraula de pas actual";
    $lang['cambio_pass_newpassword'] = "Nova paraula de pas";
    $lang['cambio_pass_newpassword2'] = "Repeteix nova paraula de pas";
    $lang['cambio_pass_OK'] = "Paraula de pas cambiada correctament";
    $lang['cambio_pass_check_pass'] = "La paraula de pas es incorrecte";
    $lang['cambio_pass_cabecera'] = "Canviar paraula de pas";


//FORMULARIO VISTA_PREVIA_RESERVA.PHP
    $lang['previa_reserva_cabecera'] = "Confirmació de la reserva";
    $lang['previa_reserva_resumen'] = "Resum de la reserva";
    $lang['previa_reserva_nombre'] = "Nom";
    $lang['previa_reserva_descripcion'] = "Descripció";
    $lang['previa_reserva_direccion'] = "Direcció";
    $lang['previa_reserva_telefono'] = "telèfon";
    $lang['previa_reserva_mapa'] = "Mapa";
    $lang['previa_reserva_datos_recurso'] = "Dades del Recurs";
    $lang['previa_reserva_recurso_nombre'] = "Nom";
    $lang['previa_reserva_recurso_descripcion'] = "Descripció";
    $lang['previa_reserva_datos_servicio'] = "Dades del servei";
    $lang['previa_reserva_servicio_nombre'] = "Nom";
    $lang['previa_reserva_servicio_descripcion'] = "Descripció";
    $lang['previa_reserva_recurso_reserva'] = "Dades de la reserva";
    $lang['previa_reserva_recurso_fecha_reserva'] = "Data reserva";
    $lang['previa_reserva_datos_hora_inicio'] = "Hora inici";
    $lang['previa_reserva_servicio_hora_fin'] = "Hora fi";
    $lang['previa_reserva_servicio_duracion'] = "Duració";
    $lang['previa_reserva_servicio_confirmar'] = "Ompli les següents dades";
    $lang['previa_reserva_servicio_confirmar_3min'] = "Confirma la reserva(Disposas de 3 minuts):";
    $lang['previa_reserva_servicio_confirmar_si'] = "SI";
    $lang['previa_reserva_servicio_confirmar_no'] = "NO";
    $lang['previa_reserva_servicio_comercio_nombre'] = "Nom";
    $lang['previa_reserva_servicio_comercio_email'] = "E-Mail";
    $lang['previa_reserva_servicio_comercio_telefono'] = "telèfon";
    $lang['previa_reserva_boton'] = "Confirmar reserva";
    $lang['previa_reserva_OK'] = "Reserva realitzada correctament";
    $lang['previa_reserva_KO'] = "Reserva NO realitzada.El temps màxim per a confirmar la reserva ha estat superat";
    $lang['previa_reserva_materiales_nombre'] = "Pots afegir els següents items a la teva reserva";
    $lang['previa_reserva_materiales_importe'] = "Import";
    $lang['previa_reserva_materiales_seleccion'] = "Seleccionar";

//FORMULARIO VISTA_LISTA_HORAS.PHP
    $lang['lista_horas_servicio'] = "Servei seleccionat";
    $lang['lista_horas_minutos'] = " minuts";
    $lang['lista_horas_reservada'] = "Reservada";
    $lang['lista_horas_nd'] = "No disponible";
    $lang['lista_horas_en_proceso'] = "En procés de reserva";


//FUNCION DE VALIDACION DE LA RESERVA
    $lang['validar_reserva_calendario_base'] = "La reserva supera l'horari del calendari base";
    $lang['validar_reserva_calendario_festivos'] = "Has escollit un dia festiu";
    $lang['validar_reserva_hora'] = "L'hora de la reserva es menor que l'hora actual";
    $lang['validar_reserva_fecha'] = "La datra de la reserva es menor que l'hora actual";
    $lang['validar_reserva_dia_habil'] = "Hi ha una restricció horaria per a la data/hora escullida";
    $lang['validar_reserva_tiempo_disponible'] = "No hi ha suficient temps disponible per al servei escollit";
    $lang['validar_reserva_url'] = "'S'ha modificat la URL.Torna a intentar la reserva";

    //FORMULARIO VISTA_ANULAR_RESERVA.PHP
    $lang['anular_fecha'] = "Data";
    $lang['anular_horaini'] = "Hora inici";
    $lang['anular_horafi'] = "Hora fi";
    $lang['anular_duracion'] = "Duració";
    $lang['anular_nombre'] = "Nom";
    $lang['anular_recurso'] = "Recurs";
    $lang['anular_servicio'] = "Servei";
    $lang['anular_direccion'] = "Direcció";
    $lang['anular_numero'] = "Número";
    $lang['anular_telefono'] = "Telefon";
    $lang['anular_codpostal'] = "CP";
    $lang['anular_cabecera_tabla'] = "Les meves reserves actives";
    $lang['qr_titulo']="QR IDENTIFICADOR DE LA RESERVA";