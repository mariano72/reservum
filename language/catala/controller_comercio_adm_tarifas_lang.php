<?php

//FORMULARIO LISTA_DE_TARIFAS
$lang['lista_tarifas_lista'] = "Llista de tarifes";
$lang['lista_tarifas_nombre'] = "Nom";
$lang['lista_tarifas_fecha_ini'] = "Data inici";
$lang['lista_tarifas_fecha_fin'] = "Data fi";
$lang['lista_tarifas_hora_ini'] = "Hora inici";
$lang['lista_tarifas_hora_fin'] = "Hora fi";
$lang['lista_tarifas_nombre_ser'] = "Nom servei";
$lang['lista_tarifas_importe'] = "Import";
$lang['lista_tarifas_iva'] = "IVA";
$lang['lista_tarifas_importetotal'] = "Import total";
$lang['lista_tarifas_estado'] = "Estat";
$lang['lista_tarifas_dias'] = "Dies";
$lang['lista_tarifas_cabecera'] = "Administrar tarifes";

//FORMULARIO ALTA_DE_TARIFAS
$lang['alta_tarifa_fecini'] = "Data de inici";
$lang['alta_tarifa_fecfin'] = "Data de fi";
$lang['alta_tarifa_horaini'] = "Hora de inici";
$lang['alta_tarifa_horafin'] = "Hora de fi";
$lang['alta_tarifa_nombre'] = "Nom tarifa";
$lang['alta_tarifa_estado'] = "Estat";
$lang['alta_tarifa_servicio'] = "Servei";
$lang['alta_tarifa_importe'] = "Import";
$lang['alta_tarifa_iva'] = "IVA";
$lang['alta_tarifa_importetotal'] = "Import total";
$lang['alta_tarifa_seleccion_dias'] = "Selecciona a quins diess de la setmana aplicarà la tarifa";
$lang['alta_tarifa_lunes'] = "Dilluns";
$lang['alta_tarifa_martes'] = "Dimarts";
$lang['alta_tarifa_miercoles'] = "Dimecres";
$lang['alta_tarifa_jueves'] = "Dijous";
$lang['alta_tarifa_viernes'] = "Divendres";
$lang['alta_tarifa_sabado'] = "Dissabte";
$lang['alta_tarifa_domingo'] = "Diumenge";
$lang['alta_tarifa_boton'] = "Alta / Modificar tarifa";
$lang['alta_tarifas_cabecera'] = "Alta / Modificar tarifes";

$lang['alta_tarifa_ok'] = "La tarifa s'ha donat d'alta correctament";
$lang['actualizar_tarifa_ok'] = "La tarifa s'ha actualizat correctament";


//VALIDACIONES CALLBACK
$lang['alta_tarifa_compara_fechas'] = "Data inici no pot ser major  a data fi";
$lang['alta_tarifa_compara_horas'] = "Hora inici no pot ser major o igual a hora fi";
$lang['alta_tarifa_check_dias'] = "Deus marcar al menys 1 dia de la setmana per aplicar la tarifa";
$lang['alta_tarifa_validar_insercion']  = "Existeixen tarifes incompatibles al sistema.<BR>Revisa les dates i hores que està assignant a la nova tarifa<br>
                                          No poden haver 2 tarifes compartint les mateixes dates,hores o dies";


