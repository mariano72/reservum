<?php


//FORMULARIO DE DATOS  VISTA_ADMIN_SERVICIOS.PHP
    	 	 	 
$lang['alta_servicios_nombre'] = "Nombre";
$lang['alta_servicios_descripcion'] = "Descripción";
$lang['alta_servicios_Duracion'] = "Duración";
$lang['alta_servicios_Importe'] = "Importe";
$lang['alta_servicios_IVA'] = "IVA";
$lang['alta_servicios_Importe_total'] = "Importe total";
$lang['alta_servicios_cabecera'] = "Lista de servicios";
$lang['alta_servicios_cabecera_alta'] = "Alta / modificar servicios";

//FORMULARIO DE DATOS  VISTA_RELACIONAR_SERVICIOS.PHP
$lang['relacionar_servicios_escoge'] = "Servicio a relacionar";
$lang['relacionar_servicios_cabecera'] = "Relacionar servicios";

//FORMULARIO DE DATOS  VISTA_RELACIONAR_SERVICIOS_RECURSOS.PHP
$lang['relacionar_servicios_rec_escoge'] = "Servicio a relacionar";
$lang['relacionar_servicios_rec_nombre'] = "Nombre";
$lang['relacionar_servicios_rec_estado'] = "Estado del recurso";
$lang['relacionar_servicios_rec_relacion'] = "Relacionar recurso y servicio";

