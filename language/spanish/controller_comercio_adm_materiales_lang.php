<?php

//FORMULARIO LISTA_DE_MATERIALES
$lang['lista_materiales_lista'] = "Lista de materiales";
$lang['lista_materiales_nombre'] = "Nombre";
$lang['lista_materiales_nombre_ser'] = "Nombre servicio";
$lang['lista_materiales_importe'] = "Importe";
$lang['lista_materiales_estado'] = "Estado";

//FORMULARIO ALTA_DE_materiales

$lang['alta_materiales_descripcion'] = "Descripción";
$lang['alta_materiales_estado'] = "Estado";
$lang['alta_materiales_servicio'] = "Servicio";
$lang['alta_materiales_importe'] = "Importe";
$lang['alta_materiales_boton'] = "Alta / Modificar material";
$lang['alta_materiales_cabecera_admin'] = "Administrar materials";
$lang['alta_materiales_cabecera'] = "Alta / Modificar materiales";

$lang['alta_material_ok'] = "El material se ha dado de alta correctamente";
$lang['actualizar_material_ok'] = "El material se ha actualizado correctamente";
    
