<?php

//FORMULARIO DE DATOS VISTA_HOME.PHP
$lang['home_cabecera']="RENTABILIZA TUS INSTALACIONES ON-LINE<BR>Y DEJA QUE RESERVUM HAGA EL RESTO";
$lang['home_paso1']="Introduce las instalaciones que quieres mostrar on-line";
$lang['home_paso2']="Introduce los servicios que vas a mostrar on-line";
$lang['home_paso3']="Gestiona la agenda y los horarios de los servicios que vas a ofrecer";
$lang['home_paso4']="Excelente.Ya puedes ofrecer tus reservas <br>on-line";
$lang['home_mensaje']="Reservum, el gestor de reservas que te ahorra trabajo, optimiza ocupación y permite que tus reservas estén abiertas las 24 horas del día. Contrata desde 70 euros al año.";
$lang['home_caracteristica1']="Gestionar reservas de pistas de tenis, pádel, squash, fútbol, baloncesto.";
$lang['home_caracteristica2']="Gestionar reservas de salas de masaje, quiroprácticos, esteticien.";
$lang['home_caracteristica3']="Ofrece GRATIS a tus clientes la posibilidad de reservar on-line.";
$lang['home_caracteristica4']="Totalmente compatible con tablets y smartphones.";
$lang['home_caracteristica5']="Abierto 24*7*365 para recibir sus reservas.";
$lang['home_boton1']="Soy una empresa, quiero darme alta YA";
$lang['home_boton2']="¿Como funciona?";
$lang['home_boton3']="Soy un usuario, quiero RESERVAR";
$lang['home_banner2_h1']="¡CONÉCTATE AL MUNDO!.DÉJAME SER TU PARTNER  Y ADEMÁS TENDRÁS 1 AÑO GRATUITO";
$lang['home_banner2_h2']="";

$lang['home_estadisticas_cabecera']="Únete a nosotros";
$lang['home_estadisticas_miles']="Miles";
$lang['home_estadisticas_est1']=" de usuarios queriendo reservar";
$lang['home_estadisticas_est2']=" de empresas esperando reservas";
$lang['home_estadisticas_est3']=" de reservas en curso";

$lang['home_formulario_cabecera']="¿Tienes dudas? Rellena el siguiente formulario y en 24 horas te contestamos:";
$lang['home_estadisticas_nombre']="Nombre";
$lang['home_estadisticas_email']="E-Mail";
$lang['home_estadisticas_telf']="Teléfono";
$lang['home_estadisticas_mensaje']="Mensaje";

$lang['home_video_h3']="Cómo funciona";
$lang['home_video_desc']="Mira este pequeño vídeo y lo sabrás al instante";
$lang['home_videos_presentacion_enlace']="http://www.youtube.com/embed/5LNy6_CfemE";
    
$lang['home_contacto_enviado']="Mensaje enviado correctamente";

//PANTALLA VIDEOS DE LA HOME
$lang['home_videos_1_video']="Como dar de alta una sala de masaje y los servicios que ofrece";
$lang['home_videos_1_video_enlace']="http://www.youtube.com/embed/Ie_zm6LPahA";
$lang['home_videos_2_video']="Como añadir restricciones horarias a las reservas";
$lang['home_videos_2_video_enlace']="http://www.youtube.com/embed/USvKj2dyDyo";

//PANTALLA HOME MASAJES
$lang['home_masaje_cabecera']="RENTABILIZA TUS MASAJES ON-LINE<BR>Y DEJA QUE RESERVUM HAGA EL RESTO";
$lang['home_masaje_paso1']="Introduce las instalaciones que quieres mostrar on-line";
$lang['home_masaje_paso2']="Introduce los servicios que vas a mostrar on-line";
$lang['home_masaje_paso3']="Gestiona la agenda y los horarios de los servicios que vas a ofrecer";
$lang['home_masaje_paso4']="Excelente.Ya puedes ofrecer tus masajes <br>on-line";
$lang['home_masaje_mensaje']="Reservum, el gestor de reservas que te ahorra trabajo, optimiza ocupación y permite que tus reservas estén abiertas las 24 horas del día.
                                Contrata desde 70 euros al año.";

$lang['home_masaje_caracteristica2']="Gestionar reservas de salas de masaje, quiroprácticos, esteticien.";
$lang['home_masaje_caracteristica3']="Desde 70 euros al año olvídate del teléfono y ofrece a tus clientes la posibilidad de reservar on-line.";
$lang['home_masaje_caracteristica4']="Totalmente compatible con tablets y smartphones.";
$lang['home_masaje_caracteristica5']="Abierto 24*7*365 para recibir sus reservas.";
$lang['home_masaje_boton1']="Soy una empresa, quiero darme alta YA";
$lang['home_masaje_boton2']="¿Como funciona?";
$lang['home_masaje_boton3']="Soy un usuario, quiero RESERVAR";
$lang['home_masaje_banner2_h1']="¡CONÉCTATE AL MUNDO!.DÉJAME SER TU PARTNER  Y ADEMÁS TENDRÁS 60 DÍAS GRATUITOS";
$lang['home_masaje_banner2_h2']="Páguelo con Paypal, tárjeta de crédito o transferencia bancaria";

//PUNTOS DE MENU COMUNES A TODAS LAS PANTALLAS
$lang['menu_comun_pantallas_home']="Inicio";
$lang['menu_comun_pantallas_iniciar']="Iniciar sesión";
$lang['menu_comun_pantallas_videos']="Vídeos";
$lang['menu_comun_pantallas_masajes']="Masajes";
$lang['menu_comun_pantallas_faq']="FAQ";
$lang['menu_comun_pantallas_contactar']="Contactanos";
$lang['menu_comun_pantallas_salir']="Salir";

//MENUS DE USUARIO QUE PUEDEN APLICAR A TODAS LAS PANTALLAS
//MENU GENERAL COMERCIO
$lang['menu_comercio_comun_reservar']="Reservar Cita Cliente";
$lang['menu_comercio_comun_anular']="Anular Cita Cliente";
$lang['menu_comercio_comun_agenda']="Agenda Del Dia";
$lang['menu_comercio_comun_adm_recursos']="Administrar Instalaciones";
$lang['menu_comercio_comun_adm_servicios']="Administrar Servicios";
$lang['menu_comercio_comun_adm_restricciones']="Administrar Restricciones";
$lang['menu_comercio_comun_estadisticas']="Estadisticas";
$lang['menu_comercio_comun_ficha']="Ficha del comercio";
$lang['menu_comercio_comun_inicio']="Ir a inicio";
$lang['menu_comercio_comun_camb_pass']="Cambiar contraseña";
$lang['menu_comercio_comun_adm_tarifas']="Administrar tarifas";
$lang['menu_comercio_comun_adm_materiales']="Administrar materiales";
$lang['menu_comercio_comun_mis_clientes']="Mis clientes";

// VISTA FICHA_COMERCIO.PHP
$lang['ficha_comercio_quiero_reservar']="Quiero reservar un servicio";
$lang['ficha_comercio_datos_contacto']="Datos de contacto";
$lang['ficha_comercio_nombre']="Nombre comercio";
$lang['ficha_comercio_cif']="CIF";
$lang['ficha_comercio_provincia']="Provincia";
$lang['ficha_comercio_poblacion']="Población";
$lang['ficha_comercio_direccion']="Dirección";
$lang['ficha_comercio_numero']="Número";
$lang['ficha_comercio_codigopostal']="Código postal";
$lang['ficha_comercio_email']="E-mail";
$lang['ficha_comercio_telefono']="teléfono";
$lang['ficha_comercio_telefono2']="teléfono 2";
$lang['ficha_comercio_fax']="Fax";
$lang['ficha_comercio_comollegar']="Como llegar";
$lang['ficha_comercio_vermapa']="Ver mapa";
$lang['ficha_comercio_imagen']="Imagen";
$lang['ficha_comercio_descripcion_corta']="Descripción corta";
$lang['ficha_comercio_descripcion']="Descripción";

$lang['ficha_comercio_ser_catalogo']="Catálogo de servicios";
$lang['ficha_comercio_ser_nombre']="Nombre";
$lang['ficha_comercio_ser_descripcion']="Descripción";
$lang['ficha_comercio_ser_duracion']="Duración";
$lang['ficha_comercio_ser_precio']="Precio";



//MENU RECURSOS
$lang['menu_comercio_recursos_alta']="Nueva instalación";
$lang['menu_comercio_recursos_modificar']="Modificar instalación";
$lang['menu_comercio_recursos_menu']="Ir a menú principal";

//MENU SERVICIOS
$lang['menu_comercio_servicios_alta']="Alta servicios";
$lang['menu_comercio_servicios_modificar']="Modificar servicios";
$lang['menu_comercio_servicios_relacionar']="Relacionar servicios";
$lang['menu_comercio_servicios_menu']="Ir a menú principal";

//MENU RESTRICCIONES
$lang['menu_comercio_restricciones_alta']="Alta restricciones";
$lang['menu_comercio_restricciones_modificar']="Modificar restricciones";
$lang['menu_comercio_restricciones_menu']="Ir a menú principal";


//MENU USUARIOS
$lang['menu_usuario_comun_reservar']="Reservar Cita";
$lang['menu_usuario_comun_anular']="Anular Cita";
$lang['menu_usuario_comun_inicio']="Ir a inicio";
$lang['menu_usuario_comun_camb_pass']="Cambiar contraseña";

//MENU FACTURAS
$lang['menu_comercio_comun_facturas']="Lista de facturas";

//MENU TARIFAS
$lang['menu_comercio_tarifas_alta']="Alta tarifas";
$lang['menu_comercio_tarifas_lista']="Lista de tarifas";
$lang['menu_comercio_tarifas_menu']="Ir a menú principal";

//MENU MATERIALES
$lang['menu_comercio_materiales_alta']="Alta materiales - extras";
$lang['menu_comercio_materiales_lista']="lista de materiales - extras";
$lang['menu_comercio_materiales_menu']="Ir a menú principal";

//MENU MIS_CLIENTES
