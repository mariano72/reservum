<?php


//FORMULARIO DE DATOS VISTA_RESERVAR_CITA_COMERCIO

    $lang['reservar_com_cabecera'] = "Reservar cita del cliente";
    $lang['reservar_com_cabecera2'] = "Reservar cita";
    $lang['reservar_com_nombre'] = "Nombre comercio";
    $lang['reservar_com_descripcion'] = "Descripción comercio";
    $lang['reservar_com_nombre_ser'] = "Nombre servicio";
    $lang['reservar_com_descripcion_ser'] = "Descripción servicio";
    $lang['reservar_com_duracion'] = "Duración";
    $lang['reservar_com_importe'] = "Importe";
    $lang['reservar_com_reservar'] = "Reservar";
    $lang['reservar_com_servicios'] = "Le ofrece los siguientes servicios.Escoja uno:";
    $lang['validar_reserva_comercio_alta'] = "El comercio está dado de baja";
    $lang['reservar_com_fecha_reserva'] = "Selecciona fecha de la reserva";
    $lang['reservar_com_ver_agenda'] = "Ver agenda";


//FORMULARIO CAMBIAR PASSWORD.PHP

    $lang['cambio_pass_password'] = "Contraseña actual";
    $lang['cambio_pass_newpassword'] = "Nueva contraseña";
    $lang['cambio_pass_newpassword2'] = "Repetir nueva contraseña";
    $lang['cambio_pass_OK'] = "La contraseña se ha cambiado correctamente";
    $lang['cambio_pass_check_pass'] = "Debe introducir su contraseña actual correctamente";
    $lang['cambio_pass_cabecera'] = "Cambiar contraseña";

//FORMULARIO VISTA_PREVIA_RESERVA.PHP
    $lang['previa_reserva_cabecera'] = "Confirmación de la reserva";
    $lang['previa_reserva_resumen'] = "Resumen de la reserva";
    $lang['previa_reserva_nombre'] = "Nombre";
    $lang['previa_reserva_descripcion'] = "Descripción";
    $lang['previa_reserva_direccion'] = "Dirección";
    $lang['previa_reserva_teléfono'] = "teléfono";
    $lang['previa_reserva_mapa'] = "Mapa";
    $lang['previa_reserva_datos_recurso'] = "Datos del Recurso";
    $lang['previa_reserva_recurso_nombre'] = "Nombre";
    $lang['previa_reserva_recurso_descripcion'] = "Descripción";
    $lang['previa_reserva_datos_servicio'] = "Datos del servicio";
    $lang['previa_reserva_servicio_nombre'] = "Nombre";
    $lang['previa_reserva_servicio_descripcion'] = "Descripción";
    $lang['previa_reserva_recurso_reserva'] = "Datos de la reserva";
    $lang['previa_reserva_recurso_fecha_reserva'] = "Fecha reserva";
    $lang['previa_reserva_datos_hora_inicio'] = "Hora inicio";
    $lang['previa_reserva_servicio_hora_fin'] = "Hora fin";
    $lang['previa_reserva_servicio_duracion'] = "Duración";
    $lang['previa_reserva_servicio_confirmar'] = "Rellene los siguientes datos";
    $lang['previa_reserva_servicio_confirmar_3min'] = "Confirma la reserva(Dispones de 3 minutos):";
    $lang['previa_reserva_servicio_confirmar_si'] = "SI";
    $lang['previa_reserva_servicio_confirmar_no'] = "NO";
    $lang['previa_reserva_servicio_comercio_nombre'] = "Nombre";
    $lang['previa_reserva_servicio_comercio_email'] = "E-Mail";
    $lang['previa_reserva_servicio_comercio_telefono'] = "Teléfono";
    $lang['previa_reserva_boton'] = "Confirmar reserva";
    $lang['previa_reserva_OK'] = "Reserva realizada correctamente";
    $lang['previa_reserva_KO'] = "Reserva NO realizada.El tiempo máximo para confirmar la reserva ha sido superado";
    $lang['previa_reserva_materiales_nombre'] = "Puedes añadir los siguientes elementos a tu reserva";
    $lang['previa_reserva_materiales_importe'] = "Importe";
    $lang['previa_reserva_materiales_seleccion'] = "Seleccionar";

//FORMULARIO VISTA_LISTA_HORAS.PHP
    $lang['lista_horas_servicio'] = "Servicio seleccionado";
    $lang['lista_horas_minutos'] = " minutos";
    $lang['lista_horas_reservada'] = "Reservada";
    $lang['lista_horas_nd'] = "No disponible";
    $lang['lista_horas_en_proceso'] = "En proceso de reserva";


//FUNCION DE VALIDACION DE LA RESERVA
    $lang['validar_reserva_calendario_base'] = "La reserva supera el horario del calendario base";
    $lang['validar_reserva_calendario_festivos'] = "Ha elegido un día festivo";
    $lang['validar_reserva_hora'] = "La hora de la reserva es menor que la hora actual";
    $lang['validar_reserva_fecha'] = "La fecha de la reserva es menor que la hora actual";
    $lang['validar_reserva_dia_habil'] = "Hay una restricción horaria para la fecha/hora escogida.";
    $lang['validar_reserva_tiempo_disponible'] = "No hay suficiente tiempo disponible para el servicio seleccionado";
    $lang['validar_reserva_url'] = "Se ha modificado la URL.Vuelva a intentar la reserva";


//FORMULARIO VISTA_MOSTRAR_RESULTADO.PHP
    $lang['anular_cabecera_tabla'] = "Mis reservas activas";
    $lang['anular_fecha'] = "Fecha";
    $lang['anular_horaini'] = "Hora inicio";
    $lang['anular_horafi'] = "Hora fin";
    $lang['anular_duracion'] = "Duración";
    $lang['anular_nombre'] = "Nombre";
    $lang['anular_recurso'] = "Recurso";
    $lang['anular_servicio'] = "Servicio";
    $lang['anular_direccion'] = "Dirección";
    $lang['anular_numero'] = "Número";
    $lang['anular_telefono'] = "teléfono";
    $lang['anular_codpostal'] = "CP";
    $lang['qr_titulo']="QR IDENTIFICADOR DE LA RESERVA";