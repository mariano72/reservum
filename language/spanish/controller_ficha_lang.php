<?php

// VISTA COMPLETAR_FICHA_COMERCIO.PHP

$lang['completar_ficha_comercio_nombre']="Nombre comercio";
$lang['completar_ficha_comercio_cif']="CIF";
$lang['completar_ficha_comercio_provincia']="Provincia";
$lang['completar_ficha_comercio_poblacion']="Población";
$lang['completar_ficha_comercio_direccion']="Dirección";
$lang['completar_ficha_comercio_numero']="Número";
$lang['completar_ficha_comercio_codigopostal']="Código postal";
$lang['completar_ficha_comercio_telefono']="teléfono";
$lang['completar_ficha_comercio_telefono2']="teléfono 2";
$lang['completar_ficha_comercio_fax']="Fax";
$lang['completar_ficha_comercio_imagen']="Imagen";
$lang['completar_ficha_comercio_descripcion_corta']="Descripción corta";
$lang['completar_ficha_comercio_descripcion']="Descripción";
$lang['completar_ficha_comercio_url_reserva']="URL que debemos facilitar a nuestros clientes o añadir a nuestra web";

$lang['completar_ficha_comercio_val_nombre']="Nombre de comercio";
$lang['completar_ficha_comercio_val_direcc']="Dirección";
$lang['completar_ficha_comercio_val_telf']="Teléfono";
$lang['completar_ficha_comercio_val_cp']="Código postal";
$lang['completar_ficha_comercio_desc1']="Descripción corta";
$lang['completar_ficha_comercio_desc2']="Descripción";

$lang['completar_ficha_comercio_cabecera']="Completar ficha";

