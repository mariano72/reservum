<?php

              
//FORMULARIO DE DATOS VISTA_RESERVAR

$lang['reservar_cabecera'] = "Reservar cita";
$lang['reservar_cabecera_tabla'] = "Mis últimas reservas";
$lang['reservar_fecha'] = "Fecha";
$lang['reservar_horaini'] = "Hora inicio";
$lang['reservar_horafi'] = "Hora fin";
$lang['reservar_duracion'] = "Duración";
$lang['reservar_nombre'] = "Nombre";
$lang['reservar_recurso'] = "Recurso";
$lang['reservar_servicio'] = "Servicio";
$lang['reservar_direccion'] = "Dirección";
$lang['reservar_numero'] = "Número";
$lang['reservar_telefono'] = "teléfono";
$lang['reservar_codpostal'] = "Código postal";

$lang['reservar_busca_comercio'] = "Búsqueda de clubs y profesionales";
$lang['reservar_busca_provincia'] = "Provincia";
$lang['reservar_busca_municipio'] = "Municipio";
$lang['reservar_link_reservar'] = "Vuelve a reservar en";

$lang['reservar_nombre_tabla'] = "Nombre del club o comercio";
$lang['reservar_descripcion'] = "Descripción";

//FORMULARIO VISTA_ANULAR_RESERVA.PHP
$lang['anular_fecha'] = "Fecha";
$lang['anular_horaini'] = "Hora inicio";
$lang['anular_horafi'] = "Hora fin";
$lang['anular_duracion'] = "Duración";
$lang['anular_nombre'] = "Nombre";
$lang['anular_recurso'] = "Recurso";
$lang['anular_servicio'] = "Servicio";
$lang['anular_direccion'] = "Dirección";
$lang['anular_numero'] = "Número";
$lang['anular_telefono'] = "teléfono";
$lang['anular_codpostal'] = "CP";
$lang['anular_anular'] = "Anular";
$lang['anular_mensaje_validacion'] = "Anular";
$lang['anular_mensaje_ok']="La reserva se ha anulado correctamente.";
$lang['anular_boton']="Cancelar reserva";
$lang['anular_mensaje_error']="Debe seleccionar una reserva";
$lang['anular_cabecera'] = "Anular cita";
$lang['anular_cabecera_tabla'] = "Lista de reservas que podemos anular";
