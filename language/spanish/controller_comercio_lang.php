<?php
//FORMULARIO DE DATOS VISTA_ANULAR_CITA_CLIENTE.PHP

$lang['anular_cita_cabecera'] = "Anular reserva";
$lang['anular_cita_fecha'] = "Fecha reserva";
$lang['anular_cita_hora_inicio'] = "Hora inicio";
$lang['anular_cita_hora_fin'] = "Hora fin";
$lang['anular_cita_duracion'] = "Duración";
$lang['anular_cita_recurso'] = "Recurslo";
$lang['anular_cita_servicio'] = "Servicio";
$lang['anular_cita_nombre'] = "Nombre usuario";
$lang['anular_cita_email'] = "E-mail";
$lang['anular_cita_telefono'] = "Teléfono";
$lang['anular_cita_anular'] = "Cancelar reserva";



//FORMULARIO DE DATOS VISTA_PREVIA_ANULAR_CITA_CLIENTE.PHP

$lang['previa_anular_cita'] = "Anular cita del cliente";
$lang['previa_anular_mail'] = "E-mail";
$lang['previa_anular_telefono'] = "Teléfono";
$lang['previa_anular_fecha'] = "Fecha";
$lang['previa_anular_cabecera1'] = "Buscar por usuario";
$lang['previa_anular_cabecera2'] = "Buscar por fecha de reserva";
$lang['previa_anular_buscar'] = "Buscar reservas";
$lang['previa_anular_mensaje_error'] = "No se han recuperado datos de la consulta";
    
//FORMULARIO DE VISTA_INICIO_COMERCIO.PHP
$lang['inicio_com_cabecera'] = "Cuadro de mandos resumido";
$lang['inicio_com_ult_res_fec'] = "La Última reserva ha sido el ";
$lang['inicio_com_ult_res_rea'] = "Fue realizada por ";
$lang['inicio_com_ult_res_curso'] = " reservas actualmente en curso";
$lang['inicio_com_ult_res_mes'] = " reservas mensuales pendientes de atender";
$lang['inicio_com_ult_res_total'] = " reservas totales pendientes de atender";
$lang['inicio_com_ult_res_cab'] = "Últimas reservas realizadas";
$lang['inicio_com_ult_fecha'] = "Fecha servicio";
$lang['inicio_com_ult_horaini'] = "Hora inicio ";
$lang['inicio_com_ult_horafin'] = "Hora fin";
$lang['inicio_com_ult_duracion'] = "Duración";
$lang['inicio_com_ult_usuario'] = "Usuario";
$lang['inicio_com_ult_servicio'] = "Servicio";
$lang['inicio_com_ult_mail'] = "Mail";
$lang['inicio_com_ult_telef'] = "Telf";
$lang['inicio_com_prox'] = "Próximas reservas a atender";

//FORMULARIO DE VISTA_AGENDA_DIA.PHP
$lang['agenda_dia_cabecera'] = "Agenda del día";
$lang['agenda_dia_dia'] = "Día a consultar";
$lang['agenda_dia_hora_inicio'] = "Hora inicio";
$lang['agenda_dia_hora_fin'] = "Hora fin";
$lang['agenda_dia_servicio'] = "Servicio";
$lang['agenda_dia_usuario'] = "Usuario";
$lang['agenda_dia_email'] = "E-mail";
$lang['agenda_dia_telefono'] = "Teléfono";
$lang['agenda_dia_seleccionar'] = "Seleccionar día";
$lang['agenda_dia_ver_agenda'] = "Ver agenda";

//FORMULARIO DE VISTA_MIS_CLIENTES.PHP
$lang['mis_clientes_cabecera'] = "Mis clientes";
$lang['mis_clientes_listado'] = "Listado de clientes";
$lang['mis_clientes_usuario'] = "Nombre usuario";
$lang['mis_clientes_mail'] = "E-mail";
$lang['mis_clientes_telefono'] = "Teléfono";


//FORMULARIO CONSULTAR_RESERVA

$lang['consultar_reserva_cabecera'] = "Consultar reserva";
$lang['consultar_reserva_datos_generales'] = "Datos de la reserva";
$lang['consultar_reserva_recurso_nombre'] = "Nombre instalación";
$lang['consultar_reserva_servicio_nombre'] = "Servicio";
$lang['consultar_reserva_recurso_fecha_reserva'] = "Fecha reserva";
$lang['consultar_reserva_datos_hora_inicio'] = "Hora inicio";
$lang['consultar_reserva_datos_hora_fin'] = "Hora fin";
$lang['consultar_reserva_duracion'] = "Duración";
$lang['consultar_reserva_datos_personales'] = "Datos del usuario";
$lang['consultar_reserva_nombre_usuario']="Nombre";
$lang['consultar_reserva_email_usuario']="E-Mail";
$lang['consultar_reserva_servicio_telefono_usuario']="Teléfono";
$lang['consultar_reserva_importe']="Importe";
$lang['consultar_reserva_materiales_descripcion']="Materiales y extras";
$lang['consultar_reserva_materiales_importe']="Importe Materiales";
$lang['consultar_reserva_materiales_tarifa']="Tarifa";


