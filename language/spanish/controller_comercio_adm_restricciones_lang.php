<?php


//FORMULARIO DE DATOS  VISTA_LISTA_RECURSO_RESTRICCION.PHP
$lang['lista_recurso_rec_lista'] = "Lista de recursos";
$lang['lista_recurso_rec_nombre'] = "Nombre";
$lang['lista_recurso_rec_estado'] = "Estado";
$lang['lista_recurso_rec_lista_restr'] = "Lista de restricciones";
$lang['lista_recurso_rec_lista_numero'] = "Nº";
$lang['lista_recurso_rec_lista_fecini'] = "Fec.Inicio";
$lang['lista_recurso_rec_lista_fecfin'] = "Fec.Fin";
$lang['lista_recurso_rec_lista_horini'] = "Hora inicio";
$lang['lista_recurso_rec_lista_horfin'] = "Hora fin";
$lang['lista_recurso_rec_lista_lun'] = "Lunes";
$lang['lista_recurso_rec_lista_mar'] = "Martes";
$lang['lista_recurso_rec_lista_mie'] = "Miércoles";
$lang['lista_recurso_rec_lista_jue'] = "Jueves";
$lang['lista_recurso_rec_lista_vie'] = "Viernes";
$lang['lista_recurso_rec_lista_sab'] = "Sábado";
$lang['lista_recurso_rec_lista_dom'] = "Domingo";
$lang['lista_recurso_rec_cabecera'] = "Administrar restricciones";


//FORMULARIO DE DATOS  VISTA_ALTA_RESTRICCIONES.PHP
//COMPARTIMOS VARIOS CAMPOS CONEL FORMULARIO VISTA_LISTA_RECURSO_RESTRICCION.PHP ASÍ
// QUE SÓLO PONDREMOS LOS DIFERENTES

$lang['alta_restricciones_dias_apl'] = "Días de aplicación de la restricción";
$lang['alta_restricciones_dias_lista_rec'] = "Lista de recursos a aplicar la restricción";
$lang['alta_restricciones_dias_nombre'] = "Nombre";
$lang['alta_restricciones_dias_estado'] = "Estado";
$lang['lista_recurso_rec_dias_fecini'] = "Fecha inicial";
$lang['lista_recurso_rec_dias_fecfin'] = "Fecha final";
$lang['alta_restricciones_dias_restriccion'] = "Aplicar restricción";

$lang['alta_restricciones_compara_horas'] = "Hora inicio no puede ser superior o igual a hora fin";
$lang['alta_restricciones_compara_fechas'] = "Fecha inicio no puede ser superior  a fecha fin";
$lang['alta_restricciones_check_dias']="Debe marcar al menos 1 día de la semana para aplicar la restricción";

$lang['modifica_restricciones_ok']="Restricción modificada correctamente";
$lang['alta_restricciones_ok']="Restricción añadida correctamente";

$lang['alta_restricciones_cabecera'] = "Alta de restricciones";
$lang['alta_restricciones_modif_cabecera'] = "Modificar restricciones";

//VALIDACIONES CALLBACK
$lang['alta_restricciones_compara_fechas'] = "Fecha inicio no puede ser superior a fecha fin";
$lang['alta_restricciones_compara_horas'] = "Hora inicio no puede ser superior o igual a hora fin";
