<?php
//FORMULARIO DE DATOS VISTA_LISTA_FACTURAS.PHP

$lang['lista_facturas_cabecera'] = "Lista de facturas";
$lang['lista_facturas_numero'] = "Nº factura";
$lang['lista_facturas_descripcion'] = "Descripción";
$lang['lista_facturas_tarifa'] = "Tarifa";
$lang['lista_facturas_estado'] = "Estado";
$lang['lista_facturas_periodo'] = "Periodo factura";
$lang['lista_facturas_fecha'] = "Fecha factura";
$lang['lista_facturas_importe'] = "Importe";
$lang['lista_facturas_iva'] = "Iva";
$lang['lista_facturas_importetotal'] = "Importe total";

//FORMULARIO DE DATOS VISTA_DETALLE_FACTURA.PHP


$lang['detalle_factura_cabecera'] = "Detalle de la factura";
$lang['detalle_factura_numero'] = "Nº factura";
$lang['detalle_factura_descripcion'] = "Descripción";
$lang['detalle_factura_tarifa'] = "Tarifa";
$lang['detalle_factura_estado'] = "Estado";
$lang['detalle_factura_periodo'] = "Periodo factura";
$lang['detalle_factura_fecha'] = "Fecha factura";
$lang['detalle_factura_importe'] = "Importe";
$lang['detalle_factura_iva'] = "Iva";
$lang['detalle_factura_importetotal'] = "Importe total";
$lang['detalle_factura_pagar'] = "Pagar factura";
$lang['detalle_factura_concepto'] = "Concepto";
$lang['detalle_factura_fecha_pago'] = "Fecha pago";
$lang['detalle_factura_paypal_gateway'] = "Nº de referencia Paypal";

