<?php

//FORMULARIO DE DATOS VISTA_ALTA_COMERCIO

$lang['alta_comercio_nombre'] = "Nombre comercio";
$lang['alta_comercio_nifcif'] = "NIF /CIF";
$lang['alta_comercio_email'] = "E-Mail";
$lang['alta_comercio_email2'] = "Repetir E-Mail";
$lang['alta_comercio_password'] = "Contraseña";
$lang['alta_comercio_password2'] = "Repetir contraseña";
$lang['alta_comercio_desc_corta'] = "Descripción corta";
$lang['alta_comercio_descripcion'] = "Descripción";
$lang['alta_comercio_pais'] = "Pais";
$lang['alta_comercio_idioma'] = "Idioma";
$lang['alta_comercio_tarifa'] = "Tarifa";
$lang['alta_comercio_telefono'] = "Teléfono";
$lang['alta_comercio_telefono2'] = "2 ºTeléfono";
$lang['alta_comercio_fax'] = "Fax";

$lang['alta_comercio_provincia'] = "Provincia";
$lang['alta_comercio_municipio'] = "Municipio";
$lang['alta_comercio_tipovia'] = "Tipo de vía";
$lang['alta_comercio_calle'] = "Nombre de la Calle";
$lang['alta_comercio_numero'] = "Número";
$lang['alta_comercio_codigopostal'] = "Código postal";

$lang['alta_comercio_mail_duplicado'] = "Ya existe un usuario con este E-mail";
$lang['alta_usuario_conf_ok'] = "Proceso de alta finalizado correctamente";
$lang['alta_usuario_conf_ko'] = "Proceso de alta no completado correctamente.Vuelva a intentar registrarse";
$lang['alta_usuario_mail_duplicado'] = "Ya existe un usuario con este E-mail";
$lang['alta_comercio_busca_municipio'] = "Sin seleccionar";
$lang['alta_comercio_boton_alta'] = "Nuevo usuario";

