<?php

//FORMULARIO LISTA_DE_TARIFAS
$lang['lista_tarifas_lista'] = "Lista de tarifas";
$lang['lista_tarifas_nombre'] = "Nombre";
$lang['lista_tarifas_fecha_ini'] = "Fecha inicio";
$lang['lista_tarifas_fecha_fin'] = "Fecha fin";
$lang['lista_tarifas_hora_ini'] = "Hora inicio";
$lang['lista_tarifas_hora_fin'] = "Hora fin";
$lang['lista_tarifas_nombre_ser'] = "Nombre servicio";
$lang['lista_tarifas_importe'] = "Importe";
$lang['lista_tarifas_iva'] = "IVA";
$lang['lista_tarifas_importetotal'] = "Importe total";
$lang['lista_tarifas_estado'] = "Estado";
$lang['lista_tarifas_dias'] = "Días";
$lang['lista_tarifas_cabecera'] = "Administrar tarifas";

//FORMULARIO ALTA_DE_TARIFAS
$lang['alta_tarifa_fecini'] = "Fecha de inicio";
$lang['alta_tarifa_fecfin'] = "Fecha de fin";
$lang['alta_tarifa_horaini'] = "Hora de inicio";
$lang['alta_tarifa_horafin'] = "Hora de fin";
$lang['alta_tarifa_nombre'] = "Nombre tarifa";
$lang['alta_tarifa_estado'] = "Estado";
$lang['alta_tarifa_servicio'] = "Servicio";
$lang['alta_tarifa_importe'] = "Importe";
$lang['alta_tarifa_iva'] = "IVA";
$lang['alta_tarifa_importetotal'] = "Importe total";
$lang['alta_tarifa_seleccion_dias'] = "Selecciona a que días de las semana aplicará la tarifa";
$lang['alta_tarifa_lunes'] = "Lunes";
$lang['alta_tarifa_martes'] = "Martes";
$lang['alta_tarifa_miercoles'] = "Miercoles";
$lang['alta_tarifa_jueves'] = "Jueves";
$lang['alta_tarifa_viernes'] = "Viernes";
$lang['alta_tarifa_sabado'] = "Sábado";
$lang['alta_tarifa_domingo'] = "Domingo";
$lang['alta_tarifa_boton'] = "Alta / Modificar tarifa";
$lang['alta_tarifas_cabecera'] = "Alta / modificación de tarifas";

$lang['alta_tarifa_ok'] = "La tarifa se ha dado de alta correctamente";
$lang['actualizar_tarifa_ok'] = "La tarifa se ha actualizado correctamente";
    

//VALIDACIONES CALLBACK
$lang['alta_tarifa_compara_fechas'] = "Fecha inicio no puede ser superior a fecha fin";
$lang['alta_tarifa_compara_horas'] = "Hora inicio no puede ser superior o igual a hora fin";
$lang['alta_tarifa_check_dias'] = "Debes marcar al menos 1 día de la semana para aplicar la tarifa";
$lang['alta_tarifa_validar_insercion'] = "Existen tarifas incompatibles en el sistema.<BR>Revise las fechas y horas que está asignando a la nueva tarifa.<br>
                                          No pueden haber 2 tarifas compartiendo las mismas fechas,horas o días de aplicación";