<?php

//FORMULARIO DE DATOS VISTA_ALTA_USUARIO.PHP
$lang['alta_usuario_nombre'] = "Nombre";
$lang['alta_usuario_email'] = "E-Mail";
$lang['alta_usuario_email2'] = "Repetir E-Mail";
$lang['alta_usuario_password'] = "Contraseña";
$lang['alta_usuario_password2'] = "Repetir Contraseña";
$lang['alta_usuario_telefono'] = "Teléfono";
$lang['alta_usuario_mail_duplicado'] = "Ya existe un usuario con este E-mail";
$lang['alta_usuario_conf_ok'] = "Proceso de alta finalizado correctamente";
$lang['alta_usuario_conf_ko'] = "Proceso de alta no completado correctamente.Vuelva a intentar registrarse";
$lang['recuperar_email_duplicado'] = "El E-mail ya existe";
$lang['alta_usuario_idioma'] = "Idioma";
$lang['alta_usuario_boton_alta'] = "Nuevo usuario";

//FORMULARIO_RECUPERAR_PASSWORD

$lang['recuperar_pass_email'] = "Introduzca su E-mail";
$lang['recuperar_pass_OK'] = "Se ha enviado la nueva  contraseña a su correo";
$lang['recuperar_password_KO'] = "Email inexistente";
$lang['recuperar_password_texto_mail'] = "Su nueva contraseña es ";

