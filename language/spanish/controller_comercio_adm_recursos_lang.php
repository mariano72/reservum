<?php

//FORMULARIO DE DATOS VISTA_DETALLE_RECURSOS.PHP
$lang['alta_recursos_nombre'] = "Nombre";
$lang['alta_recursos_descripcion'] = "Descripción";
$lang['alta_recursos_estado'] = "Estado";
$lang['alta_recursos_hora_inicio'] = "Hora inicio";
$lang['alta_recursos_hora_fin'] = "Hora fin";
$lang['alta_recursos_bloques_horarios'] = "Bloques horarios";
$lang['alta_recursos_dias_avance'] = "Dias avance calendario";
$lang['alta_recursos_minimo_dias'] = "Mínimo días anulación";
$lang['alta_recursos_operacion_ok'] = "El alta/modificación  se ha realizado correctamente";
$lang['alta_recursos_foto'] = "Añadir foto";
$lang['alta_recursos_cabecera'] = "Alta / modificación de instalaciones";


//FORMULARIO VISTA_ADMIN_RECURSOS.PHP
$lang['admin_recursos_nombre'] = "Nombre";
$lang['admin_recursos_descripcion'] = "Descripción";
$lang['admin_recursos_estado'] = "Estado";
$lang['admin_recursos_imagen'] = "Imagen";
$lang['admin_recursos_cabecera'] = "Lista de instalaciones";



//FORMULARIO VISTA_ALTA_RECURSOS.PHP, VISTA_ACTUALIZAR_RECURSOS.PHP

$lang['admin_recursos_descripcion'] = "Descripción";
$lang['admin_recursos_estado'] = "Estado";
$lang['admin_recursos_imagen'] = "Imagen";






//VALIDACIONES CALLBACK
$lang['alta_recursos_compara_fechas'] = "Fecha inicio no puede ser superior o igual a fecha fin";
$lang['alta_recursos_compara_horas'] = "Hora inicio no puede ser superior o igual a hora fin";
$lang['alta_recursos_check_tarifa'] = "Ha superado el número de recursos para la tarifa contratada";