<?php


    // Me da la diferencia entre 2 horas tanto en positivo como en negativo
    function getMyTimeDiff2($t1, $t2)

    {
        $to_time = strtotime($t1);
        $from_time = strtotime($t2);
        //return round(abs($to_time - $from_time) / 60,2);
        return round(($to_time - $from_time) / 60, 2);
    }


    function es_dia_restriccion($row)
    {

        $dias = array(7);

        if ($row->lunes == "S") {
            $dias[0] = 0;
        }
        if ($row->martes == "S") {
            $dias[1] = 1;
        }
        if ($row->miercoles == "S") {
            $dias[2] = 2;
        }
        if ($row->jueves == "S") {
            $dias[3] = 3;
        }
        if ($row->viernes == "S") {
            $dias[4] = 4;
        }
        if ($row->sabado == "S") {
            $dias[5] = 5;
        }
        if ($row->domingo == "S") {
            $dias[6] = 6;
        }
        // print_r($dias);
        return $dias;
    }


    function generar_codigo_alfanumerico($length)
    {

        $pattern = "1234567890abcdefghijklmnopqrstuvwxyz";
        $key = "";
        srand(time());

        for ($i = 0; $i < $length; $i++) {
            $key .= $pattern{rand(0, 35)};

        }

        return $key;

    }

    function generar_codigo_numerico($length)
    {

        $pattern = "1234567890";
        $key = "";
        srand(time());

        for ($i = 0; $i < $length; $i++) {
            $key .= $pattern{rand(0, 9)};

        }

        return $key;

    }

    function genera_localizador($param)
    {

        $anyo = date("y");

        $codigo_comercio = $param;
        $codigo_numerico = generar_codigo_numerico(10);
        $key = $codigo_comercio . "-" . $codigo_numerico . "-" . $anyo;

        return $key;

    }

    function actualiza_localizador($codigo_comercio, $id_reserva)
    {

        $CI =& get_instance();
        $CI->load->helper('security');
        $modulo = do_hash($id_reserva, 'md5');
        $modulo = substr($modulo, 2);
        $codigo_numerico = zerofill($id_reserva, 10);
        $key = $codigo_comercio . "-" . $codigo_numerico . "-" . $modulo;
        $key=substr($key,0,25);
        return $key;

    }


    function generar_hash($datos, $longitud_hash)
    {

        $CI =& get_instance();
        $CI->load->helper('security');
        $modulo = do_hash($datos, 'md5');
        $modulo = substr($modulo, 5);
        $hash_generado = zerofill($modulo, $longitud_hash);


        return $hash_generado;

    }

    function zerofill($numero, $ceros)
    {
        return str_pad($numero, $ceros, '0', STR_PAD_LEFT);
    }

    function calculo_fecha_desde_factura($fecha, $periodo)
    {

        $fecha_hasta = date("y-m-d", strtotime($fecha . '+1 year'));

        return $fecha_hasta;
    }

