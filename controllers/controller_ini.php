<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_ini extends CI_Controller {


    //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
    private $data=Array();
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	 public function Controller_ini()
	 {
       parent::__construct();

     }


   public function __construct()
   {
     parent::__construct();
     //$this->output->enable_profiler(TRUE);
            // Your own constructor code
        	// Now change the language
           //para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader, y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            //venga por sesi‚àö‚â•n cambiarlo din‚àö¬∞micamente y cargar el fichero de etiquetas que tengamos definido para la pantalla en cuesti‚àö‚â•n.
           //Ahora mismo en el config tenemos el espa‚àö¬±ol como predeterminado pero cambiamos f‚àö¬∞cilmente a los mensajes en ingl‚àö¬©s.
          //echo "idioma: " . $this->session->userdata('idioma');
	
	      //Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
     	//afectará sólo a esa función y dejará Ok, el resto del controlador.
	 if (!$this->session->userdata('tengo_sesion')){
        redirect('login/index');
     }
     //Si es una empresa lo mandamos a su página de inicio.
     if ($this->session->userdata('soycomercio')) {
       redirect('controller_comercio/inicio_comercio');
     }
			
	 //Asignación del idioma en función de lo que tengamos en sesión 
     $this->config->set_item('language', $this->session->userdata('idioma'));
     $this->lang->load('controller_ini');
     $this->lang->load('controller_home');
       
     //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
     $this->load->helper('language');

        //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
    //que es como lo teníamos hasta ahora.
    //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['menu']=$this->load->view('vista_menu','',TRUE);
    //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['header']=$this->load->view('vista_header','',TRUE);
   //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['footer']=$this->load->view('vista_footer','',TRUE);
	//Cargamos código jquery que gestiona cambio de idioma
	$this->data['gestion_seleccion_idioma']=$this->load->view('vista_jquery_funciones_comunes','',TRUE);
    //Cargamos HEAD
    $this->data['head']=$this->load->view('vista_head','',TRUE);
	//Cargamos definición de los scripts utilizados en la página
	$this->data['scripts_definition']=$this->load->view('vista_scripts_definition','',TRUE);
   }
 

    public function index()
	{
		
 
	}




	public function lista_comercios_municipios()
	{
        //$this->output->enable_profiler(TRUE);
        //Ponemos en la variable del método todos los contenidos comunes al controlador
        $data = $this->data;
        //Cargamos menu operaciones
        $data['menu_operaciones']=$this->load->view('vista_menu_usuario','',TRUE);
        //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
        //Sobreescribimos el header con el nuevo menú si aplica
        $data['header']=$this->load->view('vista_header',$data,TRUE);
        
        $this->load->model('municipios');
		$data['records']=$this->municipios->get_municipios_by_prov($this->input->post('provincias'));
		$data['records2']=$this->municipios->getall_provincias();
		$data['provincias']=$this->input->post('provincias');
		$data['municipios']=$this->input->post('municipios');
		$this->load->model('comercios');
		$data['comercios']=$this->comercios->get_comercios_by_municipio($this->input->post('provincias'),$this->input->post('municipios'));
		$this->load->model('calendario_reservas');
		$data['misreservas']=$this->calendario_reservas->get_last_reservas($this->session->userdata('id_usuario_portal'),5);

        $this->load->view('vista_reservar',$data);


	}





	public function reservar()
	{

        //$this->benchmark->mark('mi_marca_start');
       
        //Ponemos en la variable del método todos los contenidos comunes al controlador
        $data = $this->data;
        //Cargamos menu operaciones
        $data['menu_operaciones']=$this->load->view('vista_menu_usuario','',TRUE);
        //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
        //Sobreescribimos el header con el nuevo menú si aplica
        $data['header']=$this->load->view('vista_header',$data,TRUE);

        $data['provincias']=$this->input->post('provincias');
	    $data['municipios']=$this->input->post('municipios');
      
        $this->load->model('municipios'); 
    		
        
		$data['records']=$this->municipios->get_municipios_by_prov($this->input->post('provincias'));
		$data['records2']=$this->municipios->getall_provincias();
		$data['comercios']=array();
		$this->load->model('calendario_reservas'); 
		$data['misreservas']=$this->calendario_reservas->get_last_reservas($this->session->userdata('id_usuario_portal'),5);

        $this->load->view('vista_reservar',$data);
       
	   //$this->benchmark->mark('mi_marca_end'); 

        
	}

    public function previa_anular_reserva()
	{

        //Ponemos en la variable del método todos los contenidos comunes al controlador
        $data = $this->data;
        //Cargamos menu operaciones
        $data['menu_operaciones']=$this->load->view('vista_menu_usuario','',TRUE);
        //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
        //Sobreescribimos el header con el nuevo menú si aplica
        $data['header']=$this->load->view('vista_header',$data,TRUE);
        $data['validation_errors']=null;
        
		$this->load->model('calendario_reservas');
		$data['misreservas']=$this->calendario_reservas->get_last_reservas_para_anular($this->session->userdata('id_usuario_portal'),200);

        $this->load->view('vista_anular_reserva',$data);


	}
    
    public function anular_reserva()
    {
        //Ponemos en la variable del método todos los contenidos comunes al controlador
        $data = $this->data;
        //Cargamos menu operaciones
        $data['menu_operaciones']=$this->load->view('vista_menu_usuario','',TRUE);
        //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
        //Sobreescribimos el header con el nuevo menú si aplica
        $data['header']=$this->load->view('vista_header',$data,TRUE);
        $data['validation_errors']=null;
        //Preparamos datos
        $ip=$_SERVER['REMOTE_ADDR'];
        $datos['ip']=$ip;
        $datos['id_usuario']=$this->session->userdata('id_usuario_portal');
        $datos['id_reserva']=$this->input->post('id_reserva');

        //Validamos los datos de entrada
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('id_reserva', lang('anular_mensaje_validacion'), 'required');
	    

	    $this->load->model('calendario_reservas');

 	    if ($this->form_validation->run() == FALSE) {

          $data['validation_errors']=lang('anular_mensaje_error');
        } else {
          $this->calendario_reservas->anular_reserva($datos);
          $data['mensaje_ok']=lang('anular_mensaje_ok');
        }

        $data['misreservas']=$this->calendario_reservas->get_last_reservas_para_anular($this->session->userdata('id_usuario_portal'),5);

        $this->load->view('vista_anular_reserva',$data);


    }





}

