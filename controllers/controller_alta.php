<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_alta extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function Controller_ini()
        {
            parent::__construct();

        }


        public function __construct()
        {
            parent::__construct();
            //$this->output->enable_profiler(TRUE);


            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_alta');
            $this->lang->load('controller_home');

            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');


            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu_operaciones'] = "";
            $this->data['header'] = $this->load->view('vista_header', $this->data, TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);

        }


        public function index()
        {


        }


        public function ir_alta_usuario_reservas()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $data['errores'] = '';


            //Datos para la capa de errores
            $data['validation_errors'] = null;

            $this->load->model('idiomas');
            $data['idiomas'] = $this->idiomas->getall_idiomas();

            $this->load->view('vista_alta_usuario', $data);

        }

        public function alta_usuario_reservas()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;

            $data['errores'] = '';


            //Generar código secreto
            $this->load->helper('utilidades_helper');
            $secreto = generar_codigo_alfanumerico(10);

            //Recogemos datos entrada
            
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $data['nombre'] = $this->input->post('nombre');
            $data['email'] = $this->input->post('mail');
            $data['password'] = $this->input->post('password');
            $data['telefono'] = $this->input->post('telefono');
            $data['idioma'] = $this->input->post('idioma');
            $data['secreto'] = $secreto;
            $data['tipo_alta'] = "USUARIO";


            $data['datos_refresco_form'] = $this->input->post(NULL, TRUE); //Todo el post lo pasamos a la vista

            //Validamos los datos de entrada
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nombre', lang('alta_usuario_nombre'), 'required');
            $this->form_validation->set_rules('mail', lang('alta_usuario_email'), 'required|valid_email|callback_check_email_duplicado');
            $this->form_validation->set_rules('mail2', lang('alta_usuario_email2'), 'required|valid_email|matches[mail]');
            $this->form_validation->set_rules('password', lang('alta_usuario_password'), 'required|min_length[6]|max_length[10]');
            $this->form_validation->set_rules('password2', lang('alta_usuario_password2'), 'required|matches[password]|min_length[6]|max_length[10]');
            $this->form_validation->set_rules('telefono', lang('alta_usuario_telefono'), 'required|numeric|min_length[9]|max_length[10]');
            $this->form_validation->set_rules('idioma', lang('alta_usuario_idioma'), 'required');


            if ($this->form_validation->run() == FALSE) {
                $this->load->model('idiomas');
                $data['idiomas'] = $this->idiomas->getall_idiomas();
                $data['validation_errors'] = validation_errors();
                $this->load->view('vista_alta_usuario', $data);
                //Fin validación datos entrada
            }
            else {
                $this->load->library('encrypt');
                $data['password'] = $this->encrypt->encode($data['email'], $data['password']);

                $this->load->model('usuarios');
                $this->load->model('idiomas');
                $data['idioma'] = $this->idiomas->get_idioma_by_id($data['idioma']);
                foreach ($data['idioma'] as $valor) {
                    $data['idioma'] = $valor->cod_idioma_sesion;
                }

                $this->usuarios->alta_usuario($data);

                //Enviar mail para confirmar el al
                $this->load->helper('mail_helper');
                sendmail_alta($data);
                $data['mensaje_ok'] = "El alta se ha procesado correctamente.Recibirá un email para confirmar el proceso.";
                $this->load->view('vista_mostrar_resultado', $data);

            }

        }

        public function confirmar_alta_usuario()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;

            $data['validation_errors'] = null;
            $data['mensaje_ok'] = null;


            $datos['email'] = urldecode($this->uri->segment(3));
            $datos['secreto'] = $this->uri->segment(4);

            $this->load->model('usuarios');
            $q = $this->usuarios->confirmar_alta_usuario($datos);

            if ($q->num_rows() > 0) {
                $q = $this->usuarios->actualizar_estado_usuario($datos);
                $data['mensaje_ok'] = lang('alta_usuario_conf_ok');
            } else {
                $data['validation_errors'] = lang('alta_usuario_conf_ko');
            }

            $this->load->view('vista_mostrar_resultado', $data);
        }


        public function previa_recuperar_password()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos menu operaciones
            $data['menu_operaciones'] = null;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['mensaje_ok'] = null;

            $this->load->view('vista_recuperar_password', $data);


        }


        public function recuperar_password()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos menu operaciones
            $data['menu_operaciones'] = null;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['mensaje_ok'] = null;
            $data['datos_refresco_form'] = $this->input->post(NULL, TRUE); //Todo el post lo pasamos a la vista

            //Preparamos datos
            $datos['email'] = $this->input->post('email');
            $datos['subject'] = "Reservum - Cambio de password";
            //Validamos los datos de entrada
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', lang('cambio_pass_password'), 'required|valid_email|callback_check_email');

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();
                $this->load->view('vista_recuperar_password', $data);
            } else {
                $this->load->helper('utilidades_helper');
                $this->load->helper('mail_helper');
                $this->load->library('encrypt');
                $datos['passwordclaro'] = generar_codigo_alfanumerico(6);
                $datos['passwordcifrado'] = $this->encrypt->encode($datos['email'], $datos['passwordclaro']);
                $this->usuarios->actualizar_password_usuario_email($datos);
                $this->comercios->actualizar_password_comercio_email($datos);

                $datos['texto'] = lang('recuperar_password_texto_mail') . $datos['passwordclaro'];
                sendmail($datos);
                $data['mensaje_ok'] = lang('recuperar_pass_OK');
                $this->load->view('vista_mostrar_resultado', $data);
            }

        }


        //Funcion callback de validación de formulario para el campo mail.
        //De esta forma podemos escribir nuestras propias funciones de validación.
        public function check_email($str)
        {
            $this->load->model('usuarios');
            $this->load->model('comercios');
            $id = $this->usuarios->get_usuario_by_mail($str);
            $id2 = $this->comercios->get_comercio_by_mail($str);

            if ($id <> null || $id2 <> null) {
                return TRUE;
            } else {
                $this->form_validation->set_message('check_email', lang('recuperar_password_KO'));
                return FALSE;
            }
        }

        //Funcion callback de validación de formulario para el campo mail.
        //De esta forma podemos escribir nuestras propias funciones de validación.
        public function check_email_duplicado($str)
        {
            $this->load->model('usuarios');
            $this->load->model('comercios');
            $id = $this->usuarios->get_usuario_by_mail($str);
            $id2 = $this->comercios->get_comercio_by_mail($str);

            if ($id == null && $id2 == null) {
                return TRUE;
            } else {
                $this->form_validation->set_message('check_email_duplicado', lang('alta_usuario_mail_duplicado'));
                return FALSE;
            }
        }


    }

