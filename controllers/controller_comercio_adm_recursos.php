<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_comercio_adm_recursos extends CI_Controller {


    //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
    private $data=Array();
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

   public function Controller_comercio_adm_recursos()
   {
     parent::__construct();
	 
   }


   public function __construct()
   {
    /* Your own constructor code	 Now change the language
			para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader,
			y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
    */
    parent::__construct();
    //$this->output->enable_profiler(TRUE);
           
    /*Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
     afectará sólo a esa función y dejará Ok, el resto del controlador.
	*/ 
	if (!$this->session->userdata('soycomercio')){
        redirect('login/index');
    }
			
    //Asignación del idioma en función de lo que tengamos en sesión 
    $this->config->set_item('language', $this->session->userdata('idioma'));
    $this->lang->load('controller_comercio_adm_recursos');
    $this->lang->load('controller_home');
     //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
     $this->load->helper('language');

    //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
    //que es como lo teníamos hasta ahora.
    //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['menu']=$this->load->view('vista_menu','',TRUE);
    //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['header']=$this->load->view('vista_header','',TRUE);
   //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['footer']=$this->load->view('vista_footer','',TRUE);
	//Cargamos código jquery que gestiona cambio de idioma
	$this->data['gestion_seleccion_idioma']=$this->load->view('vista_jquery_funciones_comunes','',TRUE);
    //Cargamos HEAD
    $this->data['head']=$this->load->view('vista_head','',TRUE);
	//Cargamos definición de los scripts utilizados en la página
	$this->data['scripts_definition']=$this->load->view('vista_scripts_definition','',TRUE);
   }

    public function index()
	{
			
   
	}




    public function administrar_recursos()
    {

      //Ponemos en la variable del método todos los contenidos comunes al controlador
      $data = $this->data;
      //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
      //Sobreescribimos el header con el nuevo menú si aplica
      $data['menu_operaciones']=$this->load->view('vista_menu_adm_recursos','',TRUE);
      $data['header']=$this->load->view('vista_header',$data,TRUE);
      $data['validation_errors']=null;


      $this->load->model('recursos');
      $data['datos_recursos']=$this->recursos->get_recurso_by_idcomercio($this->session->userdata('id_usuario_portal'));

      //cuando demos de alta el recurso y despues el calendario base
      //$data['id_reserva']=$this->db->insert_id();

      $this->load->view('vista_admin_recursos',$data);


    }


    public function detalle_recursos()
    {

      //Ponemos en la variable del método todos los contenidos comunes al controlador
      $data = $this->data;
      //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
      //Sobreescribimos el header con el nuevo menú si aplica
      $data['menu_operaciones']=$this->load->view('vista_menu_adm_recursos','',TRUE);
      $data['header']=$this->load->view('vista_header',$data,TRUE);
	  
	  $data['datos_recursos']=Array();
      $data['datos_calendario']=Array();
      $id_recurso=$this->uri->segment(3);

	  $this->load->helper(array('form', 'url'));	

      $this->load->model('recursos');
	  $this->load->model('calendario_base');

      if ($id_recurso<>null){
        $data['datos_recursos']=$this->recursos->get_recurso_by_id($id_recurso);
        $data['datos_calendario']=$this->calendario_base->get_calendario_by_idrecurso($id_recurso);
        $data['url_destino']="/controller_comercio_adm_recursos/actualizar_recursos";
      } else {
        $data['url_destino']="/controller_comercio_adm_recursos/alta_recursos";
      }
      $data['estados_recursos']=$this->retorna_estados_recurso();
	  $data['tipos_fragmentacion']=$this->retorna_tipos_fragmentacion();
	  $data['horas_en_punto']=$this->retorna_horas_en_punto();
      $data['horas_medias']=$this->retorna_horas_medias();
	  $data['horas_cuartos']=$this->retorna_horas_cuartos();
	  //Para los errores del form
	  $data['validation_errors']=null;
      

      $this->load->view('vista_detalle_recursos',$data);


    }


	
    public function actualizar_recursos()
    {

       //Ponemos en la variable del método todos los contenidos comunes al controlador
       $data = $this->data;
      //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
      //Sobreescribimos el header con el nuevo menú si aplica
      $data['menu_operaciones']=$this->load->view('vista_menu_adm_recursos','',TRUE);
      $data['header']=$this->load->view('vista_header',$data,TRUE);
	  
	  $config['upload_path']=APPPATH .'/uploads';
	  $config['max_width']='4000';
      $config['max_height']='4000';
      $config['allowed_types']='gif|jpg|png';
      $config['max_size']='4000';
      $config['file_name']=$this->session->userdata('id_usuario_portal') . "_" .  $this->input->post('id_recurso') . "_". "foto_recurso";
      $config['overwrite']=TRUE;

      $this->load->library('upload', $config);
      $this->upload->do_upload();
   
      //Recuperamos datos del fichero
      $data_fichero = array('upload_data' => $this->upload->data());
	  
	  
	  $data['id_recurso']=$this->input->post('id_recurso');
	  $data['nombre']=$this->db->escape_str($this->input->post('nombre'));
	  $data['descripcion']=$this->db->escape_str($this->input->post('descripcion'));
	  $data['estado']=$this->input->post('estado');
	  $data['hora_inicio']=$this->input->post('hora_inicio');
	  $data['hora_fin']=$this->input->post('hora_fin');
	  $data['fragmentacion_horas']=$this->input->post('tipos_fragmentacion');
	  $data['mostrar_dias_avance']=$this->input->post('mostrar_dias_avance');
	  $data['min_dias_anulacion']=$this->input->post('min_dias_anulacion');
	  $data['foto_recurso']=$data_fichero['upload_data']['orig_name'];
	  
      $data['datos_refresco_form']= $this->input->post(NULL, TRUE);//Todo el post lo pasamos a la vista
	  $data['estados_recursos']=$this->retorna_estados_recurso();
	  $data['tipos_fragmentacion']=$this->retorna_tipos_fragmentacion();
	  $data['horas_en_punto']=$this->retorna_horas_en_punto();
      $data['horas_medias']=$this->retorna_horas_medias();
	  $data['horas_cuartos']=$this->retorna_horas_cuartos();
	  
	  $data['validation_errors']=null;
	  $data['mensaje_ok']=null;
	  //Validamos los datos de entrada
	  $this->load->library('form_validation');

	  $this->form_validation->set_rules('nombre', lang('alta_recursos_nombre'), 'required');
	  $this->form_validation->set_rules('descripcion',lang('alta_recursos_descripcion'), 'required');
	  $this->form_validation->set_rules('mostrar_dias_avance', lang('alta_recursos_dias_avance'), 'required|is_natural_no_zero');
	  $this->form_validation->set_rules('min_dias_anulacion', lang('alta_recursos_minimo_dias'), 'required|is_natural_no_zero');
      $this->form_validation->set_rules('hora_inicio', lang('alta_recursos_mensaje_horas'), 'callback_check_hora');
	  if ($this->form_validation->run() == FALSE)
	  {
		$data['validation_errors']=validation_errors();
		//Fin validación datos entrada
	  }
	  else{
	  
	   $this->load->model('recursos');
	   $this->load->model('calendario_base');
	   $this->recursos->modificar_recurso($data);
	   $this->calendario_base->modificar_calendario($data);
	   $data['mensaje_ok']=lang('alta_recursos_operacion_ok');
          
	   $data['datos_recursos']=$this->recursos->get_recurso_by_id($data['id_recurso']);
       $data['datos_calendario']=$this->calendario_base->get_calendario_by_idrecurso($data['id_recurso']);
	   if ($data['id_recurso']<>null){
         $data['datos_recursos']=$this->recursos->get_recurso_by_id($data['id_recurso']);
         $data['datos_calendario']=$this->calendario_base->get_calendario_by_idrecurso($data['id_recurso']);
         $data['url_destino']="/controller_comercio_adm_recursos/actualizar_recursos";
		} else {
         $data['url_destino']="/controller_comercio_adm_recursos/alta_recursos";
       }
	  }	

 
      $this->load->view('vista_detalle_recursos',$data);


    }

	public function alta_recursos()
    {

      //Ponemos en la variable del método todos los contenidos comunes al controlador
      $data = $this->data;
      //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
      //Sobreescribimos el header con el nuevo menú si aplica
      $data['menu_operaciones']=$this->load->view('vista_menu_adm_recursos','',TRUE);
      $data['header']=$this->load->view('vista_header',$data,TRUE);

	  $config['upload_path']=APPPATH .'/uploads';
	  $config['max_width']='4000';
      $config['max_height']='4000';
      $config['allowed_types']='gif|jpg|png';
      $config['max_size']='4000';
      $config['file_name']=$this->session->userdata('id_usuario_portal') . "_" .  $this->input->post('id_recurso') . "_". "foto_recurso";
      $config['overwrite']=TRUE;

      $this->load->library('upload', $config);
      $this->upload->do_upload();

      //Recuperamos datos del fichero
      $data_fichero = array('upload_data' => $this->upload->data());
	 
      $data['id_comercio']=$this->session->userdata('id_usuario_portal');
	  $data['nombre']=$this->input->post('nombre');
	  $data['descripcion']=$this->input->post('descripcion');
	  $data['estado']=$this->input->post('estado');
	  $data['hora_inicio']=$this->input->post('hora_inicio');
	  $data['hora_fin']=$this->input->post('hora_fin');
	  $data['fragmentacion_horas']=$this->input->post('tipos_fragmentacion');
	  $data['mostrar_dias_avance']=$this->input->post('mostrar_dias_avance');
	  $data['min_dias_anulacion']=$this->input->post('min_dias_anulacion');
	  $data['foto_recurso']=$data_fichero['upload_data']['orig_name'];
	  $data['datos_refresco_form']= $this->input->post(NULL, TRUE);//Todo el post lo pasamos a la vista
	  
      //Validamos los datos de entrada
	  $this->load->library('form_validation');

	  $this->form_validation->set_rules('nombre', lang('alta_recursos_nombre'), 'required|callback_check_tarifa');
	  $this->form_validation->set_rules('descripcion', lang('alta_recursos_descripcion'), 'required');
	  $this->form_validation->set_rules('mostrar_dias_avance', lang('alta_recursos_dias_avance'), 'required|is_natural_no_zero');
	  $this->form_validation->set_rules('min_dias_anulacion', lang('alta_recursos_minimo_dias'), 'required|is_natural_no_zero');
      $this->form_validation->set_rules('hora_inicio', 'hora_inicio', 'callback_check_hora');
	  if ($this->form_validation->run() == FALSE)
	  {
	  
		$data['estados_recursos']=$this->retorna_estados_recurso();
	    $data['tipos_fragmentacion']=$this->retorna_tipos_fragmentacion();
	    $data['horas_en_punto']=$this->retorna_horas_en_punto();
        $data['horas_medias']=$this->retorna_horas_medias();
	    $data['horas_cuartos']=$this->retorna_horas_cuartos();
		
		$data['validation_errors']=validation_errors();
		$this->load->view('vista_detalle_recursos',$data);
		 //Fin validación datos entrada
	  }
	  else{
	   $data['mensaje_ok']=lang('alta_recursos_operacion_ok');
       $this->load->model('recursos');
	   $this->load->model('calendario_base');
       $this->recursos->alta_recurso($data);
       //Utilizamos esta línea cuando necesitamos el ID del registro recien creado en la consulta anterior
       $data['id_recurso']=$this->db->insert_id();
       $this->calendario_base->alta_calendario($data);
	   $data['datos_recursos']=$this->recursos->get_recurso_by_idcomercio($this->session->userdata('id_usuario_portal'));
       $this->load->view('vista_admin_recursos',$data);

	  }
	  
	  

    }

	

	
	//Funcion callback de validación de formulario para los campos hora
	//De esta forma podemos escribir nuestras propias funciones de validación.
	public function check_tarifa()
	{

     $this->load->model('recursos');
     $this->load->model('tarifas');
     $numero_recursos=$this->recursos->get_num_recurso_comercio($this->session->userdata('id_usuario_portal'));
     $datos_tarifa=$this->tarifas->check_datos_tarifa_by_comercio($this->session->userdata('id_usuario_portal'));
     foreach ($numero_recursos as $row){
       $numero=$row->numero;
     }
     foreach ($datos_tarifa as $row){
       $recursos_max_disponibles=$row->recursos_max_disponibles;
     }

     if ($numero>=$recursos_max_disponibles){
        $this->form_validation->set_message('check_tarifa', lang('alta_recursos_check_tarifa'));
        return false;
     }  else {
        return true;
     }
    }

    public function check_hora()
    {

     $param['hora_inicio']=$this->input->post('hora_inicio');
     $param['hora_fin']=$this->input->post('hora_fin');
     $hora_inicio = strtotime($param['hora_inicio']);
     $hora_fin = strtotime($param['hora_fin']);
     if ($hora_inicio>=$hora_fin) {
        $this->form_validation->set_message('check_hora', lang('alta_recursos_compara_horas'));
        return false;
     } else {
        return true;
     }

    }


	//Funcion callback de validación de formulario para los campos fecha
	//De esta forma podemos escribir nuestras propias funciones de validación.
	public function check_fecha()
	{
     $param3=explode('/',$this->input->post('fecha_inicio'));

      if (count($param3)<>1){
       $param['fecha_inicio']=$param3[2] . "-" . $param3[1] . "-" . $param3[0];

      }

     $date1 = new DateTime($param['fecha_inicio']);
     $param3=explode('/',$this->input->post('fecha_fin'));

      if (count($param3)<>1){
       $param['fecha_fin']=$param3[2] . "-" . $param3[1] . "-" . $param3[0];

      }
     $date2 = new DateTime($param['fecha_fin']);


     if ($date1>=$date2) {
      $this->form_validation->set_message('check_fecha', lang('alta_restricciones_compara_fechas'));
        return false;

     } else {
     return true;
     }

	}

	//Funcion callback de validación de formulario para los campos fecha
	//De esta forma podemos escribir nuestras propias funciones de validación.
	public function dias_afectados()
	{
	  $lunes=$this->input->post('lunes');
      $martes=$this->input->post('martes');
      $miercoles=$this->input->post('miercoles');
      $jueves=$this->input->post('jueves');
      $viernes=$this->input->post('viernes');
      $sabado=$this->input->post('sabado');
      $domingo=$this->input->post('domingo');

      $dias_afectados=false;


      if ($lunes<>null) {$dias_afectados=true;}
      if ($martes<>null) {$dias_afectados=true;}
      if ($miercoles<>null) {$dias_afectados=true;}
      if ($jueves<>null) {$dias_afectados=true;}
      if ($viernes<>null) {$dias_afectados=true;}
      if ($sabado<>null) {$dias_afectados=true;}
      if ($domingo<>null) {$dias_afectados=true;}

      if ($dias_afectados) {
         return true;
      } else {
         $this->form_validation->set_message('dias_afectados', lang('alta_restricciones_check_dias'));
         return false;
      }


	}
    
	
	private function retorna_horas_en_punto(){
	  return Array("00:00:00"=>"00:00","01:00:00"=>"01:00","02:00:00"=>"02:00","03:00:00"=>"03:00","04:00:00"=>"04:00","05:00:00"=>"05:00","06:00:00"=>"06:00","07:00:00"=>"07:00","08:00:00"=>"08:00","09:00:00"=>"09:00","10:00:00"=>"10:00","11:00:00"=>"11:00","12:00:00"=>"12:00","13:00:00"=>"13:00","14:00:00"=>"14:00","15:00:00"=>"15:00","16:00:00"=>"16:00","17:00:00"=>"17:00","18:00:00"=>"18:00","19:00:00"=>"19:00","20:00:00"=>"20:00","21:00:00"=>"21:00","22:00:00"=>"22:00","23:00:00"=>"23:00");
	}
	
	private function retorna_horas_medias(){
	  return Array("00:00:00"=>"00:00","00:30:00"=>"00:30","01:00:00"=>"01:00","01:30:00"=>"01:30","02:00:00"=>"02:00","02:30:00"=>"02:30","03:00:00"=>"03:00","03:30:00"=>"03:30","04:00:00"=>"04:00","04:30:00"=>"04:30","05:00:00"=>"05:00","05:30:00"=>"05:30","06:00:00"=>"06:00","06:30:00"=>"06:30","07:00:00"=>"07:00","07:30:00"=>"07:30","08:00:00"=>"08:00","08:30:00"=>"08:30","09:00:00"=>"09:00","09:30:00"=>"09:30","10:00:00"=>"10:00","10:30:00"=>"10:30","11:00:00"=>"11:00","11:30:00"=>"11:30","12:00:00"=>"12:00","12:30:00"=>"12:30","13:00:00"=>"13:00","13:30:00"=>"13:30","14:00:00"=>"14:00","14:30:00"=>"14:30","15:00:00"=>"15:00","15:30:00"=>"15:30","16:00:00"=>"16:00","16:30:00"=>"16:30","17:00:00"=>"17:00","17:30:00"=>"17:30","18:00:00"=>"18:00","18:30:00"=>"18:30","19:00:00"=>"19:00","19:30:00"=>"19:30","20:00:00"=>"20:00","20:30:00"=>"20:30","21:00:00"=>"21:00","21:30:00"=>"21:30","22:00:00"=>"22:00","22:30:00"=>"22:30","23:00:00"=>"23:00");
	}
	
	private function retorna_horas_cuartos(){
	  return Array("00:00:00"=>"00:00","00:15:00"=>"00:15","00:30:00"=>"00:30","00:45:00"=>"00:45","01:00:00"=>"01:00","01:15:00"=>"01:15","01:30:00"=>"01:30","01:45:00"=>"01:45","02:00:00"=>"02:00","02:15:00"=>"02:15","02:30:00"=>"02:30","02:45:00"=>"02:45","03:00:00"=>"03:00","03:15:00"=>"03:15","03:30:00"=>"03:30","03:45:00"=>"03:45","04:00:00"=>"04:00","04:15:00"=>"04:15","04:30:00"=>"04:30","04:45:00"=>"04:45","05:00:00"=>"05:00","05:15:00"=>"05:15","05:30:00"=>"05:30","05:45:00"=>"05:45","06:00:00"=>"06:00","06:15:00"=>"06:15","06:30:00"=>"06:30","06:45:00"=>"06:45","07:00:00"=>"07:00","07:15:00"=>"07:15","07:30:00"=>"07:30","07:45:00"=>"07:45","08:00:00"=>"08:00","08:15:00"=>"08:15","08:30:00"=>"08:30","08:45:00"=>"08:45","09:00:00"=>"09:00","09:15:00"=>"09:15","09:30:00"=>"09:30","09:45:00"=>"09:45","10:00:00"=>"10:00","10:15:00"=>"10:15","10:30:00"=>"10:30","10:45:00"=>"10:45","11:00:00"=>"11:00","11:15:00"=>"11:15","11:30:00"=>"11:30","11:45:00"=>"11:45","12:00:00"=>"12:00","12:15:00"=>"12:15","12:30:00"=>"12:30","12:45:00"=>"12:45","13:00:00"=>"13:00","13:15:00"=>"13:15","13:30:00"=>"13:30","13:45:00"=>"13:45","14:00:00"=>"14:00","14:15:00"=>"14:15","14:30:00"=>"14:30","14:45:00"=>"14:45","15:00:00"=>"15:00","15:15:00"=>"15:15","15:30:00"=>"15:30","15:45:00"=>"15:45","16:00:00"=>"16:00","16:15:00"=>"16:15","16:30:00"=>"16:30","16:45:00"=>"16:45","17:00:00"=>"17:00","17:15:00"=>"17:15","17:30:00"=>"17:30","17:45:00"=>"17:45","18:00:00"=>"18:00","18:15:00"=>"18:15","18:30:00"=>"18:30","18:45:00"=>"18:45","19:00:00"=>"19:00","19:15:00"=>"19:15","19:30:00"=>"19:30","19:45:00"=>"19:45","20:00:00"=>"20:00","20:15:00"=>"20:15","20:30:00"=>"20:30","20:45:00"=>"20:45","21:00:00"=>"21:00","21:15:00"=>"21:15","21:30:00"=>"21:30","21:45:00"=>"21:45","22:00:00"=>"22:00","22:15:00"=>"22:15","22:30:00"=>"22:30","22:45:00"=>"22:45","23:00:00"=>"23:00","23:15:00"=>"23:15","23:30:00"=>"23:30","23:45:00"=>"23:45");
	}
	
	private function retorna_tipos_fragmentacion(){
      
      
	  if ($this->session->userdata('idioma')=='english'){
           return Array("EN_PUNTO"=>"EVERY 60 MINUTES","CUARTOS"=>"EVERY 15 MINUTES","MEDIAS"=>"EVERY 30 MINUTES");
      } elseif ($this->session->userdata('idioma')=='catala'){
          return Array("EN_PUNTO"=>"CADA 60 MINUTS","CUARTOS"=>"CADA 15 MINUTS","MEDIAS"=>"CADA 30 MINUTS");
      } else {
	     return Array("EN_PUNTO"=>"CADA 60 MINUTOS","CUARTOS"=>"CADA 15 MINUTOS","MEDIAS"=>"CADA 30 MINUTOS");
	  }


	}
	
	
	private function retorna_estados_recurso()
    {
      if ($this->session->userdata('idioma')=='english'){
          return Array("A"=>"ONLINE","B"=>"OFFLINE");
	  } elseif ($this->session->userdata('idioma')=='catala'){
         return Array("A"=>"ALTA","B"=>"BAIXA");
      } else {
	     return Array("A"=>"ALTA","B"=>"BAJA");
	  }
	}
	
	
	private function retorna_tipos_iva()
    {
     return Array("0.18"=>"18%","0.21"=>"21%","0.23"=>"23%");
	}
	
	private function retorna_lista_tiempo()
    {
      if ($this->session->userdata('idioma')=='english'){
          return Array("15"=>"15 minutes","30"=>"30 minutes","45"=>"45 minutes","60"=>"60 minutes","75"=>"75 minutes","90"=>"90 minutes","120"=>"120 minutes");
	  } elseif ($this->session->userdata('idioma')=='catala'){
         return Array("15"=>"15 minuts","30"=>"30 minuts","45"=>"45 minuts","60"=>"60 minuts","75"=>"75 minuts","90"=>"90 minuts","120"=>"120 minuts");
      } else {
	     return Array("15"=>"15 minutos","30"=>"30 minutos","45"=>"45 minutos","60"=>"60 minutos","75"=>"75 minutos","90"=>"90 minutos","120"=>"120 minutos");
	  }
	}  
	
	public function retorna_jquery_combo (){
	  $this->output->enable_profiler(FALSE); //Sino no funciona bien las peticiones ajax
	  
	  $tipo=$this->input->post('tipos_fragmentacion');
	  if ($tipo=='CUARTOS') {
	    $rpta= json_encode ($this->retorna_horas_cuartos());
	  } elseif ($tipo=='MEDIAS')	{
	    $rpta= json_encode ($this->retorna_horas_medias());
	  } elseif ($tipo=='EN_PUNTO')	{
	    $rpta= json_encode ($this->retorna_horas_en_punto());
	  }
	  echo $rpta;		
	 
	}
	  
	
}
