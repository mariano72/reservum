<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_comun extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/controller_comun
         *    - or -
         *         http://example.com/index.php/controller_comun/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function Controller_comun()
        {

            parent::__construct();

            $this->config->set_item('language', $this->session->userdata('idioma'));


        }


        public function __construct()
        {
            parent::__construct();

            // Your own constructor code
            // Now change the language
            //para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader, y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            //venga por sesi‚àö‚â•n cambiarlo din‚àö¬∞micamente y cargar el fichero de etiquetas que tengamos definido para la pantalla en cuesti‚àö‚â•n.
            //Ahora mismo en el config tenemos el espa‚àö¬±ol como predeterminado pero cambiamos f‚àö¬∞cilmente a los mensajes en ingl‚àö¬©s.
            //echo "idioma: " . $this->session->userdata('idioma');

            //Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
            //afectará sólo a esa función y dejará Ok, el resto del controlador.

            //El profiling afecta al json y no lo devuelve bien
            //$this->output->enable_profiler(TRUE);

            if (!$this->session->userdata('tengo_sesion')) {
                $redirect = 'login/index/' . $this->uri->segment(3);
                redirect($redirect);
            }


            //Asignación del idioma en función de lo que tengamos en sesión
            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_comun');
            $this->lang->load('controller_home');
            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');

            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['header'] = $this->load->view('vista_header', '', TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);
        }


        public function index()
        {


            //Se activa la cache N minutos para la p‚àö¬∞gina que se va a mostrar a continuaci‚àö‚â•n.
            //$this->output->cache(2);


        }


        //Pedimos la agenda  para un  comercio y día seleccionado y para un servicio que hemos escogido.
        public function dame_agenda_dia()
        {

            //$this->output->enable_profiler(TRUE);

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $data['validation_errors'] = null;


            //Cargamos menu operaciones
            if ($this->session->userdata('soyusuario')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
            } else if ($this->session->userdata('soycomercio')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            }
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);

            //Recogemos los datos de la reserva que hemos guardado.
            $datos_reserva = $this->session->userdata('datos_reserva');
            $param = $datos_reserva['idcomercio'];
            $fechareserva = $this->input->post('fechareserva');

            if ($fechareserva <> null) { //Venimos de búsqueda del formulario
                $param3 = explode('/', $this->input->post('fechareserva'));
                $param2 = $param3[2] . "-" . $param3[1] . "-" . $param3[0];
            } else { //Venimos de que no ha validado correctamente
                $param3 = explode('-', $datos_reserva['fechareserva']);
                $param2 = $datos_reserva['fechareserva'];
                $fechareserva = $param3[2] . "/" . $param3[1] . "/" . $param3[0];
            }

            $param4 = $datos_reserva['idservicio'];
            //Esto es una chapuza mientras busco solución definitiva al tema de los mensajes de error por sesión de esta funcionalidad
            if (count($this->session->userdata('mensajeerror')) > 0) {
                $data['mensajeerror'] = $this->session->userdata('mensajeerror');
                $this->session->set_userdata('mensajeerror', '');
            }

            $this->load->model('servicios');
            $data['datos_servicio'] = $this->servicios->get_servicio_by_id($param4);

            $this->load->model('calendario_festivos');
            $this->load->model('calendario_restricciones');
            $this->load->model('calendario_reservas');
            $this->load->model('calendario_base');
            $this->load->model('idiomas');
            $data['jquery_idioma'] = $this->idiomas->get_idioma_by_cod_sesion($this->session->userdata('idioma'));
            $this->load->helper('utilidades_helper');

            $marca_reserva = array();
            $marca_restriccion = array();
            $marca_reserva_p = array();

            $festivo = FALSE;

            //Miro si el día es festivo para el comercio
            $festivo = $this->calendario_festivos->check_festivo_comercio($param, $param2);
            $datos['id_comercio'] = $param;
            $datos['fecha'] = $param2;
            $datos['id_servicio'] = $param4;

            if (!$festivo) { // Sino es festivo todo como siempre

                //Aquí tenemos la plantilla con las horas para el recurso seleccionado y hemos añadido las reservas hechas.
                $data['horas'] = $this->calendario_base->get_horas_libres_dia($datos);
                //Aqu‚àö‚â† tenemos la plantilla con las horas para el recurso seleccionado
                $data['reservas'] = $this->calendario_reservas->get_reservas_dia($param, $param2, $param4);
                //Aqu‚àö‚â† nos falta recoger las restricciones para un d‚àö‚â†a dado y a‚àö¬±adirlas al sistema
                $data['restricciones'] = $this->calendario_restricciones->get_restricciones_dia($param, $param2, $param4);

                //Cojo por orden las horas de inicio de las reservas y comparo con la lista de horas de la plantilla.
                $marca_reserva_p = null;
                foreach ($data['reservas'] as $row) {

                    $recurso = $row->id_recurso;
                    $hora_reserva_ini = $row->hora_inicio;
                    $hora_reserva_fin = $row->hora_fin;
                    $estado_reserva = $row->estado;
                    $indice = 0;
                    //$hora_fin=false;

                    foreach ($data['horas'] as $hora) {

                        $recurso_horas = $hora[0];

                        if ($recurso_horas == $recurso) { //Si los 2 recursos son iguales marco las reservas en el array
                            //   echo count($data['horas']);
                            $Diff = getMyTimeDiff2($hora[2], $hora_reserva_ini);
                            $Diff2 = getMyTimeDiff2($hora[2], $hora_reserva_fin);

                            //echo $hora[0] . " XXX " .$hora[2] . " XXX" . $hora_reserva_ini . " XXX " . $hora_reserva_fin . " XXX "  . $Diff . " XXX " . $Diff2 . "<br>";

                            if ($Diff >= 0 && $Diff2 <= 0) {
                                if ($Diff2 == 0 && (count($data['horas']) - 1) == $indice) {
                                    if ($estado_reserva == 'A') {
                                        $marca_reserva[] = $indice;
                                    }
                                    elseif ($estado_reserva == 'P') {
                                        $marca_reserva_p[] = $indice;
                                    }
                                }
                                if ($Diff2 < 0) {
                                    if ($estado_reserva == 'A') {
                                        $marca_reserva[] = $indice;
                                    }
                                    elseif ($estado_reserva == 'P') {
                                        $marca_reserva_p[] = $indice;
                                    }
                                }
                            }

                        }


                        $indice++;
                    }
                }


                $marca_restriccion = $this->marca_restricciones_recursos($data['restricciones'], $data['horas']);

            } else { //Si es Festivo

                //Aqu‚àö‚â† tenemos la plantilla con las horas para el recurso seleccionado y hemos a‚àö¬±adido las reservas hechas.
                $data['horas'] = array();
                //Aqu‚àö‚â† tenemos la plantilla con las horas para el recurso seleccionado
                $data['reservas'] = array();
                //Aqu‚àö‚â† nos falta recoger las restricciones para un d‚àö‚â†a dado y a‚àö¬±adirlas al sistema
                $data['restricciones'] = array();


            }


            //FINALMENTE PASAMOS LAS RESERVAS Y RESTRICCIONES AL ARRAY FINAL PARA PASARLOS A LA VISTA.
            $data['reservas_marcadas'] = $marca_reserva;
            if (!empty($marca_reserva_p)) {
                $data['reservas_marcadas_pendientes'] = $marca_reserva_p;
            }
            else {
                $array_marca_falsa = array("-1");
                $data['reservas_marcadas_pendientes'] = $array_marca_falsa;
            }

            $data['restricciones_marcadas'] = $marca_restriccion;

            //aquí pasamos los datos de la reserva que ya tenemos para que si se hace efectiva se pasen los params correctamente
            $data['id_comercio'] = $param;
            $data['fechareserva'] = $fechareserva;

            $this->load->view('vista_lista_horas', $data);
        }


        private function marca_restricciones_recursos($restricciones, $horas)
        {

            //Cojo por orden las horas de inicio de las restricciones y comparo con la lista de horas de la plantilla.
            //Trabajando en el tema de restricciones que afectan a determinados d√≠as de la semana.
            $marca_restriccion = array();

            foreach ($restricciones as $row) {

                $recurso = $row->id_recurso;
                $hora_reserva_ini = $row->hora_inicio;
                $hora_reserva_fin = $row->hora_fin;
                $dia_semana = $row->dia_semana;
                $dias_afectados = $row->dias_afectados;
                $lista_dias = es_dia_restriccion($row);

                $indice = 0;
                //	$hora_fin=false;
                if ($dias_afectados == "N" || ($dias_afectados == "S" && in_array($dia_semana, $lista_dias) == true)) {
                    foreach ($horas as $hora) {

                        $recurso_horas = $hora[0];

                        if ($recurso_horas == $recurso) { //Si los 2 recursos son iguales marco las reservas en el array
                            $Diff = getMyTimeDiff2($hora[2], $hora_reserva_ini);
                            $Diff2 = getMyTimeDiff2($hora[2], $hora_reserva_fin);

                            //  echo $hora[0] . " Inicio Franja " . $hora[2] . "fin franja " . $hora[3] . " XXX" . $hora_reserva_ini . " XXX " . $hora_reserva_fin . " XXX " . $Diff . " XXX " . $Diff2 . "xx " . count($data['horas']) . " xx " . $indice . "<br>";

                            if ($Diff >= 0 && $Diff2 <= 0) {
                                if ($Diff2 < 0) {
                                    $marca_restriccion[] = $indice;

                                }

                            }
                            if ($hora_reserva_ini >= $hora[2] && $hora_reserva_fin <= $hora[3]) {
                                $marca_restriccion[] = $indice;

                            }
                            //Nueva condición
                            if ($hora_reserva_ini >= $hora[2] && $hora_reserva_ini <= $hora[3] && $hora_reserva_fin >= $hora[3]) {
                                $marca_restriccion[] = $indice;

                            }


                        }

                        $indice++;
                    }
                }
            }
            return $marca_restriccion;

        }


        //incompleto y pendiente de probar
        public function genera_pdf_factura()
        {


            //Tema de los pdf's
            require('fpdf.php');

            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(40, 10, utf8_decode('ñññññpágina pdf con FPDF!'));

            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 18);
            $pdf->Cell(40, 10, utf8_decode('ñññññpágina pdf con FPDF!'));

            $pdf->Cell(30, 30, 'Estamos viendo', 1, 1, 'C');

            $pdf->Text(10, 100, 'prueba');
            $pdf->Output();


        }

        public function reserva_cita_comercio()
        {
            //$this->output->enable_profiler(TRUE);

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $data['validation_errors'] = null;

            //Carga modelos
            $this->load->model('comercios');
            $this->load->model('servicios');
            $this->load->model('idiomas');
            $data['jquery_idioma'] = $this->idiomas->get_idioma_by_cod_sesion($this->session->userdata('idioma'));
            //Cargamos el menu de comercios o usuarios y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            if ($this->session->userdata('soycomercio')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
                $data['id_comercio'] = $this->session->userdata('id_usuario_portal');
                $data['datos_comercio'] = $this->comercios->get_comercio_by_code($this->session->userdata('id_usuario_portal'));
                $data['datos_servicios'] = $this->servicios->get_servicios_by_comercio($this->session->userdata('id_usuario_portal'));

            }
            if ($this->session->userdata('soyusuario')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
                $data['id_comercio'] = $this->uri->segment(3);
                $data['datos_comercio'] = $this->comercios->get_comercio_by_code($this->uri->segment(3));
                $data['datos_servicios'] = $this->servicios->get_servicios_by_comercio($this->uri->segment(3));

            }
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);

            $this->load->view('vista_reserva_cita_comercio', $data);


        }


        public function validar_reserva_cita_comercio()
        {
            //$this->output->enable_profiler(TRUE);

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $data['validation_errors'] = null;
            $data['datos_refresco_form'] = $this->input->post(NULL, TRUE); //Todo el post lo pasamos a la vista

            $data['id_comercio'] = $this->input->post('id_comercio');
            $data['id_servicio'] = $this->input->post('id_servicio');
            $data['fechareserva'] = $this->input->post('fechareserva');

            //Validamos los datos de entrada
            $this->load->library('form_validation');

            $this->form_validation->set_rules('id_comercio', 'Comercio', 'required|callback_comercio_alta');
            $this->form_validation->set_rules('id_servicio', 'Reservar', 'required');
            $this->form_validation->set_rules('fechareserva', 'fecha de la reserva', 'required');


            //Carga modelos
            $this->load->model('comercios');
            $this->load->model('servicios');
            $this->load->model('idiomas');
            $data['jquery_idioma'] = $this->idiomas->get_idioma_by_cod_sesion($this->session->userdata('idioma'));

            //Cargamos el menu de comercios o usuarios y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            if ($this->session->userdata('soycomercio')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
                $data['id_comercio'] = $this->session->userdata('id_usuario_portal');
                $data['datos_comercio'] = $this->comercios->get_comercio_by_code($this->session->userdata('id_usuario_portal'));
                $data['datos_servicios'] = $this->servicios->get_servicios_by_comercio($this->session->userdata('id_usuario_portal'));

            }
            if ($this->session->userdata('soyusuario')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
                $data['datos_comercio'] = $this->comercios->get_comercio_by_code($data['id_comercio']);
                $data['datos_servicios'] = $this->servicios->get_servicios_by_comercio($data['id_comercio']);

            }

            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();
                $this->load->view('vista_reserva_cita_comercio', $data);
            } else {

                //Añadimos datos a la sesión para llamar a la agenda del dia
                $datos_reserva = array(
                    'idcomercio' => $data['id_comercio'],
                    'idservicio' => $data['id_servicio'],
                    'fechareserva' => $data['fechareserva']);

                $this->session->set_userdata('datos_reserva', $datos_reserva);
                $this->dame_agenda_dia();


            }


        }


        public function previa_reserva()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $this->load->helper('utilidades_helper');

            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            if ($this->session->userdata('soycomercio')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            }
            if ($this->session->userdata('soyusuario')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
            }
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $id_sertar = 'null';
            $data['datos_materiales'] = null;

            if ($this->session->userdata('soycomercio')) {
                $id_comercio = $this->session->userdata('id_usuario_portal');
            } else {
                $id_comercio = $this->uri->segment(3);
            }
            $this->load->model('comercios');
            $data['datos_comercio'] = $this->comercios->get_comercio_by_code($id_comercio);
            $this->load->model('direcciones');
            $data['datos_direccion'] = $this->direcciones->get_direccion_by_comercio($id_comercio);
            $this->load->model('recursos');
            $data['datos_recurso'] = $this->recursos->get_recurso_by_id($this->uri->segment(4));
            $this->load->model('servicios');
            $data['datos_servicio'] = $this->servicios->get_servicio_by_id($this->uri->segment(5));

            foreach ($data['datos_servicio'] as $row)
            {
                $duracion_minutos = $row->duracion_minutos;
                $importe = $row->importe;
                $iva = $row->iva;
                $importe_total = $row->importe_total;
            }

            foreach ($data['datos_comercio'] as $row)
            {
                $codigo_comercio = $row->codigo_comercio;
            }

            $this->load->model('calendario_reservas');
            $this->load->model('calendario_base');
            $this->load->model('calendario_festivos');
            $this->load->model('calendario_restricciones');
            $this->load->model('servicios_tarifas');
            $data['datos_reserva'] = $this->calendario_reservas->get_fin_reserva(urldecode($this->uri->segment(6)), $duracion_minutos);


            $param_tarifa['fechareserva'] = $this->uri->segment(7);
            $param_tarifa['id_servicio'] = $this->uri->segment(5);
            $param_tarifa['id_comercio'] = $id_comercio;
            $param_tarifa['hora_inicio'] = urldecode($this->uri->segment(6));
            $param_tarifa['hora_fin'] = $data['datos_reserva'][0];
            $this->load->model('materiales_maestro');

            $data['datos_tarifa'] = $this->servicios_tarifas->get_tarifa_aplicable($param_tarifa);
            foreach ($data['datos_tarifa'] as $row)
            {
                $id_sertar = $row->id_sertar;
                $importe = $row->importe;
                $iva = $row->iva;
                $importe_total = $row->importe_total;
            }

            $data['datos_materiales'] = $this->materiales_maestro->get_materiales_servicio($param_tarifa);



            $datos_reserva = array(
                'idcomercio' => $id_comercio,
                'idservicio' => $this->uri->segment(5),
                'fechareserva' => $this->uri->segment(7),
                'id_recurso' => $this->uri->segment(4),
                'hora_inicio' => urldecode($this->uri->segment(6)),
                'hora_fin' => $data['datos_reserva'][0],
                'duracion_minutos' => $duracion_minutos,
                'id_usuario' => $this->session->userdata('id_usuario_portal'),
                'estado' => 'P',
                'ip' => $_SERVER['REMOTE_ADDR'],
                'importe' => $importe,
                'iva' => $iva,
                'importe_total' => $importe_total,
                'localizador' => genera_localizador($codigo_comercio),
                'codigo_comercio' => $codigo_comercio,
                'id_sertar' => $id_sertar,
                'hash_url' => $this->uri->segment(8)
            );


            if ($this->session->userdata('soycomercio')) {
                $datos_reserva['id_usuario'] = -1;
                $datos_reserva['flag_usuario_reserva'] = 'COMERCIO';
                $datos_reserva['nombre'] = $this->input->post('nombre');
                $datos_reserva['email'] = $this->input->post('email');
                $datos_reserva['telefono'] = $this->input->post('telefono');

            } else {
                $datos_reserva['flag_usuario_reserva'] = 'USUARIO';
                $datos_reserva['nombre'] = "";
                $datos_reserva['email'] = "";
                $datos_reserva['telefono'] = "";
            }
            //Asignamos los datos de la reserva a la sessión y a la variable para compartir la vista
            $this->session->set_userdata('datos_reserva', $datos_reserva);
            $data['datos_reserva'] = $datos_reserva;
            $this->session->set_userdata('mensajeerror', '');

            $data['valida_reserva'] = $this->validar_reserva();

            if ($data['valida_reserva']['validado'] == 'OK') {

                //Eliminamos las reservas pendientes que pueda tener este usuario
                $this->calendario_reservas->anular_reservas_pendientes_usuario($datos_reserva);

                //Realizar reserva en estado pendiente
                $this->calendario_reservas->realiza_reserva($datos_reserva);
                $data['id_reserva'] = $this->db->insert_id();
                $this->load->view('vista_previa_reserva', $data);

            } else {
                $this->dame_agenda_dia();
            }


        }


        private function validar_reserva()
        {

            $param = $this->session->userdata('datos_reserva');
            $q = $this->calendario_base->valida_calendario_base($param);

            if ($q->num_rows() == 0) {
                $resultado['validado'] = "KO";
                $mensaje = lang('validar_reserva_calendario_base');
                $this->session->set_userdata('mensajeerror', $mensaje);
                return $resultado;
            }
            $q = $this->calendario_festivos->valida_calendario_festivos($param);

            if ($q->num_rows() == 0) {
                $resultado['validado'] = "KO";
                $mensaje = lang('validar_reserva_calendario_festivos');
                $this->session->set_userdata('mensajeerror', $mensaje);
                return $resultado;
            }

            //Si la reserva es para el día en curso miramos no superar la hora actual
            if ($param['fechareserva'] == date("Y-m-d")) {

                $this->load->helper('utilidades_helper');
                $hora_actual = date("H:i");
                $hora_reserva = $param['hora_inicio'];
                $diferencia = getMyTimeDiff2($hora_actual, $hora_reserva);

                if ($diferencia >= 0) {
                    $resultado['validado'] = "KO";
                    $mensaje = lang('validar_reserva_hora');
                    $this->session->set_userdata('mensajeerror', $mensaje);
                    return $resultado;
                }
            } elseif (($param['fechareserva'] < date("Y-m-d"))) {
                $resultado['validado'] = "KO";
                $mensaje = lang('validar_reserva_fecha');
                $this->session->set_userdata('mensajeerror', $mensaje);
                return $resultado;
            }



            $q = $this->calendario_restricciones->valida_tiempo($param);

            if ($q->num_rows() > 0) {

                $hay_restriccion = false;

                foreach ($q->result() as $row)
                {
                    $que_dia_es = $row->dia_semana;
                    $es_lunes = $row->lunes;
                    $es_martes = $row->martes;
                    $es_miercoles = $row->miercoles;
                    $es_jueves = $row->jueves;
                    $es_viernes = $row->viernes;
                    $es_sabado = $row->sabado;
                    $es_domingo = $row->domingo;

                    if ($row->dias_afectados == 'N') $hay_restriccion = true;
                    if ($que_dia_es == 0 && $es_lunes == 'S') $hay_restriccion = true;
                    if ($que_dia_es == 1 && $es_martes == 'S') $hay_restriccion = true;
                    if ($que_dia_es == 2 && $es_miercoles == 'S') $hay_restriccion = true;
                    if ($que_dia_es == 3 && $es_jueves == 'S') $hay_restriccion = true;
                    if ($que_dia_es == 4 && $es_viernes == 'S') $hay_restriccion = true;
                    if ($que_dia_es == 5 && $es_sabado == 'S') $hay_restriccion = true;
                    if ($que_dia_es == 6 && $es_domingo == 'S') $hay_restriccion = true;
                }

                if ($hay_restriccion) {
                    $resultado['validado'] = "KO";
                    $mensaje = lang('validar_reserva_dia_habil');
                    $this->session->set_userdata('mensajeerror', $mensaje);
                    return $resultado;
                }
            }


            $q = $this->calendario_reservas->valida_tiempo_reserva($param);
            if ($q->num_rows() > 0) {
                $resultado['validado'] = "KO";
                $mensaje = lang('validar_reserva_tiempo_disponible');   
                $this->session->set_userdata('mensajeerror', $mensaje);
                return $resultado;

            }

            //Validamos que los parámetros de la url no se han alterado
            $url=site_url("/controller_comun/previa_reserva/" . $param['idcomercio'] . "/" . $param['id_recurso'] . "/" . $param['idservicio'] .
                          "/" . urlencode($param['hora_inicio']) . "/" . $param['fechareserva']);
            $nuevo_hash=generar_hash($url,20);
            if ($param['hash_url']<>$nuevo_hash){
                $resultado['validado'] = "KO";
                $mensaje = lang('validar_reserva_url');
                $this->session->set_userdata('mensajeerror', $mensaje);
                return $resultado;
            }

            $resultado['validado'] = "OK";
            $this->session->set_userdata('mensajeerror', '');
            return $resultado;


        }

        public function realiza_reserva()
        {
            //$this->output->enable_profiler(TRUE);

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;

            //Cargamos menu operaciones para usuarios o comercios
            if ($this->session->userdata('soycomercio')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            } else {
                $data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
            }
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            //Datos para la capa de errores
            $data['validation_errors'] = null;
            $datos['check_material'] = null;
            $data['datos_refresco_form'] = $this->input->post(NULL, TRUE); //Todo el post lo pasamos a la vista

            $this->load->helper('utilidades_helper');

            $datos_reserva = $this->session->userdata('datos_reserva');
            $datos['id_reserva'] = $this->input->post('id_reserva');
            $datos['estado'] = 'A';
            $datos['nombre'] = $this->input->post('nombre_cliente');
            $datos['email'] = $this->input->post('email');
            $datos['telefono'] = $this->input->post('telefono_cliente');
            $datos['localizador'] = actualiza_localizador($datos_reserva['codigo_comercio'], $datos['id_reserva']);
            //Calculos para el tema de los materiales
            $datos['id_comercio'] = $this->input->post('id_comercio');
            $datos['materiales'] = $this->input->post('materiales');
            $datos['importe_total'] = $this->input->post('importe_total');
            $datos['importe'] = $datos_reserva['importe'];
            $datos['iva'] = $datos_reserva['iva'];
            $datos['importe_total_materiales'] = $this->input->post('importe_total_materiales');
            if ($datos['importe_total_materiales'] > $datos['importe_total']) {
                $datos['importe_total'] = $datos['importe_total_materiales'];
                $datos['importe'] = round(($datos['importe_total_materiales'] / (1 + $datos['iva'])), 2);
            }

            //Validamos los datos de entrada
            $this->load->library('form_validation');
            $this->form_validation->set_rules('id_reserva', 'id_reserva', 'required');
            if ($this->session->userdata('soycomercio')) {
                $this->form_validation->set_rules('nombre_cliente', 'nombre', 'required');
                $this->form_validation->set_rules('email', 'email', 'required|valid_email');
                $this->form_validation->set_rules('telefono_cliente', 'telefono_cliente', 'required|numeric');
            }

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();
                $this->load->model('materiales_maestro');
                $param['id_comercio'] = $this->input->post('id_comercio');
                $param['id_servicio'] = $this->input->post('id_servicio');
                $data['datos_materiales'] = $this->materiales_maestro->get_materiales_servicio($param);
                $this->load->view('vista_previa_reserva', $data);
            } else {
                $this->load->model('calendario_reservas');
                $this->load->model('usuarios');
                $this->load->model('materiales_reserva');

                if ($this->session->userdata('soycomercio')) {
                    //Recuperamos el id_usuario si existe y sino queda asignado al comercio
                    $usuario_portal = $this->usuarios->get_usuario_by_mail($datos['email']);
                    foreach ($usuario_portal as $row) {
                        $datos['id_usuario'] = $row->id_usuario;
                    }
                    if (!isset($datos['id_usuario'])) {
                        $datos['id_usuario'] = -1;
                    }
                } else {
                    $datos['id_usuario'] = $this->session->userdata('id_usuario_portal');

                }
                //Verificamos que no ha pasado mas tiempo del permitido para reservar y Realizamos reserva
                if ($datos['materiales'] != null) {
                    $datos['check_material'] = 'S';
                }
                $this->calendario_reservas->actualiza_estado_reserva($datos);
                if ($this->db->affected_rows() == 1) {
                    if ($datos['materiales'] != null) {
                        foreach ($datos['materiales'] as $valor) {
                            $id_material = explode("-", $valor);
                            $datos['id_material'] = $id_material[0];
                            $this->materiales_reserva->alta_materiales_reserva($datos);

                        }
                    }

                    $data['mensaje_ok'] = lang('previa_reserva_OK');
                    $data['misreservas']=$this->calendario_reservas->get_last_reservas_para_anular($datos['id_usuario'],200);
                    $this->load->helper('mail_helper');
                    $datos['subject'] = 'Reservum.';
                    $texto = "Reserva realizada correctamente";

                    $datos['texto'] = $texto;
                    $datos['email'] =$datos['email'];
                    sendmail($datos);
                }
                else {
                    $data['validation_errors'] = lang('previa_reserva_KO');
                }
                $data['localizador']=$datos['localizador'];
                $this->load->view('vista_mostrar_resultado', $data);
            }


        }


        public function previa_cambiar_password()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos menu operaciones
            if ($this->session->userdata('soycomercio')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            } elseif ($this->session->userdata('soyusuario')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
            }

            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['mensaje_ok'] = null;

            $this->load->view('vista_cambiar_password', $data);


        }


        public function cambiar_password()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos menu operaciones
            //Cargamos menu operaciones
            if ($this->session->userdata('soycomercio')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            } elseif ($this->session->userdata('soyusuario')) {
                $data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
            }
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['mensaje_ok'] = null;
            $data['datos_refresco_form'] = $this->input->post(NULL, TRUE); //Todo el post lo pasamos a la vista

            //Preparamos datos
            $ip = $_SERVER['REMOTE_ADDR'];
            $datos['ip'] = $ip;
            $datos['id_usuario'] = $this->session->userdata('id_usuario_portal');
            $datos['password'] = $this->input->post('password');
            $datos['newpassword'] = $this->input->post('newpassword');
            $datos['newpassword2'] = $this->input->post('newpassword2');
            //Validamos los datos de entrada
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', lang('cambio_pass_password'), 'required|min_length[6]|max_length[10]|callback_check_password');
            $this->form_validation->set_rules('newpassword', lang('cambio_pass_newpassword'), 'required|min_length[6]|max_length[10]');
            $this->form_validation->set_rules('newpassword2', lang('cambio_pass_newpassword2'), 'required|matches[newpassword]|min_length[6]|max_length[10]');

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();
                $this->load->view('vista_cambiar_password', $data);
            } else {
                $this->load->library('encrypt');
                $datos['newpassword'] = $this->encrypt->encode($this->session->userdata('mail'), $datos['newpassword']);
                $this->load->model('usuarios');
                $this->load->model('comercios');
                if ($this->session->userdata('soyusuario')) {
                    $this->usuarios->actualizar_password_usuario($datos);
                } elseif ($this->session->userdata('soycomercio')) {
                    $this->comercios->actualizar_password_comercio($datos);
                }

                $data['mensaje_ok'] = lang('cambio_pass_OK');
                $this->load->view('vista_mostrar_resultado', $data);
            }

        }

        //Funcion callback de validación de formulario para el campo password.
        //De esta forma podemos escribir nuestras propias funciones de validación.
        public function check_password($str)
        {
            $this->load->model('usuarios');
            $this->load->model('comercios');
            if ($this->session->userdata('soyusuario')) {
                $datos = $this->usuarios->validar_login_usuario($this->session->userdata('mail'));
            } elseif ($this->session->userdata('soycomercio')) {
                $datos = $this->comercios->validar_login_comercio($this->session->userdata('mail'));

            }
            foreach ($datos as $row) {
                $password = $row->password;
            }
            $this->load->library('encrypt');
            $email_descifrado = $this->encrypt->decode($password, $str);

            if ($email_descifrado <> $this->session->userdata('mail')) {
                $this->form_validation->set_message('check_password', lang('cambio_pass_check_pass'));
                return FALSE;
            } else {
                return TRUE;
            }
        }

        //Funcion callback de validación de formulario para el campo id_comercio
        public function comercio_alta($str)
        {

            $this->load->model('comercios');
            $datos = $this->comercios->get_comercio_by_code($str);

            foreach ($datos as $row) {
                $estado = $row->estado;
            }

            if ($estado == 'A') {
                return true;
            } else {
                $this->form_validation->set_message('comercio_alta', lang('validar_reserva_comercio_alta'));
                return false;
            }
        }


    }

