<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_comun_no_seg extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/controller_comun
         *    - or -
         *         http://example.com/index.php/controller_comun/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function Controller_comun()
        {

            parent::__construct();

            $this->config->set_item('language', $this->session->userdata('idioma'));


        }


        public function __construct()
        {
            parent::__construct();

            // Your own constructor code
            // Now change the language
            //para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader, y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            //venga por sesi‚àö‚â•n cambiarlo din‚àö¬∞micamente y cargar el fichero de etiquetas que tengamos definido para la pantalla en cuesti‚àö‚â•n.
            //Ahora mismo en el config tenemos el espa‚àö¬±ol como predeterminado pero cambiamos f‚àö¬∞cilmente a los mensajes en ingl‚àö¬©s.
            //echo "idioma: " . $this->session->userdata('idioma');




            //Asignación del idioma en función de lo que tengamos en sesión
            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_comun_no_seg');
            $this->lang->load('controller_home');
            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');

            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['header'] = $this->load->view('vista_header', '', TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);
        }


        public function index()
        {

        }


        //Pedimos la agenda  para un  comercio y día seleccionado y para un servicio que hemos escogido.

        public function carga_municipios_json()
        {
            $this->load->model('municipios');

            $rpta = json_encode($this->municipios->get_municipios_by_prov_json($this->input->post('provincias')));

            /*$rpta= '
                        <option value="op2_1">Option1</option>
                        <option value="op2_2">Option2</option>
                        <option value="op2_3">Option3</option>';
            */
            echo $rpta;
            //$this->load->view('vista_alta_municipio',$data);
        }


        public function carga_municipios_autocomplete()
        {
            $this->load->model('municipios');
            $rpta = json_encode($this->municipios->get_municipios_by_prov_auto($this->input->get('term')));

            //$rpta="[{\"value\":\"1\",\"text\":\"ASSASASA\"},{\"value\":\"2\",\"text\":\"SDSSDSD\"}]";
            echo       $rpta;

        }

        public function localiza_reserva_qr($localizador){
            $this->load->model('calendario_reservas');


        }

    }

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
