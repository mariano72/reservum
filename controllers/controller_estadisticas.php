<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_estadisticas extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */



        public function __construct()
        {
            parent::__construct();
            //$this->output->enable_profiler(TRUE);
            // Your own constructor code
            // Now change the language
            //para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader, y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            //venga por sesi‚àö‚â•n cambiarlo din‚àö¬∞micamente y cargar el fichero de etiquetas que tengamos definido para la pantalla en cuesti‚àö‚â•n.
            //Ahora mismo en el config tenemos el espa‚àö¬±ol como predeterminado pero cambiamos f‚àö¬∞cilmente a los mensajes en ingl‚àö¬©s.
            //echo "idioma: " . $this->session->userdata('idioma');

            //Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
            //afectará sólo a esa función y dejará Ok, el resto del controlador.
            if (!$this->session->userdata('soycomercio')) {
                redirect('login/index');
            }

            //Asignación del idioma en función de lo que tengamos en sesión
            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_estadisticas');
            $this->lang->load('controller_home');

            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');

            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['header'] = $this->load->view('vista_header', '', TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);


        }






        public function resumen_inicial()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;

            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;



            $this->load->model('calendario_reservas');
            $data['total_reservas'] = $this->calendario_reservas->estadistica_reservas_totales_por_comercio($this->session->userdata('id_usuario_portal'));
            $data['total_reservas_anyo'] = $this->calendario_reservas->estadistica_reservas_totales_por_comercio_anyo($this->session->userdata('id_usuario_portal'));
            $data['total_reservas_mes'] = $this->calendario_reservas->estadistica_reservas_totales_por_comercio_mes($this->session->userdata('id_usuario_portal'));
            $data['total_reservas_semana'] = $this->calendario_reservas->estadistica_reservas_totales_por_comercio_semana($this->session->userdata('id_usuario_portal'));

            $this->load->view('vista_resumen_inicial_estadisticas', $data);

        }

    }
