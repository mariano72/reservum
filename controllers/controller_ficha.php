<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_ficha extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function Controller_ficha()
        {
            parent::__construct();

        }


        public function __construct()
        {
            parent::__construct();
            //$this->output->enable_profiler(TRUE);
            // Your own constructor code
            // Now change the language
            //para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader, y en el constructor de los controller lo que  haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            //venga por sesi‚àö‚â•n cambiarlo din‚àö¬∞micamente y cargar el fichero de etiquetas que tengamos definido para la pantalla en cuesti‚àö‚â•n.
            //Ahora mismo en el config tenemos el espa‚àö¬±ol como predeterminado pero cambiamos f‚àö¬∞cilmente a los mensajes en ingl‚àö¬©s.
            //echo "idioma: " . $this->session->userdata('idioma');
            //Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
            //afectará sólo a esa función y dejará Ok, el resto del controlador.
            if (!$this->session->userdata('soycomercio')) {
                redirect('login/index');
            }
            //Asignación del idioma en función de lo que tengamos en sesión
            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_ficha');
            $this->lang->load('controller_home');
            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');


            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu_operaciones'] = "";
            $this->data['header'] = $this->load->view('vista_header', $this->data, TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);
        }


        public function index()
        {

        }


        public function completar_ficha_comercio()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);

            $this->load->model('comercios');
            $data['datos_comercio'] = $this->comercios->get_comercio_by_code($this->session->userdata('id_usuario_portal'));

            $this->load->model('direcciones');
            $data['datos_direccion'] = $this->direcciones->get_direccion_by_comercio($this->session->userdata('id_usuario_portal'));


            $this->load->view('vista_completar_ficha_comercio', $data);


        }


        function actualizar_datos_comercio()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['mensaje_ok'] = null;

            $config['upload_path'] = APPPATH . '/uploads';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '4000';
            $config['file_name'] = $this->session->userdata('id_usuario_portal') . "_" . "foto_ficha_comercio";
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);


            $this->upload->do_upload();

            //Recuperamos datos del fichero
            $data_fichero = array('upload_data' => $this->upload->data());

            $data['id_comercio'] = $this->input->post('id_comercio');
            $data['nombre'] = $this->input->post('nombre');
            $data['descripcion_corta'] = $this->input->post('descripcion_corta');
            $data['descripcion'] = $this->input->post('descripcion');
            $data['foto_comercio'] = $data_fichero['upload_data']['orig_name'];

            $data['nombre_calle'] = $this->input->post('nombre_calle');
            $data['numero'] = $this->input->post('numero');
            $data['codigo_postal'] = $this->input->post('codigo_postal');
            $data['telefono'] = $this->input->post('telefono');
            $data['telefono2'] = $this->input->post('telefono2');
            $data['fax'] = $this->input->post('fax');
            // print_r($data_fichero);
            //Completar actualización de los datos del comercio y añadir el nombre de la foto a la tabla

            //Validamos los datos de entrada
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nombre', lang('completar_ficha_comercio_val_nombre'), 'required');
            $this->form_validation->set_rules('direccion', lang('completar_ficha_comercio_val_direcc'), 'required');
            $this->form_validation->set_rules('codigo_postal', lang('completar_ficha_comercio_val_cp'), 'required');
            $this->form_validation->set_rules('telefono', lang('completar_ficha_comercio_val_telf'), 'required');
            $this->form_validation->set_rules('descripcion_corta', lang('completar_ficha_comercio_desc1'), 'required');
            $this->form_validation->set_rules('descripcion', lang('completar_ficha_comercio_desc2'), 'required');

            $this->load->model('comercios');
            
            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();
                //Fin validación datos entrada
            }
            else {

                $this->comercios->update_comercio_by_code($data);
                $this->load->model('direcciones');
                $this->direcciones->modifica_direccion($data);

                $data['mensaje_ok'] = 'Ficha modificada correctamente';
            }
            $data['datos_comercio'] = $this->comercios->get_comercio_by_code($this->session->userdata('id_usuario_portal'));

            $this->load->model('direcciones');
            $data['datos_direccion'] = $this->direcciones->get_direccion_by_comercio($this->session->userdata('id_usuario_portal'));


            $this->load->view('vista_completar_ficha_comercio', $data);

        }


    }
