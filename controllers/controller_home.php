<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_home extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function Controller_ini()
        {
            parent::__construct();

        }


        public function __construct()
        {
            parent::__construct();
            //$this->output->enable_profiler(TRUE);
            // Your own constructor code
            // Now change the language
            //para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader, y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            //venga por sesi‚àö‚â•n cambiarlo din‚àö¬∞micamente y cargar el fichero de etiquetas que tengamos definido para la pantalla en cuesti‚àö‚â•n.
            //Ahora mismo en el config tenemos el espa‚àö¬±ol como predeterminado pero cambiamos f‚àö¬∞cilmente a los mensajes en ingl‚àö¬©s.
            //echo "idioma: " . $this->session->userdata('idioma');


            $this->load->library('session');
            //Asignación del idioma en función de lo que tengamos en sesión
            $idioma = $this->session->userdata('idioma');


            if ($idioma <> null) {
                $this->session->set_userdata('idioma', $idioma);
                $this->config->set_item('language', $idioma);
            } else {
                $this->config->set_item('language', 'spanish');
                $this->session->set_userdata('idioma', 'spanish');

            }
            $this->lang->load('controller_home');

            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');

            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
              //Cargamos menu operaciones
            if ($this->session->userdata('soyusuario')) {
               $this->data['menu_operaciones'] = $this->load->view('vista_menu_usuario', '', TRUE);
            } else if ($this->session->userdata('soycomercio')) {
                $this->data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            }
            $this->data['header'] = $this->load->view('vista_header', $this->data, TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);

        }


        public function index()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);

            $this->load->model('usuarios');
            $this->load->model('comercios');
            $this->load->model('calendario_reservas');


            //Cargamos el menu de comercios o usuarios y asignamos todo el contenido a una variable string, que luego podremos printar en la vista
           // $data['total_usuarios'] = $this->usuarios->count_usuarios();
           // $data['total_comercios'] = $this->comercios->count_comercios();
           // $data['total_reservas'] = $this->calendario_reservas->count_reservas();


            $this->load->view('vista_home', $data);
        }

        public function masajes()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);

            $this->load->model('usuarios');
            $this->load->model('comercios');
            $this->load->model('calendario_reservas');




            $this->load->view('vista_home_masaje', $data);
        }

        public function alta_contacto()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);


            $data['nombre'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['telefono'] = $this->input->post('phone');
            $data['mensaje'] = $this->input->post('message');
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $this->load->model('formulario_contacto');

            $this->formulario_contacto->alta_contacto($data);
            $this->load->helper('mail_helper');
            $datos['subject'] = 'Te han contactado desde la home.Revisar el mail';
            $datos['texto'] = 'Te ha contactado ' . $data['nombre'] . " - " . $data['email'] . " - " . $data['telefono'];
            $datos['email'] = 'mariano.morales.arce@gmail.com';
            sendmail($datos);

        }

        public function videos()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;

            $this->load->view('vista_videos_home', $data);
        }

        public function faq()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;

            $this->load->model('contenidos');
            $data['pantalla'] = "vista_home.php";
            $data['punto'] = "1";
            $data['idioma'] = $this->session->userdata('idioma');


            $data['faq1'] = $this->contenidos->get_contenidos_by_pantalla_punto_idioma($data);
            $data['punto'] = "2";
            $data['faq2'] = $this->contenidos->get_contenidos_by_pantalla_punto_idioma($data);
            $data['punto'] = "3";
            $data['faq3'] = $this->contenidos->get_contenidos_by_pantalla_punto_idioma($data);

            $this->load->view('vista_faq_home', $data);


        }

        public function consultar_ficha_comercio()
        {
            //http://localhost/colegueo/index.php/controller_ficha/consultar_ficha_comercio/BBBBB
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Para los errores del form
            $data['validation_errors'] = null;
            $this->load->model('comercios');

            $data['datos_comercio'] = $this->comercios->get_comercio_by_codigo_comercio($this->uri->segment(3));
            foreach ($data['datos_comercio'] as $row) {
                $id_comercio = $row->id_comercio;
            }

            //Recuperamos id_comercio y lo ponemos en la siguiente función
            $this->load->model('direcciones');
            $data['datos_direccion'] = $this->direcciones->get_direccion_by_comercio($id_comercio);

            $this->load->model('servicios');
            $data['datos_servicios'] = $this->servicios->get_servicios_by_comercio($id_comercio);

            $this->load->view('vista_consultar_ficha_comercio', $data);


        }


    }

