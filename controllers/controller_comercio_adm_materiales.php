<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_comercio_adm_materiales extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function Controller_comercio_adm_materiales()
        {
            parent::__construct();

        }


        public function __construct()
        {
            /* Your own constructor code	 Now change the language
                    para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader,
                    y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            */
            parent::__construct();
            //$this->output->enable_profiler(TRUE);

            /*Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
        afectará sólo a esa función y dejará Ok, el resto del controlador.
       */
            if (!$this->session->userdata('soycomercio')) {
                redirect('login/index');
            }

            //Asignación del idioma en función de lo que tengamos en sesión
            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_comercio_adm_materiales');
            $this->lang->load('controller_home');
            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');

            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['header'] = $this->load->view('vista_header', '', TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);
        }

        public function index()
        {


        }


        public function lista_materiales()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_adm_materiales', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['datos_tarifas'] = Array();


            $param['id_comercio'] = $this->session->userdata('id_usuario_portal');

            $this->load->model('materiales_maestro');
            $data['datos_materiales'] = $this->materiales_maestro->lista_materiales($param);

            $this->load->view('vista_lista_materiales', $data);


        }


        public function previa_alta_materiales()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_adm_materiales', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            //Datos para la capa de errores
            $data['validation_errors'] = null;
            $data['datos_material']=Array();
            
            $id_material = $this->uri->segment(3);
            $param['id_comercio'] = $this->session->userdata('id_usuario_portal');

            $this->load->model('materiales_maestro');
            if ($id_material <> null) {
                $data['datos_material'] = $this->materiales_maestro->get_material_by_id($id_material);

            }
            $data['url_destino'] = "/controller_comercio_adm_materiales/alta_material";
            
            $this->load->model('maestro_servicios');
            $data['datos_servicios'] = $this->maestro_servicios->get_servicio_by_idcomercio($param['id_comercio']);
            $this->load->model('idiomas');
            $data['jquery_idioma'] = $this->idiomas->get_idioma_by_cod_sesion($this->session->userdata('idioma'));

            $data['estados_materiales'] = $this->retorna_estados();
           
            $this->load->view('vista_alta_materiales', $data);


        }

        public function alta_material()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_adm_materiales', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            //Datos para la capa de errores
            $data['validation_errors'] = null;
            $data['datos_refresco_form'] = $this->input->post(NULL, TRUE); //Todo el post lo pasamos a la vista
            $data['mensaje_ok'] = null;

            $data['id_material'] = $this->input->post('id_material');
            $data['url_destino'] = $this->input->post('url_destino');
            $param['estado'] = $this->input->post('estado');
            $param['id_comercio'] = $this->session->userdata('id_usuario_portal');
            $param['descripcion'] = $this->input->post('descripcion');
            $param['id_servicio'] = $this->input->post('servicios');
            $param['importe'] = $this->input->post('importe');

            //Validamos los datos de entrada
            $this->load->library('form_validation');

            $this->form_validation->set_rules('importe', lang('alta_materiales_importe'), 'required|numeric');
            $this->form_validation->set_rules('descripcion', lang('alta_materiales_descripcion'), 'required');


            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();

                $this->load->model('maestro_servicios');
                $data['datos_servicios'] = $this->maestro_servicios->get_servicio_by_idcomercio($param['id_comercio']);
                $this->load->model('idiomas');
                $data['jquery_idioma'] = $this->idiomas->get_idioma_by_cod_sesion($this->session->userdata('idioma'));

                $data['estados_materiales'] = $this->retorna_estados();
                $this->load->view('vista_alta_materiales', $data);
            } else {
                $this->load->model('materiales_maestro');

                if ($data['id_material']<>null) {
                    $param['id_material']=$data['id_material'];
                    $this->materiales_maestro->actualizar_material($param);

                    $data['mensaje_ok'] = lang('actualizar_material_ok');
                } else {
                    $this->materiales_maestro->alta_material($param);

                    $data['mensaje_ok'] = lang('alta_material_ok');

                }
               

                $this->load->model('materiales_maestro');
                $data['datos_materiales'] = $this->materiales_maestro->lista_materiales($param);

                $this->load->view('vista_lista_materiales', $data);

            }


        }


        private function retorna_estados()
        {
            if ($this->session->userdata('idioma') == 'english') {
                return Array("A" => "ONLINE", "B" => "OFFLINE");
            } elseif ($this->session->userdata('idioma') == 'catala') {
                return Array("A" => "ALTA", "B" => "BAIXA");
            } else {
                return Array("A" => "ALTA", "B" => "BAJA");
            }
        }



    }
