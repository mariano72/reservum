<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
    private $data=Array();


	public function __construct()
    {
    /* Your own constructor code	 Now change the language
			para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader,
			y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
    */
    parent::__construct();
    //$this->output->enable_profiler(TRUE);
           
    /*Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
     afectará sólo a esa función y dejará Ok, el resto del controlador.
	*/ 
			
    //Asignación del idioma en función de lo que tengamos en sesión
 
    //Asignación del idioma en función de lo que tengamos en sesión
     $idioma=$this->session->userdata('idioma');


     if ($idioma<>null){
       $this->session->set_userdata('idioma', $idioma);
       $this->config->set_item('language', $idioma);
     } else {
         $this->config->set_item('language', 'spanish');
         $this->session->set_userdata('idioma','spanish');

     }
    $this->lang->load('login');
    $this->lang->load('controller_home');
        
    //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
    $this->load->helper('language');


    //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
    //que es como lo teníamos hasta ahora.
    //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['menu']=$this->load->view('vista_menu','',TRUE);
    //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['menu_operaciones']="";
    $this->data['header']=$this->load->view('vista_header',$this->data,TRUE);
   //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
    $this->data['footer']=$this->load->view('vista_footer','',TRUE);
	//Cargamos código jquery que gestiona cambio de idioma
	$this->data['gestion_seleccion_idioma']=$this->load->view('vista_jquery_funciones_comunes','',TRUE);
    //Cargamos HEAD
    $this->data['head']=$this->load->view('vista_head','',TRUE);
	//Cargamos definición de los scripts utilizados en la página
	$this->data['scripts_definition']=$this->load->view('vista_scripts_definition','',TRUE);

   
   }


	function index()
	{
      //Ponemos en la variable del método todos los contenidos comunes al controlador
      $data = $this->data;

      //Muestra los errores en el login
      $data['validation_errors']=null;
      $data['id_comercio']=$this->uri->segment(3);
      //faltaria verificar que el id_comercio existe
        
	  $this->load->view('vista_login', $data);
	}

	public function procesa_login_usuario()
	{
        //$this->output->enable_profiler(TRUE);

        //Ponemos en la variable del método todos los contenidos comunes al controlador
        $data = $this->data;

	    $username = $this->input->post('username');    
	    $password  = $this->input->post('password');
		$id_comercio=$this->input->post('id_comercio');
        $email_cifrado=null;
		$data['validation_errors']=null;

		
		$data['datos_refresco_form']= $this->input->post(NULL, TRUE);//Todo el post lo pasamos a la vista
	  
	    //Validamos los datos de entrada
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('username', lang('login_email'), 'required|valid_email');
	    $this->form_validation->set_rules('password', lang('login_password'), 'required|min_length[6]|max_length[12]');
	    
		if ($this->form_validation->run() == FALSE)
		{
		 $data['validation_errors']=validation_errors();
		 $this->load->view('vista_login', $data);
		 //Fin validación datos entrada
		} else {
		
		  $this->load->model('usuarios');
		  $data['login']=$this->usuarios->validar_login_usuario($username);

	      if ($data['login']!=null)
	      {
	       foreach($data['login'] as $row) {
	        $mail=$row->email;
	        $id_usuario=$row->id_usuario;	
            $email_cifrado=$row->password;
			$idioma=$row->idioma;
	       }
          }
       
          $this->load->library('encrypt');
          $email_descifrado= $this->encrypt->decode($email_cifrado,$password);
          if (strtolower($email_descifrado)==strtolower($username)) {
           //Insertamos en la sesión los datos más importantes.
	       $this->session->set_userdata('id_usuario_portal',$id_usuario);
           $this->session->set_userdata('mail',$mail);
           $this->session->set_userdata('soyusuario',TRUE);
           $this->session->set_userdata('tengo_sesion',TRUE);
		   $this->session->set_userdata('idioma',$idioma);
           $ip=$_SERVER['REMOTE_ADDR'];
		   $datos['ip']=$ip;
           $datos['id_usuario']=$id_usuario;
           $this->usuarios->insert_log_usuario($datos);
		   if ($id_comercio<>null){
		    $url='/controller_comun/reserva_cita_comercio/' . $id_comercio;
		    redirect($url);
		  } else {
		     redirect('/controller_ini/reservar/');
		   }
         
	     } 
	     else 
	     {
		  $data['validation_errors']=lang('login_alta_log_incorrecto');
		  $data['id_comercio']=$id_comercio;
	      $this->load->view('vista_login', $data);
	     }
		
		
		}

        
	}


    public function procesa_login_comercio()
	{


        //Ponemos en la variable del método todos los contenidos comunes al controlador
        $data = $this->data;
        
	    $username_comercio = $this->input->post('username_comercio');
	    $password  = $this->input->post('password');
	    $id_comercio=$this->input->post('id_comercio');
        $email_cifrado=null;
		$data['validation_errors']=null;


		$data['datos_refresco_form']= $this->input->post(NULL, TRUE);//Todo el post lo pasamos a la vista
		
		//Validamos los datos de entrada
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('username_comercio', lang('login_email'), 'required|valid_email');
	    $this->form_validation->set_rules('password', lang('login_password'), 'required|min_length[6]|max_length[12]');
        if ($this->form_validation->run() == FALSE)
		{
		 $data['validation_errors']=validation_errors();
		 $this->load->view('vista_login', $data);
		 //Fin validación datos entrada
		} else {
		
         $this->load->model('comercios'); 
		 $data['login']=$this->comercios->validar_login_comercio($username_comercio);
		
		 if ($data['login']!=null) {	  
           foreach($data['login'] as $row) {
	        $mail=$row->mail;
	        $id_comercio=$row->id_comercio;
            $email_cifrado=$row->password;
			$idioma=$row->idioma;
	      }
         }

         $this->load->library('encrypt');
         $email_descifrado= $this->encrypt->decode($email_cifrado,$password);
         if (strtolower($email_descifrado)==strtolower($username_comercio)) {
          //Insertamos en la sesión los datos más importantes.
	      $this->session->set_userdata('id_usuario_portal',$id_comercio);
          $this->session->set_userdata('mail',$mail);
          $this->session->set_userdata('soycomercio',TRUE);
          $this->session->set_userdata('tengo_sesion',TRUE);
		  $this->session->set_userdata('idioma',$idioma);
          redirect('/controller_comercio/inicio_comercio/');
	     } 
	     else 
	     {
		  $data['validation_errors']=lang('login_alta_log_incorrecto');
    	  $data['id_comercio']=$id_comercio;  
		  $this->load->view('vista_login', $data);
	     }
	   }
	   
	}


  

	public function salir()
	{
	    $this->session->sess_destroy();

	    redirect('login/index');
	}


     

    public function cambia_idioma(){

       $idioma=$this->input->post('idioma');
        
       if ($idioma==null) {$idioma='spanish';}

	   $this->session->set_userdata('idioma', $idioma);
       $this->config->set_item('language', $idioma);
    }
}

