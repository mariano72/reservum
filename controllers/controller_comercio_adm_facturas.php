<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_comercio_adm_facturas extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */


        public function __construct()
        {
            /* Your own constructor code	 Now change the language
                    para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader,
                    y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            */
            parent::__construct();
            //$this->output->enable_profiler(TRUE);

            /*Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
        afectará sólo a esa función y dejará Ok, el resto del controlador.
       */
            if (!$this->session->userdata('soycomercio')) {
                redirect('login/index');
            }

            //Asignación del idioma en función de lo que tengamos en sesión
            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_comercio_adm_facturas');
            $this->lang->load('controller_home');
            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');

            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['header'] = $this->load->view('vista_header', '', TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);
        }


        public function lista_facturas()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_adm_facturas', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;


            $this->load->model('facturas');
            $data['datos_facturas'] = $this->facturas->get_facturas_by_comercio($this->session->userdata('id_usuario_portal'));


            $this->load->view('vista_lista_facturas', $data);


        }


        public function detalle_factura()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_adm_facturas', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;

            $data['id_comercio'] = $this->session->userdata('id_usuario_portal');
            $data['id_factura'] = $this->uri->segment(3);
            $this->load->model('facturas');
            $data['datos_factura'] = $this->facturas->detalle_factura($data);

            //Requerimiento paypal
            foreach ($data['datos_factura'] as $row) {
                $importe_total = $row->importe_total;

            }
            $_SESSION["Payment_Amount"] = $importe_total;

            $this->load->view('vista_detalle_factura', $data);


        }

        public function pagar_factura()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_adm_facturas', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;

            $data['importe_total'] = $this->input->post('importe_total');
            $data['id_factura'] = $this->input->post('id_factura');

            $data['return_url'] = site_url() . "/controller_comercio_adm_facturas/actualizar_pago_factura/" . $data['id_factura'];

            $this->load->library('merchant');
            $this->merchant->load('paypal_express');

            $this->load->model('constantes');
            $paypal_username = $this->constantes->get_constante('paypal_username');
            $paypal_password = $this->constantes->get_constante('paypal_password');
            $paypal_signature = $this->constantes->get_constante('paypal_signature');
            $valor_booleano = $this->constantes->get_constante('paypal_testmode');
            if ($valor_booleano == 'false') {
                $paypal_testmode = false;
            }
            elseif (($valor_booleano == 'true'))
            {
                $paypal_testmode = true;
            }


            $settings = array(
                'username' => $paypal_username,
                'password' => $paypal_password,
                'signature' => $paypal_signature,
                'test_mode' => $paypal_testmode);
            $this->merchant->initialize($settings);
            // print_r($settings);
            $params = array(
                'amount' => $data['importe_total'],
                'currency' => 'EUR',
                'return_url' => $data['return_url'],
                'cancel_url' => site_url() . '/controller_comercio_adm_facturas/lista_facturas/');

            $response = $this->merchant->purchase($params);
            $data['validation_errors'] = print_r($response);
            $this->load->view('vista_mostrar_resultado', $data);


        }

        public function actualizar_pago_factura()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_adm_facturas', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['mensaje_ok'] = null;

            $data['id_comercio'] = $this->session->userdata('id_usuario_portal');
            $data['id_factura'] = $this->uri->segment(3);
            $data['return_url'] = site_url() . "/controller_comercio_adm_facturas/actualizar_pago_factura/" . $data['id_factura'];

            $this->load->model('facturas');
            $data['datos_factura'] = $this->facturas->detalle_factura($data);

            foreach ($data['datos_factura'] as $row) {
                $data['importe_total'] = $row->importe_total;
            }

            $this->load->library('merchant');
            $this->merchant->load('paypal_express');

            $this->load->model('constantes');
            $paypal_username = $this->constantes->get_constante('paypal_username');
            $paypal_password = $this->constantes->get_constante('paypal_password');
            $paypal_signature = $this->constantes->get_constante('paypal_signature');
            $paypal_testmode = $this->constantes->get_constante('paypal_testmode');


            $settings = array(
                'username' => $paypal_username,
                'password' => $paypal_password,
                'signature' => $paypal_signature,
                'test_mode' => $paypal_testmode);

            $this->merchant->initialize($settings);

            $params = array(
                'amount' => $data['importe_total'],
                'currency' => 'EUR',
                'return_url' => $data['return_url'],
                'cancel_url' => site_url() . '/controller_comercio_adm_facturas/lista_facturas/');

            $response = $this->merchant->purchase_return($params);


            if ($response->success()) {
                $data['paypal_gateway'] = $response->reference();

                $this->facturas->actualizar_estado_factura_paypal($data);
                $data['datos_facturas'] = $this->facturas->get_facturas_by_comercio($this->session->userdata('id_usuario_portal'));

                $data['mensaje_ok'] = "Pago realizado correctamente nº referencia paypal -> " . $data['paypal_gateway'];
                $this->load->view('vista_lista_facturas', $data);
            }
            else
            {
                $data['validation_errors'] = $response->message();
                $this->load->view('vista_mostrar_resultado', $data);
            }


        }


    }
