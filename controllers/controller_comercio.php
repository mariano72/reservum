<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_comercio extends CI_Controller
    {


        //Esta variables será global a todo el controlador y nos servirá para toda la transimisón de datos a la vista
        private $data = Array();

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function Controller_comercio()
        {
            parent::__construct();

            $this->config->set_item('language', $this->session->userdata('idioma'));


        }


        public function __construct()
        {
            parent::__construct();
            //  $this->output->enable_profiler(TRUE);
            // Your own constructor code
            // Now change the language
            //para gestionar el tema del multiidioma no autocargamos ningun php de idioma en el autoloader, y en el constructor de los controller lo que haremos ser‚àö¬∞ en funci‚àö‚â•n del idioma que nos
            //venga por sesi‚àö‚â•n cambiarlo din‚àö¬∞micamente y cargar el fichero de etiquetas que tengamos definido para la pantalla en cuesti‚àö‚â•n.
            //Ahora mismo en el config tenemos el espa‚àö¬±ol como predeterminado pero cambiamos f‚àö¬∞cilmente a los mensajes en ingl‚àö¬©s.
            //echo "idioma: " . $this->session->userdata('idioma');

            //Con esto nos aseguramos que si no se tiene sesión no se puede invocar al controlador, si se pone dentro de una función en concreto
            //afectará sólo a esa función y dejará Ok, el resto del controlador.
            if (!$this->session->userdata('soycomercio')) {
                redirect('login/index');
            }

            //Asignación del idioma en función de lo que tengamos en sesión
            $this->config->set_item('language', $this->session->userdata('idioma'));
            $this->lang->load('controller_comercio');
            $this->lang->load('controller_home');

            //Imprescindible para poder manipular los arrays de mensajes,sino no cargan
            $this->load->helper('language');

            //Iniciamos los arrays con partes de las vistas que son comunes a todas las pantallas y las sacamos de cada método
            //que es como lo teníamos hasta ahora.
            //Cargar menu y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['menu'] = $this->load->view('vista_menu', '', TRUE);
            //Cargamos header y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['header'] = $this->load->view('vista_header', '', TRUE);
            //Cargamos footer y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            $this->data['footer'] = $this->load->view('vista_footer', '', TRUE);
            //Cargamos código jquery que gestiona cambio de idioma
            $this->data['gestion_seleccion_idioma'] = $this->load->view('vista_jquery_funciones_comunes', '', TRUE);
            //Cargamos HEAD
            $this->data['head'] = $this->load->view('vista_head', '', TRUE);
            //Cargamos definición de los scripts utilizados en la página
            $this->data['scripts_definition'] = $this->load->view('vista_scripts_definition', '', TRUE);


        }


        public function index()
        {


            //Se activa la cache N minutos para la p‚àö¬∞gina que se va a mostrar a continuaci‚àö‚â•n.
            //$this->output->cache(2);

        }


        public function agenda_dia_comercio()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;

            $data['id_comercio'] = $this->session->userdata('id_usuario_portal');
            $data['fechareserva'] = "";
            if ($this->input->post('fechareserva') != null) {
                $param3 = explode('/', $this->input->post('fechareserva'));
                $param2 = $param3[2] . "-" . $param3[1] . "-" . $param3[0];
                $data['fechareserva'] = $param2;

            }


            $data['reservas'] = array();
            $this->load->model('calendario_reservas');
            $this->load->model('idiomas');

            if ($data['fechareserva'] != null) {
                $data['reservas'] = $this->calendario_reservas->agenda_dia_comercio($data);
            }

            $data['jquery_idioma'] = $this->idiomas->get_idioma_by_cod_sesion($this->session->userdata('idioma'));
            $data['fechareserva'] = $this->input->post('fechareserva');
            $this->load->view('vista_agenda_dia_comercio', $data);
        }

        public function previa_anular_cita_cliente()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;

            $data['mensajeanular'] = '';
            $this->load->model('idiomas');
            $data['jquery_idioma'] = $this->idiomas->get_idioma_by_cod_sesion($this->session->userdata('idioma'));

            $this->load->view('vista_previa_anular_cita_cliente', $data);


        }


        public function recuperar_datos_anular_cita_comercio_por_fecha()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;
            $data['id_usuario'] = null;
            $data['mensaje_ok'] = null;
            $data['mensajeanular'] = '';
            $data['email'] = null;
            $data['telefono'] = null;
            $data['fechareserva'] = $this->input->post('fechareserva');

            $this->load->model('calendario_reservas');

            //pasamos el formato de la fecha a mysql
            if ($data['fechareserva'] <> null) {
                $param3 = explode('/', $this->input->post('fechareserva'));
                $param2 = $param3[2] . "-" . $param3[1] . "-" . $param3[0];
                $data['fechareserva'] = $param2;
            }

            $data['id_comercio'] = $this->session->userdata('id_usuario_portal');

            //Validamos los datos de entrada
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fechareserva', lang('lista_recurso_rec_dias_fecini'), 'required');

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();
                $this->load->view('vista_previa_anular_cita_cliente', $data);
            } else {
                $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio_por_dia($data);
                $this->load->view('vista_recuperar_datos_anular_cita_cliente', $data);
            }


        }


        public function recuperar_datos_anular_cita_cliente()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);


            $data['fechareserva'] = null;
            $data['id_usuario'] = null;
            $data['mensajeanular'] = '';
            $data['misreservas'] = array();
            $data['mensaje_ok'] = null;
            //Preparamos datos
            $data['email'] = $this->input->post('email');
            $data['telefono'] = $this->input->post('telefono');

            //Buscamos en tabla de usuarios y faltará buscar en tabla de reservas sino encontramos aquí
            //Ahora funciona solo para registrados, no reservas de comercio.
            $this->load->model('usuarios');
            $this->load->model('calendario_reservas');

            $data['datos_usuario'] = $this->usuarios->comprobar_datos_cliente($data);
            $data['id_comercio'] = $this->session->userdata('id_usuario_portal');

            if (COUNT($data['datos_usuario']) != 0) {
                foreach ($data['datos_usuario'] as $row) {
                    $id_usuario = $row->id_usuario;
                }
                //Miramos las reservas futuras del cliente
                //Parámetros entrada
                $data['id_usuario'] = $id_usuario;
                $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio_por_usuario($data);

            } else {
                $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio_por_usuario_no_registrado($data);
            }

            if ($data['misreservas'] == null) {
                $data['validation_errors'] = lang('previa_anular_mensaje_error');
            }
            $this->load->view('vista_recuperar_datos_anular_cita_cliente', $data);


        }


        public function inicio_comercio()
        {
            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;

            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['validation_errors'] = null;

            $data['ultimareserva'] = null;
            $data['numeroreservasactivas'] = null;
            $data['numeroreservas'] = null;
            $data['misreservas'] = null;
            $data['misproximasreservas'] = null;

            $this->load->model('calendario_reservas');
            $data['ultimareserva'] = $this->calendario_reservas->get_last_reserva_activa($this->session->userdata('id_usuario_portal'));
            $data['numeroreservasactivas'] = $this->calendario_reservas->get_num_reservas_activas_now($this->session->userdata('id_usuario_portal'));
            $data['numeroreservas'] = $this->calendario_reservas->get_num_reservas_activas($this->session->userdata('id_usuario_portal'));
            $data['numeroreservasmes'] = $this->calendario_reservas->get_num_reservas_activas_mes($this->session->userdata('id_usuario_portal'));
            $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio($this->session->userdata('id_usuario_portal'), 5);
            $data['misproximasreservas'] = $this->calendario_reservas->get_next_reservas_comercio($this->session->userdata('id_usuario_portal'), 5);

            $this->load->view('vista_inicio_comercio', $data);

        }


        public function anular_cita_cliente()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);

            $data['misreservas'] = array();
            $data['validation_errors'] = null;
            $data['datos_refresco_form'] = $this->input->post(NULL, TRUE); //Todo el post lo pasamos a la vista
            $data['mensaje_ok'] = null;
            //Preparamos datos
            $ip = $_SERVER['REMOTE_ADDR'];
            $data['ip'] = $ip;
            $data['id_usuario'] = $this->input->post('id_usuario');
            $data['id_reserva'] = $this->input->post('id_reserva');
            $data['fechareserva'] = $this->input->post('fechareserva');
            $data['id_comercio'] = $this->session->userdata('id_usuario_portal');
            $data['email'] = $this->input->post('email');
            $data['telefono'] = $this->input->post('telefono');

            //Validamos los datos de entrada
            $this->load->library('form_validation');
            $this->form_validation->set_rules('id_reserva', lang('anular_cita_anular'), 'required');
            // $this->form_validation->set_rules('fechareserva', 'fechareserva', 'required');

            $this->load->model('calendario_reservas');

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = validation_errors();
            } else {
                $this->calendario_reservas->anular_reserva_cliente($data);
                $data['mensaje_ok'] = "Reserva anulada correctamente";

            }

            if ($data['id_usuario'] <> null) {
                $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio_por_usuario($data);
            } elseif ($data['fechareserva'] <> null) {
                $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio_por_dia($data);
            } elseif ($data['email'] <> null) {
                $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio_por_usuario_no_registrado($data);
            } elseif ($data['telefono'] <> null) {
                $data['misreservas'] = $this->calendario_reservas->get_last_reservas_comercio_por_usuario_no_registrado($data);
            }

            $this->load->view('vista_recuperar_datos_anular_cita_cliente', $data);


        }


        public function mis_clientes()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);


            $data['mensaje_ok'] = null;
            //Preparamos datos

            //Buscamos en tabla de usuarios y faltará buscar en tabla de reservas sino encontramos aquí
            //Ahora funciona solo para registrados, no reservas de comercio.
            $this->load->model('usuarios');
            $this->load->model('calendario_reservas');
            $data['id_comercio'] = $this->session->userdata('id_usuario_portal');
            $data['mis_clientes'] = $this->usuarios->lista_de_clientes($data['id_comercio']);


            $this->load->view('vista_mis_clientes', $data);


        }


        //Trabajando en esta nueva funcionalidad
        public function consultar_detalle_reserva()
        {

            //Ponemos en la variable del método todos los contenidos comunes al controlador
            $data = $this->data;
            //Cargamos el menu de operaciones y asignamos todo el contenido a una variable string, que luego podremos printar en la vista.
            //Sobreescribimos el header con el nuevo menú si aplica
            $data['menu_operaciones'] = $this->load->view('vista_menu_comercio', '', TRUE);
            $data['header'] = $this->load->view('vista_header', $data, TRUE);
            $data['mensaje_ok'] = null;
            //Preparamos datos
            $param['id_comercio']= $this->session->userdata('id_usuario_portal');
            $param['id_reserva']=$this->uri->segment(3);

            $this->load->model('calendario_reservas');
            $this->load->model('materiales_reserva');
            $data['datos_reserva'] = $this->calendario_reservas->get_datos_reserva_by_id($param);

            $data['datos_materiales']=$this->materiales_reserva->get_materiales_by_reserva($param);


            $this->load->view('vista_consulta_reserva', $data);


        }


    }
