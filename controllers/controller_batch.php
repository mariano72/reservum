<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Controller_batch extends CI_Controller
    {

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *         http://example.com/index.php/welcome
         *    - or -
         *         http://example.com/index.php/welcome/index
         *    - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see http://codeigniter.com/user_guide/general/urls.html
         */

        public function anular_reservas_pendientes()
        {

            echo "INICIO EJECUCIÓN BATCH ANULACIÓN RESERVAS";
            $this->load->model('calendario_reservas');
            $data['datos_comercio'] = $this->calendario_reservas->anular_reservas_pendientes_batch();
            echo "FIN EJECUCIÓN BATCH ANULACIÓN RESERVAS";

        }

        public function anular_altas_pendientes()
        {
            echo "INICIO EJECUCION BATCH ANULACION ALTAS PENDIENTES";
            $this->load->model('usuarios');
            $this->load->model('comercios');
            $this->usuarios->anular_altas_pendientes_batch();
            $this->comercios->anular_altas_pendientes_batch();
            echo "INICIO EJECUCION BATCH ANULACION ALTAS PENDIENTES";

        }

        public function get_reservas_dia_siguiente_batch()
        {

            echo "INICIO EJECUCIÓN BATCH RESERVAS DEL DIA SIGUIENTE";
            $this->load->model('calendario_reservas');
            $data['reservas'] = $this->calendario_reservas->get_reservas_dia_siguiente_batch();
            $this->load->helper('mail_helper');
            $datos['subject'] = 'Reservum.com le avisa de que mañana tiene una reserva';

            foreach ($data['reservas'] as $row) {
                $fecha_reserva = $row->fecha_reserva;
                $hora_inicio = $row->hora_inicio;
                $hora_fin = $row->hora_fin;
                $duracion = $row->duracion;
                $nombre_recurso = $row->nombre_recurso;
                $nombre_servicio = $row->nombre_servicio;
                $nombre_comercio = $row->nombre_comercio;
                $nombre_calle = $row->nombre_calle;
                $numero = $row->numero;
                $telefono = $row->telefono;


                if ($row->flag_usuario_reserva == 'COMERCIO') {
                    $email = $row->email;
                } else {
                    $email = $row->email_reserva;
                }
                $texto = " Estimado usuario le recordamos que en fecha " . $fecha_reserva . " debe acudir a " . $nombre_comercio . "\r" .
                         " para disfrutar del siguiente servicio: " . $nombre_servicio . " de " . $hora_inicio . " a " . $hora_fin . "\r" .
                         " en " . $nombre_calle . " nº " . $numero .
                         " para cualquier duda con la reserva puede dirigirse al siguiente nº " . $telefono;

                $datos['texto'] = $texto;
                $datos['email'] = $email;
                sendmail($datos);
            }


            echo "FIN  EJECUCIÓN BATCH RESERVAS DEL DIA SIGUIENTE";

        }


        public function gestion_facturas_pagadas_batch()
        {
            echo "INICIO EJECUCION BATCH GESTION FACTURAS PAGADAS";
            $this->load->model('facturas');
            $this->load->model('comercios');

            $data['datos_facturas'] = $this->facturas->lista_facturas_pendiente_batch(60);

            foreach ($data['datos_facturas'] as $row) {
                $id_factura = $row->id_factura;
                $id_comercio = $row->id_comercio;
                $this->facturas->actualizar_factura_a_impagada_batch($id_factura);
                $this->comercios->actualizar_estado_baja_batch($id_comercio);
            }

            echo "FIN EJECUCION BATCH GESTION FACTURAS PAGADAS";

        }

        public function aviso_facturas_pagadas_batch()
        {
            echo "INICIO EJECUCION BATCH AVISOS FACTURAS PAGADAS";
            $this->load->model('facturas');
            $this->load->model('comercios');

            $data['datos_facturas'] = $this->facturas->avisos_facturas_pendiente_batch(15);
            $this->load->helper('mail_helper');
            $datos['subject'] = 'Reservum.com le avisa de que tiene facturas pendientes de pago';

            foreach ($data['datos_facturas'] as $row) {
                $numero_factura = $row->numero_factura;
                $id_comercio = $row->id_comercio;
                $data['datos_comercio']=$this->comercios->get_comercio_by_code($id_comercio);
                foreach ($data['datos_comercio'] as $valor){
                  $email=$valor->mail;
                  $idioma=$valor->idioma;
                }
             $this->config->set_item('language', $idioma);
                
             $datos['texto'] = "La siguiente factura con número " . $numero_factura . " la tiene pendiente de pago desde hace 15 días.\n
             Recuerde que puede proceder a pagar la factura desde el menú lista de facturas";
             $datos['email'] = $email;
             sendmail($datos);    
            }

            echo "FIN EJECUCION BATCH AVISOS FACTURAS PAGADAS";

        }


    }

