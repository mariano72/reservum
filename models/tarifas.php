<?php

    class Tarifas extends CI_Model
    {


        function getall_tarifas($idioma)
        {

            $data = array();

            $query = "select  id_tarifa,
                      (select descripcion from traducciones b where a.id_tarifa=b.id_opcion and idioma='" . $idioma . "') as descripcion
                      from tarifas a where activa='A'";
            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function check_datos_tarifa_by_comercio($id_comercio)
        {

            $data = array();

            $query = "select a.importe,a.iva,a.importe_total,a.recursos_max_disponibles
                       from tarifas a,comercio b
                      where b.id_tarifa=a.id_tarifa and b.id_comercio=" . $id_comercio;
            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

    }





