<?php

class Calendario_base extends CI_Model {



  function valida_calendario_base ($param){



     $query="SELECT * FROM calendario_base
             WHERE ((fecha_inicio<='" . $param['fechareserva'] . "' and fecha_fin>='". $param['fechareserva'] . "') or fecha_fin is null) 
              and hora_inicio<='" . $param['hora_inicio']  . "' and hora_fin>='" . $param['hora_fin']  ."' and
              '" . $param['hora_inicio']  . "'<'" . $param['hora_fin']  ."' and id_recurso=" . $param['id_recurso'];

     $q=$this->db->query($query);
     return $q;
  }

//Se cogen las horas libres para todos los recursos de un cliente-comercio-servicio
//uno de los parámetros sería el intérvalo a sumar para dar los rangos horarios.
//lo primero que hemos de hacer aquí es coger el calendarios base con todos los parámetros y devolver las horas para ese día.

 function get_horas_libres_dia ($param){

  //$this->db->cache_on();

  $data=array();
  $sql="select a.id_recurso as recurso,a.nombre,b.hora_inicio,b.hora_fin,
        case b.fragmentacion_horas when 'EN_PUNTO' then 60  when 'MEDIAS' then 30 when 'CUARTOS' then 15 end as fragmentacion
        from recursos a, calendario_base b,servicios c
        where a.id_recurso=b.id_recurso and  a.estado='A' and a.id_comercio=" . $param['id_comercio'] . " and
        ((b.fecha_inicio<='" . $param['fecha'] ."' and  '" . $param['fecha'] . "'<=b.fecha_fin) OR  b.fecha_fin IS null ) 
		and c.id_recurso=b.id_recurso and
         c.id_recurso=a.id_recurso and c.estado='A' and c.id_servicio=" . $param['id_servicio'] . "
		and DATE_ADD( CURDATE() ,INTERVAL b.mostrar_dias_avance DAY ) >='" . $param['fecha'] . "'" .
		" order by a.id_recurso";




  $q=$this->db->query($sql);

  $this->load->helper('utilidades_helper');
     
  foreach ($q->result() as $row){

	$diferencia=getMyTimeDiff2($row->hora_fin,$row->hora_inicio);
	
	if ($diferencia==0){
       $diferencia=1440; //en minutos
    }	
    $fragmentos=$diferencia/$row->fragmentacion;

    $contador=0;
    $minutos=0;
    $hora_inicio=null;
      
    while ($contador<$fragmentos) {

      if ($contador==0) {
         $hora_inicio= date("H:i", strtotime($row->hora_inicio .'+0 minutes'));
      } else {
         $hora_inicio= date("H:i", strtotime( $hora_inicio. '+0 minutes'));
      }
      if ($row->fragmentacion==60){
            $hora_fin= date("H:i", strtotime($hora_inicio .'+60 minutes'));
      } elseif ($row->fragmentacion==30) {
            $hora_fin= date("H:i", strtotime($hora_inicio .'+30 minutes'));
      } elseif ($row->fragmentacion==15) {
            $hora_fin= date("H:i", strtotime($hora_inicio .'+15 minutes'));
      }


      $data[]=array($row->recurso,$row->nombre,$hora_inicio,$hora_fin);

      $hora_inicio=$hora_fin;
  

      $contador++;
      $minutos=$minutos+$row->fragmentacion;;
    }

  }
   return $data;
  }


  
  function get_calendario_by_idrecurso ($param){

     $data=array();

     $query="SELECT hora_inicio,hora_fin,fragmentacion_horas,mostrar_dias_avance,min_dias_anulacion
			 FROM calendario_base WHERE id_recurso=" . $param;

     $q=$this->db->query($query);
	 
	 
	  if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }
   return $data;
  }

  
  function modificar_calendario ($param){

     $data=array();

     $query="update calendario_base  set hora_inicio='" . $param['hora_inicio'] . "',hora_fin='" . $param['hora_fin'] . 	  "',fragmentacion_horas='" .
             $param['fragmentacion_horas'] . "',mostrar_dias_avance=" . $param['mostrar_dias_avance']  .  ",min_dias_anulacion=" .
             $param['min_dias_anulacion'] . " WHERE id_recurso=" . $param['id_recurso'];

     $q=$this->db->query($query);
	 
	 

  }

  function alta_calendario ($param){

     $data=array();

     $query="insert into calendario_base values (null,curdate(),null,'" . $param['hora_inicio'] . "','" . $param['hora_fin'] .
            "','" . $param['fragmentacion_horas'] . "'," . $param['id_recurso'] . "," . $param['mostrar_dias_avance']  . "," .  $param['min_dias_anulacion'] . ")";

     $q=$this->db->query($query);


	 
  }


}





