<?php

    class Facturas extends CI_Model
    {


        function get_facturas_by_comercio($id_comercio)
        {

            $data = array();

            $query = "select  a.id_factura,a.numero_factura,a.fecha_factura,a.fecha_desde,a.fecha_hasta,a.importe_total,
                      (select b.descripcion from traducciones b where a.id_tarifa=b.id_opcion and proceso='TARIFAS' and idioma='" . $this->session->userdata('idioma') . "') as desc_tarifa,
                      (select b.descripcion as estado_factura from traducciones b where a.estado=b.id_opcion and proceso='ESTADOS_FACTURAS' and idioma='" . $this->session->userdata('idioma') . "') as estado_factura 
                      from facturas a where id_comercio=" . $id_comercio . " order by fecha_factura desc";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function detalle_factura($param)
        {

            $data = array();

            $query = "select  a.id_factura,a.numero_factura,a.fecha_factura,a.fecha_desde,a.fecha_hasta,a.importe_total,a.importe,a.iva,a.concepto,a.estado,fecha_pago,a.paypal_gateway,
                           (select b.descripcion from traducciones b where a.id_tarifa=b.id_opcion and proceso='TARIFAS' and idioma='" . $this->session->userdata('idioma') . "') as desc_tarifa,
                           (select b.descripcion as estado_factura from traducciones b where a.estado=b.id_opcion and proceso='ESTADOS_FACTURAS' and idioma='" .
                           $this->session->userdata('idioma') . "') as estado_factura
                           from facturas a where id_comercio=" . $param['id_comercio'] . " and a.id_factura=" . $param['id_factura'] . " order by fecha_factura desc";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function alta_factura($param)
        {

            $query = "insert into facturas values(null, '" . $param['numero_factura'] . "'," . $param['idcomercio'] . ",'" .
                     $param['fecha_desde'] . "','" . $param['fecha_hasta'] . "','PENDIENTE','',curdate(),'" . $param['importe'] .
                     "','" . $param['iva'] . "','" . $param['importe_total'] . "'," . $param['tarifa'] . ",null,null,null,null)";


            $q = $this->db->query($query);


        }

        function actualizar_estado_factura_paypal($param)
        {

            $query = "update facturas set estado='PAGADA',paypal_gateway='" . $param['paypal_gateway']  . "',fecha_pago=curdate() 
                      where id_comercio=" . $param['id_comercio'] . " and id_factura=" . $param['id_factura'];

            $q = $this->db->query($query);


        }

        function get_numero_facturas_by_anyo()
        {
            //en vez de retornar array como es habitual retorno escalar.

            $data = null;

            $query = "select count(1) + 1 as numero from facturas where year(fecha_factura) = year(curdate())";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->numero;
                }

            }

            return $data;
        }

        function lista_facturas_pendiente_batch($numero_dias)
        {

            $data = array();

            $query = "SELECT a.id_factura, a.id_comercio
                      FROM facturas a
                      WHERE estado =  'PENDIENTE'
                      AND fecha_factura +" . $numero_dias . " <= CURDATE()";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

       function actualizar_factura_a_impagada_batch($id_factura){

            $query = "update facturas set estado='IMPAGADA',fecha_impagada=curdate() where id_factura=" . $id_factura;

            $q = $this->db->query($query);

       }

        function avisos_facturas_pendiente_batch($numero_dias)
         {

             $data = array();

             $query = "SELECT a.id_factura,a.numero_factura, a.id_comercio
                       FROM facturas a
                       WHERE estado =  'PENDIENTE'
                       AND fecha_factura +" . $numero_dias . " = CURDATE()";

             $q = $this->db->query($query);

             if ($q->num_rows > 0) {
                 foreach ($q->result() as $row) {
                     $data[] = $row;
                 }

             }

             return $data;
         }


    }





