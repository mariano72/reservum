<?php

    class Calendario_restricciones extends CI_Model
    {


        function valida_tiempo($param)
        {


            $query = "SELECT id_restriccion,lunes,martes,miercoles,jueves,viernes,sabado,domingo,weekday('" . $param['fechareserva'] . "' ) as dia_semana,dias_afectados
             FROM calendario_restricciones
             WHERE ('" . $param['fechareserva'] . "' between fecha_inicio and fecha_fin) OR
                     (fecha_inicio is null and fecha_fin is null) " .
                     "and id_recurso=" . $param['id_recurso'] . " and
             (('" . $param['hora_inicio'] . "'>=hora_inicio and '" . $param['hora_fin'] . "'<=hora_fin) or
             ('" . $param['hora_fin'] . "'>hora_inicio and '" . $param['hora_fin'] . "'<hora_fin) or
             ('" . $param['hora_inicio'] . "'<=hora_inicio and '" . $param['hora_fin'] . "'>=hora_fin) or
             ('" . $param['hora_inicio'] . "'>'" . $param['hora_fin'] . "'))";


            $q = $this->db->query($query);
            return $q;


        }


        //Cogemos las restricciones para el día y el comercio,servicio solicitado,

        function get_restricciones_dia($param, $param2, $param4)
        {


            $data = array();

    
            $sql = " select a.id_recurso,hora_inicio,hora_fin,lunes,martes,miercoles,jueves,
	            viernes,sabado,domingo,dias_afectados,weekday('" . $param2 . "') as dia_semana
	            from calendario_restricciones a,recursos b ,servicios c
	            where a.id_recurso=b.id_recurso and (a.fecha_inicio<='" . $param2 . "' OR a.fecha_inicio IS NULL) and (a.fecha_fin>='" . $param2 . "' OR a.fecha_fin IS NULL) 
	            and c.id_recurso=a.id_recurso and c.id_recurso=b.id_recurso and c.id_servicio=" . $param4 . "
	            and b.id_comercio='" . $param . "'";

            $q = $this->db->query($sql);
          
            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_restricciones_recurso($param)
        {


            $data = array();

            $sql = " select id_restriccion,id_recurso,fecha_inicio,fecha_fin,hora_inicio,hora_fin,
				case lunes when 'S' then 'X' end as lunes,
				case martes when 'S' then 'X' end as martes,
				case miercoles when 'S' then 'X' end as miercoles,
				case jueves when 'S' then 'X' end as jueves,
				case viernes when 'S' then 'X' end as viernes,
				case sabado when 'S' then 'X' end as sabado,
				case domingo when 'S' then 'X' end as domingo,
	            dias_afectados
	            from calendario_restricciones a
	            where id_recurso=" . $param['id_recurso'] . " and id_comercio=" . $param['id_comercio'] .
                   " order by fecha_fin,fecha_inicio desc";

            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_restriccion_by_id($param)
        {


            $data = array();

            $sql = " select id_restriccion,id_recurso,
                case fecha_inicio when '0000-00-00' then null else fecha_inicio end as fecha_inicio,
                case fecha_fin when '0000-00-00' then null else fecha_fin end as fecha_fin,
                hora_inicio,hora_fin,
				lunes,martes,miercoles,jueves,viernes,sabado,domingo,dias_afectados
	            from calendario_restricciones a
	            where id_restriccion=" . $param['id_restriccion'] . " and id_comercio=" . $param['id_comercio'];

            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function actualiza_restriccion($param)
        {


            $data = array();

            $sql = "update calendario_restricciones set fecha_inicio='" . $param['fecha_inicio'] . "',fecha_fin='" . $param['fecha_fin'] . "',hora_inicio='" . $param['hora_inicio'] .
                   "',hora_fin='" . $param['hora_fin'] . "',lunes='" . $param['lunes'] . "',martes='" . $param['martes'] . "',miercoles='" . $param['miercoles'] . "',jueves='" . $param['jueves'] . "',viernes='" . $param['viernes'] . "',sabado='" . $param['sabado'] . "',domingo='" . $param['domingo'] .
                   "',dias_afectados='" . $param['dias_afectados'] . "'" .
                   " where id_restriccion=" . $param['id_restriccion'] . " and id_comercio=" . $param['id_comercio'];

            $q = $this->db->query($sql);

            return $data;
        }

        function alta_restriccion($param)
        {


            $data = array();

            $sql = "insert into calendario_restricciones values (null," . $param['id_recurso'] . "," . $param['id_comercio'] . ",
	   '" . $param['fecha_inicio'] . "','" . $param['fecha_fin'] . "','" . $param['hora_inicio'] .
                   "','" . $param['hora_fin'] . "','" . $param['lunes'] . "','" . $param['martes'] . "','" .
                   $param['miercoles'] . "','" . $param['jueves'] . "','" . $param['viernes'] . "','" .
                   $param['sabado'] . "','" . $param['domingo'] . "','" . $param['dias_afectados'] . "')";


            $q = $this->db->query($sql);

            return $data;
        }


    }





