<?php

    class Direcciones extends CI_Model
    {


        function get_direccion_by_comercio($id_comercio)
        {

            $data = array();

            $query = "SELECT a.id_direccion, a.nombre_calle, a.telefono,a.numero,a.codigo_postal,a.telefono2,a.fax,
                     b.descprov as provincia, c.nommuni as municipio     
                     FROM direcciones a
                     LEFT JOIN  provincias b ON a.provincia = b.codprov
                     LEFT JOIN  municipios c ON a.municipio = c.codmuni and a.provincia = c.codprov
                     WHERE id_comercio =" . $id_comercio;

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function alta_direccion($param)
        {

            $query = "insert into direcciones values(null," . $param['idcomercio'] . ",'ESP','" . $param['provincias'] .
                     "','" . $param['municipios'] . "'," . $this->db->escape($param['tipovia']) . "," . $this->db->escape($param['calle']) .
                     "," . $this->db->escape($param['numero']) . "," . $this->db->escape($param['codigopostal']) . "," . $this->db->escape($param['telefono']) . "," .
                     $this->db->escape($param['telefono2']) . "," . $this->db->escape($param['fax']) . ")";


            $q = $this->db->query($query);


        }

        function modifica_direccion($param)
        {

            $query = "update direcciones set nombre_calle =" . $this->db->escape($param['nombre_calle']) . ",numero =" . $this->db->escape($param['numero']) . ",
     codigo_postal =" . $this->db->escape($param['codigo_postal']) . ",telefono =" . $this->db->escape($param['telefono']) . ",telefono2 =" .
                     $this->db->escape($param['telefono2']) . ",fax =" . $this->db->escape($param['fax']) . " where id_comercio=" . $param['id_comercio'];


            $q = $this->db->query($query);


        }


    }





