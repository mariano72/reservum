<?php

    class Municipios extends CI_Model
    {


        function getall_provincias()
        {
            $this->db->cache_on();

            $data = array();
            $q = $this->db->query('select * from provincias');

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }
            $this->db->cache_off();
            return $data;
        }

        function getall_municipios()
        {

            $data = array();
            $q = $this->db->query('select * from municipios');

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_municipios_by_prov($codigo)
        {

            $data = array();
            $query = "select codmuni,nommuni from municipios where codprov='" . $codigo . "' ORDER BY 2 ASC";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_municipios_by_prov_json($codigo)
        {

            $query = "select codmuni as value,nommuni as text from municipios where codprov='" . $codigo . "' ORDER BY 2 ASC";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_municipios_by_prov_auto($codigo)
        {

            $query = "select codmuni as text,nommuni as value from municipios where nommuni like '%" . $codigo . "%' ORDER BY 2 ASC";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }
//

    }





