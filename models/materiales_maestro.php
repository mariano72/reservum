<?php

    class Materiales_maestro extends CI_Model
    {



        function get_materiales_servicio($param)
        {

            $data = array();
            $sql = "select * from materiales_maestro
                    where estado='A' and id_comercio=" . $param['id_comercio'] . "  and id_servicio=" . $param['id_servicio'];
            
            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_material_by_id($param)
        {

            $data = array();
            $sql = "select * from materiales_maestro where  id_material=" . $param['id_material'];

            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function lista_materiales($param)
        {

            $data = array();
            $sql = "select a.id_material,a.descripcion,a.importe,a.estado,b.nombre
                    from materiales_maestro a,maestro_servicios b
                    where a.id_servicio=b.id_servicio and a.id_comercio=" . $param['id_comercio'];

            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }




        function alta_material($param)
        {


            $data = array();

            $sql = "insert into materiales_maestro values (null," . $param['id_comercio'] . "," . $param['id_servicio'] . "," . $this->db->escape($param['descripcion']) . ",'" .
                   $param['importe'] . "','" . $param['estado'] . "')";


            $q = $this->db->query($sql);

            return $data;
        }

        function actualizar_material($param)
        {


            $data = array();

            $sql = "update materiales_maestro set descripcion=" . $this->db->escape($param['descripcion']) . ",id_servicio=" . $param['id_servicio'] . ",importe='" .
                   $param['importe'] . "',estado='" . $param['estado'] . "'
                    where id_material=" . $param['id_material'];



            $q = $this->db->query($sql);

            return $data;
        }

    }





