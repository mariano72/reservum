<?php

class Servicios extends CI_Model {



  function get_servicios_by_comercio ($id_comercio){
   
   $data=array();
   
   $query="select a.id_servicio,nombre,descripcion,duracion_minutos,importe_total
           from servicios a,maestro_servicios m where m.id_servicio=a.id_servicio and a.id_comercio=" . $id_comercio .
          " and a.estado='A' group by id_servicio,nombre,descripcion,duracion_minutos,importe_total";
  
   
   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }

  function get_servicios_by_comercio_adm ($id_comercio){

   $data=array();

   $query="select a.id_servicio,nombre,descripcion,duracion_minutos,importe,iva,importe_total
           from servicios a,maestro_servicios m where m.id_servicio=a.id_servicio and a.id_comercio=" . $id_comercio .
          " group by id_servicio,nombre,descripcion,duracion_minutos,importe,iva,importe_total";

   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }

  function get_servicio_by_id ($id_servicio){
   
   $data=array();
   
   $query="select a.id_servicio,nombre,descripcion,duracion_minutos,importe,iva,importe_total
            from servicios a,maestro_servicios m
            where a.id_servicio=" . $id_servicio . " and a.estado='A' and m.id_servicio=a.id_servicio
            group by id_servicio,nombre,descripcion,duracion_minutos,importe,iva,importe_total";
   
   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }

  function get_relaciones ($param){

   $data=array();

   $query="select id_relacion,estado from servicios where id_recurso=" . $param['id_recurso'] . " and id_servicio=" . $param['id_servicio'] .
           " and id_comercio=" . $param['id_comercio'];

   
   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }
  
  function get_relaciones_by_servicio ($param){

   $data=array();

   $query="select id_relacion,estado from servicios where id_servicio=" . $param['id_servicio'] .
           " and id_comercio=" . $param['id_comercio'];

   
   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }

  function alta_relacion_servicio ($param){

   $data=array();

   $query="insert into servicios values (null," . $param['id_servicio'] . "," . $param['id_recurso']   . ",null,null,curdate(),'A'," . $param['id_comercio'] .")";


   $q=$this->db->query($query);

  }


  function modificar_relacion_servicio ($param){

   $data=array();

   $query="update servicios set estado='A' where id_relacion=". $param['id_relacion'];


   $q=$this->db->query($query);

  }


  function modificar_relacion_masiva_baja ($param){

   $data=array();

   $query="update servicios set estado='B' where id_comercio=" . $param['id_comercio'] . " and id_relacion not in (". $param['id_relaciones'] .
           ") and id_servicio=" . $param['id_servicio'];

   $q=$this->db->query($query);


  }

  function modificar_relacion_masiva_baja_total ($param){

     $data=array();

     $query="update servicios set estado='B' where id_comercio=" . $param['id_comercio'] . " and id_servicio=" . $param['id_servicio'];
     
     $q=$this->db->query($query);


  }

}





