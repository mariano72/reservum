<?php

    class Usuarios extends CI_Model
    {


        function validar_login_usuario($username)
        {

            $data = array();

            $query = "select email,id_usuario,password,idioma from usuarios a where email='" . $username . "' and estado='A'";


            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_usuario_by_mail($username)
        {

            $data = array();

            $query = "select id_usuario from usuarios a where email='" . $username . "'";


            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function comprobar_datos_cliente($param)
        {

            $data = array();

            $query = "select id_usuario,nombre,email,telefono from usuarios a where email='" . $param['email'] . "' or telefono='" . $param['telefono'] . "'";


            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function insert_log_usuario($datos)
        {

            $data = array();
            $fecha = date("Y-m-d H:i:s");

            $query = "update usuarios set ip='" . $datos['ip'] . "' , last_login_fecha='" . $fecha . "' where id_usuario=" . $datos['id_usuario'];


            $q = $this->db->query($query);


        }

        function alta_usuario($param)
        {
            $fecha = date("Y-m-d H:i:s");

            $query = "insert into usuarios values  (null," . $this->db->escape($param['nombre']) . ",'" . $param['email'] .
                     "'," . $this->db->escape($param['password']) . ",'" . $fecha . "','P'," . $this->db->escape($param['telefono']) . ",'" . $param['ip'] .
                     "','" . $fecha . "','" . $param['secreto'] . "','" . $param['idioma'] . "')";

            $q = $this->db->query($query);


        }

        function confirmar_alta_usuario($param)
        {

            $data = array();

            $query = "select id_usuario from usuarios a where estado='P' and email='" . $param['email'] . "' and codigo_activacion='" . $param['secreto'] . "'";


            $q = $this->db->query($query);
            return $q;
        }

        function actualizar_estado_usuario($param)
        {

            $data = array();

            $query = "update usuarios set estado='A' where email='" . $param['email'] . "'";


            $q = $this->db->query($query);
            return $q;
        }

        function actualizar_password_usuario($param)
        {

            $data = array();

            $query = "update usuarios set password='" . $param['newpassword'] . "' where id_usuario=" . $param['id_usuario'];


            $q = $this->db->query($query);
            return $q;
        }

        function actualizar_password_usuario_email($param)
        {

            $data = array();

            $query = "update usuarios set password='" . $param['passwordcifrado'] . "' where  email='" . $param['email'] . "'";


            $q = $this->db->query($query);
            return $q;
        }

        function anular_altas_pendientes_batch()
        {

            $query = "delete from usuarios where estado='P' and TIMESTAMPDIFF(MINUTE,last_login_fecha ,current_timestamp ())>60";

            $q = $this->db->query($query);


        }

        function count_usuarios()
        {

            $data = null;
            $query = "select count(1) as total from usuarios where estado='A'";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->total;
                }

            }

            return $data;
        }



        function lista_de_clientes($id_comercio)
        {

               $data = array();

             $query = "SELECT c.id_usuario,c.nombre AS nombre_usuario, c.email, c.telefono, a.nombre AS nombre_reserva,
                a.email AS email_reserva, a.telefono AS telefono_reserva, a.flag_usuario_reserva
    		FROM calendario_reservas a
	    	LEFT JOIN usuarios c ON a.id_usuario = c.id_usuario
		    WHERE a.id_comercio = " . $id_comercio . " 
		    GROUP BY
	    	 c.id_usuario ,c.nombre, c.email, c.telefono, a.nombre ,
                a.email, a.telefono, a.flag_usuario_reserva
	    	ORDER BY c.nombre,a.nombre ASC ";



            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


    }
