<?php

    class Comercios extends CI_Model
    {


        function count_comercios()
        {

            $data = null;
             $query ="select count(1) as total from comercio where estado='A'";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->total;
                }

            }

            return $data;
        }


        function get_comercios_by_municipio($provincia, $municipio)
        {

            $data = array();
            $query = "select a.id_comercio,a.nombre,a.descripcion_corta from comercio a,direcciones b
          where b.provincia='" . $provincia . "' AND b.municipio='" . $municipio .
                     "' AND a.id_comercio=b.id_comercio and a.estado='A' ORDER BY 2 ASC";


            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function get_comercio_by_code($id_comercio)
        {

            $data = array();

            $query = "select a.id_comercio,a.nombre,a.descripcion_corta,a.descripcion,a.codigo_comercio,a.cif,a.foto_comercio,a.estado,a.mail,a.idioma
           from comercio a where id_comercio=" . $id_comercio;
            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_comercio_by_codigo_comercio($codigo_comercio)
        {

            $data = array();

            $query = "select a.id_comercio,a.nombre,a.descripcion_corta,a.descripcion,a.codigo_comercio,a.cif,a.foto_comercio,a.estado,a.mail,a.idioma 
           from comercio a where codigo_comercio='" . $codigo_comercio . "'";
            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function validar_login_comercio($username)
        {

            $data = array();

            $query = "select mail,id_comercio,password,idioma from comercio a where mail='" . $username . "' and estado='A'";
            $q = $this->db->query($query);
            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }
            return $data;
        }

        function get_comercio_by_mail($param)
        {

            $data = array();

            $query = "select id_comercio,codigo_comercio from comercio a where mail='" . $param . "'";
            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }
            return $data;
        }

        function alta_comercio($param)
        {
            $fecha_alta = date("Y-m-d H:i:s");

            $query = "insert into comercio values  (null,'" . $param['email'] . "'," . $this->db->escape($param['password']) .
                     "," . $this->db->escape($param['nombre']) . "," . $this->db->escape($param['cif']) . ",'" . $fecha_alta . "','P'," .
                     $this->db->escape($param['descripcion_corta']) . "," . $this->db->escape($param['descripcion']) . ",null,'" . $param['codigo_comercio'] . "','"
                     . $param['secreto'] . "','" . $param['ip'] . "','" . $fecha_alta . "',null,'" . $param['pais'] . "','" .
                     $param['idioma'] . "'," . $param['tarifa'] . ",null)";

            $q = $this->db->query($query);


        }

        function update_comercio_by_code($param)
        {
            $fecha_alta = date("Y-m-d H:i:s");

            $query = "update comercio set foto_comercio='" . $param['foto_comercio'] . "',descripcion=" . $this->db->escape($param['descripcion']) . ",
             descripcion_corta=" . $this->db->escape($param['descripcion_corta']) . ",
             nombre=" . $this->db->escape($param['nombre']) . "  where id_comercio=" . $param['id_comercio'];

            $q = $this->db->query($query);


        }

        function confirmar_alta_comercio($param)
        {

            $data = array();

            $query = "select id_comercio from comercio a where estado='P' and mail='" . $param['email'] .
                     "' and codigo_activacion='" . $param['secreto'] . "'";


            $q = $this->db->query($query);
            return $q;
        }


        function actualizar_estado_comercio($param)
        {

            $data = array();

            $query = "update comercio set estado='A' where mail='" . $param['email'] . "'";


            $q = $this->db->query($query);
            return $q;
        }

        function actualizar_password_comercio($param)
        {

            $data = array();

            $query = "update comercio set password='" . $param['newpassword'] . "' where id_comercio=" . $param['id_usuario'];


            $q = $this->db->query($query);
            return $q;
        }

        function actualizar_password_comercio_email($param)
        {

            $data = array();

            $query = "update comercio set password='" . $param['passwordcifrado'] . "' where  mail='" . $param['email'] . "'";


            $q = $this->db->query($query);
            return $q;
        }

        function anular_altas_pendientes_batch()
        {

            $query = "delete from comercio where estado='P' and TIMESTAMPDIFF(MINUTE,last_login_fecha ,current_timestamp ())>60";

            $q = $this->db->query($query);


        }

        function actualizar_estado_baja_batch($id_comercio)
        {

            $data = array();

            $query = "update comercio set estado='B',fecha_baja=curdate() where  id_comercio=" . $id_comercio;


            $q = $this->db->query($query);
            return $q;
        }



    }





