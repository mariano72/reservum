<?php

class Maestro_servicios extends CI_Model {


  function get_servicio_by_id ($id_servicio){
   
   $data=array();
   
   $query="select id_servicio,nombre,descripcion,duracion_minutos,importe,iva,importe_total
            from maestro_servicios m where id_servicio=" . $id_servicio;
      
   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }

  function get_servicio_by_idcomercio ($id_comercio){

   $data=array();

   $query="select id_servicio,nombre,descripcion,duracion_minutos,importe,concat((100*iva),'%')  as iva,importe_total
            from maestro_servicios m where id_comercio=" . $id_comercio;

   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }


  function alta_servicio ($param){

   $data=array();

   $query="insert into maestro_servicios values (null," . $param['id_comercio'] . "," . $this->db->escape($param['nombre'])   . "," . $this->db->escape($param['descripcion'])    . "," .
           $param['duracion_minutos'] .  "," . $param['importe'] . "," . $param['iva'] . "," . $param['importe_total'] .")";
           
   $q=$this->db->query($query);

  }

  function modificar_servicio ($param){

   $data=array();


  $query="update maestro_servicios set nombre=" . $this->db->escape($param['nombre'])   . ",descripcion=" . $this->db->escape($param['descripcion'])    . ",
           duracion_minutos=" . $param['duracion_minutos'] . ",importe=" . $param['importe'] . ",iva=" . $param['iva'] . ",importe_total=" . $param['importe_total'] .
          " where id_servicio=" . $param['id_servicio'];


   $q=$this->db->query($query);



  }

}





