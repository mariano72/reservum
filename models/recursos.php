<?php

class Recursos extends CI_Model {



  function get_recurso_by_id ($param){
   
   $data=array();
   
   $query="select * from recursos a where id_recurso=" . $param;
          
   
   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }

  function get_recurso_by_idcomercio ($param){

   $data=array();
      
   $query="select id_recurso,nombre,descripcion,foto_recurso,case estado when 'A' then 'ALTA' when 'B' then 'BAJA' end as estado from recursos a where id_comercio=" . $param;


   $q=$this->db->query($query);

   if ($q->num_rows>0){
       foreach ($q->result() as $row){
         $data[]=$row;
       }

   }

   return $data;
  }


  function alta_recurso ($param){

   $data=array();

   $query="insert into recursos values (null," . $param['id_comercio'] . ",curdate()," . $this->db->escape($param['nombre'])   . "," . $this->db->escape($param['descripcion'])    . ",'A','" .
           $param['foto_recurso'] . "')";


   $q=$this->db->query($query);

  }

  function modificar_recurso ($param){

   $data=array();

   if ($param['foto_recurso']==null){
    $cadena_foto="foto_recurso";
   } else {
    $cadena_foto="'" . $param['foto_recurso'] . "'";
   }
   
   $query="update recursos set nombre=" . $this->db->escape($param['nombre'])  . ",descripcion=" . $this->db->escape($param['descripcion'])    . ",estado='" . $param['estado'] .  "',
           foto_recurso=" . $cadena_foto .  " where id_recurso=" . $param['id_recurso'];
  	   

   $q=$this->db->query($query);

   
   
  }


  function get_num_recurso_comercio ($param){

     $data=array();

     $query="select count(1) as numero from recursos a where estado='A' and id_comercio=" . $param;


     $q=$this->db->query($query);

     if ($q->num_rows>0){
         foreach ($q->result() as $row){
           $data[]=$row;
         }

     }

     return $data;
    }

}





