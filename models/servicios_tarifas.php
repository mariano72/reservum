<?php

    class Servicios_tarifas extends CI_Model
    {


        //Cogemos los datos de la tarifa que se puede aplicar para esa fecha/hora

        function get_tarifa_aplicable($param)
        {


            $data = array();


            $sql = " select id_sertar,nombre_tarifa,importe,iva,importe_total,descuento,tipo_descuento,
                     dias_afectados,weekday ('" . $param['fechareserva'] . "') as dia_semana
	            from servicios_tarifas
	            where (('" . $param['fechareserva'] . "' between fecha_inicio and fecha_fin) or (fecha_inicio is null or fecha_fin is null))
                  and estado='A'  and hora_inicio<='" . $param['hora_inicio'] . "' and hora_fin>='" . $param['hora_fin'] . "' and id_comercio=" . $param['id_comercio'] . "
                  and id_servicio=" . $param['id_servicio'] . " and find_in_set(weekday ('" . $param['fechareserva'] . "'),dias_afectados)<>0  limit 1";

            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_tarifa_by_id($id_sertar)
        {

            $data = array();
            $sql = "select id_sertar,hora_inicio,hora_fin,nombre_tarifa,id_servicio,importe,iva,importe_total,
                    descuento,estado,dias_afectados,
                    case fecha_inicio when '0000-00-00' then null else fecha_inicio end as fecha_inicio,
                    case fecha_fin when '0000-00-00' then null else fecha_fin end as fecha_fin
             from servicios_tarifas where id_sertar=" . $id_sertar;
            
            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        //Query de función callback
        function validar_insercion_tarifa($param)
        {


            $data =null;

            $sql = " select count(1) as numero_tarifas
	            from servicios_tarifas
	            where (('" . $param['fecha_inicio'] . "' between fecha_inicio and fecha_fin) and
	              ('" . $param['fecha_fin'] . "' between fecha_inicio and fecha_fin)
	              or (fecha_inicio is null or fecha_fin is null))
                  and estado='A'
                  and ((  hora_inicio<='" . $param['hora_inicio'] . "' and hora_fin>='" . $param['hora_fin'] . "')
                  or (  hora_inicio<='" . $param['hora_inicio'] . "' and hora_fin>'" . $param['hora_inicio'] . "')
                  or (  hora_inicio<'" . $param['hora_fin'] . "' and hora_fin>='" . $param['hora_fin'] . "') 
                  or ( hora_inicio>='" . $param['hora_inicio'] . "' and hora_fin<='" . $param['hora_fin'] . "'))
                  and id_comercio=" . $param['id_comercio'] . "
                  and id_servicio=" . $param['id_servicio'] . " and find_in_set('" . $param['dias_afectados'] . "',dias_afectados)<>0 and
                  id_sertar<>" . $param['id_sertar'];
         
     
            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->numero_tarifas;
                }

            }

            return $data;
        }

        function lista_tarifas($param)
        {


            $data = array();


            $sql = " select a.id_sertar,a.fecha_inicio,a.fecha_fin,a.hora_inicio,a.hora_fin,a.nombre_tarifa,a.importe,concat((100*a.iva),'%')  as iva,a.importe_total,
                    a.descuento,a.tipo_descuento,c.nombre as nombre_servicio,a.estado,a.dias_afectados
	                from servicios_tarifas a,maestro_servicios c
                    where  a.id_servicio=c.id_servicio and a.id_comercio=" . $param['id_comercio'];


            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function alta_tarifa($param)
        {


            $data = array();

            $sql = "insert into servicios_tarifas values (null,'"  . $param['fecha_inicio'] . "','" . $param['fecha_fin'] . "','" . $param['hora_inicio'] .
                   "','" . $param['hora_fin'] . "'," . $param['id_comercio'] . "," . $this->db->escape($param['nombre']) . "," . $param['id_servicio'] . ",'" .
                   $param['importe'] . "','" . $param['iva'] . "','" . $param['importe_total'] . "','0','A','POR_IMPORTE','" . $param['dias_afectados'] .  "')";


            $q = $this->db->query($sql);

            return $data;
        }

        function actualizar_tarifa($param)
        {


            $data = array();

            $sql = "update servicios_tarifas set fecha_inicio='". $param['fecha_inicio'] . "',fecha_fin='" . $param['fecha_fin'] . "',hora_inicio='" . $param['hora_inicio'] .
                   "',hora_fin='" . $param['hora_fin'] . "',nombre_tarifa=" . $this->db->escape($param['nombre']) . ",id_servicio=" . $param['id_servicio'] . ",importe='" .
                   $param['importe'] . "',iva='" . $param['iva'] . "',importe_total='" . $param['importe_total'] . "',estado='A',dias_afectados='" . $param['dias_afectados'] .  "'
                    where id_sertar=" . $param['id_sertar'];



            $q = $this->db->query($sql);

            return $data;
        }

    }





