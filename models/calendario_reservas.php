<?php

    class Calendario_reservas extends CI_Model
    {


        function get_last_reservas($id_usuario, $num_reservas)
        {

            $data = array();
            $query = "select id_reserva,a.fecha_reserva,a.hora_inicio,a.hora_fin,a.duracion,m.nombre as nombre_servicio,
                      d.nombre as nombre_recurso,b.nombre as nombre_comercio,e.nombre_calle,e.numero,e.codigo_postal,e.telefono,b.codigo_comercio
           from calendario_reservas a ,comercio b ,servicios c,recursos d,direcciones e,maestro_servicios m
           where id_usuario='" . $id_usuario . "' and a.estado='A' and a.id_recurso=d.id_recurso and a.id_recurso=c.id_recurso and a.id_servicio=c.id_servicio
           and c.id_recurso=d.id_recurso  and b.id_comercio=c.id_comercio and b.id_comercio=d.id_comercio and e.id_comercio=b.id_comercio and
           e.id_comercio=d.id_comercio and e.id_comercio=c.id_comercio and c.id_comercio=m.id_comercio and c.id_servicio=m.id_servicio 
           ORDER BY fecha_reserva_log DESC LIMIT " . $num_reservas;

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function get_last_reservas_para_anular($id_usuario, $num_reservas)
        {

            $fecha = date("Y-m-d H:i:s");
            $data = array();
            $query = "select id_reserva,a.fecha_reserva,a.hora_inicio,a.hora_fin,a.duracion,m.nombre as nombre_servicio,
            d.nombre as nombre_recurso,b.nombre as nombre_comercio,e.nombre_calle,e.numero,e.codigo_postal,e.telefono
           from calendario_reservas a ,comercio b ,servicios c,recursos d,direcciones e,calendario_base f,maestro_servicios m
           where id_usuario='" . $id_usuario . "' and a.estado='A' and a.id_recurso=d.id_recurso and a.id_recurso=c.id_recurso and a.id_servicio=c.id_servicio
           and c.id_recurso=d.id_recurso  and b.id_comercio=c.id_comercio and b.id_comercio=d.id_comercio and e.id_comercio=b.id_comercio and
           e.id_comercio=d.id_comercio and e.id_comercio=c.id_comercio and f.id_recurso=a.id_recurso and f.id_recurso=d.id_recurso and f.id_recurso=c.id_recurso and
           DATE_ADD(curdate(), INTERVAL f.min_dias_anulacion DAY)<= a.fecha_reserva and c.id_comercio=m.id_comercio and c.id_servicio=m.id_servicio
           ORDER BY fecha_reserva DESC LIMIT " . $num_reservas;

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function get_last_reservas_comercio_por_dia($param)
        {
            $fecha = date("Y-m-d H:i:s");
            $data = array();
            $query = "select id_reserva,a.fecha_reserva,a.hora_inicio,a.hora_fin,a.duracion,m.nombre as nombre_servicio,
            d.nombre as nombre_recurso,b.nombre as nombre_comercio,e.nombre_calle,e.numero,e.codigo_postal,e.telefono,
            g.nombre AS nombre_reserva,g.email AS email_reserva, g.telefono AS telefono_reserva, a.flag_usuario_reserva,
            a.nombre AS nombre_usuario,a.email,a.telefono
           from calendario_reservas a 
            INNER JOIN recursos        d ON a.id_recurso=d.id_recurso
		    INNER JOIN servicios       c ON a.id_recurso=c.id_recurso and a.id_servicio=c.id_servicio and c.id_recurso=d.id_recurso
		    INNER JOIN comercio        b ON b.id_comercio=c.id_comercio and b.id_comercio=d.id_comercio
		    INNER JOIN direcciones     e ON e.id_comercio=b.id_comercio and e.id_comercio=d.id_comercio and e.id_comercio=c.id_comercio
		    INNER JOIN calendario_base f ON f.id_recurso=a.id_recurso and f.id_recurso=d.id_recurso and f.id_recurso=c.id_recurso
		    INNER JOIN maestro_servicios m ON a.id_servicio = m.id_servicio
		    LEFT  JOIN usuarios        g ON a.id_usuario = g.id_usuario
		   where 
		    a.id_comercio='" . $param['id_comercio'] . "' and a.estado='A' and fecha_reserva='" . $param['fechareserva'] . "'
		    and  DATE_ADD(curdate(), INTERVAL f.min_dias_anulacion DAY)<= a.fecha_reserva
           ORDER BY d.nombre";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;

        }


        function get_last_reservas_comercio_por_usuario($param)
        {
            $data = array();

            $query = "select id_reserva,a.fecha_reserva,a.hora_inicio,a.hora_fin,a.duracion,m.nombre as nombre_servicio,
            d.nombre as nombre_recurso,b.nombre as nombre_comercio,e.nombre_calle,e.numero,e.codigo_postal,e.telefono,
            g.nombre AS nombre_reserva,g.email AS email_reserva, g.telefono AS telefono_reserva, a.flag_usuario_reserva,
            a.nombre AS nombre_usuario,a.email,a.telefono
           from calendario_reservas a
            INNER JOIN recursos        d ON a.id_recurso=d.id_recurso
		    INNER JOIN servicios       c ON a.id_recurso=c.id_recurso and a.id_servicio=c.id_servicio and c.id_recurso=d.id_recurso
		    INNER JOIN comercio        b ON b.id_comercio=c.id_comercio and b.id_comercio=d.id_comercio
		    INNER JOIN direcciones     e ON e.id_comercio=b.id_comercio and e.id_comercio=d.id_comercio and e.id_comercio=c.id_comercio
		    INNER JOIN calendario_base f ON f.id_recurso=a.id_recurso and f.id_recurso=d.id_recurso and f.id_recurso=c.id_recurso
		    INNER JOIN maestro_servicios m ON a.id_servicio = m.id_servicio
		    LEFT  JOIN usuarios        g ON a.id_usuario = g.id_usuario
		   where
		    a.id_comercio=" . $param['id_comercio'] . " and a.estado='A' and a.id_usuario='" . $param['id_usuario'] . "'
		    and  a.fecha_reserva>=curdate() or  (a.fecha_reserva =curdate() and a.hora_inicio>=curtime())
           ORDER BY d.nombre";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }
            }
            return $data;
        }

        function get_last_reservas_comercio_por_usuario_no_registrado($param)
        {
            $data = array();

            $query = "select id_reserva,a.fecha_reserva,a.hora_inicio,a.hora_fin,a.duracion,m.nombre as nombre_servicio,
            d.nombre as nombre_recurso,b.nombre as nombre_comercio,e.nombre_calle,e.numero,e.codigo_postal,e.telefono,
            g.nombre AS nombre_reserva,g.email AS email_reserva, g.telefono AS telefono_reserva, a.flag_usuario_reserva,
            a.nombre AS nombre_usuario,a.email,a.telefono
           from calendario_reservas a
            INNER JOIN recursos        d ON a.id_recurso=d.id_recurso
		    INNER JOIN servicios       c ON a.id_recurso=c.id_recurso and a.id_servicio=c.id_servicio and c.id_recurso=d.id_recurso
		    INNER JOIN comercio        b ON b.id_comercio=c.id_comercio and b.id_comercio=d.id_comercio
		    INNER JOIN direcciones     e ON e.id_comercio=b.id_comercio and e.id_comercio=d.id_comercio and e.id_comercio=c.id_comercio
		    INNER JOIN calendario_base f ON f.id_recurso=a.id_recurso and f.id_recurso=d.id_recurso and f.id_recurso=c.id_recurso
		    INNER JOIN maestro_servicios m ON a.id_servicio = m.id_servicio
		    LEFT  JOIN usuarios        g ON a.id_usuario = g.id_usuario
		   where
		    a.id_comercio=" . $param['id_comercio'] . " and a.estado='A' and a.email='" . $param['email'] . "'
		    and  a.fecha_reserva>=curdate() or  (a.fecha_reserva =curdate() and a.hora_inicio>=curtime())
           ORDER BY d.nombre";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }
            }
            return $data;
        }

        function get_last_reservas_comercio($id_comercio, $num_reservas)
        {

            $data = array();


            $query = "SELECT fecha_reserva, hora_inicio, hora_fin, duracion, c.nombre AS nombre_usuario,
                m.nombre AS nombre_servicio, c.email, c.telefono, a.nombre AS nombre_reserva,
                a.email AS email_reserva, a.telefono AS telefono_reserva, a.flag_usuario_reserva,a.materiales
		    FROM calendario_reservas a
		    INNER JOIN recursos b ON a.id_recurso = b.id_recurso
		    INNER JOIN servicios d ON a.id_recurso = d.id_recurso
		    AND b.id_recurso = d.id_recurso
		    AND a.id_servicio = d.id_servicio
            INNER JOIN maestro_servicios m ON a.id_servicio = m.id_servicio
	    	LEFT JOIN usuarios c ON a.id_usuario = c.id_usuario
	    	WHERE b.id_comercio =" . $id_comercio . " and a.estado='A'
	    	ORDER BY fecha_reserva_log DESC
	    	LIMIT " . $num_reservas;

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function get_next_reservas_comercio($id_comercio, $num_reservas)
        {

            $data = array();
            $query = "SELECT fecha_reserva, hora_inicio, hora_fin, duracion, c.nombre AS nombre_usuario,
                m.nombre AS nombre_servicio, c.email, c.telefono, a.nombre AS nombre_reserva,
                a.email AS email_reserva, a.telefono AS telefono_reserva, a.flag_usuario_reserva,a.materiales
		FROM calendario_reservas a
		INNER JOIN recursos b ON a.id_recurso = b.id_recurso
		INNER JOIN servicios d ON a.id_recurso = d.id_recurso
		AND b.id_recurso = d.id_recurso
		AND a.id_servicio = d.id_servicio
		INNER JOIN maestro_servicios m ON a.id_servicio = m.id_servicio
		LEFT JOIN usuarios c ON a.id_usuario = c.id_usuario
		WHERE b.id_comercio =" . $id_comercio . " and fecha_reserva>=curdate() and a.estado='A'
		ORDER BY fecha_reserva ASC 
		LIMIT " . $num_reservas;
            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        function get_fin_reserva($hora_inicio, $duracion)
        {


            $data = array();
            $query = " SELECT '2011-07-01 " . $hora_inicio . "' + INTERVAL " . $duracion . " MINUTE as hora_fin";
            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $datos = explode(" ", $row->hora_fin);
                    $hora = explode(":", $datos[1]);

                    $data[] = $hora[0] . ":" . $hora[1];

                }

            }
            return $data;
        }

        function realiza_reserva($param)
        {
            $fecha = date("Y-m-d H:i:s");

            $query = "insert into calendario_reservas values  (null,'" . $param['fechareserva'] . "','" . urldecode($param['hora_inicio']) .
                     "','" . $param['hora_fin'] . "'," . $param['duracion_minutos'] . "," . $param['id_usuario'] . ",'" .
                     $param['idcomercio'] . "'," . $param['id_recurso'] . "," . $param['idservicio'] .
                     ",'" . $fecha . "','" . $param['estado'] . "','" . $param['localizador'] . "'," . $this->db->escape($param['nombre']) . "," .
                     $this->db->escape($param['email']) . "," . $this->db->escape($param['telefono']) . ",'" .
                     $param['flag_usuario_reserva'] . "','" . $param['ip'] . "','" . $param['importe'] . "','" . $param['iva'] . "','" . $param['importe_total'] . "'," .
                     $param['id_sertar'] . ",null)";

            $q = $this->db->query($query);

            //Borramos la cache de las 3 tablas de consulta, parece que sólo funciona la opción de borrar todo.
            //$this->db->cache_delete('controller_ini', 'lista_votos_municipios');
            //$this->db->cache_delete_all()
        }

        function valida_tiempo_reserva($param)
        {

            $query = "SELECT * FROM calendario_reservas
             WHERE fecha_reserva='" . $param['fechareserva'] . "' and id_recurso=" . $param['id_recurso'] . " and estado='A' and
             (('" . $param['hora_inicio'] . "'>=hora_inicio and '" . $param['hora_fin'] . "'<=hora_fin) or
             ('" . $param['hora_fin'] . "'>hora_inicio and '" . $param['hora_fin'] . "'<hora_fin) or
             ('" . $param['hora_inicio'] . "'<=hora_inicio and '" . $param['hora_fin'] . "'>=hora_fin) or
             ('" . $param['hora_inicio'] . "'>'" . $param['hora_fin'] . "'))";


            $q = $this->db->query($query);
            return $q;


        }


        function actualiza_estado_reserva($param)
        {
            $fecha = date("Y-m-d H:i:s");

            $query = "update calendario_reservas set estado='" . $param['estado'] . "',nombre='" . $this->db->escape_str($param['nombre']) . "',email='" . $this->db->escape_str($param['email']) .
                     "',telefono='" . $this->db->escape_str($param['telefono']) . "',localizador='" . $param['localizador'] . "',fecha_reserva_log='" . $fecha . "',
                     id_usuario=" . $param['id_usuario'] . ",importe_total='" . $param['importe_total'] . "',importe='" . $param['importe'] . "',iva='" . $param['iva'] . "',
                     materiales=" . $this->db->escape($param['check_material']) . "
                     where id_usuario in (-1," . $param['id_usuario'] . ") and id_reserva=" . $param['id_reserva'] . " and estado='P'";

            $q = $this->db->query($query);


        }


        function anular_reservas_pendientes_usuario($param)
        {

            $query = "update calendario_reservas set estado='B'
               where id_usuario=" . $param['id_usuario'] . " and estado='P'";

            $q = $this->db->query($query);


        }

        function anular_reservas_pendientes_batch()
        {

            $query = "update calendario_reservas set estado='B' where estado='P' and TIMESTAMPDIFF(MINUTE,fecha_reserva_log ,current_timestamp ())>3";

            $q = $this->db->query($query);


        }

        function anular_reserva($param)
        {

            $fecha = date("Y-m-d H:i:s");
            $data = array();
            $query = "update calendario_reservas set ip='" . $param['ip'] . "',fecha_reserva_log='" . $fecha . "',estado='B'
               where id_usuario=" . $param['id_usuario'] . " and id_reserva=" . $param['id_reserva'];

            $q = $this->db->query($query);


        }

        function anular_reserva_cliente($param)
        {

            $fecha = date("Y-m-d H:i:s");
            $data = array();
            $query = "update calendario_reservas set ip='" . $param['ip'] . "',fecha_reserva_log='" . $fecha . "',estado='B'
               where id_comercio=" . $param['id_comercio'] . " and id_reserva=" . $param['id_reserva'];

            $q = $this->db->query($query);


        }

        function get_reservas_dia_siguiente_batch()
        {

            $data = array();

            $query = "SELECT id_reserva, a.fecha_reserva, a.hora_inicio, a.hora_fin, a.duracion, h.nombre AS nombre_servicio,
             d.nombre AS nombre_recurso, b.nombre AS nombre_comercio, e.nombre_calle, e.numero, e.codigo_postal,
             e.telefono, g.nombre AS nombre_reserva, g.email AS email_reserva, g.telefono AS telefono_reserva,
             a.flag_usuario_reserva, a.nombre AS nombre_usuario, a.email, a.telefono
	         FROM calendario_reservas a
    	      INNER JOIN recursos d ON a.id_recurso = d.id_recurso
    	      INNER JOIN maestro_servicios h ON h.id_servicio=a.id_servicio
    	      INNER JOIN servicios c ON a.id_recurso = c.id_recurso
    	      AND a.id_servicio = c.id_servicio
	          AND c.id_recurso = d.id_recurso
	          INNER JOIN comercio b ON b.id_comercio = c.id_comercio
	          AND b.id_comercio = d.id_comercio
	          INNER JOIN direcciones e ON e.id_comercio = b.id_comercio
	          AND e.id_comercio = d.id_comercio
	          AND e.id_comercio = c.id_comercio
	          INNER JOIN calendario_base f ON f.id_recurso = a.id_recurso
	          AND f.id_recurso = d.id_recurso
	          AND f.id_recurso = c.id_recurso
	          LEFT JOIN usuarios g ON a.id_usuario = g.id_usuario
	          WHERE a.fecha_reserva = CURDATE() +1 AND a.estado='A'";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }
            }

            return $data;
        }


        //Cogemos las reservas para el día y el comercio ,servicio solicitado,
        function get_reservas_dia($param, $param2, $param4)
        {


            $data = array();
            $sql = " select b.id_recurso,a.hora_inicio,a.hora_fin,a.duracion,a.estado
                    from calendario_reservas a, recursos b ,servicios c
                    where a.id_recurso=b.id_recurso and b.id_comercio='" . $param . "' and a.fecha_reserva='" . $param2 . "'
                    and a.estado in ('A','P') and c.id_recurso=a.id_recurso and c.id_recurso=b.id_recurso and c.id_servicio=" . $param4 . "
                    order by b.id_recurso,a.hora_inicio asc";
            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        //Cogemos las reservas para el día y el comercio
        function agenda_dia_comercio($param)
        {

            $data = array();
            $sql = "SELECT id_reserva, a.fecha_reserva, a.hora_inicio, a.hora_fin, a.duracion,
       m.nombre AS nombre_servicio, d.nombre AS nombre_recurso,
       b.nombre AS nombre_comercio, e.nombre_calle, e.numero, e.codigo_postal,
       e.telefono, g.nombre AS nombre_reserva, g.email AS email_reserva, g.telefono AS telefono_reserva,
       a.flag_usuario_reserva, a.nombre AS nombre_usuario, a.email, a.telefono
       FROM calendario_reservas a
       INNER JOIN recursos d ON a.id_recurso = d.id_recurso
       INNER JOIN servicios c ON a.id_recurso = c.id_recurso
       AND a.id_servicio = c.id_servicio
       AND c.id_recurso = d.id_recurso
       INNER JOIN comercio b ON b.id_comercio = c.id_comercio
       AND b.id_comercio = d.id_comercio
       INNER JOIN direcciones e ON e.id_comercio = b.id_comercio
       AND e.id_comercio = d.id_comercio
       AND e.id_comercio = c.id_comercio
       INNER JOIN calendario_base f ON f.id_recurso = a.id_recurso
       AND f.id_recurso = d.id_recurso
       AND f.id_recurso = c.id_recurso
       INNER JOIN maestro_servicios m ON a.id_servicio = m.id_servicio
       LEFT JOIN usuarios g ON a.id_usuario = g.id_usuario
       WHERE a.id_comercio =" . $param['id_comercio'] . " 
       AND a.estado =  'A'
       AND fecha_reserva =  '" . $param['fechareserva'] . "'
       ORDER BY d.nombre, a.hora_inicio";


            $q = $this->db->query($sql);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;

        }


        function get_num_reservas_activas($id_comercio)
        {

            $data = array();

            $query = "SELECT count(1) as numero FROM calendario_reservas a
			WHERE id_comercio =  '" . $id_comercio . "' and estado='A' AND
			(a.fecha_reserva>curdate() or  (a.fecha_reserva =curdate() and a.hora_inicio>curtime()))";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_num_reservas_activas_mes($id_comercio)
        {

            $data = array();

            $query = "SELECT COUNT(1) AS numero
            FROM calendario_reservas a
            WHERE id_comercio =  '" . $id_comercio . "'
            AND estado =  'A'
            AND (a.fecha_reserva >CURDATE()
            AND MONTH(fecha_reserva) = MONTH(CURDATE())
            OR (a.fecha_reserva = CURDATE() AND a.hora_inicio >= CURTIME()))";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_num_reservas_activas_now($id_comercio)
        {

            $data = array();

            $query = "SELECT count(1) as numero FROM calendario_reservas a
			WHERE id_comercio =  '" . $id_comercio . "' and estado='A' AND
			a.fecha_reserva =curdate() and a.hora_inicio<curtime() and a.hora_fin>curtime()";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }

        function get_last_reserva_activa($id_comercio)
        {

            $data = array();

            $query = "SELECT  a.fecha_reserva_log,a.nombre as nombre1,a.email as email1,
			b.nombre as nombre2,b.email as email2,a.flag_usuario_reserva
			FROM calendario_reservas a 
	        LEFT JOIN usuarios b ON a.id_usuario = b.id_usuario
			WHERE id_comercio =" . $id_comercio . " and a.estado='A' 
			ORDER BY fecha_reserva_log DESC 
			LIMIT 1";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }

            }

            return $data;
        }


        // PARA EL MÓDULO DE ESTADISTICA
        function estadistica_reservas_totales_por_comercio($id_comercio)
        {

            $data = null;

            $query = "SELECT  count(1) as numero from calendario_reservas WHERE id_comercio =" . $id_comercio . " and estado='A'";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->numero;
                }

            }

            return $data;
        }

        function estadistica_reservas_totales_por_comercio_mes($id_comercio)
        {

            $data = null;

            $query = "SELECT  count(1) as numero from calendario_reservas WHERE id_comercio =" . $id_comercio . " and estado='A' and
                     month(curdate())=month(fecha_reserva) and year(curdate())=year(fecha_reserva)";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->numero;
                }

            }

            return $data;
        }

        function estadistica_reservas_totales_por_comercio_semana($id_comercio)
        {

            $data = null;

            $query = "SELECT  count(1) as numero from calendario_reservas WHERE id_comercio =" . $id_comercio . " and estado='A' and
                     month(curdate())=month(fecha_reserva) and year(curdate())=year(fecha_reserva) and week(curdate())=week(fecha_reserva)";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->numero;
                }

            }

            return $data;
        }

        function estadistica_reservas_totales_por_comercio_anyo($id_comercio)
        {

            $data = null;

            $query = "SELECT  count(1) as numero from calendario_reservas WHERE id_comercio =" . $id_comercio . " and estado='A' and
                      year(curdate())=year(fecha_reserva)";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->numero;
                }

            }

            return $data;
        }


        function count_reservas()
        {

            $data = null;
            $query = "select count(1) as total from calendario_reservas where estado='A'";

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data = $row->total;
                }

            }

            return $data;
        }


        function get_datos_reserva_by_id($param)
        {

            $data = array();

            $query = "SELECT id_reserva, a.fecha_reserva, a.hora_inicio, a.hora_fin, a.duracion,a.importe_total, h.nombre AS nombre_servicio,
             d.nombre AS nombre_recurso, g.nombre AS nombre, g.email AS email, g.telefono AS telefono,
             a.flag_usuario_reserva, a.nombre AS nombre_reserva, a.email as email_reserva, a.telefono as telefono_reserva, i.nombre_tarifa
	         FROM calendario_reservas a
    	      INNER JOIN recursos d ON a.id_recurso = d.id_recurso
    	      INNER JOIN maestro_servicios h ON h.id_servicio=a.id_servicio
    	      INNER JOIN servicios c ON a.id_recurso = c.id_recurso
    	      AND a.id_servicio = c.id_servicio
	          AND c.id_recurso = d.id_recurso
	          INNER JOIN calendario_base f ON f.id_recurso = a.id_recurso
	          AND f.id_recurso = d.id_recurso
	          AND f.id_recurso = c.id_recurso
	          LEFT JOIN usuarios g ON a.id_usuario = g.id_usuario
	          LEFT JOIN servicios_tarifas i ON a.id_sertar= i.id_sertar
	          WHERE id_reserva=" . $param['id_reserva'] . " and a.id_comercio=" . $param['id_comercio'];

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }
            }

            return $data;
        }


        function get_datos_reserva_by_localizador($param)
        {

            $data = array();

            $query = "SELECT id_reserva, a.fecha_reserva, a.hora_inicio, a.hora_fin, a.duracion,a.importe_total, h.nombre AS nombre_servicio,
             d.nombre AS nombre_recurso, g.nombre AS nombre, g.email AS email, g.telefono AS telefono,
             a.flag_usuario_reserva, a.nombre AS nombre_reserva, a.email as email_reserva, a.telefono as telefono_reserva, i.nombre_tarifa
	         FROM calendario_reservas a
    	      INNER JOIN recursos d ON a.id_recurso = d.id_recurso
    	      INNER JOIN maestro_servicios h ON h.id_servicio=a.id_servicio
    	      INNER JOIN servicios c ON a.id_recurso = c.id_recurso
    	      AND a.id_servicio = c.id_servicio
	          AND c.id_recurso = d.id_recurso
	          INNER JOIN calendario_base f ON f.id_recurso = a.id_recurso
	          AND f.id_recurso = d.id_recurso
	          AND f.id_recurso = c.id_recurso
	          LEFT JOIN usuarios g ON a.id_usuario = g.id_usuario
	          LEFT JOIN servicios_tarifas i ON a.id_sertar= i.id_sertar
	          WHERE localizador=" . $param['localizador'];

            $q = $this->db->query($query);

            if ($q->num_rows > 0) {
                foreach ($q->result() as $row) {
                    $data[] = $row;
                }
            }

            return $data;
        }

    }





